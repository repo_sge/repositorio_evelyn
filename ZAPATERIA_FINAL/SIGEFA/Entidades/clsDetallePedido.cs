﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetallePedido
    {
        #region propiedades


        private Int32 iCodDetallePedido;
        private Int32 iCodProducto;
        private String sReferencia;
        private String sDescripcion;
        private Int32 iCodPedido;
        private Int32 iCodAlmacen;
        private Int32 iUnidadIngresada;
        private String sSerieLote;
        private Decimal dCantidad;
        private Decimal dCantidadPendiente;
        private Int32 iCodUnidad;
        private String sUnidad;
        private Decimal dPrecioUnitario;
        private Decimal dSubtotal;
        private Decimal dDescuento1;
        private Decimal dDescuento2;
        private Decimal dDescuento3;
        private Decimal dMontoDescuento;
        private Decimal dIgv;
        private Decimal dImporte;
        private Decimal dPrecioVenta;
        private Decimal dValorVenta;
        private Decimal dPrecioReal;
        private Decimal dValoReal;
        private DateTime dFechaRegistro;
        private Int32 iCodUser;

        private Decimal dCantidadVendida;
        private Decimal dCantidadDevuelta;
        private Decimal iImpDevuelto;
        private Decimal iImpVend;
        private Int32 codtipodoc;
        private Boolean precioigv;
        private Decimal valorpromedio;
        private Decimal precioMargen;
        private Int32 codProv;

        public Int32 CodProv
        {
            get { return codProv; }
            set { codProv = value; }
        }
        public Decimal Valorpromedio
        {
            get { return valorpromedio; }
            set { valorpromedio = value; }
        }
        public decimal PrecioMargen
        {
            get { return precioMargen; }
            set { precioMargen = value; }
        }
        public Boolean Precioigv
        {
            get { return precioigv; }
            set { precioigv = value; }
        }

        public Int32 Codtipodoc
        {
            get { return codtipodoc; }
            set { codtipodoc = value; }
        }

        public Decimal DCantidadVendida
        {
            get { return dCantidadVendida; }
            set { dCantidadVendida = value; }
        }

        public Decimal DCantidadDevuelta
        {
            get { return dCantidadDevuelta; }
            set { dCantidadDevuelta = value; }
        }

        public Decimal IImpDevuelto
        {
            get { return iImpDevuelto; }
            set { iImpDevuelto = value; }
        }

        public Decimal IImpVend
        {
            get { return iImpVend; }
            set { iImpVend = value; }
        }

        public Int32 CodDetallePedido
        {
            get { return iCodDetallePedido; }
            set { iCodDetallePedido = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        public String Referencia
        {
            get { return sReferencia; }
            set { sReferencia = value; }
        }
        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }
        public Int32 CodPedido
        {
            get { return iCodPedido; }
            set { iCodPedido = value; }
        }
        public Int32 CodAlmacen
        {
            get { return iCodAlmacen; }
            set { iCodAlmacen = value; }
        }
        public Int32 UnidadIngresada
        {
            get { return iUnidadIngresada; }
            set { iUnidadIngresada = value; }
        }
        public String Unidad
        {
            get { return sUnidad; }
            set { sUnidad = value; }
        }
        public String SerieLote
        {
            get { return sSerieLote; }
            set { sSerieLote = value; }
        }
        public Decimal Cantidad
        {
            get { return dCantidad; }
            set { dCantidad = value; }
        }
        public Decimal CantidadPendiente
        {
            get { return dCantidadPendiente; }
            set { dCantidadPendiente = value; }
        }
        public Int32 CodUnidad
        {
            get { return iCodUnidad; }
            set { iCodUnidad = value; }
        }
        public Decimal PrecioUnitario
        {
            get { return dPrecioUnitario; }
            set { dPrecioUnitario = value; }
        }
        public Decimal Subtotal
        {
            get { return dSubtotal; }
            set { dSubtotal = value; }
        }
        public Decimal Descuento1
        {
            get { return dDescuento1; }
            set { dDescuento1 = value; }
        }
        public Decimal Descuento2
        {
            get { return dDescuento2; }
            set { dDescuento2 = value; }
        }
        public Decimal Descuento3
        {
            get { return dDescuento3; }
            set { dDescuento3 = value; }
        }
        public Decimal MontoDescuento
        {
            get { return dMontoDescuento; }
            set { dMontoDescuento = value; }
        }
        public Decimal Igv
        {
            get { return dIgv; }
            set { dIgv = value; }
        }
        public Decimal Importe
        {
            get { return dImporte; }
            set { dImporte = value; }
        }
        public Decimal PrecioVenta
        {
            get { return dPrecioVenta; }
            set { dPrecioVenta = value; }
        }
        public Decimal ValorVenta
        {
            get { return dValorVenta; }
            set { dValorVenta = value; }
        }
        public Decimal PrecioReal
        {
            get { return dPrecioReal; }
            set { dPrecioReal = value; }
        }
        public Decimal ValoReal
        {
            get { return dValoReal; }
            set { dValoReal = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dFechaRegistro; }
            set { dFechaRegistro = value; }
        }
        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }

        #endregion propiedades
    }
}
