﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsModelo
    {
        #region propiedades

        private Int32 iCodModelo;
        private Int32 iCodModeloNuevo;
        private String sDescripcion;
        private DateTime dtFechaRegistro;
        private Int32 iCodProveedor;

        public Int32 CodMarcaNuevo
        {
            get { return iCodModeloNuevo; }
            set { iCodModeloNuevo = value; }
        }

        public Int32 CodProveedor
        {
            get { return iCodProveedor; }
            set { iCodProveedor = value; }
        }

        public DateTime FechaRegistro
        {
            get { return dtFechaRegistro; }
            set { dtFechaRegistro = value; }
        }

        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }

        public Int32 CodModelo
        {
            get { return iCodModelo; }
            set { iCodModelo = value; }
        }

        #endregion
    }
}
