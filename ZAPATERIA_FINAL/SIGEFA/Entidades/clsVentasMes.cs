﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsVentasMes
    {
        Int32 icodventas;        
        Int32 idia;
        Int32 imes;
        String ianio;
        String inombredia;
        String inombremes;
        DateTime ifecharegistro;

        public DateTime Ifecharegistro
        {
            get { return ifecharegistro; }
            set { ifecharegistro = value; }
        }

        Decimal iventascontado;

        public Decimal Iventascontado
        {
            get { return iventascontado; }
            set { iventascontado = value; }
        }

        Decimal iventascredito;

        public Decimal Iventascredito
        {
            get { return iventascredito; }
            set { iventascredito = value; }
        }

        Decimal itotalventas;

        public Decimal Itotalventas
        {
            get { return itotalventas; }
            set { itotalventas = value; }
        }

        public Int32 Codventas
        {
            get { return icodventas; }
            set { icodventas = value; }
        }

        public Int32 Dia
        {
            get { return idia; }
            set { idia = value; }
        }

        public Int32 Mes
        {
            get { return imes; }
            set { imes = value; }
        }

        public String Anio
        {
            get { return ianio; }
            set { ianio = value; }
        }

        public String Nombredia
        {
            get { return inombredia; }
            set { inombredia = value; }
        }

        public String NombreMes
        {
            get { return inombremes; }
            set { inombremes = value; }
        }
    }
}
