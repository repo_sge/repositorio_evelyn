﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    public class clsCorte
    {
        int codCorte;
        string referencia;
        string descripcion;
        int estado;
        int codUser;

        public int CodCorte { get => codCorte; set => codCorte = value; }
        public string Referencia { get => referencia; set => referencia = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public int Estado { get => estado; set => estado = value; }
        public int CodUser { get => codUser; set => codUser = value; }
    }
}
