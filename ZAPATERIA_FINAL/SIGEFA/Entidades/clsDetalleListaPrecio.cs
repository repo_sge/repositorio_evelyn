﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetalleListaPrecio
    {
        #region propiedades

        private Int32 iCodDetalleLista;
        private Int32 iCodListaPrecio;
        private Int32 iCodProducto;
        private Decimal dValor;
        private Decimal dMargen;
        private Decimal dDescuento1;
        private Decimal dDescuento2;
        private Decimal dDescuento3;
        private Decimal dPrecioNeto;
        private Decimal dPrecio;        
        private Boolean bEstado;
        private Int32 iCodUser;
        private DateTime dtFechaRegistro;


        public Int32 CodDetalleLista
        {
            get { return iCodDetalleLista; }
            set { iCodDetalleLista = value; }
        }
        public Int32 CodListaPrecio
        {
            get { return iCodListaPrecio; }
            set { iCodListaPrecio = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        public Decimal Valor
        {
            get { return dValor; }
            set { dValor = value; }
        }
        public Decimal Margen
        {
            get { return dMargen; }
            set { dMargen = value; }
        }
        public Decimal Descuento1
        {
            get { return dDescuento1; }
            set { dDescuento1 = value; }
        }
        public Decimal Descuento2
        {
            get { return dDescuento2; }
            set { dDescuento2 = value; }
        }
        public Decimal Descuento3
        {
            get { return dDescuento3; }
            set { dDescuento3 = value; }
        }
        public Decimal PrecioNeto
        {
            get { return dPrecioNeto; }
            set { dPrecioNeto = value; }
        }
        public Decimal Precio
        {
            get { return dPrecio; }
            set { dPrecio = value; }
        }
        public Boolean Estado
        {
            get { return bEstado; }
            set { bEstado = value; }
        }
        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dtFechaRegistro; }
            set { dtFechaRegistro = value; }
        }        


        #endregion propiedades
    }
}
