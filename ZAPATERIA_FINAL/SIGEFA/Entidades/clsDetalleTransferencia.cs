﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsDetalleTransferencia
    {

        #region propiedades


        private Int32 iCodDetalleTransfer;
        private Int32 iCodProducto;
        private Int32 codProv;
        private String sReferencia;
        private String sDescripcion;
        private Int32 iCodTransDir;
        private Int32 iCodAlmacenOrigen;
        private Int32 iCodAlmacenDestino;
        private Int32 iUnidadIngresada;
        private String sSerieLote;
        private Decimal dCantidad;
        private Decimal dCantidadPendiente;
        private Int32 iCodUnidad;
        private String sUnidad;
        private Decimal dPrecioUnitario;
        private Decimal dSubtotal;
        private Decimal dDescuento1;
        private Decimal dDescuento2;
        private Decimal dDescuento3;
        private Decimal dMontoDescuento;
        private Decimal dIgv;
        private Decimal dImporte;
        private Decimal dPrecioVenta;
        private Decimal dValorVenta;
        private Decimal dPrecioReal;
        private Decimal dValoReal;
        private DateTime dFechaRegistro;
        private Boolean precioigv;
        private Decimal valorpromedio;

        public Decimal Valorpromedio
        {
            get { return valorpromedio; }
            set { valorpromedio = value; }
        }

        public Boolean Precioigv
        {
            get { return precioigv; }
            set { precioigv = value; }
        }
        private Int32 iCodUser;


        public Int32 CodDetalleTransfer
        {
            get { return iCodDetalleTransfer; }
            set { iCodDetalleTransfer = value; }
        }
        public Int32 CodProv
        {
            get { return codProv; }
            set { codProv = value; }
        }
        public Int32 CodProducto
        {
            get { return iCodProducto; }
            set { iCodProducto = value; }
        }
        public String Referencia
        {
            get { return sReferencia; }
            set { sReferencia = value; }
        }
        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }
        public Int32 CodTransDir
        {
            get { return iCodTransDir; }
            set { iCodTransDir = value; }
        }
        public Int32 CodAlmacenOrigen
        {
            get { return iCodAlmacenOrigen; }
            set { iCodAlmacenOrigen = value; }
        }

        public Int32 CodAlmacenDestino
        {
            get { return iCodAlmacenDestino; }
            set { iCodAlmacenDestino = value; }
        }
        public Int32 UnidadIngresada
        {
            get { return iUnidadIngresada; }
            set { iUnidadIngresada = value; }
        }
        public String Unidad
        {
            get { return sUnidad; }
            set { sUnidad = value; }
        }
        public String SerieLote
        {
            get { return sSerieLote; }
            set { sSerieLote = value; }
        }
        public Decimal Cantidad
        {
            get { return dCantidad; }
            set { dCantidad = value; }
        }
        public Decimal CantidadPendiente
        {
            get { return dCantidadPendiente; }
            set { dCantidadPendiente = value; }
        }
        public Int32 CodUnidad
        {
            get { return iCodUnidad; }
            set { iCodUnidad = value; }
        }
        public Decimal PrecioUnitario
        {
            get { return dPrecioUnitario; }
            set { dPrecioUnitario = value; }
        }
        public Decimal Subtotal
        {
            get { return dSubtotal; }
            set { dSubtotal = value; }
        }
        public Decimal Descuento1
        {
            get { return dDescuento1; }
            set { dDescuento1 = value; }
        }
        public Decimal Descuento2
        {
            get { return dDescuento2; }
            set { dDescuento2 = value; }
        }
        public Decimal Descuento3
        {
            get { return dDescuento3; }
            set { dDescuento3 = value; }
        }
        public Decimal MontoDescuento
        {
            get { return dMontoDescuento; }
            set { dMontoDescuento = value; }
        }
        public Decimal Igv
        {
            get { return dIgv; }
            set { dIgv = value; }
        }
        public Decimal Importe
        {
            get { return dImporte; }
            set { dImporte = value; }
        }
        public Decimal PrecioVenta
        {
            get { return dPrecioVenta; }
            set { dPrecioVenta = value; }
        }
        public Decimal ValorVenta
        {
            get { return dValorVenta; }
            set { dValorVenta = value; }
        }
        public Decimal PrecioReal
        {
            get { return dPrecioReal; }
            set { dPrecioReal = value; }
        }
        public Decimal ValoReal
        {
            get { return dValoReal; }
            set { dValoReal = value; }
        }
        public DateTime FechaRegistro
        {
            get { return dFechaRegistro; }
            set { dFechaRegistro = value; }
        }
        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }

        #endregion propiedades
    }
}
