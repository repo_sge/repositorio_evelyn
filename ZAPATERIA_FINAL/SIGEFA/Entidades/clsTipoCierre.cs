﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsTipoCierre
    {
        private Int32 icodTipoCierre;
        private String iDescripcion;
        private DateTime dtFechaRegistro;
        private Boolean iestado;
        private Int32 icodUsuario;

        public Int32 CodUsuario
        {
            get { return icodUsuario; }
            set { icodUsuario = value; }
        }

        public Int32 CodTipoCierre
        {
            get { return icodTipoCierre; }
            set { icodTipoCierre = value; }
        }

        public String Descripcion
        {
            get { return iDescripcion; }
            set { iDescripcion = value; }
        }

        public DateTime FechaRegistro
        {
            get { return dtFechaRegistro; }
            set { dtFechaRegistro = value; }
        }

        public Boolean estado
        {
            get { return iestado; }
            set { iestado = value; }
        }
    }
}
