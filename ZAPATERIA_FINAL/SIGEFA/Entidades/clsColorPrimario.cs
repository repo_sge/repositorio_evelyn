﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    public class clsColorPrimario
    {
        #region propiedades

        
        private Int32 iCodColorPrimario;
        private String sDescripcion;
        private Int32 iEstado;


        public Int32 Estado
        {
            get { return iEstado; }
            set { iEstado = value; }
        }

        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }

        public Int32 CodColorPrimario
        {
            get { return iCodColorPrimario; }
            set { iCodColorPrimario = value; }
        }
        
        #endregion propiedades
    }
}
