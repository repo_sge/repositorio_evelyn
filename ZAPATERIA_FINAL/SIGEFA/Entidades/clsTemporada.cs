﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsTemporada
    {
        Int32 iCodTemporada;
        String iDescripcion;
        Int32 iCodUser;
        DateTime iFechaRegistro;
        Int32 iCodAlmacen;
        Int32 iEstado;

        public Int32 CodTemporada
        {
            get { return iCodTemporada; }
            set { iCodTemporada = value; }
        }

        public String Descripcion
        {
            get { return iDescripcion; }
            set { iDescripcion = value; }
        }

        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }

        public DateTime FechaRegistro
        {
            get { return iFechaRegistro; }
            set { iFechaRegistro = value; }
        }

        public Int32 CodAlmacen
        {
            get { return iCodAlmacen; }
            set { iCodAlmacen = value; }
        }

        public Int32 Estado
        {
            get { return iEstado; }
            set { iEstado = value; }
        }
    }
}
