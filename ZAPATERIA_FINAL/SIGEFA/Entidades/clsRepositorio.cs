﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsRepositorio
    {
        private int idRepositorio;
        private int idTipodocumento;
        private string serie;
        private string correlativo;
        private DateTime fechaemision;
        private decimal monto;
        private int estadosunat;
        private string mensajesunat;
        private string nombredocxml;
        private string rutaxml;
        private string nombredocpdf;
        private string rutapdf;
        private string pcorigen;
        private string usuariopc;
        private clsArchivo archivo;
        private string nombredoc;
        private int codEmpresa;
        private int codSucursal;
        private int codAlmacen;
        private int codFacturaVenta;
        private int iCodUser;

        public string Serie
        {
            get
            {
                return serie;
            }

            set
            {
                serie = value;
            }
        }

        public string Correlativo
        {
            get
            {
                return correlativo;
            }

            set
            {
                correlativo = value;
            }
        }

        public DateTime Fechaemision
        {
            get
            {
                return fechaemision;
            }

            set
            {
                fechaemision = value;
            }
        }

        public decimal Monto
        {
            get
            {
                return monto;
            }

            set
            {
                monto = value;
            }
        }

        public int Estadosunat
        {
            get
            {
                return estadosunat;
            }

            set
            {
                estadosunat = value;
            }
        }

        public string Mensajesunat
        {
            get
            {
                return mensajesunat;
            }

            set
            {
                mensajesunat = value;
            }
        }

        public string Nombredocxml
        {
            get
            {
                return nombredocxml;
            }

            set
            {
                nombredocxml = value;
            }
        }

        public string Rutaxml
        {
            get
            {
                return rutaxml;
            }

            set
            {
                rutaxml = value;
            }
        }

        public string Nombredocpdf
        {
            get
            {
                return nombredocpdf;
            }

            set
            {
                nombredocpdf = value;
            }
        }

        public string Rutapdf
        {
            get
            {
                return rutapdf;
            }

            set
            {
                rutapdf = value;
            }
        }

        public string Pcorigen
        {
            get
            {
                return pcorigen;
            }

            set
            {
                pcorigen = value;
            }
        }

        public string Usuariopc
        {
            get
            {
                return usuariopc;
            }

            set
            {
                usuariopc = value;
            }
        }

        internal clsArchivo Archivo
        {
            get
            {
                return archivo;
            }

            set
            {
                archivo = value;
            }
        }


        public int IdRepositorio { get => idRepositorio; set => idRepositorio = value; }
        public string Nombredoc { get => nombredoc; set => nombredoc = value; }
        public int CodEmpresa { get => codEmpresa; set => codEmpresa = value; }
        public int CodSucursal { get => codSucursal; set => codSucursal = value; }
        public int CodAlmacen { get => codAlmacen; set => codAlmacen = value; }
        public int CodFacturaVenta { get => codFacturaVenta; set => codFacturaVenta = value; }
        public int ICodUser { get => iCodUser; set => iCodUser = value; }
        public int IdTipodocumento { get => idTipodocumento; set => idTipodocumento = value; }
    }
}

