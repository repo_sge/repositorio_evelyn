﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
    class clsDiseño
    {
        Int32 iCodDiseño;
        String iDescripcion;
        Int32 iCodUser;
        DateTime iFechaRegistro;
        Int32 iEstado;
        Int32 iCodCuelloNuevo;

        public Int32 CodDiseño
        {
            get { return iCodDiseño; }
            set { iCodDiseño = value; }
        }

        public String Descripcion
        {
            get { return iDescripcion; }
            set { iDescripcion = value; }
        }

        public Int32 CodUser
        {
            get { return iCodUser; }
            set { iCodUser = value; }
        }

        public DateTime FechaRegistro
        {
            get { return iFechaRegistro; }
            set { iFechaRegistro = value; }
        }

        public Int32 CodCuelloNuevo
        {
            get { return iCodCuelloNuevo; }
            set { iCodCuelloNuevo = value; }
        }

        public Int32 Estado
        {
            get { return iEstado; }
            set { iEstado = value; }
        }
    }
}
