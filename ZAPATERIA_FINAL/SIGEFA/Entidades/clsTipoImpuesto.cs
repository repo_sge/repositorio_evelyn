﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    class clsTipoImpuesto
    {
        private int idtipoimpuesto;
        private string codsunat;
        private string descripcion;
        private int estado;

        public int Idtipoimpuesto { get => idtipoimpuesto; set => idtipoimpuesto = value; }
        public string Codsunat { get => codsunat; set => codsunat = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public int Estado { get => estado; set => estado = value; }
    }
}
