﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;

namespace SIGEFA.Formularios
{
    public partial class frmSeleccionCuenta : Telerik.WinControls.UI.RadForm
    {
        clsAdmCtaCte admcta = new clsAdmCtaCte();

        public int Codcta { get; set; }
        public string Operacion { get; set; }

        public frmSeleccionCuenta()
        {
            InitializeComponent();
        }

        private void frmSeleccionCuenta_Load(object sender, EventArgs e)
        {
            Cargacuentas();
        }

        private void Cargacuentas()
        {
            cmbCta.ValueMember = "codcuentacorriente";
            cmbCta.DisplayMember = "banco";
            cmbCta.DataSource= admcta.ListaCtaCteGeneral();
            cmbCta.SelectedIndex = -1;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            Codcta = Convert.ToInt32(cmbCta.SelectedValue);
            Operacion = txtoperacion.Text.Trim();
            if (Codcta != -1)
            {
                if (!Operacion.Equals(""))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Ingrese numero de operacion.");
                }  
            }
            else
            {
                MessageBox.Show("Seleccione una cuenta...");
            }
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            this.Dispose();
        }
    }
}
