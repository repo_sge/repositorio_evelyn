﻿namespace SIGEFA.Formularios
{
    partial class frmVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVenta));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pbFoto = new System.Windows.Forms.PictureBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtDetalle = new System.Windows.Forms.TextBox();
            this.txtDsctoGobal = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dgvDetalle = new System.Windows.Forms.DataGridView();
            this.coddetalle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codSalida1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codunidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serielote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipodscto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscto3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montodscto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valoreal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockPend = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsctoMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.coduser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecharegistro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.txtPrecioVenta = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtIGV = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDscto = new System.Windows.Forms.TextBox();
            this.txtValorVenta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtvendedor = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txttasa = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txttienda = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtMoneda = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtplazo = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.lbLineaCredito = new System.Windows.Forms.Label();
            this.txtLineaCredito = new System.Windows.Forms.TextBox();
            this.txtLineaCreditoUso = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtLineaCreditoDisponible = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pbCapchatS = new System.Windows.Forms.PictureBox();
            this.txtSunat_Capchat = new System.Windows.Forms.TextBox();
            this.btnNuevaVenta = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.txtCodDocumento = new System.Windows.Forms.TextBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbTimpuesto = new System.Windows.Forms.ComboBox();
            this.labelnotacredito = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtcodpedido = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ckbguia = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtSerieG = new System.Windows.Forms.TextBox();
            this.txtcodserie = new System.Windows.Forms.TextBox();
            this.txtNumeroG = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtRazonSocialTransporte = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cmbTransportista = new System.Windows.Forms.ComboBox();
            this.cmbVehiculo = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbAlmacen = new System.Windows.Forms.ComboBox();
            this.lblAlmacen = new System.Windows.Forms.Label();
            this.txtDireccionCliente = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCodigoCli = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCotizacion = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTipoCambio = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cbostand = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbovendedor = new System.Windows.Forms.ComboBox();
            this.txtGuias = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtOrdenTrab = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPDescuento = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbListaPrecios = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dtpFechaPago = new System.Windows.Forms.DateTimePicker();
            this.cmbFormaPago = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbAutorizado = new System.Windows.Forms.Label();
            this.txtAutorizacion = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.cmbMoneda = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNombreCliente = new System.Windows.Forms.TextBox();
            this.txtCodCliente = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtComentario = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNumDoc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDocRef = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lbNombreTransaccion = new System.Windows.Forms.Label();
            this.txtTransaccion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.customValidator5 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator1 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator2 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator3 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator4 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator11 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator7 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator10 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator6 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.cachedCRCuotasPrestamo1 = new SIGEFA.Reportes.clsReportes.CachedCRCuotasPrestamo();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.txtDetalle);
            this.groupBox2.Controls.Add(this.txtDsctoGobal);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.dgvDetalle);
            this.groupBox2.Controls.Add(this.btnNuevo);
            this.groupBox2.Controls.Add(this.btnEditar);
            this.groupBox2.Controls.Add(this.btnEliminar);
            this.groupBox2.Controls.Add(this.txtBruto);
            this.groupBox2.Controls.Add(this.txtPrecioVenta);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtIGV);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtDscto);
            this.groupBox2.Controls.Add(this.txtValorVenta);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1007, 409);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalle";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.pbFoto);
            this.groupBox6.Location = new System.Drawing.Point(809, 33);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(198, 262);
            this.groupBox6.TabIndex = 53;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Fotografía";
            // 
            // pbFoto
            // 
            this.pbFoto.Location = new System.Drawing.Point(9, 19);
            this.pbFoto.Name = "pbFoto";
            this.pbFoto.Size = new System.Drawing.Size(180, 241);
            this.pbFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFoto.TabIndex = 38;
            this.pbFoto.TabStop = false;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(247, 331);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(37, 12);
            this.label36.TabIndex = 34;
            this.label36.Text = "Detalle:";
            this.label36.Visible = false;
            // 
            // txtDetalle
            // 
            this.txtDetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDetalle.BackColor = System.Drawing.SystemColors.Window;
            this.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDetalle.Location = new System.Drawing.Point(249, 347);
            this.txtDetalle.Multiline = true;
            this.txtDetalle.Name = "txtDetalle";
            this.txtDetalle.Size = new System.Drawing.Size(375, 53);
            this.txtDetalle.TabIndex = 33;
            this.txtDetalle.Visible = false;
            // 
            // txtDsctoGobal
            // 
            this.txtDsctoGobal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDsctoGobal.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDsctoGobal.Location = new System.Drawing.Point(920, 307);
            this.txtDsctoGobal.Name = "txtDsctoGobal";
            this.txtDsctoGobal.ReadOnly = true;
            this.txtDsctoGobal.Size = new System.Drawing.Size(75, 18);
            this.txtDsctoGobal.TabIndex = 25;
            this.txtDsctoGobal.Tag = "7";
            this.txtDsctoGobal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(840, 310);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 12);
            this.label21.TabIndex = 23;
            this.label21.Tag = "7";
            this.label21.Text = "Dscto Global :";
            // 
            // dgvDetalle
            // 
            this.dgvDetalle.AllowUserToAddRows = false;
            this.dgvDetalle.AllowUserToDeleteRows = false;
            this.dgvDetalle.AllowUserToResizeRows = false;
            this.dgvDetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetalle.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coddetalle,
            this.codSalida1,
            this.codproducto,
            this.referencia,
            this.descripcion,
            this.codunidad,
            this.unidad,
            this.serielote,
            this.cantidad,
            this.preciounit,
            this.importe,
            this.dscto1,
            this.aumento,
            this.tipodscto,
            this.dscto2,
            this.dscto3,
            this.montodscto,
            this.valorventa,
            this.igv,
            this.precioventa,
            this.precioreal,
            this.valoreal,
            this.stockPend,
            this.dsctoMax,
            this.coduser,
            this.fecharegistro});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDetalle.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvDetalle.Location = new System.Drawing.Point(3, 16);
            this.dgvDetalle.Name = "dgvDetalle";
            this.dgvDetalle.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDetalle.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvDetalle.RowHeadersVisible = false;
            this.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetalle.Size = new System.Drawing.Size(802, 309);
            this.dgvDetalle.TabIndex = 2;
            this.superValidator1.SetValidator1(this.dgvDetalle, this.customValidator5);
            this.dgvDetalle.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDetalle_CellValueChanged);
            this.dgvDetalle.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvDetalle_RowsAdded);
            this.dgvDetalle.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvDetalle_RowsRemoved);
            this.dgvDetalle.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvDetalle_RowStateChanged);
            // 
            // coddetalle
            // 
            this.coddetalle.DataPropertyName = "codDetalle";
            this.coddetalle.HeaderText = "CodDetalle";
            this.coddetalle.Name = "coddetalle";
            this.coddetalle.ReadOnly = true;
            this.coddetalle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.coddetalle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.coddetalle.Visible = false;
            // 
            // codSalida1
            // 
            this.codSalida1.HeaderText = "CodSalida";
            this.codSalida1.Name = "codSalida1";
            this.codSalida1.ReadOnly = true;
            this.codSalida1.Visible = false;
            // 
            // codproducto
            // 
            this.codproducto.DataPropertyName = "codProducto";
            this.codproducto.HeaderText = "CodProducto";
            this.codproducto.Name = "codproducto";
            this.codproducto.ReadOnly = true;
            this.codproducto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codproducto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codproducto.Visible = false;
            // 
            // referencia
            // 
            this.referencia.DataPropertyName = "referencia";
            this.referencia.HeaderText = "Codigo";
            this.referencia.Name = "referencia";
            this.referencia.ReadOnly = true;
            this.referencia.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.referencia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.referencia.Width = 87;
            // 
            // descripcion
            // 
            this.descripcion.DataPropertyName = "producto";
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            this.descripcion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.descripcion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.descripcion.Width = 350;
            // 
            // codunidad
            // 
            this.codunidad.DataPropertyName = "codUnidadMedida";
            this.codunidad.HeaderText = "Cod. Unidad";
            this.codunidad.Name = "codunidad";
            this.codunidad.ReadOnly = true;
            this.codunidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codunidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codunidad.Visible = false;
            // 
            // unidad
            // 
            this.unidad.DataPropertyName = "unidad";
            this.unidad.HeaderText = "Unidad";
            this.unidad.Name = "unidad";
            this.unidad.ReadOnly = true;
            this.unidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.unidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.unidad.Width = 75;
            // 
            // serielote
            // 
            this.serielote.DataPropertyName = "serielote";
            this.serielote.HeaderText = "Serie/Lote";
            this.serielote.Name = "serielote";
            this.serielote.ReadOnly = true;
            this.serielote.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.serielote.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.serielote.Visible = false;
            this.serielote.Width = 80;
            // 
            // cantidad
            // 
            this.cantidad.DataPropertyName = "cantidad";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.ReadOnly = true;
            this.cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.cantidad.Width = 70;
            // 
            // preciounit
            // 
            this.preciounit.DataPropertyName = "preciounitario";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.preciounit.DefaultCellStyle = dataGridViewCellStyle3;
            this.preciounit.HeaderText = "P. Unit.";
            this.preciounit.Name = "preciounit";
            this.preciounit.ReadOnly = true;
            this.preciounit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.preciounit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.preciounit.Width = 75;
            // 
            // importe
            // 
            this.importe.DataPropertyName = "subtotal";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.importe.DefaultCellStyle = dataGridViewCellStyle4;
            this.importe.HeaderText = "Importe";
            this.importe.Name = "importe";
            this.importe.ReadOnly = true;
            this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.importe.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.importe.Width = 85;
            // 
            // dscto1
            // 
            this.dscto1.DataPropertyName = "descuento1";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.dscto1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dscto1.HeaderText = "Dscto";
            this.dscto1.Name = "dscto1";
            this.dscto1.ReadOnly = true;
            this.dscto1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // aumento
            // 
            this.aumento.DataPropertyName = "aumento";
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.aumento.DefaultCellStyle = dataGridViewCellStyle6;
            this.aumento.HeaderText = "Aumento";
            this.aumento.Name = "aumento";
            this.aumento.ReadOnly = true;
            this.aumento.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.aumento.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tipodscto
            // 
            this.tipodscto.HeaderText = "tidodescuento";
            this.tipodscto.Name = "tipodscto";
            this.tipodscto.ReadOnly = true;
            this.tipodscto.Visible = false;
            // 
            // dscto2
            // 
            this.dscto2.DataPropertyName = "descuento2";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            this.dscto2.DefaultCellStyle = dataGridViewCellStyle7;
            this.dscto2.HeaderText = "% Dscto2";
            this.dscto2.Name = "dscto2";
            this.dscto2.ReadOnly = true;
            this.dscto2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto2.Visible = false;
            // 
            // dscto3
            // 
            this.dscto3.DataPropertyName = "descuento3";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.dscto3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dscto3.HeaderText = "% Dscto3";
            this.dscto3.Name = "dscto3";
            this.dscto3.ReadOnly = true;
            this.dscto3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dscto3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dscto3.Visible = false;
            // 
            // montodscto
            // 
            this.montodscto.DataPropertyName = "montodscto";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            this.montodscto.DefaultCellStyle = dataGridViewCellStyle9;
            this.montodscto.HeaderText = "Monto Dscto";
            this.montodscto.Name = "montodscto";
            this.montodscto.ReadOnly = true;
            this.montodscto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.montodscto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.montodscto.Visible = false;
            this.montodscto.Width = 80;
            // 
            // valorventa
            // 
            this.valorventa.DataPropertyName = "valorventa";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N2";
            dataGridViewCellStyle10.NullValue = null;
            this.valorventa.DefaultCellStyle = dataGridViewCellStyle10;
            this.valorventa.HeaderText = "V. Venta";
            this.valorventa.Name = "valorventa";
            this.valorventa.ReadOnly = true;
            this.valorventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valorventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valorventa.Width = 85;
            // 
            // igv
            // 
            this.igv.DataPropertyName = "igv";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N2";
            dataGridViewCellStyle11.NullValue = null;
            this.igv.DefaultCellStyle = dataGridViewCellStyle11;
            this.igv.HeaderText = "IGV";
            this.igv.Name = "igv";
            this.igv.ReadOnly = true;
            this.igv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.igv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.igv.Width = 85;
            // 
            // precioventa
            // 
            this.precioventa.DataPropertyName = "importe";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.precioventa.DefaultCellStyle = dataGridViewCellStyle12;
            this.precioventa.HeaderText = "P. Venta";
            this.precioventa.Name = "precioventa";
            this.precioventa.ReadOnly = true;
            this.precioventa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioventa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioventa.Width = 85;
            // 
            // precioreal
            // 
            this.precioreal.DataPropertyName = "precioreal";
            this.precioreal.HeaderText = "P. real";
            this.precioreal.Name = "precioreal";
            this.precioreal.ReadOnly = true;
            this.precioreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.precioreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.precioreal.Visible = false;
            // 
            // valoreal
            // 
            this.valoreal.DataPropertyName = "valoreal";
            this.valoreal.HeaderText = "V. real";
            this.valoreal.Name = "valoreal";
            this.valoreal.ReadOnly = true;
            this.valoreal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valoreal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.valoreal.Visible = false;
            // 
            // stockPend
            // 
            this.stockPend.DataPropertyName = "stockdisponible";
            this.stockPend.HeaderText = "StockDisponible";
            this.stockPend.Name = "stockPend";
            this.stockPend.ReadOnly = true;
            this.stockPend.Visible = false;
            // 
            // dsctoMax
            // 
            this.dsctoMax.DataPropertyName = "maxPorcDescto";
            this.dsctoMax.HeaderText = "%DsctoMax";
            this.dsctoMax.Name = "dsctoMax";
            this.dsctoMax.ReadOnly = true;
            this.dsctoMax.Visible = false;
            // 
            // coduser
            // 
            this.coduser.DataPropertyName = "codUser";
            this.coduser.HeaderText = "CodUser";
            this.coduser.Name = "coduser";
            this.coduser.ReadOnly = true;
            this.coduser.Visible = false;
            // 
            // fecharegistro
            // 
            this.fecharegistro.DataPropertyName = "fecharegistro";
            this.fecharegistro.HeaderText = "Fecha Reg";
            this.fecharegistro.Name = "fecharegistro";
            this.fecharegistro.ReadOnly = true;
            this.fecharegistro.Visible = false;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevo.ImageIndex = 1;
            this.btnNuevo.ImageList = this.imageList1;
            this.btnNuevo.Location = new System.Drawing.Point(12, 334);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(71, 32);
            this.btnNuevo.TabIndex = 20;
            this.btnNuevo.Text = "Nuevo";
            this.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // btnEditar
            // 
            this.btnEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEditar.Enabled = false;
            this.btnEditar.ImageIndex = 0;
            this.btnEditar.ImageList = this.imageList1;
            this.btnEditar.Location = new System.Drawing.Point(89, 334);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(66, 32);
            this.btnEditar.TabIndex = 21;
            this.btnEditar.Text = "Editar";
            this.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEliminar.Enabled = false;
            this.btnEliminar.ImageIndex = 2;
            this.btnEliminar.ImageList = this.imageList1;
            this.btnEliminar.Location = new System.Drawing.Point(161, 334);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 32);
            this.btnEliminar.TabIndex = 22;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // txtBruto
            // 
            this.txtBruto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBruto.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBruto.Location = new System.Drawing.Point(884, 331);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.ReadOnly = true;
            this.txtBruto.Size = new System.Drawing.Size(88, 18);
            this.txtBruto.TabIndex = 23;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPrecioVenta
            // 
            this.txtPrecioVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrecioVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioVenta.Location = new System.Drawing.Point(718, 378);
            this.txtPrecioVenta.Name = "txtPrecioVenta";
            this.txtPrecioVenta.ReadOnly = true;
            this.txtPrecioVenta.Size = new System.Drawing.Size(88, 18);
            this.txtPrecioVenta.TabIndex = 28;
            this.txtPrecioVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(652, 381);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "P. Venta :";
            // 
            // txtIGV
            // 
            this.txtIGV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIGV.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIGV.Location = new System.Drawing.Point(718, 356);
            this.txtIGV.Name = "txtIGV";
            this.txtIGV.ReadOnly = true;
            this.txtIGV.Size = new System.Drawing.Size(88, 18);
            this.txtIGV.TabIndex = 27;
            this.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(652, 362);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "I.G.V. :";
            // 
            // txtDscto
            // 
            this.txtDscto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDscto.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDscto.Location = new System.Drawing.Point(884, 378);
            this.txtDscto.Name = "txtDscto";
            this.txtDscto.ReadOnly = true;
            this.txtDscto.Size = new System.Drawing.Size(88, 18);
            this.txtDscto.TabIndex = 24;
            this.txtDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtValorVenta
            // 
            this.txtValorVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorVenta.Location = new System.Drawing.Point(718, 334);
            this.txtValorVenta.Name = "txtValorVenta";
            this.txtValorVenta.ReadOnly = true;
            this.txtValorVenta.Size = new System.Drawing.Size(88, 18);
            this.txtValorVenta.TabIndex = 26;
            this.txtValorVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(652, 340);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "V. Venta :";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(826, 381);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 12);
            this.label11.TabIndex = 14;
            this.label11.Text = "Descuento :";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(846, 334);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 12);
            this.label10.TabIndex = 12;
            this.label10.Text = "Bruto :";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(5, 154);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 55;
            this.label39.Text = "T. Impuesto :";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.groupBox3.Controls.Add(this.txtvendedor);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.txttasa);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.txttienda);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.txtMoneda);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.txtplazo);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.lbLineaCredito);
            this.groupBox3.Controls.Add(this.txtLineaCredito);
            this.groupBox3.Controls.Add(this.txtLineaCreditoUso);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.txtLineaCreditoDisponible);
            this.groupBox3.Enabled = false;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(786, 46);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(221, 192);
            this.groupBox3.TabIndex = 101;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Condiciones de Crédito:";
            // 
            // txtvendedor
            // 
            this.txtvendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvendedor.Location = new System.Drawing.Point(103, 161);
            this.txtvendedor.Name = "txtvendedor";
            this.txtvendedor.ReadOnly = true;
            this.txtvendedor.Size = new System.Drawing.Size(112, 18);
            this.txtvendedor.TabIndex = 108;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(52, 164);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(48, 12);
            this.label31.TabIndex = 107;
            this.label31.Text = "Vendedor:";
            // 
            // txttasa
            // 
            this.txttasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttasa.Location = new System.Drawing.Point(103, 78);
            this.txttasa.Name = "txttasa";
            this.txttasa.ReadOnly = true;
            this.txttasa.Size = new System.Drawing.Size(112, 18);
            this.txttasa.TabIndex = 106;
            this.txttasa.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(29, 81);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 12);
            this.label30.TabIndex = 105;
            this.label30.Text = "Tasa de Interés:";
            // 
            // txttienda
            // 
            this.txttienda.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttienda.Location = new System.Drawing.Point(103, 119);
            this.txttienda.Name = "txttienda";
            this.txttienda.ReadOnly = true;
            this.txttienda.Size = new System.Drawing.Size(112, 18);
            this.txttienda.TabIndex = 104;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(65, 122);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(35, 12);
            this.label29.TabIndex = 103;
            this.label29.Text = "Tienda:";
            // 
            // txtMoneda
            // 
            this.txtMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoneda.Location = new System.Drawing.Point(103, 99);
            this.txtMoneda.Name = "txtMoneda";
            this.txtMoneda.ReadOnly = true;
            this.txtMoneda.Size = new System.Drawing.Size(112, 18);
            this.txtMoneda.TabIndex = 102;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(58, 99);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(42, 12);
            this.label28.TabIndex = 101;
            this.label28.Text = "Moneda:";
            // 
            // txtplazo
            // 
            this.txtplazo.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtplazo.Location = new System.Drawing.Point(103, 140);
            this.txtplazo.Name = "txtplazo";
            this.txtplazo.ReadOnly = true;
            this.txtplazo.Size = new System.Drawing.Size(112, 18);
            this.txtplazo.TabIndex = 100;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(63, 143);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 12);
            this.label27.TabIndex = 99;
            this.label27.Text = "Plazo:";
            // 
            // lbLineaCredito
            // 
            this.lbLineaCredito.AutoSize = true;
            this.lbLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLineaCredito.Location = new System.Drawing.Point(6, 16);
            this.lbLineaCredito.Name = "lbLineaCredito";
            this.lbLineaCredito.Size = new System.Drawing.Size(94, 12);
            this.lbLineaCredito.TabIndex = 85;
            this.lbLineaCredito.Text = "Línea de Crédito (S/.):";
            // 
            // txtLineaCredito
            // 
            this.txtLineaCredito.Enabled = false;
            this.txtLineaCredito.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCredito.Location = new System.Drawing.Point(103, 13);
            this.txtLineaCredito.Name = "txtLineaCredito";
            this.txtLineaCredito.ReadOnly = true;
            this.txtLineaCredito.Size = new System.Drawing.Size(112, 18);
            this.txtLineaCredito.TabIndex = 84;
            this.txtLineaCredito.Text = "0";
            // 
            // txtLineaCreditoUso
            // 
            this.txtLineaCreditoUso.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoUso.Location = new System.Drawing.Point(103, 57);
            this.txtLineaCreditoUso.Name = "txtLineaCreditoUso";
            this.txtLineaCreditoUso.ReadOnly = true;
            this.txtLineaCreditoUso.Size = new System.Drawing.Size(112, 18);
            this.txtLineaCreditoUso.TabIndex = 98;
            this.txtLineaCreditoUso.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 36);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 12);
            this.label23.TabIndex = 95;
            this.label23.Text = "Línea Disponible (S/.):";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(7, 60);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 12);
            this.label25.TabIndex = 97;
            this.label25.Text = "Línea C. en Uso (S/.):";
            // 
            // txtLineaCreditoDisponible
            // 
            this.txtLineaCreditoDisponible.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineaCreditoDisponible.Location = new System.Drawing.Point(103, 36);
            this.txtLineaCreditoDisponible.Name = "txtLineaCreditoDisponible";
            this.txtLineaCreditoDisponible.ReadOnly = true;
            this.txtLineaCreditoDisponible.Size = new System.Drawing.Size(112, 18);
            this.txtLineaCreditoDisponible.TabIndex = 96;
            this.txtLineaCreditoDisponible.Text = "0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.pbCapchatS);
            this.groupBox4.Controls.Add(this.txtSunat_Capchat);
            this.groupBox4.Controls.Add(this.btnNuevaVenta);
            this.groupBox4.Controls.Add(this.btnImprimir);
            this.groupBox4.Controls.Add(this.txtCodDocumento);
            this.groupBox4.Controls.Add(this.btnSalir);
            this.groupBox4.Controls.Add(this.btnGuardar);
            this.groupBox4.Controls.Add(this.txtDireccion);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(0, 671);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1007, 50);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.ImageList = this.imageList1;
            this.button1.Location = new System.Drawing.Point(154, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 32);
            this.button1.TabIndex = 126;
            this.button1.Text = "Contingencia";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // pbCapchatS
            // 
            this.pbCapchatS.Location = new System.Drawing.Point(493, 15);
            this.pbCapchatS.Name = "pbCapchatS";
            this.pbCapchatS.Size = new System.Drawing.Size(19, 21);
            this.pbCapchatS.TabIndex = 125;
            this.pbCapchatS.TabStop = false;
            this.pbCapchatS.Visible = false;
            // 
            // txtSunat_Capchat
            // 
            this.txtSunat_Capchat.Location = new System.Drawing.Point(475, 16);
            this.txtSunat_Capchat.Name = "txtSunat_Capchat";
            this.txtSunat_Capchat.Size = new System.Drawing.Size(16, 20);
            this.txtSunat_Capchat.TabIndex = 124;
            this.txtSunat_Capchat.Visible = false;
            // 
            // btnNuevaVenta
            // 
            this.btnNuevaVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNuevaVenta.Enabled = false;
            this.btnNuevaVenta.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevaVenta.ImageIndex = 1;
            this.btnNuevaVenta.ImageList = this.imageList1;
            this.btnNuevaVenta.Location = new System.Drawing.Point(8, 13);
            this.btnNuevaVenta.Name = "btnNuevaVenta";
            this.btnNuevaVenta.Size = new System.Drawing.Size(105, 32);
            this.btnNuevaVenta.TabIndex = 25;
            this.btnNuevaVenta.Text = "Nueva Venta";
            this.btnNuevaVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNuevaVenta.UseVisualStyleBackColor = true;
            this.btnNuevaVenta.Visible = false;
            this.btnNuevaVenta.Click += new System.EventHandler(this.btnNuevaVenta_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.ImageIndex = 3;
            this.btnImprimir.ImageList = this.imageList1;
            this.btnImprimir.Location = new System.Drawing.Point(746, 13);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(78, 32);
            this.btnImprimir.TabIndex = 24;
            this.btnImprimir.Text = "Im&primir";
            this.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Visible = false;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // txtCodDocumento
            // 
            this.txtCodDocumento.Location = new System.Drawing.Point(308, 18);
            this.txtCodDocumento.Name = "txtCodDocumento";
            this.txtCodDocumento.Size = new System.Drawing.Size(39, 20);
            this.txtCodDocumento.TabIndex = 19;
            this.txtCodDocumento.Visible = false;
            this.txtCodDocumento.TextChanged += new System.EventHandler(this.txtCodDocumento_TextChanged);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.ImageIndex = 5;
            this.btnSalir.ImageList = this.imageList1;
            this.btnSalir.Location = new System.Drawing.Point(933, 12);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(68, 32);
            this.btnSalir.TabIndex = 26;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageIndex = 4;
            this.btnGuardar.ImageList = this.imageList1;
            this.btnGuardar.Location = new System.Drawing.Point(850, 12);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(77, 32);
            this.btnGuardar.TabIndex = 23;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtDireccion
            // 
            this.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDireccion.Location = new System.Drawing.Point(357, 18);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.ReadOnly = true;
            this.txtDireccion.Size = new System.Drawing.Size(66, 20);
            this.txtDireccion.TabIndex = 14;
            this.txtDireccion.Tag = "21";
            this.txtDireccion.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbTimpuesto);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.labelnotacredito);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.txtcodpedido);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.ckbguia);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.cmbAlmacen);
            this.groupBox1.Controls.Add(this.lblAlmacen);
            this.groupBox1.Controls.Add(this.txtDireccionCliente);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtCodigoCli);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCotizacion);
            this.groupBox1.Controls.Add(this.txtNumero);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtTipoCambio);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.cbostand);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.cbovendedor);
            this.groupBox1.Controls.Add(this.txtGuias);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtOrdenTrab);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txtPDescuento);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbListaPrecios);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.dtpFechaPago);
            this.groupBox1.Controls.Add(this.cmbFormaPago);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbAutorizado);
            this.groupBox1.Controls.Add(this.txtAutorizacion);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtSerie);
            this.groupBox1.Controls.Add(this.cmbMoneda);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtNombreCliente);
            this.groupBox1.Controls.Add(this.txtCodCliente);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtComentario);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtNumDoc);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDocRef);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbNombreTransaccion);
            this.groupBox1.Controls.Add(this.txtTransaccion);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpFecha);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 401);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1007, 270);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Moneda:";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // cbTimpuesto
            // 
            this.cbTimpuesto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTimpuesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTimpuesto.FormattingEnabled = true;
            this.cbTimpuesto.Location = new System.Drawing.Point(76, 152);
            this.cbTimpuesto.Name = "cbTimpuesto";
            this.cbTimpuesto.Size = new System.Drawing.Size(161, 20);
            this.cbTimpuesto.TabIndex = 107;
            // 
            // labelnotacredito
            // 
            this.labelnotacredito.AutoSize = true;
            this.labelnotacredito.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelnotacredito.Location = new System.Drawing.Point(504, 48);
            this.labelnotacredito.Name = "labelnotacredito";
            this.labelnotacredito.Size = new System.Drawing.Size(234, 13);
            this.labelnotacredito.TabIndex = 106;
            this.labelnotacredito.Text = "Este Cliente tiene Notas de Credito no aplicadas";
            this.labelnotacredito.Visible = false;
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(791, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 12);
            this.label38.TabIndex = 105;
            this.label38.Tag = "21";
            this.label38.Text = "Pedido";
            this.label38.Visible = false;
            this.label38.Click += new System.EventHandler(this.label38_Click);
            // 
            // txtcodpedido
            // 
            this.txtcodpedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtcodpedido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtcodpedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcodpedido.ForeColor = System.Drawing.Color.Red;
            this.txtcodpedido.Location = new System.Drawing.Point(833, 14);
            this.txtcodpedido.Multiline = true;
            this.txtcodpedido.Name = "txtcodpedido";
            this.txtcodpedido.Size = new System.Drawing.Size(164, 28);
            this.txtcodpedido.TabIndex = 104;
            this.txtcodpedido.Tag = "21";
            this.txtcodpedido.Text = ".";
            this.txtcodpedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtcodpedido.Visible = false;
            this.txtcodpedido.TextChanged += new System.EventHandler(this.txtcodpedido_TextChanged);
            this.txtcodpedido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcodpedido_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(416, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(8, 12);
            this.label37.TabIndex = 103;
            this.label37.Text = "-";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(242, 186);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(90, 17);
            this.checkBox1.TabIndex = 102;
            this.checkBox1.Text = "Venta Normal";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ckbguia
            // 
            this.ckbguia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbguia.AutoSize = true;
            this.ckbguia.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbguia.Location = new System.Drawing.Point(345, 100);
            this.ckbguia.Name = "ckbguia";
            this.ckbguia.Size = new System.Drawing.Size(78, 16);
            this.ckbguia.TabIndex = 18;
            this.ckbguia.Text = "Generar Guia";
            this.ckbguia.UseVisualStyleBackColor = true;
            this.ckbguia.Visible = false;
            this.ckbguia.CheckedChanged += new System.EventHandler(this.ckbguia_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.txtSerieG);
            this.groupBox5.Controls.Add(this.txtcodserie);
            this.groupBox5.Controls.Add(this.txtNumeroG);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.txtRazonSocialTransporte);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.cmbTransportista);
            this.groupBox5.Controls.Add(this.cmbVehiculo);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(429, 106);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(327, 109);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Datos de Guia";
            this.groupBox5.Visible = false;
            // 
            // txtSerieG
            // 
            this.txtSerieG.Location = new System.Drawing.Point(68, 19);
            this.txtSerieG.Name = "txtSerieG";
            this.txtSerieG.ReadOnly = true;
            this.txtSerieG.Size = new System.Drawing.Size(61, 18);
            this.txtSerieG.TabIndex = 1;
            this.superValidator1.SetValidator1(this.txtSerieG, this.customValidator1);
            this.txtSerieG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSerieG_KeyDown);
            // 
            // txtcodserie
            // 
            this.txtcodserie.Location = new System.Drawing.Point(213, 19);
            this.txtcodserie.Name = "txtcodserie";
            this.txtcodserie.Size = new System.Drawing.Size(16, 18);
            this.txtcodserie.TabIndex = 3;
            this.txtcodserie.Visible = false;
            // 
            // txtNumeroG
            // 
            this.txtNumeroG.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNumeroG.Enabled = false;
            this.txtNumeroG.Location = new System.Drawing.Point(137, 19);
            this.txtNumeroG.Name = "txtNumeroG";
            this.txtNumeroG.Size = new System.Drawing.Size(65, 18);
            this.txtNumeroG.TabIndex = 2;
            this.txtNumeroG.Tag = "";
            this.superValidator1.SetValidator1(this.txtNumeroG, this.customValidator2);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(35, 21);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 12);
            this.label35.TabIndex = 83;
            this.label35.Text = "Serie.";
            // 
            // txtRazonSocialTransporte
            // 
            this.txtRazonSocialTransporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRazonSocialTransporte.Location = new System.Drawing.Point(70, 85);
            this.txtRazonSocialTransporte.Name = "txtRazonSocialTransporte";
            this.txtRazonSocialTransporte.ReadOnly = true;
            this.txtRazonSocialTransporte.Size = new System.Drawing.Size(253, 18);
            this.txtRazonSocialTransporte.TabIndex = 6;
            this.txtRazonSocialTransporte.Tag = "6";
            this.txtRazonSocialTransporte.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRazonSocialTransporte_KeyDown);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(12, 89);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 12);
            this.label34.TabIndex = 79;
            this.label34.Text = "Raz. Social";
            // 
            // cmbTransportista
            // 
            this.cmbTransportista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTransportista.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTransportista.FormattingEnabled = true;
            this.cmbTransportista.Location = new System.Drawing.Point(70, 42);
            this.cmbTransportista.Name = "cmbTransportista";
            this.cmbTransportista.Size = new System.Drawing.Size(253, 20);
            this.cmbTransportista.TabIndex = 4;
            this.superValidator1.SetValidator1(this.cmbTransportista, this.customValidator3);
            // 
            // cmbVehiculo
            // 
            this.cmbVehiculo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVehiculo.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehiculo.FormattingEnabled = true;
            this.cmbVehiculo.Location = new System.Drawing.Point(70, 63);
            this.cmbVehiculo.Name = "cmbVehiculo";
            this.cmbVehiculo.Size = new System.Drawing.Size(253, 20);
            this.cmbVehiculo.TabIndex = 5;
            this.superValidator1.SetValidator1(this.cmbVehiculo, this.customValidator4);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(20, 62);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 12);
            this.label32.TabIndex = 78;
            this.label32.Text = "Vehiculo:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(6, 43);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 12);
            this.label33.TabIndex = 77;
            this.label33.Text = "Transportista:";
            // 
            // cmbAlmacen
            // 
            this.cmbAlmacen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbAlmacen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAlmacen.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAlmacen.FormattingEnabled = true;
            this.cmbAlmacen.Location = new System.Drawing.Point(713, 243);
            this.cmbAlmacen.Name = "cmbAlmacen";
            this.cmbAlmacen.Size = new System.Drawing.Size(289, 20);
            this.cmbAlmacen.TabIndex = 16;
            this.cmbAlmacen.Tag = "0";
            this.cmbAlmacen.Visible = false;
            this.cmbAlmacen.SelectionChangeCommitted += new System.EventHandler(this.cmbAlmacen_SelectionChangeCommitted);
            this.cmbAlmacen.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbAlmacen_KeyDown);
            // 
            // lblAlmacen
            // 
            this.lblAlmacen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAlmacen.AutoSize = true;
            this.lblAlmacen.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlmacen.Location = new System.Drawing.Point(628, 246);
            this.lblAlmacen.Name = "lblAlmacen";
            this.lblAlmacen.Size = new System.Drawing.Size(72, 12);
            this.lblAlmacen.TabIndex = 100;
            this.lblAlmacen.Tag = "";
            this.lblAlmacen.Text = "Almacen Venta:";
            this.lblAlmacen.Visible = false;
            // 
            // txtDireccionCliente
            // 
            this.txtDireccionCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccionCliente.Location = new System.Drawing.Point(76, 72);
            this.txtDireccionCliente.Name = "txtDireccionCliente";
            this.txtDireccionCliente.ReadOnly = true;
            this.txtDireccionCliente.Size = new System.Drawing.Size(422, 18);
            this.txtDireccionCliente.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 72);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 12);
            this.label16.TabIndex = 89;
            this.label16.Text = "Direccion";
            // 
            // txtCodigoCli
            // 
            this.txtCodigoCli.Enabled = false;
            this.txtCodigoCli.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoCli.Location = new System.Drawing.Point(505, 46);
            this.txtCodigoCli.Name = "txtCodigoCli";
            this.txtCodigoCli.ReadOnly = true;
            this.txtCodigoCli.Size = new System.Drawing.Size(19, 18);
            this.txtCodigoCli.TabIndex = 88;
            this.txtCodigoCli.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(243, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 12);
            this.label4.TabIndex = 87;
            this.label4.Text = "Cotizacion:";
            this.label4.Visible = false;
            // 
            // txtCotizacion
            // 
            this.txtCotizacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCotizacion.ForeColor = System.Drawing.Color.Red;
            this.txtCotizacion.Location = new System.Drawing.Point(297, 118);
            this.txtCotizacion.Multiline = true;
            this.txtCotizacion.Name = "txtCotizacion";
            this.txtCotizacion.Size = new System.Drawing.Size(122, 30);
            this.txtCotizacion.TabIndex = 1;
            this.txtCotizacion.Text = ".";
            this.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCotizacion.Visible = false;
            this.txtCotizacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCotizacion_KeyPress);
            this.txtCotizacion.Leave += new System.EventHandler(this.txtCotizacion_Leave);
            // 
            // txtNumero
            // 
            this.txtNumero.Enabled = false;
            this.txtNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.Location = new System.Drawing.Point(427, 20);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(69, 18);
            this.txtNumero.TabIndex = 6;
            this.superValidator1.SetValidator1(this.txtNumero, this.customValidator11);
            this.txtNumero.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumero_KeyDown);
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            this.txtNumero.Leave += new System.EventHandler(this.txtNumero_Leave);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(588, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 80;
            this.label8.Text = "Tipo Cambio:";
            // 
            // txtTipoCambio
            // 
            this.txtTipoCambio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTipoCambio.Enabled = false;
            this.txtTipoCambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTipoCambio.Location = new System.Drawing.Point(653, 86);
            this.txtTipoCambio.Name = "txtTipoCambio";
            this.txtTipoCambio.ReadOnly = true;
            this.txtTipoCambio.Size = new System.Drawing.Size(90, 18);
            this.txtTipoCambio.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(229, 152);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 12);
            this.label22.TabIndex = 77;
            this.label22.Text = "Stand:";
            this.label22.Visible = false;
            // 
            // cbostand
            // 
            this.cbostand.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbostand.FormattingEnabled = true;
            this.cbostand.Location = new System.Drawing.Point(267, 149);
            this.cbostand.Name = "cbostand";
            this.cbostand.Size = new System.Drawing.Size(28, 20);
            this.cbostand.TabIndex = 76;
            this.cbostand.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 123);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 12);
            this.label24.TabIndex = 75;
            this.label24.Text = "Vendedor:";
            // 
            // cbovendedor
            // 
            this.cbovendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbovendedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbovendedor.FormattingEnabled = true;
            this.cbovendedor.Items.AddRange(new object[] {
            "Sin Vendedor"});
            this.cbovendedor.Location = new System.Drawing.Point(76, 124);
            this.cbovendedor.Name = "cbovendedor";
            this.cbovendedor.Size = new System.Drawing.Size(161, 20);
            this.cbovendedor.TabIndex = 10;
            this.cbovendedor.SelectionChangeCommitted += new System.EventHandler(this.cbovendedor_SelectionChangeCommitted);
            this.cbovendedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbovendedor_KeyDown);
            // 
            // txtGuias
            // 
            this.txtGuias.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGuias.Location = new System.Drawing.Point(327, 151);
            this.txtGuias.Name = "txtGuias";
            this.txtGuias.Size = new System.Drawing.Size(19, 18);
            this.txtGuias.TabIndex = 7;
            this.txtGuias.Visible = false;
            this.txtGuias.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGuias_KeyDown);
            this.txtGuias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGuias_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(294, 155);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 12);
            this.label19.TabIndex = 68;
            this.label19.Text = "Guia:";
            this.label19.Visible = false;
            // 
            // txtOrdenTrab
            // 
            this.txtOrdenTrab.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrdenTrab.Location = new System.Drawing.Point(938, 176);
            this.txtOrdenTrab.Name = "txtOrdenTrab";
            this.txtOrdenTrab.Size = new System.Drawing.Size(40, 18);
            this.txtOrdenTrab.TabIndex = 23;
            this.txtOrdenTrab.Tag = "19";
            this.txtOrdenTrab.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(865, 181);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 12);
            this.label20.TabIndex = 66;
            this.label20.Tag = "19";
            this.label20.Text = "Orden Trabajo :";
            this.label20.Visible = false;
            // 
            // txtPDescuento
            // 
            this.txtPDescuento.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPDescuento.Location = new System.Drawing.Point(416, 151);
            this.txtPDescuento.Name = "txtPDescuento";
            this.txtPDescuento.Size = new System.Drawing.Size(12, 18);
            this.txtPDescuento.TabIndex = 21;
            this.txtPDescuento.Tag = "7";
            this.txtPDescuento.Visible = false;
            this.txtPDescuento.TextChanged += new System.EventHandler(this.txtPDescuento_TextChanged);
            this.txtPDescuento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPDescuento_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(350, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 12);
            this.label6.TabIndex = 64;
            this.label6.Tag = "7";
            this.label6.Text = "% Descuento";
            this.label6.Visible = false;
            // 
            // cbListaPrecios
            // 
            this.cbListaPrecios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbListaPrecios.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbListaPrecios.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbListaPrecios.FormattingEnabled = true;
            this.cbListaPrecios.Location = new System.Drawing.Point(76, 180);
            this.cbListaPrecios.Name = "cbListaPrecios";
            this.cbListaPrecios.Size = new System.Drawing.Size(161, 20);
            this.cbListaPrecios.TabIndex = 9;
            this.cbListaPrecios.Visible = false;
            this.cbListaPrecios.SelectionChangeCommitted += new System.EventHandler(this.cbListaPrecios_SelectionChangeCommitted);
            this.cbListaPrecios.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbListaPrecios_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 179);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 12);
            this.label26.TabIndex = 60;
            this.label26.Text = "Lista Prec.";
            this.label26.Visible = false;
            // 
            // dtpFechaPago
            // 
            this.dtpFechaPago.Enabled = false;
            this.dtpFechaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaPago.Location = new System.Drawing.Point(242, 98);
            this.dtpFechaPago.Name = "dtpFechaPago";
            this.dtpFechaPago.Size = new System.Drawing.Size(81, 18);
            this.dtpFechaPago.TabIndex = 8;
            this.dtpFechaPago.Tag = "16";
            this.dtpFechaPago.Visible = false;
            // 
            // cmbFormaPago
            // 
            this.cmbFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormaPago.Enabled = false;
            this.cmbFormaPago.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFormaPago.FormattingEnabled = true;
            this.cmbFormaPago.Location = new System.Drawing.Point(76, 98);
            this.cmbFormaPago.Name = "cmbFormaPago";
            this.cmbFormaPago.Size = new System.Drawing.Size(161, 20);
            this.cmbFormaPago.TabIndex = 7;
            this.cmbFormaPago.Tag = "16";
            this.superValidator1.SetValidator1(this.cmbFormaPago, this.customValidator7);
            this.cmbFormaPago.Visible = false;
            this.cmbFormaPago.SelectionChangeCommitted += new System.EventHandler(this.cmbFormaPago_SelectionChangeCommitted);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 12);
            this.label3.TabIndex = 44;
            this.label3.Tag = "16";
            this.label3.Text = "Forma de Pago";
            this.label3.Visible = false;
            // 
            // lbAutorizado
            // 
            this.lbAutorizado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAutorizado.AutoSize = true;
            this.lbAutorizado.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAutorizado.Location = new System.Drawing.Point(914, 148);
            this.lbAutorizado.Name = "lbAutorizado";
            this.lbAutorizado.Size = new System.Drawing.Size(50, 12);
            this.lbAutorizado.TabIndex = 38;
            this.lbAutorizado.Tag = "22";
            this.lbAutorizado.Text = "Autorizado";
            // 
            // txtAutorizacion
            // 
            this.txtAutorizacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAutorizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAutorizacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAutorizacion.Location = new System.Drawing.Point(917, 125);
            this.txtAutorizacion.Name = "txtAutorizacion";
            this.txtAutorizacion.Size = new System.Drawing.Size(81, 18);
            this.txtAutorizacion.TabIndex = 15;
            this.txtAutorizacion.Tag = "22";
            this.txtAutorizacion.Visible = false;
            this.txtAutorizacion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAutorizacion_KeyDown);
            this.txtAutorizacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAutorizacion_KeyPress);
            this.txtAutorizacion.Leave += new System.EventHandler(this.txtAutorizacion_Leave);
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(849, 127);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 12);
            this.label18.TabIndex = 36;
            this.label18.Tag = "22";
            this.label18.Text = "Autorizacion :";
            this.label18.Visible = false;
            // 
            // txtSerie
            // 
            this.txtSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.Location = new System.Drawing.Point(376, 20);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.ReadOnly = true;
            this.txtSerie.Size = new System.Drawing.Size(38, 18);
            this.txtSerie.TabIndex = 5;
            this.txtSerie.Tag = "13";
            this.superValidator1.SetValidator1(this.txtSerie, this.customValidator10);
            this.txtSerie.Visible = false;
            this.txtSerie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSerie_KeyDown);
            this.txtSerie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSerie_KeyPress);
            this.txtSerie.Leave += new System.EventHandler(this.txtSerie_Leave);
            // 
            // cmbMoneda
            // 
            this.cmbMoneda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMoneda.FormattingEnabled = true;
            this.cmbMoneda.Location = new System.Drawing.Point(653, 62);
            this.cmbMoneda.Name = "cmbMoneda";
            this.cmbMoneda.Size = new System.Drawing.Size(89, 20);
            this.cmbMoneda.TabIndex = 12;
            this.cmbMoneda.SelectionChangeCommitted += new System.EventHandler(this.cmbMoneda_SelectionChangeCommitted);
            this.cmbMoneda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbMoneda_KeyDown);
            this.cmbMoneda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbMoneda_KeyPress);
            this.cmbMoneda.Leave += new System.EventHandler(this.cmbMoneda_Leave);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(603, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 12);
            this.label17.TabIndex = 31;
            this.label17.Text = "Moneda :";
            // 
            // txtNombreCliente
            // 
            this.txtNombreCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombreCliente.Enabled = false;
            this.txtNombreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreCliente.Location = new System.Drawing.Point(182, 46);
            this.txtNombreCliente.Name = "txtNombreCliente";
            this.txtNombreCliente.ReadOnly = true;
            this.txtNombreCliente.Size = new System.Drawing.Size(316, 18);
            this.txtNombreCliente.TabIndex = 2;
            this.txtNombreCliente.Tag = "3";
            this.txtNombreCliente.Text = "S/C";
            // 
            // txtCodCliente
            // 
            this.txtCodCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodCliente.Location = new System.Drawing.Point(76, 46);
            this.txtCodCliente.Name = "txtCodCliente";
            this.txtCodCliente.Size = new System.Drawing.Size(100, 18);
            this.txtCodCliente.TabIndex = 1;
            this.txtCodCliente.Tag = "5";
            this.txtCodCliente.Text = "00000000";
            this.txtCodCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.superValidator1.SetValidator1(this.txtCodCliente, this.customValidator6);
            this.txtCodCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodCliente_KeyDown);
            this.txtCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliente_KeyPress);
            this.txtCodCliente.Leave += new System.EventHandler(this.txtCodCliente_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 12);
            this.label15.TabIndex = 20;
            this.label15.Text = "Cliente";
            // 
            // txtComentario
            // 
            this.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComentario.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentario.Location = new System.Drawing.Point(65, 221);
            this.txtComentario.Multiline = true;
            this.txtComentario.Name = "txtComentario";
            this.txtComentario.Size = new System.Drawing.Size(533, 37);
            this.txtComentario.TabIndex = 15;
            this.txtComentario.Tag = "21";
            this.txtComentario.Visible = false;
            this.txtComentario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtComentario_KeyDown);
            this.txtComentario.Leave += new System.EventHandler(this.txtComentario_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 17;
            this.label9.Tag = "21";
            this.label9.Text = "Comentario";
            this.label9.Visible = false;
            // 
            // txtNumDoc
            // 
            this.txtNumDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNumDoc.Enabled = false;
            this.txtNumDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumDoc.Location = new System.Drawing.Point(653, 17);
            this.txtNumDoc.Name = "txtNumDoc";
            this.txtNumDoc.ReadOnly = true;
            this.txtNumDoc.Size = new System.Drawing.Size(89, 18);
            this.txtNumDoc.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(597, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "Num. Doc.";
            // 
            // txtDocRef
            // 
            this.txtDocRef.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocRef.Location = new System.Drawing.Point(342, 20);
            this.txtDocRef.Name = "txtDocRef";
            this.txtDocRef.ReadOnly = true;
            this.txtDocRef.Size = new System.Drawing.Size(28, 18);
            this.txtDocRef.TabIndex = 4;
            this.txtDocRef.Tag = "10";
            this.txtDocRef.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDocRef_KeyDown);
            this.txtDocRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocRef_KeyPress);
            this.txtDocRef.Leave += new System.EventHandler(this.txtDocRef_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(283, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "Doc. Ref.";
            // 
            // lbNombreTransaccion
            // 
            this.lbNombreTransaccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreTransaccion.Location = new System.Drawing.Point(110, 22);
            this.lbNombreTransaccion.Name = "lbNombreTransaccion";
            this.lbNombreTransaccion.Size = new System.Drawing.Size(160, 13);
            this.lbNombreTransaccion.TabIndex = 4;
            this.lbNombreTransaccion.Text = "NombreTransaccion";
            // 
            // txtTransaccion
            // 
            this.txtTransaccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTransaccion.Enabled = false;
            this.txtTransaccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransaccion.Location = new System.Drawing.Point(76, 20);
            this.txtTransaccion.Name = "txtTransaccion";
            this.txtTransaccion.ReadOnly = true;
            this.txtTransaccion.Size = new System.Drawing.Size(28, 18);
            this.txtTransaccion.TabIndex = 17;
            this.txtTransaccion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTransaccion_KeyDown);
            this.txtTransaccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTransaccion_KeyPress);
            this.txtTransaccion.Leave += new System.EventHandler(this.txtTransaccion_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Transacción";
            // 
            // dtpFecha
            // 
            this.dtpFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha.Location = new System.Drawing.Point(653, 39);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(89, 18);
            this.dtpFecha.TabIndex = 11;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            this.dtpFecha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpFecha_KeyDown);
            this.dtpFecha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtpFecha_KeyPress);
            this.dtpFecha.Leave += new System.EventHandler(this.dtpFecha_Leave);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(611, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha :";
            // 
            // superValidator1
            // 
            this.superValidator1.ContainerControl = this;
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // customValidator5
            // 
            this.customValidator5.ErrorMessage = "Ingrese Detalle.";
            this.customValidator5.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator5.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator5_ValidateValue);
            // 
            // customValidator1
            // 
            this.customValidator1.ErrorMessage = "codSerie";
            this.customValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator1.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator1_ValidateValue);
            // 
            // customValidator2
            // 
            this.customValidator2.ErrorMessage = "Ingrese Numeracion";
            this.customValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator2.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator2_ValidateValue);
            // 
            // customValidator3
            // 
            this.customValidator3.ErrorMessage = "Ingrese Transportista";
            this.customValidator3.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator3.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator3_ValidateValue);
            // 
            // customValidator4
            // 
            this.customValidator4.ErrorMessage = "Ingrese Vehiculo";
            this.customValidator4.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator4.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator4_ValidateValue);
            // 
            // customValidator11
            // 
            this.customValidator11.ErrorMessage = "Ingrese Numero";
            this.customValidator11.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator11.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator11_ValidateValue);
            // 
            // customValidator7
            // 
            this.customValidator7.ErrorMessage = "Ingrese Forma Pago";
            this.customValidator7.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator7.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator7_ValidateValue);
            // 
            // customValidator10
            // 
            this.customValidator10.ErrorMessage = "Ingrese Serie.";
            this.customValidator10.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator10.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator10_ValidateValue);
            // 
            // customValidator6
            // 
            this.customValidator6.ErrorMessage = "Ingrese Cliente";
            this.customValidator6.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator6.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator6_ValidateValue);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // frmVenta
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1007, 721);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVenta";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Venta";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmVenta_Load);
            this.Shown += new System.EventHandler(this.frmVenta_Shown);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalle)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCapchatS)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.TextBox txtPrecioVenta;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtIGV;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDscto;
        private System.Windows.Forms.TextBox txtValorVenta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNombreCliente;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtComentario;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDocRef;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbNombreTransaccion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSerie;
        public System.Windows.Forms.DataGridView dgvDetalle;
        private System.Windows.Forms.TextBox txtAutorizacion;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtTransaccion;
        public System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label lbAutorizado;
        private System.Windows.Forms.TextBox txtCodDocumento;
        private System.Windows.Forms.DateTimePicker dtpFechaPago;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtPDescuento;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtGuias;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtOrdenTrab;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDsctoGobal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnNuevaVenta;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbostand;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbovendedor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumDoc;
        public System.Windows.Forms.ComboBox cmbFormaPago;
        public System.Windows.Forms.ComboBox cbListaPrecios;
        public System.Windows.Forms.ComboBox cmbMoneda;
        public System.Windows.Forms.TextBox txtTipoCambio;
        private System.Windows.Forms.TextBox txtLineaCredito;
        private System.Windows.Forms.Label lbLineaCredito;
        public System.Windows.Forms.Button btnEditar;
        public System.Windows.Forms.Button btnEliminar;
        public System.Windows.Forms.TextBox txtCotizacion;
        private System.Windows.Forms.TextBox txtCodigoCli;
        public System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.TextBox txtDireccionCliente;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtNumero;
        public System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.TextBox txtLineaCreditoUso;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtLineaCreditoDisponible;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.TextBox txtCodCliente;
        private System.Windows.Forms.ComboBox cmbAlmacen;
        private System.Windows.Forms.Label lblAlmacen;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtvendedor;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txttasa;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txttienda;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtMoneda;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtplazo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox ckbguia;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cmbTransportista;
        private System.Windows.Forms.ComboBox cmbVehiculo;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.TextBox txtRazonSocialTransporte;
        private System.Windows.Forms.Label label34;
        public System.Windows.Forms.TextBox txtSerieG;
        private System.Windows.Forms.TextBox txtcodserie;
        private System.Windows.Forms.TextBox txtNumeroG;
        private System.Windows.Forms.Label label35;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator1;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator3;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator4;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtDetalle;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator11;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator7;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator10;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator6;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator5;
        public System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label37;
        public System.Windows.Forms.TextBox txtcodpedido;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label38;
        private Reportes.clsReportes.CachedCRCuotasPrestamo cachedCRCuotasPrestamo1;
        private System.Windows.Forms.Label labelnotacredito;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.PictureBox pbFoto;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cbTimpuesto;
        private System.Windows.Forms.PictureBox pbCapchatS;
        private System.Windows.Forms.TextBox txtSunat_Capchat;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn coddetalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn codSalida1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn codunidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn serielote;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciounit;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn aumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipodscto;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto3;
        private System.Windows.Forms.DataGridViewTextBoxColumn montodscto;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn igv;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioreal;
        private System.Windows.Forms.DataGridViewTextBoxColumn valoreal;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockPend;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsctoMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn coduser;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecharegistro;
    }
}