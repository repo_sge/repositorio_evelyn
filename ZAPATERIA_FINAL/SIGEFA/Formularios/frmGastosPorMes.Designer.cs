namespace SIGEFA.Formularios
{
    partial class frmGastosPorMes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvGastosGenerales = new System.Windows.Forms.DataGridView();
            this.Items = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codTipoGasto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescripcionGasto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtdescripcion = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbtipogasto = new System.Windows.Forms.ComboBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.udmeses = new System.Windows.Forms.DomainUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGastosGenerales)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvGastosGenerales
            // 
            this.dgvGastosGenerales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGastosGenerales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Items,
            this.Mes,
            this.codTipoGasto,
            this.DescripcionGasto,
            this.Total});
            this.dgvGastosGenerales.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvGastosGenerales.Location = new System.Drawing.Point(0, 0);
            this.dgvGastosGenerales.MultiSelect = false;
            this.dgvGastosGenerales.Name = "dgvGastosGenerales";
            this.dgvGastosGenerales.ReadOnly = true;
            this.dgvGastosGenerales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGastosGenerales.Size = new System.Drawing.Size(548, 552);
            this.dgvGastosGenerales.TabIndex = 0;
            this.dgvGastosGenerales.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvGastosGenerales_RowsAdded);
            // 
            // Items
            // 
            this.Items.DataPropertyName = "Items";
            this.Items.HeaderText = "Items";
            this.Items.Name = "Items";
            this.Items.ReadOnly = true;
            // 
            // Mes
            // 
            this.Mes.DataPropertyName = "Mes";
            this.Mes.HeaderText = "Mes";
            this.Mes.Name = "Mes";
            this.Mes.ReadOnly = true;
            // 
            // codTipoGasto
            // 
            this.codTipoGasto.DataPropertyName = "codTipoGasto";
            this.codTipoGasto.HeaderText = "codTipoGasto";
            this.codTipoGasto.Name = "codTipoGasto";
            this.codTipoGasto.ReadOnly = true;
            this.codTipoGasto.Visible = false;
            // 
            // DescripcionGasto
            // 
            this.DescripcionGasto.DataPropertyName = "DescripcionGasto";
            this.DescripcionGasto.HeaderText = "Descripcion de Gasto";
            this.DescripcionGasto.Name = "DescripcionGasto";
            this.DescripcionGasto.ReadOnly = true;
            this.DescripcionGasto.Width = 200;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(554, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "MES";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(555, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "TIPO DE GASTO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(555, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "MONTO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(555, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "DESCRIPCION";
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtdescripcion.Location = new System.Drawing.Point(658, 121);
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Size = new System.Drawing.Size(383, 20);
            this.txtdescripcion.TabIndex = 8;
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(658, 148);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(100, 20);
            this.txtTotal.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.DeepPink;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(557, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(282, 31);
            this.label6.TabIndex = 11;
            this.label6.Text = "GASTOS GENERALES";
            // 
            // cbtipogasto
            // 
            this.cbtipogasto.FormattingEnabled = true;
            this.cbtipogasto.Location = new System.Drawing.Point(658, 94);
            this.cbtipogasto.Name = "cbtipogasto";
            this.cbtipogasto.Size = new System.Drawing.Size(257, 21);
            this.cbtipogasto.TabIndex = 12;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(555, 192);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(90, 23);
            this.btnAgregar.TabIndex = 13;
            this.btnAgregar.Text = "REGISTRAR";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // udmeses
            // 
            this.udmeses.Items.Add("1");
            this.udmeses.Items.Add("2");
            this.udmeses.Items.Add("3");
            this.udmeses.Items.Add("4");
            this.udmeses.Items.Add("5");
            this.udmeses.Items.Add("6");
            this.udmeses.Items.Add("7");
            this.udmeses.Items.Add("8");
            this.udmeses.Items.Add("9");
            this.udmeses.Items.Add("10");
            this.udmeses.Items.Add("11");
            this.udmeses.Items.Add("12");
            this.udmeses.Location = new System.Drawing.Point(658, 68);
            this.udmeses.Name = "udmeses";
            this.udmeses.Size = new System.Drawing.Size(63, 20);
            this.udmeses.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(658, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "ELIMINAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(554, 529);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "CALCULAR TOTAL";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmGastosPorMes
            // 
            this.ClientSize = new System.Drawing.Size(1067, 552);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.udmeses);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.cbtipogasto);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtdescripcion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvGastosGenerales);
            this.DoubleBuffered = true;
            this.Name = "frmGastosPorMes";
            this.Text = "Registrar Los Gastos al Mes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmGastosPorMes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGastosGenerales)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvGastosGenerales;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtdescripcion;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbtipogasto;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.DomainUpDown udmeses;
        private System.Windows.Forms.DataGridViewTextBoxColumn Items;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mes;
        private System.Windows.Forms.DataGridViewTextBoxColumn codTipoGasto;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescripcionGasto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}