﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;
using Telerik.WinControls.UI;

namespace SIGEFA.Formularios
{
    public partial class frmListDescuentos : Telerik.WinControls.UI.RadForm
    {
        clsAdmFacturaVenta admventa = new clsAdmFacturaVenta();


        public frmListDescuentos()
        {
            InitializeComponent();
        }

        private void frmListDescuentos_Load(object sender, EventArgs e)
        {
            cargaCajas();

            var diasmes = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            dtpFinicio.Value = DateTime.Now.AddDays(-DateTime.Now.Day + 1);
            dtpFfin.Value = DateTime.Now.AddDays(diasmes - DateTime.Now.Day);
        }

        public void listaDescuentos()
        {
            dgvDescuentos.DataSource = admventa.listaDescuentos(Convert.ToInt32(cmbTipo.SelectedValue),dtpFinicio.Value,dtpFfin.Value);
            dgvDescuentos.ClearSelection();

        }

        private void cargaCajas()
        {
            cmbTipo.ValueMember = "Key";
            cmbTipo.DisplayMember = "Value";

            Dictionary<int, string> pairs = new Dictionary<int, string>();
            pairs.Add(0, "Todos");
            pairs.Add(1, "Descuento");
            pairs.Add(2, "Aumento");

            cmbTipo.DataSource = pairs;

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            listaDescuentos();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            dgvDescuentos.MultiSelect = true;
            dgvDescuentos.SelectAll();
            dgvDescuentos.ClipboardCopyMode = GridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            DataObject dataObj = dgvDescuentos.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);

            dgvDescuentos.MultiSelect = false;
        }
    }
}
