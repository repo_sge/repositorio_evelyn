﻿namespace SIGEFA.Formularios
{
	partial class frmRegeneracionArchivos
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegeneracionArchivos));
            this.dgvVentaSinRepositorio = new System.Windows.Forms.DataGridView();
            this.fecha = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
            this.tipo_documento = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
            this.numdoc = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
            this.numerocliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codcliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTipoDoc = new Telerik.WinControls.UI.RadDropDownList();
            this.btnBuscarDocumentos = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGenerarArchivos = new System.Windows.Forms.Button();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentaSinRepositorio)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipoDoc)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvVentaSinRepositorio
            // 
            this.dgvVentaSinRepositorio.AllowUserToAddRows = false;
            this.dgvVentaSinRepositorio.AllowUserToDeleteRows = false;
            this.dgvVentaSinRepositorio.AllowUserToResizeRows = false;
            this.dgvVentaSinRepositorio.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvVentaSinRepositorio.ColumnHeadersHeight = 40;
            this.dgvVentaSinRepositorio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fecha,
            this.tipo_documento,
            this.numdoc,
            this.numerocliente,
            this.cliente,
            this.moneda,
            this.total,
            this.codFactura,
            this.codcliente});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVentaSinRepositorio.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVentaSinRepositorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVentaSinRepositorio.Location = new System.Drawing.Point(0, 0);
            this.dgvVentaSinRepositorio.MultiSelect = false;
            this.dgvVentaSinRepositorio.Name = "dgvVentaSinRepositorio";
            this.dgvVentaSinRepositorio.ReadOnly = true;
            this.dgvVentaSinRepositorio.RowHeadersVisible = false;
            this.dgvVentaSinRepositorio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVentaSinRepositorio.Size = new System.Drawing.Size(1062, 398);
            this.dgvVentaSinRepositorio.TabIndex = 0;
            this.dgvVentaSinRepositorio.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvVentaSinRepositorio_RowStateChanged);
            this.dgvVentaSinRepositorio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvVentaSinRepositorio_KeyDown);
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "Fecha del Documento";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fecha.Width = 140;
            // 
            // tipo_documento
            // 
            this.tipo_documento.DataPropertyName = "tipo_documento";
            this.tipo_documento.HeaderText = "Tipo de Documento";
            this.tipo_documento.Name = "tipo_documento";
            this.tipo_documento.ReadOnly = true;
            this.tipo_documento.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tipo_documento.Width = 180;
            // 
            // numdoc
            // 
            this.numdoc.DataPropertyName = "numdoc";
            this.numdoc.HeaderText = "Serie-Correlativo de Documento";
            this.numdoc.Name = "numdoc";
            this.numdoc.ReadOnly = true;
            this.numdoc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.numdoc.Width = 180;
            // 
            // numerocliente
            // 
            this.numerocliente.DataPropertyName = "numerocliente";
            this.numerocliente.HeaderText = "Nº Documento Cliente";
            this.numerocliente.Name = "numerocliente";
            this.numerocliente.ReadOnly = true;
            this.numerocliente.Width = 140;
            // 
            // cliente
            // 
            this.cliente.DataPropertyName = "cliente";
            this.cliente.HeaderText = "Razón Social Cliente";
            this.cliente.Name = "cliente";
            this.cliente.ReadOnly = true;
            this.cliente.Width = 300;
            // 
            // moneda
            // 
            this.moneda.DataPropertyName = "moneda";
            this.moneda.HeaderText = "Moneda";
            this.moneda.Name = "moneda";
            this.moneda.ReadOnly = true;
            this.moneda.Width = 83;
            // 
            // total
            // 
            this.total.DataPropertyName = "total";
            this.total.HeaderText = "Monto Total";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            this.total.Width = 98;
            // 
            // codFactura
            // 
            this.codFactura.DataPropertyName = "codFactura";
            this.codFactura.HeaderText = "Código Factura";
            this.codFactura.Name = "codFactura";
            this.codFactura.ReadOnly = true;
            this.codFactura.Visible = false;
            this.codFactura.Width = 114;
            // 
            // codcliente
            // 
            this.codcliente.DataPropertyName = "codcliente";
            this.codcliente.HeaderText = "Codigo Cliente";
            this.codcliente.Name = "codcliente";
            this.codcliente.ReadOnly = true;
            this.codcliente.Visible = false;
            this.codcliente.Width = 111;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cmbTipoDoc);
            this.panel2.Controls.Add(this.btnBuscarDocumentos);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnGenerarArchivos);
            this.panel2.Controls.Add(this.dtpHasta);
            this.panel2.Controls.Add(this.dtpDesde);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1062, 120);
            this.panel2.TabIndex = 2;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 20);
            this.label6.TabIndex = 18;
            this.label6.Text = "COMPROBANTE:";
            // 
            // cmbTipoDoc
            // 
            this.cmbTipoDoc.Location = new System.Drawing.Point(135, 84);
            this.cmbTipoDoc.Name = "cmbTipoDoc";
            this.cmbTipoDoc.Size = new System.Drawing.Size(165, 24);
            this.cmbTipoDoc.TabIndex = 17;
            this.cmbTipoDoc.ThemeName = "TelerikMetroBlue";
            // 
            // btnBuscarDocumentos
            // 
            this.btnBuscarDocumentos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarDocumentos.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarDocumentos.Image")));
            this.btnBuscarDocumentos.Location = new System.Drawing.Point(429, 39);
            this.btnBuscarDocumentos.Name = "btnBuscarDocumentos";
            this.btnBuscarDocumentos.Size = new System.Drawing.Size(111, 33);
            this.btnBuscarDocumentos.TabIndex = 14;
            this.btnBuscarDocumentos.Text = "BUSCAR";
            this.btnBuscarDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscarDocumentos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBuscarDocumentos.UseVisualStyleBackColor = true;
            this.btnBuscarDocumentos.Click += new System.EventHandler(this.btnBuscarDocumentos_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(709, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(302, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Seleccione una venta y haga clic en el botón";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(409, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Seleccione las fechas para filtrar las ventas que desea buscar";
            // 
            // btnGenerarArchivos
            // 
            this.btnGenerarArchivos.Enabled = false;
            this.btnGenerarArchivos.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerarArchivos.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerarArchivos.Image")));
            this.btnGenerarArchivos.Location = new System.Drawing.Point(709, 50);
            this.btnGenerarArchivos.Name = "btnGenerarArchivos";
            this.btnGenerarArchivos.Size = new System.Drawing.Size(302, 35);
            this.btnGenerarArchivos.TabIndex = 5;
            this.btnGenerarArchivos.Text = "GENERAR ARCHIVOS (supr)";
            this.btnGenerarArchivos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGenerarArchivos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGenerarArchivos.UseVisualStyleBackColor = true;
            this.btnGenerarArchivos.Click += new System.EventHandler(this.btnGenerarArchivos_Click);
            // 
            // dtpHasta
            // 
            this.dtpHasta.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(291, 42);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(132, 27);
            this.dtpHasta.TabIndex = 3;
            this.dtpHasta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpHasta_KeyDown);
            // 
            // dtpDesde
            // 
            this.dtpDesde.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDesde.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(77, 42);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(132, 27);
            this.dtpDesde.TabIndex = 2;
            this.dtpDesde.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpDesde_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "DESDE:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(227, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "HASTA:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvVentaSinRepositorio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 120);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1062, 398);
            this.panel1.TabIndex = 3;
            // 
            // frmRegeneracionArchivos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 518);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmRegeneracionArchivos";
            this.Text = "Generación de PDF y XML";
            this.Load += new System.EventHandler(this.frmRegeneracionArchivos_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmRegeneracionArchivos_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentaSinRepositorio)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipoDoc)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvVentaSinRepositorio;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtpHasta;
		private System.Windows.Forms.DateTimePicker dtpDesde;
		private System.Windows.Forms.Button btnGenerarArchivos;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBuscarDocumentos;
        private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn fecha;
        private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn tipo_documento;
        private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn numdoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn numerocliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn codFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn codcliente;
        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private Telerik.WinControls.UI.RadDropDownList cmbTipoDoc;
        private System.Windows.Forms.Label label6;
    }
}