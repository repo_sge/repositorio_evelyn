using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Entidades;
using SIGEFA.Administradores;
using SIGEFA.Reportes;
using SIGEFA.Reportes.clsReportes;

namespace SIGEFA.Formularios
{
    public partial class frmVentasAr : DevComponents.DotNetBar.OfficeForm
    {
        public Int32 Proceso = 0, Procede = 0;
        public Int32 CodTransaccion;
        public String CodPago;

        clsConsultasExternas ext = new clsConsultasExternas();
        clsReporteFlujoCaja dsf = new clsReporteFlujoCaja();
        clsReporteFactura ds = new clsReporteFactura();
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();

        clsFacturaVenta venta = new clsFacturaVenta();
        clsFacturaVenta factura = new clsFacturaVenta();

        clsAdmSerie Admser = new clsAdmSerie();
        clsSerie ser = new clsSerie();

        clsAdmTipoDocumento Admdoc = new clsAdmTipoDocumento();
        clsTipoDocumento doc = new clsTipoDocumento();

        clsTransaccion tran = new clsTransaccion();
        clsAdmTransaccion AdmTran = new clsAdmTransaccion();

        clsAdmTipoCambio AdmTc = new clsAdmTipoCambio();
        clsTipoCambio tc = new clsTipoCambio();

        clsFormaPago fpago = new clsFormaPago();
        clsListaPrecio Listap = new clsListaPrecio();
        clsAdmVendedor AdmVen = new clsAdmVendedor();

        clsMoneda moneda = new clsMoneda();
        clsAdmMoneda AdmMon = new clsAdmMoneda();

        clsAdmCliente AdmCli = new clsAdmCliente();
        clsCliente cli = new clsCliente();

        clsAdmPago AdmPagos = new clsAdmPago();
        clsValidar ok = new clsValidar();

        clsAdmFormaPago AdmPago = new clsAdmFormaPago();
        clsFormaPago forma = new clsFormaPago();
        clsAdmAlmacen Admalmac = new clsAdmAlmacen();
        clsPago Pag = new clsPago();
       
       
        public Int32 CodCliente;
        public Int32 CodSerie, CodSerieG = 0, numG = 0, manual = 0;
        public String numSerie;       
        public Int32 CodDocumento;
        Decimal TipoCambio = 0, ret = 0;
        public Int32 CodVendedor;
        private String CodVenta;

        public List<Int32> codsalida = new List<Int32>();
        private List<Int32> correlativo = new List<Int32>();
        private List<clsFacturaVenta> ltaventa = new List<clsFacturaVenta>();  
        public List<clsDetalleNotaSalida> detalle = new List<clsDetalleNotaSalida>();
        public List<clsDetalleFacturaVenta> detalle1 = new List<clsDetalleFacturaVenta>();
        public List<clsDetalleGuiaRemision> detalleg = new List<clsDetalleGuiaRemision>();

        public Int32 CodigoCaja = 0;
        clsCaja Caja = new clsCaja();
        clsAdmAperturaCierre AdmCaja = new clsAdmAperturaCierre();

        public frmVentasAr()
        {
            InitializeComponent();
        }

        private void frmVentasAr_Load(object sender, EventArgs e)
        {
            iniciaformulario();
            cargaAlmacenes();
        }

        private void cargaAlmacenes()
        {
            cmbAlmacen.DataSource = Admalmac.CargaAlmacen2(frmLogin.iCodEmpresa);
            cmbAlmacen.DisplayMember = "nombre";
            cmbAlmacen.ValueMember = "codAlmacen";
            cmbAlmacen.SelectedValue = frmLogin.iCodAlmacen; ;
        }

        private void iniciaformulario()
        {
            CargaMoneda();
            dtpFecha.MaxDate = DateTime.Today.Date;
            tc = AdmTc.CargaTipoCambio(dtpFecha.Value.Date, 2);
            CargaVendedores();
            CargaFormaPagos();
            btnNuevo.Focus();
        }

        private void CargaFormaPagos()
        {
            cmbFormaPago.DataSource = AdmPago.CargaFormaPagos(1);
            cmbFormaPago.DisplayMember = "descripcion";
            cmbFormaPago.ValueMember = "codFormaPago";
            //Se agrego estas lineas
            cmbFormaPago.SelectedIndex = 0;
            cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null);
        }

        private void CargaVendedores()
        {
            cbovendedor.DataSource = AdmVen.MuestraVendedoresDestaque();
            cbovendedor.DisplayMember = "apellido";
            cbovendedor.ValueMember = "codVendedor";
            cbovendedor.SelectedIndex = 0;
        }

        private void CargaMoneda()
        {
            cmbMoneda.DataSource = AdmMon.CargaMonedasHabiles();
            cmbMoneda.DisplayMember = "descripcion";
            cmbMoneda.ValueMember = "codMoneda";
            cmbMoneda.SelectedIndex = 0;
        }

        private void txtTransaccion_KeyDown(object sender, KeyEventArgs e)
        {
            if (!txtTransaccion.ReadOnly)
            {
                if (e.KeyCode == Keys.F1)
                {
                    if (Application.OpenForms["frmTransacciones"] != null)
                    {
                        Application.OpenForms["frmTransacciones"].Activate();
                    }
                    else
                    {
                        frmTransacciones form = new frmTransacciones();
                        form.Proceso = 4;
                        form.ShowDialog();
                        if (CodTransaccion != 0)
                        {
                            CargaTransaccion();
                            ProcessTabKey(true);
                        }
                    }

                }
            }
        }

        private void CargaTransaccion()
        {
            tran = AdmTran.MuestraTransaccion(CodTransaccion);
            tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
            txtTransaccion.Text = tran.Sigla;
            lbNombreTransaccion.Text = tran.Descripcion;
            lbNombreTransaccion.Visible = true;
            foreach (Control t in groupBox1.Controls)
            {
                if (t.Tag != null)
                {
                    if (t.Tag != "")
                    {
                        Int32 con = Convert.ToInt32(t.Tag);
                        if (tran.Configuracion.Contains(con))
                        {
                            t.Visible = true;
                        }
                        else
                        {
                            t.Visible = false;
                        }
                    }
                }
            }
        }

        private void frmVentasAr_Shown(object sender, EventArgs e)
        {
            txtTransaccion.Focus();
            txtTransaccion.Text = "FT";
            KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
            txtTransaccion_KeyPress(txtTransaccion, ee);
            cmbAlmacen.Visible = true;
            btnNuevo.Focus();
            if (Proceso == 1)
            {
                if (txtTipoCambio.Visible)
                {
                    if (tc == null)
                    {
                        MessageBox.Show("Debe registrar el tipo de cambio del d�a", "Tipo de Cambio", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        txtTipoCambio.Text = tc.Compra.ToString();                        
                    }
                }
            }
        }

        private void txtTransaccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtTransaccion.Text != "")
                {
                    if (BuscaTransaccion())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de transacci�n no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private bool BuscaTransaccion()
        {
            tran = AdmTran.MuestraTransaccionS(txtTransaccion.Text, 1);
            if (tran != null)
            {
                CodTransaccion = tran.CodTransaccion;
                tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
                txtTransaccion.Text = tran.Sigla;
                lbNombreTransaccion.Text = tran.Descripcion;
                lbNombreTransaccion.Visible = true;
                foreach (Control t in groupBox1.Controls)
                {
                    if (t.Tag != null && t.Tag != "")
                    {
                        Int32 con = Convert.ToInt32(t.Tag);
                        if (tran.Configuracion.Contains(con))
                        {
                            t.Visible = true;
                        }
                        else
                        {
                            t.Visible = false;
                        }
                    }
                }
                return true;
            }
            else
            {
                lbNombreTransaccion.Text = "";
                lbNombreTransaccion.Visible = false;
                foreach (Control t in groupBox1.Controls)
                {
                    if (t.Tag != null)
                    {
                        t.Visible = false;
                    }
                }
                return false;
            }
        }

        private void txtTransaccion_Leave(object sender, EventArgs e)
        {
            if (CodTransaccion == 0)
            {
                txtTransaccion.Focus();
            }
        }

        private void txtDocRef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmDocumentos"] != null)
                {
                    Application.OpenForms["frmDocumentos"].Activate();
                }
                else
                {
                    if (cli.Ruc != "")
                    {
                        frmDocumentos form = new frmDocumentos();
                        form.Proceso = 3;
                        form.ShowDialog();
                        doc = form.doc;
                        CodDocumento = doc.CodTipoDocumento;
                        txtCodDocumento.Text = CodDocumento.ToString();
                        txtDocRef.Text = doc.Sigla;
                        if (CodDocumento != 0) { ProcessTabKey(true); }
                    }
                }
            }
        }

        private void txtDocRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtDocRef.Text != "")
                {
                    if (BuscaTipoDocumento())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de Documento no existe, Presione F1 para consultar la tabla de ayuda", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private bool BuscaTipoDocumento()
        {
            doc = Admdoc.BuscaTipoDocumento(txtDocRef.Text);
            if (doc != null)
            {
                CodDocumento = doc.CodTipoDocumento;
                txtCodDocumento.Text = CodDocumento.ToString();
                return true;
            }
            else
            {
                CodDocumento = 0;
                txtCodDocumento.Text = CodDocumento.ToString();
                return false;
            }
        }

        private void txtSerie_Leave(object sender, EventArgs e)
        {
            if (BuscaSerie2())
            {
                txtSerie.Text = ser.Serie.ToString();
                if (ser.PreImpreso)
                {
                    txtNumero.Visible = true;
                    txtNumero.Text = "";
                    
                    txtNumero.Focus();
                }
                else
                {
                    txtNumero.Text = "";
                    txtNumero.Visible = false;
                    txtNumero.Text = ser.Numeracion.ToString();
                }
            }
        }

        private Boolean BuscaSerie2()
        {
            ser = Admser.MuestraSerie(CodSerie, frmLogin.iCodAlmacen);

            if (ser != null)
            {
                CodSerie = ser.CodSerie;
                return true;
            }
            else
            {
                CodSerie = 0;
                return false;
            }
        }

        private Boolean BuscaSerie3(int codDocumento)
        {
            ser = Admser.MuestraSerie(codDocumento, frmLogin.iCodAlmacen);

            if (ser != null)
            {
                CodSerie = ser.CodSerie;
                return true;
            }
            else
            {
                CodSerie = 0;
                return false;
            }
        }

        private void txtDocRef_Leave(object sender, EventArgs e)
        {
            BuscaTipoDocumento();
        }

        private void txtSerie_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.enteros(e);
            if (e.KeyChar == (char)Keys.Return)
            {
                //if (txtSerie.Text != "")
                //{
                if (BuscaSerie())
                {
                    txtSerie.Text = ser.Serie.ToString();
                    if (ser.PreImpreso)
                    {
                        txtNumero.Visible = true;
                        txtNumero.Enabled = false;
                        
                        txtNumero.Focus();
                        txtNumero.Text = "";
                    }
                    else
                    {
                        txtNumero.Text = "";
                        //txtNumero.Enabled = true;
                        txtNumero.Enabled = false;
                        //txtNumero.Visible = false;
                        txtNumero.Text = ser.Numeracion.ToString();
                    }

                    ProcessTabKey(true);
                    cmbFormaPago.Focus();
                }

            }
            if (e.KeyChar == (char)Keys.Enter)
            {
                cmbFormaPago.Focus();
            }
        }

        private Boolean BuscaSerie()
        {
            //ser = Admser.BuscaSerie(txtSerie.Text,CodDocumento,frmLogin.iCodAlmacen);
            ser = Admser.BuscaSeriexDocumento(CodDocumento, frmLogin.iCodAlmacen);
            if (ser != null)
            {
                CodSerie = ser.CodSerie;
                return true;
            }
            else
            {
                CodSerie = 0;
                return false;
            }

        }

        private void txtSerie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmSerie"] != null)
                {
                    Application.OpenForms["frmSerie"].Activate();
                }
                else
                {
                    frmSerie form = new frmSerie();
                    form.Proceso = 3;
                    form.DocSeleccionado = CodDocumento;
                    form.ShowDialog();
                    ser = form.ser;
                    CodSerie = ser.CodSerie;
                    manual = Convert.ToInt32(ser.PreImpreso);
                    if (CodSerie != 0)
                    {
                        txtSerie.Text = ser.Serie;
                        //if (Procede != 4) txtNumero.Text = ser.Numeracion.ToString();
                        //else txtNumero.Text = numSerie;
                    }
                    if (CodSerie != 0) { ProcessTabKey(true); }
                }
            }
        }

        private void cmbFormaPago_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {                
                fpago = AdmPago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
                dtpFechaPago.Value = dtpFecha.Value.AddDays(fpago.Dias); 
                /*if (fpago.Dias > forma.Dias)
                {
                    DialogResult result =
                        MessageBox.Show("Esta forma de pago excede a la Forma de Pago del Cliente" + Environment.NewLine +
                                        "M�x.FormaPago del Cliente = " + forma.Descripcion, "Facturaci�n Venta",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (result == DialogResult.OK)
                    {
                        cmbFormaPago.SelectedValue = forma.CodFormaPago;
                        //cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, eeee);
                    }
                }*/
            }
            catch (Exception ex)
            {
            }
        }

        private void txtCodCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (Application.OpenForms["frmClientesLista"] != null)
                {
                    Application.OpenForms["frmClientesLista"].Activate();
                }
                else
                {
                    frmClientesLista form = new frmClientesLista();
                    form.Proceso = 3;
                    //form.Tipo = cmbTipoCodigo.SelectedIndex;
                    form.ShowDialog();
                    cli = form.cli;
                    CodCliente = cli.CodCliente;
                    if (CodCliente != 0)
                    {
                        CargaCliente();
                        btnNuevo.Enabled = true; ProcessTabKey(true);
                        txtDocRef.Focus();
                    }
                }
            }
        }

        private void CargaCliente()
        {
            cli = AdmCli.MuestraCliente(CodCliente);
            cli = AdmCli.CargaDeuda(cli);
            if (cli.Cantidad > 0)
            {
                DialogResult dlgResult = MessageBox.Show("El cliente selecionado presenta" + Environment.NewLine + "Facturas pendientes = " + cli.Cantidad + Environment.NewLine + "Deuda Total = " + cli.Deuda + " soles" + Environment.NewLine + "Linea de cr�dito = " + cli.LineaCredito + Environment.NewLine + " Desea continuar con la venta?", "Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    ret = 1;
                    //txtCotizacion.Text = "";
                    return;
                }
                else
                {
                    cargadatoscliente();
                    ret = 0;
                }
            }
            else
            {
                cargadatoscliente();
                ret = 0;
            }
        }

        private void cargadatoscliente()
        {
            txtCodCliente.Text = cli.Dni;
            if (cli.Ruc != "" && cli.Dni == "")
            {
                txtDocRef.Text = "FT";
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtDocRef_KeyPress(txtDocRef, ee);
               
                txtSerie_KeyPress(txtDocRef, ee);
                txtCodCliente.Text = cli.Ruc;
            }
            else if (cli.Dni != "" && cli.Ruc == "")
            {

                txtDocRef.Text = "BV";
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtDocRef_KeyPress(txtDocRef, ee);
              
                txtSerie_KeyPress(txtDocRef, ee);
                txtCodigoCli.Text = cli.Dni;
            }
            else if (cli.Ruc != "" && cli.Dni != "")
            {
                if (MessageBox.Show("Si para FT(Factura) o No para BV(Boleta)", "Seleccione Tipo de Doc. Ref.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    txtDocRef.Text = "BV";
                    KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                    txtDocRef_KeyPress(txtDocRef, ee);

                    txtSerie_KeyPress(txtDocRef, ee);
                    txtCodigoCli.Text = cli.Dni;
                }
                else
                {
                    txtDocRef.Text = "FT";
                    KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                    txtDocRef_KeyPress(txtDocRef, ee);
                    
                    txtSerie_KeyPress(txtDocRef, ee);
                    txtCodCliente.Text = cli.Ruc;
                }
            }
            txtNombreCliente.Text = cli.RazonSocial;
            txtDireccionCliente.Text = cli.DireccionLegal;
            txtCodigoCli.Text = cli.CodCliente.ToString();
            

            //cmbFormaPago.SelectedValue = cli.FormaPago;
            //Se cambio por la linea de arriba
            cmbFormaPago.SelectedIndex = 0;
            //Fin se cambio por la linea de arriba
            forma = AdmPago.BuscaFormaPagoVenta(cli.FormaPago);
            //if (cli.CodListaPrecio != null)
            //{
            //    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, null);
            //    txtplazo.Text = cmbFormaPago.Text;
            //    cbListaPrecios.SelectedValue = cli.CodListaPrecio;
            //}
            //cmbFormaPago.SelectedIndex = 0;
            if (cli.FormaPago != 0)
            {
                EventArgs ee = new EventArgs();
                cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);

            }
            else
            {
                dtpFechaPago.Value = DateTime.Today;
            }
            if (cli.CodVendedor != 0)
            {

                cbovendedor.SelectedValue = cli.CodVendedor;
               
            }
            
            cmbMoneda.SelectedValue = cli.Moneda;
            
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            if (txtDetalle.Text == "")
            {
                RecorreDetalle();
                if (Application.OpenForms["frmDetalleSalida"] != null)
                {
                    Application.OpenForms["frmDetalleSalida"].Activate();
                }
                else
                {
                    frmDetalleSalida form = new frmDetalleSalida();
                    form.Procede = 2;
                    form.Proceso = 1;
                    form.consultorext = checkBox1.Checked;
                    if (checkBox1.Checked == true)
                    {
                        form.CodVendedor = CodVendedor;
                        form.Procede = 42;
                        form.Proceso = 1;
                        form.consultorext = checkBox1.Checked;
                    }
                    form.Tipo = 2;
                    form.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                    //form.Codlista = Convert.ToInt32(cbListaPrecios.SelectedValue);
                    form.tc = tc.Compra;
                    form.productoscargados = detalle1;
                    form.alma = Convert.ToInt32(cmbAlmacen.SelectedValue);
                    form.ShowDialog();

                    //dgvDetalle.Rows.Add("", form.detalle.CodProducto, form.detalle.Referencia, form.detalle.Descripcion, form.detalle.CodUnidad
                    //    , form.detalle.Unidad, form.detalle.SerieLote, form.detalle.Cantidad, form.detalle.PrecioUnitario, form.detalle.Importe
                    //    , form.detalle.Descuento1, form.detalle.Descuento2, form.detalle.Descuento3, form.detalle.MontoDescuento, form.detalle.ValorVenta
                    //    , form.detalle.Igv, form.detalle.PrecioVenta, form.detalle.PrecioReal, form.detalle.ValoReal);            
                }
            }
            else { MessageBox.Show("No Puede Seguir Agregando m�s Detalles", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }

        private void RecorreDetalle()
        {
            detalle.Clear();
            detalle1.Clear();
            if (dgvDetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    a�adedetalle(row);
                }
            }
        }

        private void a�adedetalle(DataGridViewRow fila)
        {
            clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodVenta = Convert.ToInt32(venta.CodFacturaVenta);
            deta.CodAlmacen = Convert.ToInt32(cmbAlmacen.SelectedValue);
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.SerieLote = fila.Cells[serielote.Name].Value.ToString();
            deta.Cantidad = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            deta.PrecioUnitario = Convert.ToDecimal(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDecimal(fila.Cells[importe.Name].Value);
            //deta.Subtotal = Convert.ToDecimal(fila.Cells[importe.Name].Value);
            //deta.Subtotal = Convert.ToDecimal(fila.Cells[valorventa.Name].Value);
            deta.Descuento1 = Convert.ToDecimal(fila.Cells[dscto1.Name].Value);
            deta.MontoDescuento = Convert.ToDecimal(fila.Cells[montodscto.Name].Value);
            deta.Igv = Convert.ToDecimal(fila.Cells[igv.Name].Value);
            //  deta.Importe = Convert.ToDecimal(fila.Cells[precioventa.Name].Value);
            deta.Importe = Convert.ToDecimal(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDecimal(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDecimal(fila.Cells[valoreal.Name].Value);
            deta.CodUser = frmLogin.iCodUser;
            deta.CantidadPendiente = Convert.ToDecimal(fila.Cells[cantidad.Name].Value);
            deta.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
            deta.CodDetallePedido = 0;            
            detalle1.Add(deta);
        }

        private void cmbAlmacen_SelectionChangeCommitted(object sender, EventArgs e)
        {
            cmbAlmacen.Enabled = false;
            btnNuevo.Focus();
        }

        public void calculatotales()
        {
            if (Proceso != 0)
            {
                if (Procede != 3)
                {
                    Decimal bruto = 0;
                    Decimal descuen = 0;
                    Decimal valor = 0;
                    Decimal preciovent = 0;
                    Decimal igvt = 0;

                    foreach (DataGridViewRow row in dgvDetalle.Rows)
                    {
                        bruto = bruto + Convert.ToDecimal(row.Cells[importe.Name].Value);
                        descuen = descuen + Convert.ToDecimal(row.Cells[montodscto.Name].Value);
                        valor = valor + Convert.ToDecimal(row.Cells[valorventa.Name].Value);
                        preciovent = preciovent + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                        igvt = igvt + Convert.ToDecimal(row.Cells[igv.Name].Value);
                    }
                    txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
                    txtDscto.Text = String.Format("{0:#,##0.00}", descuen);
                    txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);

                    //txtIGV.Text = String.Format("{0:#,##0.00}", bruto - descuen - valor);
                    txtIGV.Text = String.Format("{0:#,##0.00}", igvt);
                    //txtPrecioVenta.Text = String.Format("{0:#,##0.00}", bruto);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", preciovent);
                }
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvDetalle.Rows.Count > 0 & dgvDetalle.SelectedRows.Count > 0)
            {
                pbFoto.Image = null;
                dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow);
                calculatotales();
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvDetalle.Rows.Count > 0 & dgvDetalle.SelectedRows.Count > 0)
            {
                DataGridViewRow row = dgvDetalle.SelectedRows[0];
                if (Application.OpenForms["frmDetalleSalida"] != null)
                {
                    Application.OpenForms["frmDetalleSalida"].Activate();
                }
                else
                {
                    frmDetalleSalida form = new frmDetalleSalida();
                    form.Proceso = 2;
                    form.Procede = 2;
                    form.alma = Convert.ToInt32(cmbAlmacen.SelectedValue);
                    form.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                    form.tc = Convert.ToDecimal(txtTipoCambio.Text);
                    //form.Codlista = Convert.ToInt32(cbListaPrecios.SelectedValue);
                    form.txtCodigo.Text = row.Cells[codproducto.Name].Value.ToString();
                    form.txtReferencia.Text = row.Cells[referencia.Name].Value.ToString();
                    form.BuscaProducto();
                    form.txtControlStock.Text = row.Cells[serielote.Name].Value.ToString();
                    form.txtCantidad.Text = String.Format("{0:#,##0.00}", row.Cells[cantidad.Name].Value);
                    form.txtPrecio.Text = String.Format("{0:#,##0.00}", row.Cells[preciounit.Name].Value);
                    form.txtDscto1.Text = String.Format("{0:#,##0.00}", row.Cells[dscto1.Name].Value);
                    form.txtPrecioNeto.Text = String.Format("{0:#,##0.00}", row.Cells[importe.Name].Value);
                    form.ShowDialog();
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Decimal totalsoles = 0;

                if (superValidator1.Validate())
                {
                    if (Convert.ToInt32(cli.Moneda) != Convert.ToInt32(cmbMoneda.SelectedValue))
                    {
                        if (Convert.ToInt32(cli.Moneda) == 2 || Convert.ToInt32(cmbMoneda.SelectedValue) == 1)
                            totalsoles = Convert.ToDecimal(txtPrecioVenta.Text) / Convert.ToDecimal(txtTipoCambio.Text);
                        else if (Convert.ToInt32(cli.Moneda) == 1 || Convert.ToInt32(cmbMoneda.SelectedValue) == 2)
                            totalsoles = Convert.ToDecimal(txtPrecioVenta.Text) * Convert.ToDecimal(txtTipoCambio.Text);

                    }
                    else
                    {
                        totalsoles = Convert.ToDecimal(txtPrecioVenta.Text);
                    }

                    if (Proceso != 0)
                    {
                        venta.CodSucursal = frmLogin.iCodSucursal;
                        venta.CodAlmacen = Convert.ToInt32(cmbAlmacen.SelectedValue);
                        venta.CodTipoTransaccion = tran.CodTransaccion;
                        venta.CodCliente = Convert.ToInt32(txtCodigoCli.Text);
                        venta.CodTipoDocumento = doc.CodTipoDocumento;
                        venta.CodSerie = CodSerie;
                        /* VALIDO QUE LA VENTA SEA AL CONTADO PARA NO ENVIAR SERIE NI CORRELATIVO, TIPO DOC SI ENVIO  */
                        /* JC Ato if (Convert.ToInt32(cmbFormaPago.SelectedValue) == 6)
                        {
                            if (manual == 0)
                            {
                                venta.Serie = "";
                                venta.NumDoc = "";
                            }
                            else
                            {
                                venta.Serie = txtSerie.Text;
                                venta.NumDoc = txtNumero.Text;
                            }
                        }
                        else
                        {*/
                        venta.Serie = txtSerie.Text;
                        venta.NumDoc = txtNumero.Text;
                        /*}

                        /* VALIDO QUE LA VENTA SEA AL CONTADO PARA NO ENVIAR SERIE NI CORRELATIVO, TIPO DOC SI ENVIO  */
                        venta.Moneda = Convert.ToInt32(cmbMoneda.SelectedValue);
                        if (txtTipoCambio.Visible)
                        {
                            venta.TipoCambio = Convert.ToDecimal(txtTipoCambio.Text);
                        }
                        venta.FechaSalida = dtpFecha.Value;
                        venta.FechaPago = dtpFechaPago.Value;
                        venta.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                        //venta.CodListaPrecio = Convert.ToInt32(cbListaPrecios.SelectedValue);
                        venta.CodVendedor = Convert.ToInt32(cbovendedor.SelectedValue);
                        venta.Comentario = txtComentario.Text;
                        venta.MontoBruto = Convert.ToDecimal(txtBruto.Text);
                        //venta.MontoBruto = Convert.ToDecimal(txtValorVenta.Text);
                        venta.MontoDscto = Convert.ToDecimal(txtDscto.Text);
                        venta.Igv = Convert.ToDecimal(txtIGV.Text);
                        venta.Total = Convert.ToDecimal(txtPrecioVenta.Text);
                        venta.CodUser = frmLogin.iCodUser;
                        txtDetalle.Text = txtDetalle.Text.Replace(" - ", "");
                        txtDetalle.Text = txtDetalle.Text.Replace("\r\n", "\r\n - ");
                        if (txtDetalle.Text != "") txtDetalle.Text = " - " + txtDetalle.Text;
                        venta.Detallecomentario = txtDetalle.Text;
                        //if (txtCotizacion.Text == "" || txtCotizacion.Text == ".") { venta.CodCotizacion = 0; } else { venta.CodCotizacion = Convert.ToInt32(txtCotizacion.Text); }
                        venta.Estado = 1;
                        venta.Consultorext = checkBox1.Checked;
                        /*venta.Codsalidaconsulext = CodSalConsulExt;
                        venta.CodPedido = Convert.ToInt32(pedido.CodPedido);
                        */
                        factura = AdmVenta.FechaCorrelativoAnterior(venta.CodSerie);

                        if (Proceso == 1)
                        {
                            if (factura.FechaSalida > venta.FechaSalida.Date)
                            {
                                MessageBox.Show("Error No se puede Registrar los Datos. Verifique Fecha");
                            }
                            else
                            {
                                if (VerificarDetracciones())
                                {
                                    if (AdmVenta.insert(venta))
                                    {
                                        RecorreDetalle();
                                        if (detalle1.Count > 0)
                                        {
                                            foreach (clsDetalleFacturaVenta det in detalle1)
                                            {
                                                AdmVenta.insertdetalle(det);
                                                if (det.CodDetalleVenta == 0)
                                                {
                                                    MessageBox.Show("Error No se puede Registrar los Datos. Falta Stock de Productos");
                                                    AdmVenta.rollback(Convert.ToInt32(venta.CodFacturaVenta), 0);
                                                    //break;
                                                    return;
                                                }

                                            }

                                            btnImprimir.Visible = true;
                                            btnGuardar.Enabled = false;
                                        }


                                        MessageBox.Show("Los datos se guardaron correctamente", "Venta",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        txtNumDoc.Text = venta.CodFacturaVenta.PadLeft(11, '0');
                                        ltaventa.Add(venta);
                                        if (fpago.Dias == 0 && venta.CodTipoTransaccion == 7)
                                        //se comprueba que el pago sea al contado y que la trnasaccion sea ingreso por compra
                                        {
                                            ingresarpago();

                                        }
                                        CodVenta = venta.CodFacturaVenta;
                                        Proceso = 0;
                                        if (venta.FormaPago != 6)
                                        {
                                            btnImprimir.Visible = true;
                                            xgenerar = true;
                                            //fnImprimir();
                                            this.Close();
                                        }
                                        //else 
                                        //    btnImprimir.Visible = false;
                                        //CargaVenta();
                                        //sololectura(true);
                                    }
                                }
                            }


                        }
                        /*else if (Proceso == 2)
                        {
                            if (VerificarDetracciones())
                            {
                                if (AdmVenta.update(venta))
                                {
                                    RecorreDetalle();
                                    foreach (clsDetalleFacturaVenta det in venta.Detalle)
                                    {
                                        foreach (clsDetalleFacturaVenta det1 in detalle1)
                                        {
                                            if (det.Equals(det1))
                                            {
                                                AdmVenta.updatedetalle(det1);
                                                return;
                                            }
                                        }
                                        //AdmVenta.deletedetalle(det.CodDetalleSalida);
                                    }
                                    foreach (clsDetalleFacturaVenta deta in detalle1)
                                    {
                                        if (deta.CodDetalleVenta == 0)
                                        {
                                            AdmVenta.insertdetalle(deta);
                                        }
                                    }

                                    MessageBox.Show("Los datos se actualizaron correctamente", "Venta",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    this.Close();
                                }
                            }
                        }*/

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool VerificarDetracciones()
        {
            Boolean grav = false;
            Decimal sumadet = 0;
            if (CodDocumento == 2)
            {
                if (dgvDetalle.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in dgvDetalle.Rows)
                    {
                        if (Convert.ToDecimal(row.Cells[igv.Name].Value) != 0)
                        {
                            grav = true;
                        }
                        else
                        {
                            if (Convert.ToDecimal(row.Cells[precioventa.Name].Value) == 0)
                            {
                                grav = true;
                            }
                            else
                            {
                                sumadet = sumadet + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                            }
                        }
                    }
                }
                if (sumadet <= 700)
                {
                    return true;
                }
                else
                {
                    if (grav)
                    {
                        MessageBox.Show("Operacion no permitida, por estar afecta a detracci�n!", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
        }
        public Boolean xgenerar = false;

        private void VerificaSaldoCaja()
        {
            Caja = AdmCaja.ValidarAperturaDia(frmLogin.iCodSucursal, DateTime.Now.Date, 1, frmLogin.iCodAlmacen);//1 caja ventas
            CodigoCaja = Caja.Codcaja;
        }

       

        private void ingresarpago()
        {
            //Se quito para enviar el pago directo cuando es contado por defecto efectivo
            //frmCancelarPago form = new frmCancelarPago();
            //form.CodNota = venta.CodFacturaVenta;
            //form.tipo = 3;
            //form.tip = 3;
            //form.Monto = venta.Total;
            //form.venta = venta;
            //form.montoPag = 0;
            //form.ShowDialog();
            //FIn se quito para enviar el pago directo cuando es contado por defecto efectivo
            VerificaSaldoCaja();

            Pag.CodNota = venta.CodFacturaVenta.ToString();
            Pag.CodLetra = 0;
            Pag.CodTipoPago = 5; //metodo de pago
            Pag.CodMoneda = venta.Moneda;
            //Pag.CodCobrador = Convert.ToInt32(cbovendedor.SelectedValue); //Cobrador
            Pag.CodCobrador = Convert.ToInt32(frmLogin.iCodUser);
            Pag.Tipo = true;// total o parcial
            Pag.IngresoEgreso = true;//ingreso
            Pag.TipoCambio = Convert.ToDecimal(venta.TipoCambio);
            Pag.MontoPagado = Convert.ToDecimal(venta.MontoBruto);
            Pag.MontoCobrado = Convert.ToDecimal(venta.MontoBruto);
            Pag.Vuelto = 0;
            Pag.codCtaCte = 0;
            Pag.CtaCte = "";
            Pag.NOperacion = "";
            Pag.NCheque = "";
            Pag.FechaPago = venta.FechaRegistro.Date;
            Pag.Observacion = "";
            Pag.CodUser = frmLogin.iCodUser;
            Pag.CodAlmacen = frmLogin.iCodAlmacen;
            //Pag.CodSerie = CodSerie;
            Pag.CodSucursal = frmLogin.iCodSucursal;
            Pag.CodDoc = 18;
            Pag.Serie = txtSerie.Text;
            Pag.NumDoc = txtNumero.Text;
            Pag.Referencia = txtDocRef.Text;
            Pag.Codcaja = CodigoCaja;
            Pag.CodBanco = 0;
            Pag.CodTarjeta = 0;
            Pag.Aprobado = 4;
            //montoPag = 1;
            if (AdmPagos.insert(Pag))
            {
                CodPago = Pag.CodPago.ToString();
                xgenerar = true;
                //fnImprimir();
                //Reiniciar Formulario
                nuevaVenta();
                this.Close();
            }
        }

        private void nuevaVenta()
        {
            throw new NotImplementedException();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                fnImprimir();
            }
            catch (Exception ex)
            {
            }
        }

        public void fnImprimir()
        {
            Boolean rpta;
            try
            {
                if (AdmVenta.chekeaImpresion(Convert.ToInt32(venta.CodFacturaVenta)) == 0)
                {
                    if (xgenerar && manual == 0)
                    {
                        if (BuscaSerie())
                        {
                            if (BuscaSerie2())
                            {
                                txtSerie.Text = ser.Serie.ToString();
                                if (ser.PreImpreso)
                                {
                                    txtNumero.Visible = true;
                                    txtNumero.Text = "";
                                    //ckbguia.Visible = false;
                                    txtNumero.Focus();
                                }
                                else
                                {
                                    txtNumero.Text = "";
                                    txtNumero.Visible = true;
                                    txtNumero.Text = ser.Numeracion.ToString();
                                }
                                //if (ActualizaCobro(CodPago))
                                //{
                                //printaRecibo(CodPago);
                                if (venta.FormaPago != 6)
                                {
                                    if (ser.CodDocumento == 1)//Es Boleta
                                    {
                                        printaBoleta();
                                    }
                                    else//Es Factura
                                    {
                                        printaFactura();
                                    }
                                }
                                else// if (ActualizaCorrelativos(CodSerie, txtSerie.Text, txtNumero.Text, venta.CodFacturaVenta))
                                {
                                    if (ser.CodDocumento == 1)//Es Boleta
                                    {
                                        printaBoleta();
                                    }
                                    else//Es Factura
                                    {
                                        printaFactura();
                                    }
                                    if (CodPago != "")
                                    {
                                        printaRecibo(CodPago);

                                    }
                                }
                                

                            }

                        }
                    }
                    else
                    {
                        if (ser.CodDocumento == 1)//Es Boleta
                        {
                            printaBoleta();
                        }
                        else//Es Factura
                        {
                            printaFactura();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void printaRecibo(string CodPago)
        {
            try
            {
                /* MUESTRO LA FACTURA PARA IMPRIMIRLA */
                CRImpresionPago rpt = new CRImpresionPago();
                frmRptImpresionPago frm = new frmRptImpresionPago();
                CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                rptoption.PrinterName = ser.NombreImpresora;//Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]);
                rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza"); 
                rpt.SetDataSource(dsf.ReporteImpresionPago(Convert.ToInt32(CodPago), frmLogin.iCodAlmacen));
                frm.cRVImpresionPago.ReportSource = rpt;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private Boolean rpta;
        private void printaFactura()
        {
            try
            {
                if (frmLogin.iCodAlmacen == 20)
                {
                    ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rd = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                    CRFacturaFomatoContinuo rpt = new CRFacturaFomatoContinuo();
                    rd.Load("CRFacturaFomatoContinuo.rpt");
                    rd.SetDataSource(ds.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta)));
                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rd.PrintOptions;
                    rptoption.PrinterName = ser.NombreImpresora;
                    rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza");           

                    rd.PrintToPrinter(1, false, 1, 1);
                    if (AdmVenta.ActualizaEstadoImpreso(Convert.ToInt32(venta.CodFacturaVenta)))
                    {
                        rpta = true;
                    }
                    else
                    {
                        rpta = false;
                    }
                    rd.Close();
                    rd.Dispose();
                }
                else
                {
                    ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rd = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                    CRReporteFactura rpt = new CRReporteFactura();
                    rd.Load("CRReporteFactura.rpt");
                    rd.SetDataSource(ds.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta)));
                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rd.PrintOptions;
                    rptoption.PrinterName = ser.NombreImpresora;
                    rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza");           

                    rd.PrintToPrinter(1, false, 1, 1);
                    if (AdmVenta.ActualizaEstadoImpreso(Convert.ToInt32(venta.CodFacturaVenta)))
                    {
                        rpta = true;
                    }
                    else
                    {
                        rpta = false;
                    }
                    rd.Close();
                    rd.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void printaBoleta()
        {
            try
            {
                if (frmLogin.iCodAlmacen == 19 || frmLogin.iCodAlmacen == 20 || frmLogin.iCodAlmacen == 21)
                {
                    ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rd = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                    CRReporteBoleta rpt = new CRReporteBoleta();
                    rd.Load("CRReporteBoleta.rpt");
                    rd.SetDataSource(ds.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta)));
                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rd.PrintOptions;
                    rptoption.PrinterName = ser.NombreImpresora;
                    rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza");           

                    rd.PrintToPrinter(1, false, 1, 1);
                    if (AdmVenta.ActualizaEstadoImpreso(Convert.ToInt32(venta.CodFacturaVenta)))
                    {
                        rpta = true;
                    }
                    else
                    {
                        rpta = false;
                    }
                    rd.Close();
                    rd.Dispose();
                }
                else
                {
                    ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                    CrystalDecisions.CrystalReports.Engine.ReportDocument rd = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                    CRReporteBoletaFormatoAntiguo rpt = new CRReporteBoletaFormatoAntiguo();
                    rd.Load("CRReporteBoletaFormatoAntiguo.rpt");
                    rd.SetDataSource(ds.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta)));
                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rd.PrintOptions;
                    rptoption.PrinterName = ser.NombreImpresora;
                    rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);//(CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(Convert.ToString(System.Drawing.Printing.PrinterSettings.InstalledPrinters[3]), "documentoFioviza");           

                    rd.PrintToPrinter(1, false, 1, 1);
                    if (AdmVenta.ActualizaEstadoImpreso(Convert.ToInt32(venta.CodFacturaVenta)))
                    {
                        rpta = true;
                    }
                    else
                    {
                        rpta = false;
                    }
                    rd.Close();
                    rd.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private Boolean ActualizaCorrelativos(int CodSerie, string txtSeries, string txtNumeros, string p_3)
        {
            try
            {
                if (AdmVenta.actualizaFactura_venta(CodSerie, txtSeries, txtNumeros, CodVenta))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            
        }

        private void dgvDetalle_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {            
            Int32 codProducto = 0;
            if (dgvDetalle.Rows.Count >= 1 && e.Row.Selected)
            {
                codProducto = Convert.ToInt32(e.Row.Cells[codproducto.Name].Value.ToString());
            }
            CargaFotografia(codProducto);
        }

        private clsAdmFotografia admfotografia = new clsAdmFotografia();

        private void CargaFotografia(Int32 codProd)
        {
            clsEntFotografia entfoto = new clsEntFotografia();
            entfoto = admfotografia.CargaFotografia(codProd);
            if (entfoto != null)
            {
                pbFoto.Image = entfoto.Fotografia;
                pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;
                //btnGuardar.Visible = false;
            }
            //else {
            //    MessageBox.Show("No se pudo cargar la fotograf�a", "Gesti�n Relaci�n ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }


       
    }
}