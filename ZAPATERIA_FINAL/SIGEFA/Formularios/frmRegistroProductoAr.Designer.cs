namespace SIGEFA.Formularios
{
    partial class frmRegistroProductoAr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistroProductoAr));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnColors = new System.Windows.Forms.Button();
            this.btnColorp = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtPrecioCom = new System.Windows.Forms.TextBox();
            this.cmbTallasProducto = new System.Windows.Forms.ComboBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPrecioVen = new System.Windows.Forms.TextBox();
            this.grpCalzado = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.btnAddTallas = new System.Windows.Forms.Button();
            this.txtDetCodProd3 = new System.Windows.Forms.TextBox();
            this.txtDetCodProd2 = new System.Windows.Forms.TextBox();
            this.txtDetCodProd1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dgvTalla = new System.Windows.Forms.DataGridView();
            this.codtalla1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Talla1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbTipoTalla = new System.Windows.Forms.ComboBox();
            this.button9 = new System.Windows.Forms.Button();
            this.btnDeleteTalla = new System.Windows.Forms.Button();
            this.btnAddTalla = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.cbTaco = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbSeriePro = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbPrecioCosto = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAnnio = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDel_Fotografia = new System.Windows.Forms.Button();
            this.btnAdd_Fotografia = new System.Windows.Forms.Button();
            this.pbFoto = new System.Windows.Forms.PictureBox();
            this.cbTemporada = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.txtCodCompraProducto = new System.Windows.Forms.TextBox();
            this.cbTipoCierre = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cbUnidadBase = new System.Windows.Forms.ComboBox();
            this.linkConfiguraUnidadesEquivalentes = new System.Windows.Forms.LinkLabel();
            this.cbControlStock = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.btnUnidad = new System.Windows.Forms.Button();
            this.cbCategoria = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPrecioCata = new System.Windows.Forms.TextBox();
            this.txtPrecioCompra = new System.Windows.Forms.TextBox();
            this.cbIgv = new System.Windows.Forms.CheckBox();
            this.cbFamilia = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFamilia = new System.Windows.Forms.Button();
            this.cbColorSecundario = new System.Windows.Forms.ComboBox();
            this.cbColorPrimario = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cbCorte = new System.Windows.Forms.ComboBox();
            this.cbModelo = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnMarca = new System.Windows.Forms.Button();
            this.btnLinea = new System.Windows.Forms.Button();
            this.btnTipoArticulo = new System.Windows.Forms.Button();
            this.cbTipoArticulo = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cbMarca = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbLinea = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbEstado = new System.Windows.Forms.CheckBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodProducto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.requiredFieldValidator2 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.codtalla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.groupBox1.SuspendLayout();
            this.grpCalzado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTalla)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnColors);
            this.groupBox1.Controls.Add(this.btnColorp);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.txtPrecioCom);
            this.groupBox1.Controls.Add(this.cmbTallasProducto);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtPrecioVen);
            this.groupBox1.Controls.Add(this.grpCalzado);
            this.groupBox1.Controls.Add(this.lbPrecioCosto);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtAnnio);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.cbTemporada);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.txtCodCompraProducto);
            this.groupBox1.Controls.Add(this.cbTipoCierre);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.cbUnidadBase);
            this.groupBox1.Controls.Add(this.linkConfiguraUnidadesEquivalentes);
            this.groupBox1.Controls.Add(this.cbControlStock);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.btnUnidad);
            this.groupBox1.Controls.Add(this.cbCategoria);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtPrecioCata);
            this.groupBox1.Controls.Add(this.txtPrecioCompra);
            this.groupBox1.Controls.Add(this.cbIgv);
            this.groupBox1.Controls.Add(this.cbFamilia);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnFamilia);
            this.groupBox1.Controls.Add(this.cbColorSecundario);
            this.groupBox1.Controls.Add(this.cbColorPrimario);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.cbCorte);
            this.groupBox1.Controls.Add(this.cbModelo);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnMarca);
            this.groupBox1.Controls.Add(this.btnLinea);
            this.groupBox1.Controls.Add(this.btnTipoArticulo);
            this.groupBox1.Controls.Add(this.cbTipoArticulo);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.cbMarca);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbLinea);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cbEstado);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtReferencia);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCodProducto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(688, 521);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ingreso Productos";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnColors
            // 
            this.btnColors.Location = new System.Drawing.Point(100, 374);
            this.btnColors.Name = "btnColors";
            this.btnColors.Size = new System.Drawing.Size(23, 23);
            this.btnColors.TabIndex = 113;
            this.btnColors.Text = ">";
            this.btnColors.UseVisualStyleBackColor = true;
            this.btnColors.Click += new System.EventHandler(this.btnColors_Click);
            // 
            // btnColorp
            // 
            this.btnColorp.Location = new System.Drawing.Point(100, 345);
            this.btnColorp.Name = "btnColorp";
            this.btnColorp.Size = new System.Drawing.Size(23, 23);
            this.btnColorp.TabIndex = 112;
            this.btnColorp.Text = ">";
            this.btnColorp.UseVisualStyleBackColor = true;
            this.btnColorp.Click += new System.EventHandler(this.btnColorp_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCancelar.ImageIndex = 5;
            this.btnCancelar.ImageList = this.imageList1;
            this.btnCancelar.Location = new System.Drawing.Point(522, 470);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(64, 32);
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "Salir";
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // txtPrecioCom
            // 
            this.txtPrecioCom.BackColor = System.Drawing.Color.White;
            this.txtPrecioCom.ForeColor = System.Drawing.Color.Black;
            this.txtPrecioCom.Location = new System.Drawing.Point(462, 348);
            this.txtPrecioCom.Name = "txtPrecioCom";
            this.txtPrecioCom.Size = new System.Drawing.Size(132, 20);
            this.txtPrecioCom.TabIndex = 111;
            this.txtPrecioCom.Text = "0.00";
            this.txtPrecioCom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbTallasProducto
            // 
            this.cmbTallasProducto.FormattingEnabled = true;
            this.cmbTallasProducto.Location = new System.Drawing.Point(129, 289);
            this.cmbTallasProducto.Name = "cmbTallasProducto";
            this.cmbTallasProducto.Size = new System.Drawing.Size(147, 21);
            this.cmbTallasProducto.TabIndex = 94;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.ImageIndex = 4;
            this.btnGuardar.ImageList = this.imageList1;
            this.btnGuardar.Location = new System.Drawing.Point(434, 471);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(78, 32);
            this.btnGuardar.TabIndex = 3;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(339, 351);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 110;
            this.label16.Text = "Precio Compra con IGV";
            // 
            // txtPrecioVen
            // 
            this.txtPrecioVen.BackColor = System.Drawing.Color.White;
            this.txtPrecioVen.ForeColor = System.Drawing.Color.Black;
            this.txtPrecioVen.Location = new System.Drawing.Point(462, 372);
            this.txtPrecioVen.Name = "txtPrecioVen";
            this.txtPrecioVen.Size = new System.Drawing.Size(132, 20);
            this.txtPrecioVen.TabIndex = 109;
            this.txtPrecioVen.Text = "0.00";
            this.txtPrecioVen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpCalzado
            // 
            this.grpCalzado.Controls.Add(this.button5);
            this.grpCalzado.Controls.Add(this.btnAddTallas);
            this.grpCalzado.Controls.Add(this.txtDetCodProd3);
            this.grpCalzado.Controls.Add(this.txtDetCodProd2);
            this.grpCalzado.Controls.Add(this.txtDetCodProd1);
            this.grpCalzado.Controls.Add(this.label17);
            this.grpCalzado.Controls.Add(this.dgvTalla);
            this.grpCalzado.Controls.Add(this.cmbTipoTalla);
            this.grpCalzado.Controls.Add(this.button9);
            this.grpCalzado.Controls.Add(this.btnDeleteTalla);
            this.grpCalzado.Controls.Add(this.btnAddTalla);
            this.grpCalzado.Controls.Add(this.label21);
            this.grpCalzado.Controls.Add(this.button3);
            this.grpCalzado.Controls.Add(this.cbTaco);
            this.grpCalzado.Controls.Add(this.label8);
            this.grpCalzado.Controls.Add(this.cbSeriePro);
            this.grpCalzado.Controls.Add(this.label11);
            this.grpCalzado.Location = new System.Drawing.Point(735, 14);
            this.grpCalzado.Name = "grpCalzado";
            this.grpCalzado.Size = new System.Drawing.Size(120, 101);
            this.grpCalzado.TabIndex = 77;
            this.grpCalzado.TabStop = false;
            this.grpCalzado.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(99, 47);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(23, 23);
            this.button5.TabIndex = 112;
            this.button5.Text = ">";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnAddTallas
            // 
            this.btnAddTallas.Location = new System.Drawing.Point(99, 79);
            this.btnAddTallas.Name = "btnAddTallas";
            this.btnAddTallas.Size = new System.Drawing.Size(23, 24);
            this.btnAddTallas.TabIndex = 95;
            this.btnAddTallas.Text = ">";
            this.btnAddTallas.UseVisualStyleBackColor = true;
            // 
            // txtDetCodProd3
            // 
            this.txtDetCodProd3.Enabled = false;
            this.txtDetCodProd3.Location = new System.Drawing.Point(262, 321);
            this.txtDetCodProd3.Name = "txtDetCodProd3";
            this.txtDetCodProd3.Size = new System.Drawing.Size(100, 20);
            this.txtDetCodProd3.TabIndex = 93;
            // 
            // txtDetCodProd2
            // 
            this.txtDetCodProd2.Enabled = false;
            this.txtDetCodProd2.Location = new System.Drawing.Point(156, 321);
            this.txtDetCodProd2.Name = "txtDetCodProd2";
            this.txtDetCodProd2.Size = new System.Drawing.Size(100, 20);
            this.txtDetCodProd2.TabIndex = 92;
            this.txtDetCodProd2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDetCodProd2_KeyPress);
            // 
            // txtDetCodProd1
            // 
            this.txtDetCodProd1.Location = new System.Drawing.Point(44, 321);
            this.txtDetCodProd1.Name = "txtDetCodProd1";
            this.txtDetCodProd1.Size = new System.Drawing.Size(100, 20);
            this.txtDetCodProd1.TabIndex = 91;
            this.txtDetCodProd1.TextChanged += new System.EventHandler(this.txtDetCodProd1_TextChanged);
            this.txtDetCodProd1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDetCodProd1_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(41, 295);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 90;
            this.label17.Text = "Codigos Producto";
            // 
            // dgvTalla
            // 
            this.dgvTalla.AllowUserToAddRows = false;
            this.dgvTalla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTalla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codtalla1,
            this.Talla1});
            this.dgvTalla.Location = new System.Drawing.Point(126, 108);
            this.dgvTalla.Name = "dgvTalla";
            this.dgvTalla.RowHeadersVisible = false;
            this.dgvTalla.Size = new System.Drawing.Size(122, 135);
            this.dgvTalla.TabIndex = 89;
            this.dgvTalla.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvTalla_EditingControlShowing_1);
            // 
            // codtalla1
            // 
            this.codtalla1.DataPropertyName = "codTalla";
            this.codtalla1.HeaderText = "codtalla";
            this.codtalla1.Name = "codtalla1";
            this.codtalla1.Visible = false;
            // 
            // Talla1
            // 
            this.Talla1.DataPropertyName = "idtalla";
            this.Talla1.HeaderText = "Talla";
            this.Talla1.Name = "Talla1";
            this.Talla1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // cmbTipoTalla
            // 
            this.cmbTipoTalla.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbTipoTalla.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTipoTalla.FormattingEnabled = true;
            this.cmbTipoTalla.Location = new System.Drawing.Point(125, 18);
            this.cmbTipoTalla.Name = "cmbTipoTalla";
            this.cmbTipoTalla.Size = new System.Drawing.Size(121, 21);
            this.cmbTipoTalla.TabIndex = 74;
            this.cmbTipoTalla.Tag = "";
            this.cmbTipoTalla.SelectionChangeCommitted += new System.EventHandler(this.cmbTipoTalla_SelectionChangeCommitted);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(100, 17);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(23, 23);
            this.button9.TabIndex = 73;
            this.button9.Text = ">";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // btnDeleteTalla
            // 
            this.btnDeleteTalla.Location = new System.Drawing.Point(262, 79);
            this.btnDeleteTalla.Name = "btnDeleteTalla";
            this.btnDeleteTalla.Size = new System.Drawing.Size(37, 24);
            this.btnDeleteTalla.TabIndex = 70;
            this.btnDeleteTalla.Text = "-";
            this.btnDeleteTalla.UseVisualStyleBackColor = true;
            this.btnDeleteTalla.Click += new System.EventHandler(this.btnDeleteTalla_Click);
            // 
            // btnAddTalla
            // 
            this.btnAddTalla.Enabled = false;
            this.btnAddTalla.Location = new System.Drawing.Point(305, 79);
            this.btnAddTalla.Name = "btnAddTalla";
            this.btnAddTalla.Size = new System.Drawing.Size(38, 24);
            this.btnAddTalla.TabIndex = 71;
            this.btnAddTalla.Text = "+";
            this.btnAddTalla.UseVisualStyleBackColor = true;
            this.btnAddTalla.Click += new System.EventHandler(this.btnAddTalla_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(38, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 72;
            this.label21.Text = "Tipo Talla:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(100, 257);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(23, 23);
            this.button3.TabIndex = 62;
            this.button3.Text = ">";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cbTaco
            // 
            this.cbTaco.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTaco.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTaco.FormattingEnabled = true;
            this.cbTaco.Location = new System.Drawing.Point(126, 259);
            this.cbTaco.Name = "cbTaco";
            this.cbTaco.Size = new System.Drawing.Size(122, 21);
            this.cbTaco.TabIndex = 67;
            this.cbTaco.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(54, 262);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 66;
            this.label8.Text = "Taco :";
            this.label8.Visible = false;
            // 
            // cbSeriePro
            // 
            this.cbSeriePro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbSeriePro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSeriePro.FormattingEnabled = true;
            this.cbSeriePro.Location = new System.Drawing.Point(125, 49);
            this.cbSeriePro.Name = "cbSeriePro";
            this.cbSeriePro.Size = new System.Drawing.Size(121, 21);
            this.cbSeriePro.TabIndex = 76;
            this.cbSeriePro.SelectionChangeCommitted += new System.EventHandler(this.cbSeriePro_SelectionChangeCommitted);
            this.cbSeriePro.SelectedValueChanged += new System.EventHandler(this.cbSeriePro_SelectedValueChanged);
            this.cbSeriePro.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbSeriePro_KeyDown);
            this.cbSeriePro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbSeriePro_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(57, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 75;
            this.label11.Text = "Serie :";
            // 
            // lbPrecioCosto
            // 
            this.lbPrecioCosto.AutoSize = true;
            this.lbPrecioCosto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.lbPrecioCosto.ForeColor = System.Drawing.Color.Black;
            this.lbPrecioCosto.Location = new System.Drawing.Point(339, 377);
            this.lbPrecioCosto.Name = "lbPrecioCosto";
            this.lbPrecioCosto.Size = new System.Drawing.Size(113, 13);
            this.lbPrecioCosto.TabIndex = 108;
            this.lbPrecioCosto.Text = "Precio Venta con IGV:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 293);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 69;
            this.label6.Text = "Tallas :";
            // 
            // txtAnnio
            // 
            this.txtAnnio.Location = new System.Drawing.Point(253, 486);
            this.txtAnnio.Name = "txtAnnio";
            this.txtAnnio.Size = new System.Drawing.Size(100, 20);
            this.txtAnnio.TabIndex = 102;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnDel_Fotografia);
            this.groupBox2.Controls.Add(this.btnAdd_Fotografia);
            this.groupBox2.Controls.Add(this.pbFoto);
            this.groupBox2.Location = new System.Drawing.Point(347, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 239);
            this.groupBox2.TabIndex = 101;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fotograf�a";
            // 
            // btnDel_Fotografia
            // 
            this.btnDel_Fotografia.Image = ((System.Drawing.Image)(resources.GetObject("btnDel_Fotografia.Image")));
            this.btnDel_Fotografia.Location = new System.Drawing.Point(238, 48);
            this.btnDel_Fotografia.Name = "btnDel_Fotografia";
            this.btnDel_Fotografia.Size = new System.Drawing.Size(23, 23);
            this.btnDel_Fotografia.TabIndex = 1;
            this.btnDel_Fotografia.UseVisualStyleBackColor = true;
            this.btnDel_Fotografia.Click += new System.EventHandler(this.btnDel_Fotografia_Click);
            // 
            // btnAdd_Fotografia
            // 
            this.btnAdd_Fotografia.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd_Fotografia.Image")));
            this.btnAdd_Fotografia.Location = new System.Drawing.Point(238, 19);
            this.btnAdd_Fotografia.Name = "btnAdd_Fotografia";
            this.btnAdd_Fotografia.Size = new System.Drawing.Size(23, 23);
            this.btnAdd_Fotografia.TabIndex = 0;
            this.btnAdd_Fotografia.UseVisualStyleBackColor = true;
            this.btnAdd_Fotografia.Click += new System.EventHandler(this.btnAdd_Fotografia_Click);
            // 
            // pbFoto
            // 
            this.pbFoto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbFoto.Location = new System.Drawing.Point(9, 19);
            this.pbFoto.Name = "pbFoto";
            this.pbFoto.Size = new System.Drawing.Size(223, 213);
            this.pbFoto.TabIndex = 38;
            this.pbFoto.TabStop = false;
            // 
            // cbTemporada
            // 
            this.cbTemporada.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTemporada.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTemporada.FormattingEnabled = true;
            this.cbTemporada.Location = new System.Drawing.Point(126, 486);
            this.cbTemporada.Name = "cbTemporada";
            this.cbTemporada.Size = new System.Drawing.Size(121, 21);
            this.cbTemporada.TabIndex = 98;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(27, 492);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 13);
            this.label23.TabIndex = 99;
            this.label23.Text = "Temporada:";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(781, 113);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(23, 23);
            this.button6.TabIndex = 88;
            this.button6.Text = ">";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtCodCompraProducto
            // 
            this.txtCodCompraProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodCompraProducto.Location = new System.Drawing.Point(129, 83);
            this.txtCodCompraProducto.Name = "txtCodCompraProducto";
            this.txtCodCompraProducto.Size = new System.Drawing.Size(148, 20);
            this.txtCodCompraProducto.TabIndex = 85;
            // 
            // cbTipoCierre
            // 
            this.cbTipoCierre.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTipoCierre.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoCierre.FormattingEnabled = true;
            this.cbTipoCierre.Location = new System.Drawing.Point(805, 113);
            this.cbTipoCierre.Name = "cbTipoCierre";
            this.cbTipoCierre.Size = new System.Drawing.Size(121, 21);
            this.cbTipoCierre.TabIndex = 86;
            this.cbTipoCierre.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 405);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Unidad Base * :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(695, 118);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 87;
            this.label20.Text = "Tipo De Cierre: ";
            this.label20.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(97, 13);
            this.label19.TabIndex = 84;
            this.label19.Text = "Cod. Producto Uni.";
            // 
            // cbUnidadBase
            // 
            this.cbUnidadBase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbUnidadBase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbUnidadBase.FormattingEnabled = true;
            this.cbUnidadBase.Location = new System.Drawing.Point(129, 402);
            this.cbUnidadBase.Name = "cbUnidadBase";
            this.cbUnidadBase.Size = new System.Drawing.Size(147, 21);
            this.cbUnidadBase.TabIndex = 15;
            this.cbUnidadBase.Tag = "1";
            this.superValidator1.SetValidator1(this.cbUnidadBase, this.requiredFieldValidator2);
            // 
            // linkConfiguraUnidadesEquivalentes
            // 
            this.linkConfiguraUnidadesEquivalentes.AutoSize = true;
            this.linkConfiguraUnidadesEquivalentes.Location = new System.Drawing.Point(368, 417);
            this.linkConfiguraUnidadesEquivalentes.Name = "linkConfiguraUnidadesEquivalentes";
            this.linkConfiguraUnidadesEquivalentes.Size = new System.Drawing.Size(213, 13);
            this.linkConfiguraUnidadesEquivalentes.TabIndex = 83;
            this.linkConfiguraUnidadesEquivalentes.TabStop = true;
            this.linkConfiguraUnidadesEquivalentes.Text = "Configurar Unidades Equivalentes y Precios";
            this.linkConfiguraUnidadesEquivalentes.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkConfiguraUnidadesEquivalentes_LinkClicked);
            // 
            // cbControlStock
            // 
            this.cbControlStock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbControlStock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbControlStock.DisplayMember = "1,2,3,4";
            this.cbControlStock.FormattingEnabled = true;
            this.cbControlStock.Location = new System.Drawing.Point(127, 431);
            this.cbControlStock.Name = "cbControlStock";
            this.cbControlStock.Size = new System.Drawing.Size(150, 21);
            this.cbControlStock.TabIndex = 16;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(15, 435);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 13);
            this.label38.TabIndex = 35;
            this.label38.Text = "Control Stock :";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(100, 170);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 23);
            this.button4.TabIndex = 82;
            this.button4.Text = ">";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnUnidad
            // 
            this.btnUnidad.Location = new System.Drawing.Point(100, 402);
            this.btnUnidad.Name = "btnUnidad";
            this.btnUnidad.Size = new System.Drawing.Size(23, 23);
            this.btnUnidad.TabIndex = 14;
            this.btnUnidad.Text = ">";
            this.btnUnidad.UseVisualStyleBackColor = true;
            this.btnUnidad.Click += new System.EventHandler(this.btnUnidad_Click);
            // 
            // cbCategoria
            // 
            this.cbCategoria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCategoria.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCategoria.FormattingEnabled = true;
            this.cbCategoria.Location = new System.Drawing.Point(129, 172);
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Size = new System.Drawing.Size(148, 21);
            this.cbCategoria.TabIndex = 80;
            this.cbCategoria.SelectedValueChanged += new System.EventHandler(this.cbCategoria_SelectedValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 462);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "Precio Cat�logo :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(55, 175);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 81;
            this.label15.Text = "Grupo:";
            // 
            // txtPrecioCata
            // 
            this.txtPrecioCata.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrecioCata.Location = new System.Drawing.Point(127, 459);
            this.txtPrecioCata.MaxLength = 5;
            this.txtPrecioCata.Name = "txtPrecioCata";
            this.txtPrecioCata.Size = new System.Drawing.Size(70, 20);
            this.txtPrecioCata.TabIndex = 17;
            this.txtPrecioCata.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioCata.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioCata_KeyPress);
            // 
            // txtPrecioCompra
            // 
            this.txtPrecioCompra.Location = new System.Drawing.Point(203, 459);
            this.txtPrecioCompra.Name = "txtPrecioCompra";
            this.txtPrecioCompra.ReadOnly = true;
            this.txtPrecioCompra.Size = new System.Drawing.Size(62, 20);
            this.txtPrecioCompra.TabIndex = 79;
            this.txtPrecioCompra.Visible = false;
            // 
            // cbIgv
            // 
            this.cbIgv.AutoSize = true;
            this.cbIgv.Location = new System.Drawing.Point(271, 462);
            this.cbIgv.Name = "cbIgv";
            this.cbIgv.Size = new System.Drawing.Size(62, 17);
            this.cbIgv.TabIndex = 78;
            this.cbIgv.Text = "Inc. Igv";
            this.cbIgv.UseVisualStyleBackColor = true;
            this.cbIgv.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cbFamilia
            // 
            this.cbFamilia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFamilia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFamilia.FormattingEnabled = true;
            this.cbFamilia.Location = new System.Drawing.Point(129, 113);
            this.cbFamilia.Name = "cbFamilia";
            this.cbFamilia.Size = new System.Drawing.Size(148, 21);
            this.cbFamilia.TabIndex = 5;
            this.cbFamilia.Tag = "1";
            this.cbFamilia.SelectionChangeCommitted += new System.EventHandler(this.cbFamilia_SelectionChangeCommitted);
            this.cbFamilia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbFamilia_KeyDown);
            this.cbFamilia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbFamilia_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Familia :";
            // 
            // btnFamilia
            // 
            this.btnFamilia.Location = new System.Drawing.Point(100, 111);
            this.btnFamilia.Name = "btnFamilia";
            this.btnFamilia.Size = new System.Drawing.Size(23, 23);
            this.btnFamilia.TabIndex = 37;
            this.btnFamilia.Text = ">";
            this.btnFamilia.UseVisualStyleBackColor = true;
            this.btnFamilia.Click += new System.EventHandler(this.btnFamilia_Click);
            // 
            // cbColorSecundario
            // 
            this.cbColorSecundario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbColorSecundario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbColorSecundario.FormattingEnabled = true;
            this.cbColorSecundario.Location = new System.Drawing.Point(129, 375);
            this.cbColorSecundario.Name = "cbColorSecundario";
            this.cbColorSecundario.Size = new System.Drawing.Size(148, 21);
            this.cbColorSecundario.TabIndex = 65;
            // 
            // cbColorPrimario
            // 
            this.cbColorPrimario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbColorPrimario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbColorPrimario.FormattingEnabled = true;
            this.cbColorPrimario.Location = new System.Drawing.Point(129, 347);
            this.cbColorPrimario.Name = "cbColorPrimario";
            this.cbColorPrimario.Size = new System.Drawing.Size(148, 21);
            this.cbColorPrimario.TabIndex = 64;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 378);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 13);
            this.label14.TabIndex = 63;
            this.label14.Text = "Color Secundario :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 350);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 62;
            this.label12.Text = "Color Primario :";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(100, 318);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(23, 23);
            this.button2.TabIndex = 61;
            this.button2.Text = ">";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(100, 259);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 23);
            this.button1.TabIndex = 60;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbCorte
            // 
            this.cbCorte.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCorte.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCorte.FormattingEnabled = true;
            this.cbCorte.Location = new System.Drawing.Point(129, 319);
            this.cbCorte.Name = "cbCorte";
            this.cbCorte.Size = new System.Drawing.Size(148, 21);
            this.cbCorte.TabIndex = 59;
            // 
            // cbModelo
            // 
            this.cbModelo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbModelo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbModelo.FormattingEnabled = true;
            this.cbModelo.Location = new System.Drawing.Point(129, 262);
            this.cbModelo.Name = "cbModelo";
            this.cbModelo.Size = new System.Drawing.Size(148, 21);
            this.cbModelo.TabIndex = 58;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(44, 323);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 57;
            this.label18.Text = "Material:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 56;
            this.label7.Text = "Modelo :";
            // 
            // btnMarca
            // 
            this.btnMarca.Location = new System.Drawing.Point(100, 233);
            this.btnMarca.Name = "btnMarca";
            this.btnMarca.Size = new System.Drawing.Size(23, 23);
            this.btnMarca.TabIndex = 40;
            this.btnMarca.Text = ">";
            this.btnMarca.UseVisualStyleBackColor = true;
            this.btnMarca.Click += new System.EventHandler(this.btnMarca_Click);
            // 
            // btnLinea
            // 
            this.btnLinea.Location = new System.Drawing.Point(100, 139);
            this.btnLinea.Name = "btnLinea";
            this.btnLinea.Size = new System.Drawing.Size(23, 23);
            this.btnLinea.TabIndex = 38;
            this.btnLinea.Text = ">";
            this.btnLinea.UseVisualStyleBackColor = true;
            this.btnLinea.Click += new System.EventHandler(this.btnLinea_Click);
            // 
            // btnTipoArticulo
            // 
            this.btnTipoArticulo.Location = new System.Drawing.Point(100, 201);
            this.btnTipoArticulo.Name = "btnTipoArticulo";
            this.btnTipoArticulo.Size = new System.Drawing.Size(23, 23);
            this.btnTipoArticulo.TabIndex = 36;
            this.btnTipoArticulo.Text = ">";
            this.btnTipoArticulo.UseVisualStyleBackColor = true;
            this.btnTipoArticulo.Click += new System.EventHandler(this.btnTipoArticulo_Click);
            // 
            // cbTipoArticulo
            // 
            this.cbTipoArticulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTipoArticulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoArticulo.FormattingEnabled = true;
            this.cbTipoArticulo.Location = new System.Drawing.Point(129, 203);
            this.cbTipoArticulo.Name = "cbTipoArticulo";
            this.cbTipoArticulo.Size = new System.Drawing.Size(148, 21);
            this.cbTipoArticulo.TabIndex = 4;
            this.cbTipoArticulo.Tag = "1";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(15, 206);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(81, 13);
            this.label36.TabIndex = 24;
            this.label36.Text = "Tipo Art�culo * :";
            // 
            // cbMarca
            // 
            this.cbMarca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbMarca.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMarca.FormattingEnabled = true;
            this.cbMarca.Location = new System.Drawing.Point(129, 235);
            this.cbMarca.Name = "cbMarca";
            this.cbMarca.Size = new System.Drawing.Size(148, 21);
            this.cbMarca.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(48, 238);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Marca :";
            // 
            // cbLinea
            // 
            this.cbLinea.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbLinea.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLinea.Enabled = false;
            this.cbLinea.FormattingEnabled = true;
            this.cbLinea.Location = new System.Drawing.Point(129, 140);
            this.cbLinea.Name = "cbLinea";
            this.cbLinea.Size = new System.Drawing.Size(148, 21);
            this.cbLinea.TabIndex = 6;
            this.cbLinea.SelectionChangeCommitted += new System.EventHandler(this.cbLinea_SelectionChangeCommitted);
            this.cbLinea.SelectedValueChanged += new System.EventHandler(this.cbLinea_SelectedValueChanged);
            this.cbLinea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbLinea_KeyDown);
            this.cbLinea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbLinea_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Linea :";
            // 
            // cbEstado
            // 
            this.cbEstado.AutoSize = true;
            this.cbEstado.Checked = true;
            this.cbEstado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEstado.Location = new System.Drawing.Point(501, 18);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(56, 17);
            this.cbEstado.TabIndex = 1;
            this.cbEstado.Text = "Activo";
            this.cbEstado.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(196, 41);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(477, 20);
            this.txtNombre.TabIndex = 3;
            this.txtNombre.Tag = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(193, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nombre * :";
            // 
            // txtReferencia
            // 
            this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReferencia.Location = new System.Drawing.Point(90, 41);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.ReadOnly = true;
            this.txtReferencia.Size = new System.Drawing.Size(100, 20);
            this.txtReferencia.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Referencia:";
            // 
            // txtCodProducto
            // 
            this.txtCodProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodProducto.Enabled = false;
            this.txtCodProducto.Location = new System.Drawing.Point(18, 41);
            this.txtCodProducto.Name = "txtCodProducto";
            this.txtCodProducto.ReadOnly = true;
            this.txtCodProducto.Size = new System.Drawing.Size(66, 20);
            this.txtCodProducto.TabIndex = 0;
            this.txtCodProducto.TextChanged += new System.EventHandler(this.txtCodProducto_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "C�digo:";
            // 
            // superValidator1
            // 
            this.superValidator1.ContainerControl = this;
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // requiredFieldValidator2
            // 
            this.requiredFieldValidator2.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // codtalla
            // 
            this.codtalla.DataPropertyName = "codTalla";
            this.codtalla.HeaderText = "codtalla";
            this.codtalla.Name = "codtalla";
            this.codtalla.Visible = false;
            // 
            // frmRegistroProductoAr
            // 
            this.ClientSize = new System.Drawing.Size(703, 545);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Name = "frmRegistroProductoAr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registro Producto";
            this.Load += new System.EventHandler(this.frmRegistroProductoAr_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpCalzado.ResumeLayout(false);
            this.grpCalzado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTalla)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPrecioCata;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnUnidad;
        private System.Windows.Forms.Button btnMarca;
        private System.Windows.Forms.Button btnLinea;
        private System.Windows.Forms.Button btnFamilia;
        private System.Windows.Forms.Button btnTipoArticulo;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbControlStock;
        private System.Windows.Forms.ComboBox cbUnidadBase;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbTipoArticulo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbMarca;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbLinea;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbFamilia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cbEstado;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodProducto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTaco;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbColorSecundario;
        private System.Windows.Forms.ComboBox cbColorPrimario;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbCorte;
        private System.Windows.Forms.ComboBox cbModelo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnAddTalla;
        private System.Windows.Forms.Button btnDeleteTalla;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.ComboBox cmbTipoTalla;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbSeriePro;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox grpCalzado;
        private System.Windows.Forms.TextBox txtPrecioCompra;
        private System.Windows.Forms.CheckBox cbIgv;
        public System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCodCompraProducto;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private System.Windows.Forms.Label label19;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox cbTipoCierre;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbTemporada;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDel_Fotografia;
        private System.Windows.Forms.Button btnAdd_Fotografia;
        private System.Windows.Forms.PictureBox pbFoto;
        private System.Windows.Forms.TextBox txtAnnio;
        private System.Windows.Forms.TextBox txtPrecioVen;
        private System.Windows.Forms.Label lbPrecioCosto;
        private System.Windows.Forms.LinkLabel linkConfiguraUnidadesEquivalentes;
        private System.Windows.Forms.TextBox txtPrecioCom;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridViewTextBoxColumn codtalla;
        private System.Windows.Forms.DataGridView dgvTalla;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtDetCodProd3;
        private System.Windows.Forms.TextBox txtDetCodProd2;
        private System.Windows.Forms.TextBox txtDetCodProd1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnAddTallas;
        private System.Windows.Forms.ComboBox cmbTallasProducto;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codtalla1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Talla1;
        private System.Windows.Forms.ComboBox cbCategoria;
        private System.Windows.Forms.Button btnColorp;
        private System.Windows.Forms.Button btnColors;
    }
}