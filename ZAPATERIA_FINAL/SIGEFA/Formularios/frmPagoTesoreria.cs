﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;

namespace SIGEFA.Formularios
{
    public partial class frmPagoTesoreria : Telerik.WinControls.UI.RadForm
    {
        public int CodCaja {get;set;}

        clsAdmCajaAhorros admahorros = new clsAdmCajaAhorros();
        clsAdmCajaGeneral admgeneral = new clsAdmCajaGeneral();
        clsAdmTesoreria admtesoreria = new clsAdmTesoreria();
        clsAdmTipoPagoCaja admtipopago = new clsAdmTipoPagoCaja();
        clsAdmFactura admfact = new clsAdmFactura();

        clsAdmCtaCte admcta = new clsAdmCtaCte();

        public int Proceso { get; set; } //proceso 1 abrir desde gestion tesoreria, proceso 2 abrir desde caja ahorros, proceso 3, abrir desde caja general
        public int origendinero { get; set; }

        public int IdCompra { get; set; }
        public string NombreFact { get; set; }
        public decimal Mpago { get; set; }

        public frmPagoTesoreria()
        {
            InitializeComponent();
        }

        private void cargaCajas()
        {
            cmborigen.DataSource = null;
            cmborigen.ValueMember = "Key";
            cmborigen.DisplayMember = "Value";

            Dictionary<int, string> pairs = new Dictionary<int, string>();
            pairs.Add(1, "Caja Ahorros");
            pairs.Add(2, "Caja General");

            cmborigen.DataSource = pairs;
            cmborigen.SelectedIndex = -1;
        }

        private void cargaTipoPago()
        {
            cmborigen.DataSource = null;
            cmbTipoPago.ValueMember = "cod";
            cmbTipoPago.DisplayMember = "descripcion";
            cmbTipoPago.DataSource = admtipopago.ListaTipoPagoTesoreria();
            cmbTipoPago.SelectedIndex = -1;
        }

        private void cargaFpago()
        {
            cmborigen.DataSource = null;
            cmbfpago.ValueMember = "Key";
            cmbfpago.DisplayMember = "Value";

            Dictionary<int, string> pairs = new Dictionary<int, string>();
            pairs.Add(1, "Efectivo");
            pairs.Add(2, "Ctas. Empresa");

            cmbfpago.DataSource = pairs;
            cmbfpago.SelectedIndex = -1;
        }

        private void cargaTarjetas()
        {
            cmborigen.DataSource = null;
            cmborigen.ValueMember = "cod";
            cmborigen.DisplayMember = "descripcion";
            cmborigen.DataSource = admcta.ListaCtaTesoreria();
            cmborigen.SelectedIndex = -1;
        }

        private void frmRetiroNuevo_Load(object sender, EventArgs e)
        {
            IdCompra = 0;
            NombreFact = "";
            cargaFpago();
            cargaTipoPago();
            txtMonto.Text = "0.00";
            txtobs.Text = "";
            lblsaldo.Text = "0.00";

            if (Proceso == 1)
            {

                gbUsuario.Visible = false;

            }else if (Proceso == 2)
            {
                cmbfpago.SelectedIndex = 0; //seleccionamos efectivo
                cmbfpago.Enabled = false;
                cmborigen.SelectedIndex = 0;//seleccionamos caja ahorros
                cmborigen.Enabled = false;
                gbUsuario.Visible = true;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Proceso == 1)
            {
                decimal monto = Convert.ToDecimal(txtMonto.Text);

                if (Convert.ToDecimal(lblsaldo.Text) > monto)
                {
                    origendinero = Convert.ToInt32(cmborigen.SelectedValue);

                    string observaciones = txtobs.Text;

                    int fpago = Convert.ToInt32(cmbfpago.SelectedValue);

                    int tpago = Convert.ToInt32(cmbTipoPago.SelectedValue);

                    if (origendinero > 0 && fpago > 0 && tpago > 0)
                    {
                        if (fpago == 1) //efectivo
                        {
                            //caja ahorros
                            int idcaja = origendinero;
                            int idcta = 0;

                            if (admtesoreria.insertPagoTesoreria(fpago, idcta, idcaja, monto, tpago, observaciones,frmLogin.iCodAlmacen,frmLogin.iCodUser,IdCompra))
                            {
                                MessageBox.Show("Pago registrado correctamente...");
                                this.DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                MessageBox.Show("No se pudo registrar pago...");
                                this.DialogResult = DialogResult.Cancel;
                            }

                            this.Close();
                        }
                        else if (fpago == 2) //ctas
                        {
                            int idcaja = 0;
                            int idcta = origendinero;

                            if (admtesoreria.insertPagoTesoreria(fpago, idcta, idcaja, monto, tpago, observaciones, frmLogin.iCodAlmacen, frmLogin.iCodUser,IdCompra))
                            {
                                MessageBox.Show("Operacion realizada correctamente.");
                                this.DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                MessageBox.Show("Hubo error en la operacion.");
                                this.DialogResult = DialogResult.Cancel;
                            }

                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Tienes que completar todos los campos.");
                    }
                }
                else
                {
                    MessageBox.Show("No tienes saldo suficiente para realizar esta operacion.");
                }
            }else if (Proceso == 2)
            {
                decimal monto = Convert.ToDecimal(txtMonto.Text);

                if (Convert.ToDecimal(lblsaldo.Text) > monto)
                {
                    string nombre = txtNombre.Text.Trim();
                    string dni = txtDni.Text.Trim();
                    int tpago = Convert.ToInt32(cmbTipoPago.SelectedValue);
                    string observaciones = txtobs.Text;

                    bool a = admahorros.insertPagoCAhorros(frmLogin.iCodAlmacen, monto, frmLogin.iCodUser, nombre, dni, tpago, observaciones);

                    if (a)
                    {
                        MessageBox.Show("Operacion realizada correctamente.");
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Hubo error en la operacion.");
                        this.DialogResult = DialogResult.Cancel;
                    }

                    this.Close();
                }
                else
                {
                    MessageBox.Show("No tienes saldo suficiente para realizar esta operacion.");
                }
            }
        }


        public void limpiacontroles()
        {
            cmbfpago.SelectedIndex = -1;
            cmborigen.SelectedIndex = -1;
            cmborigen.Enabled = false;
            cmbTipoPago.SelectedIndex = -1;
            txtMonto.Text = "0.00";
            lblsaldo.Text = "0.00";
            txtobs.Text = "";
        }

        private void cmbfpago_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (cmbfpago.SelectedIndex!=-1)
            {
                cmborigen.Enabled = true;
                if (cmbfpago.SelectedIndex == 0)
                {
                    cargaCajas();
                }
                else if (cmbfpago.SelectedIndex == 1)
                {
                    cargaTarjetas();
                }
            }
            else
            {
                cmborigen.Enabled = false;
                cmborigen.SelectedIndex = -1;
            }
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void cmborigen_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            int fpago = Convert.ToInt32(cmbfpago.SelectedValue);
            int origen = Convert.ToInt32(cmborigen.SelectedValue);
            if (fpago == 1) //efectivo
            {
                if (origen != -1)
                {
                    if (origen == 1)
                    {
                       lblsaldo.Text= admtesoreria.saldoCajaAhorros().ToString();
                    }else if (origen == 2)
                    {
                        lblsaldo.Text = admtesoreria.saldoCajaGeneral().ToString();
                    }
                }
                
            }else if (fpago == 2) //ctas
            {
                if (origen != -1)
                {
                    lblsaldo.Text = admtesoreria.saldoCtaCte(origen).ToString();
                }
            }
        }

        private void cmbTipoPago_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (cmbTipoPago.SelectedIndex != -1)
            {
                if (cmbTipoPago.Text.Equals("COMPRAS"))
                {
                    
                    //abrimos la ventana de pagos para seleccionar la compra a pagar
                    frmPagos frmpag = new frmPagos();
                    frmpag.WindowState = FormWindowState.Normal;
                    frmpag.ShowDialog();
                    if (frmpag.IdCompra != 0)
                    {
                        IdCompra = frmpag.IdCompra;
                        NombreFact = frmpag.NombreFact;
                        Mpago = frmpag.Mpago;
                        lblcompra.Visible = true;
                        lblnombre.Visible = true;
                        lblnmonto.Visible = true;
                        lblmonto.Visible = true;
                        lblnombre.Text = NombreFact;
                        lblmonto.Text = Mpago.ToString();
                    }
                    else
                    {
                        lblcompra.Visible = false;
                        lblnombre.Visible = false;
                        lblnmonto.Visible = false;
                        lblmonto.Visible = false;
                        lblnombre.Text = "------------";
                        lblmonto.Text = "0.0000";
                        IdCompra = 0;
                        NombreFact = "";
                        Mpago = 0.00m;
                    }
                    
                }
                else
                {
                    lblcompra.Visible = false;
                    lblnombre.Visible = false;
                    lblnmonto.Visible = false;
                    lblmonto.Visible = false;
                    lblnombre.Text = "------------";
                    lblmonto.Text = "0.0000";
                    IdCompra = 0;
                    NombreFact = "";
                    Mpago = 0.00m;
                }
            }
        }
    }
}
