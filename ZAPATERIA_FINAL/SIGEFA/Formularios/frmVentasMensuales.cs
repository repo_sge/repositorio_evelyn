using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Administradores;
using System.Linq;
using Yogesh.ExcelXml;

namespace SIGEFA.Formularios
{
    public partial class frmVentasMensuales : DevComponents.DotNetBar.OfficeForm
    {

        clsAdmVentasMes ventasM = new clsAdmVentasMes();

        public frmVentasMensuales()
        {
            InitializeComponent();
        }


        System.Data.DataTable datos = new System.Data.DataTable();

        private void frmVentasMensuales_Load(object sender, EventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(System.DateTime.Now.ToString());
            txtanio.Text = fecha.Year.ToString();            
            MostrarDatos();
            
        }
        private void LimpiarTabla()
        {
            dgvVentasMes.Columns.Clear();
            dt.Columns.Clear();
            dt.Rows.Clear();
            dgvVentasMes.Refresh();
        }

        System.Data.DataTable dt = new System.Data.DataTable();

        private void MostrarDatos()
        {
            LimpiarTabla();
            datos = ventasM.MostrarVentasMes(12, Convert.ToInt32(txtanio.Text), frmLogin.iCodAlmacen);
            CargaTabla();
            CargarVentasEnero();
            CargarVentasFebrero();
            CargarVentasMarzo();
            CargarVentasAbril();
            CargarVentasMayo();
            CargarVentasJunio();
            CargarVentasJulio();
            CargaVentasAgosto();
            CargaVentasSeptiembre();
            CargaVentasOctubre();
            CargaVentasNoviembre();
            CargaVentasDiciembre();
            dt.Rows.Add("");
            //dgvVentasMes.AutoGenerateColumns = false;
            DataGridViewRow row = dgvVentasMes.Rows[dgvVentasMes.Rows.Count - 1];
            row.Cells["MES"].Value = "SUMA TOTAL";
            row.Cells["Total"].Value = 0;            
            CalcularTotalAnual();
            dgvVentasMes.Columns[0].DefaultCellStyle.BackColor = Color.Red;
            CambiaColor();
        }

       

        private void CambiaColor()
        {
            for (int i = 1; i <= 31; i++)
            {                
                //dgvVentasMes.Columns[i].DefaultCellStyle.BackColor = Color.PapayaWhip;
            }
            dgvVentasMes.Columns[32].DefaultCellStyle.BackColor = Color.Yellow;
        }

        private void CalcularTotalAnual()
        {
            Double resul = dgvVentasMes.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDouble(x.Cells["Total"].Value is DBNull ? 0 : x.Cells["Total"].Value)); 
            DataGridViewRow rowtotal = dgvVentasMes.Rows[dgvVentasMes.Rows.Count - 1];
            rowtotal.Cells["Total"].Value = resul;
            rowtotal.Cells["Total"].Style.BackColor = Color.LightCyan;
        }        

        private void CargaTabla()
        {
            try
            {
                dt.Columns.Add("MES");
                for (int i = 1; i <= 31; i++)
                {
                    dt.Columns.Add(i.ToString());
                }
                dt.Columns.Add("Total");
            }
            catch (Exception ex)
            {
            }
        }        

        private void CargarVentasEnero()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "ENERO")
                    {

                        /*for (int i = 0; i < 31; i++)
                        {*/
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        /*}*/
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                    
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargarVentasFebrero()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "FEBRERO")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargarVentasMarzo()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "MARZO")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargarVentasAbril()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "ABRIL")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargarVentasMayo()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "MAYO")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total; ;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargarVentasJunio()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "JUNIO")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }    

        private void CargarVentasJulio()
        {
            if (datos != null)
            {
                DataRow dRow = dt.NewRow();
                Double Total = 0;
                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "JULIO")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }                      
        }

        private void CargaVentasAgosto()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "AGOSTO")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargaVentasSeptiembre()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "SEPTIEMBRE")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargaVentasOctubre()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "OCTUBRE")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargaVentasNoviembre()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "NOVIEMBRE")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void CargaVentasDiciembre()
        {
            if (datos != null)
            {
                Double Total = 0;
                DataRow dRow = dt.NewRow();

                foreach (DataRow row in datos.Rows)
                {
                    if (row[0].ToString() == "DICIEMBRE")
                    {

                        for (int i = 0; i < 31; i++)
                        {
                            dRow["MES"] = row[0].ToString();
                            dRow[row[1].ToString()] = row[3].ToString();
                        }
                        Total = Total + Convert.ToDouble(row[3].ToString());
                        dRow["Total"] = Total;
                    }
                }
                dt.Rows.Add(dRow);
                dgvVentasMes.DataSource = dt;

            }
        }

        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            MostrarDatos();
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                ExcelXmlWorkbook book = new ExcelXmlWorkbook();
                Worksheet objHojaExcel = book[0];

                objHojaExcel.Name = "Ventas Por Mes";
                objHojaExcel.PrintOptions.Orientation = PageOrientation.Portrait;
                objHojaExcel.PrintOptions.SetMargins(0.78,0.91,0,0.91);
                objHojaExcel.PrintOptions.SetScaleToSize(90);

                objHojaExcel[0, 0].Value = "REPORTE DE VENTAS POR MES";
                Range rgTit = new Range(objHojaExcel[0,0], objHojaExcel[14,0]);
                rgTit.Merge();
                rgTit.Interior.Color = Color.Bisque;
                rgTit.Font.Name = "Calibri";
                rgTit.Font.Size = 14;
                rgTit.Font.Bold = true;
                rgTit.Alignment.Horizontal = Yogesh.ExcelXml.HorizontalAlignment.Center;

                int comienza = 1;
                int IndiceColumna = -1;

                foreach (DataGridViewColumn col in dgvVentasMes.Columns)
                {
                    if (col.Visible == true)
                    {
                        IndiceColumna++;
                        objHojaExcel[IndiceColumna, comienza].Value = col.HeaderText;
                    }
                }

                //Rango cabecera

                Range rgCabe = new Range(objHojaExcel[0,comienza], objHojaExcel[IndiceColumna, comienza]);
                rgCabe.Font.Size = 10;
                rgCabe.Font.Bold = true;
                rgCabe.Alignment.Horizontal = Yogesh.ExcelXml.HorizontalAlignment.Center;
                rgCabe.Border.Sides = Yogesh.ExcelXml.BorderSides.All;

                //estilo celda numerica

                XmlStyle cell = new XmlStyle();
                cell.Alignment.Horizontal = Yogesh.ExcelXml.HorizontalAlignment.Center;

                
                int IndiceFila = 1;
                
                foreach (DataGridViewRow row in dgvVentasMes.Rows)
                {
                    IndiceFila++;
                    IndiceColumna = -1;
                    foreach (DataGridViewColumn col in dgvVentasMes.Columns)
                    {
                        if (col.Visible)
                        {
                            IndiceColumna++;
                            objHojaExcel[IndiceColumna, IndiceFila].Value = row.Cells[col.Name].Value.ToString();                            
                        }
                    }
                }

                Range rgbordes = new Range(objHojaExcel[0,comienza], objHojaExcel[IndiceColumna, IndiceFila]);
                rgbordes.Border.Sides = Yogesh.ExcelXml.BorderSides.All;

                String desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                String outfile = desktopPath + "\\ReporteVentas.xls";
                book.Export(outfile);

                Microsoft.Office.Interop.Excel.Application m_excel = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook objObjLibro = m_excel.Workbooks.Open(outfile, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", true, false, 0, true, true, 0);
                Microsoft.Office.Interop.Excel.Sheets sheets = objObjLibro.Worksheets;
                Microsoft.Office.Interop.Excel.Worksheet worksheets1 = (Microsoft.Office.Interop.Excel.Worksheet)sheets.get_Item(1);
                worksheets1.PageSetup.PaperSize = Microsoft.Office.Interop.Excel.XlPaperSize.xlPaperA4;
                m_excel.DisplayAlerts = false;
                m_excel.Visible = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
      

    }
}