using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmRegistroProductoAr : DevComponents.DotNetBar.OfficeForm
    {
        
        clsAdmTipoArticulo admTip = new clsAdmTipoArticulo();
        clsAdmFamilia admFamilia = new clsAdmFamilia();
        clsAdmModelo admModelo = new clsAdmModelo();
        clsAdmMarca admMarca = new clsAdmMarca();
        clsAdmCorte admCorte = new clsAdmCorte();
        clsAdmTaco admTaco = new clsAdmTaco();
        clsAdmCuello admCuello = new clsAdmCuello();
        clsAdmLinea admLinea = new clsAdmLinea();
        clsAdmColor admColor = new clsAdmColor();
        clsAdmUnidad admUnidad = new clsAdmUnidad();
        cldAdmTipoTalla admTipoTalla = new cldAdmTipoTalla();
        clsAdmTalla admTalla = new clsAdmTalla();
        clsAdmSerieProducto admSerieP = new clsAdmSerieProducto();
        clsAdmProducto admProducto = new clsAdmProducto();
        clsAdmGrupo admGrupo = new clsAdmGrupo();
        clsAdmControlStock admControl = new clsAdmControlStock();
        clsAdmTipoCierre admtipoCierre = new clsAdmTipoCierre();
        clsAdmTemporada admTemporada = new clsAdmTemporada();

        clsEntFotografia entfotografia = new clsEntFotografia();
        clsAdmFotografia admfotografia = new clsAdmFotografia();

        clsUnidadEquivalente equi = new clsUnidadEquivalente();

        clsSerieProducto ser = new clsSerieProducto();
        clsAdmTallaProducto admtallaprod = new clsAdmTallaProducto();
        clsAdmTalla admtallas = new clsAdmTalla();

        public clsProducto pro = new clsProducto();
        //public List<clsTallaProducto> tallasprod = new List<clsTallaProducto>();
        clsTalla talla = new clsTalla();
        public Int32 Proceso = 0;

        public frmRegistroProductoAr()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmRegistroProductoAr_Load(object sender, EventArgs e)
        {
            CargaFamilias();
            CargaTipoArticulos();
            CargaMarcas();
            CargaModelo();
            CargarCorte();
            //CargarTaco();            
            CargarColorPrimario();
            CargarColorSecundario();
            CargarUnidad();
            //CargaSerieProducto();
            CargaControlStock();
            //cargaTipoCierre();
            CargaTemporada();
            //CargaTipoTalla();
            CargarLinea();
            cbLinea.Enabled = true;

            txtAnnio.Text = Convert.ToString(DateTime.Today.Year);
            if (Proceso == 1)
            {
                linkConfiguraUnidadesEquivalentes.Visible = false;
                //cbSeriePro.SelectedValue = 14;
                //cargaCmbTallas();

            }
            if (Proceso == 2)
            {
                CargaProducto();
                CargaFotografia();
                txtDetCodProd1.Enabled = true;
                txtDetCodProd2.Enabled = true;
                txtDetCodProd3.Enabled = true;
                CargaTalla();
                if (cmbTallasProducto.Items.Count > 0)
                {
                    cmbTallasProducto.SelectedValue = pro.Codtalla;
                }
                //cargaCmbTallas();
                //cargaTallasxproducto();
            }
        }

        //private void cargaCmbTallas()
        //{
            
        //    //if (Convert.ToInt32(cmbTipoTalla.SelectedValue) != -1)
        //    //{
        //    //    if (ser.Tallamin > 0 && ser.Tallamax > 0)
        //    //    {
        //    //        if (ser.Tallamax > ser.Tallamin)
        //    //        {
        //    cmbTallasProducto.DataSource = admTalla.ListaTallaporLinea(Convert.ToInt32(cbLinea.SelectedValue), ser.Tallamin, ser.Tallamax, Convert.ToInt32(cmbTipoTalla.SelectedValue));
        //    cmbTallasProducto.ValueMember = "codTalla";
        //    cmbTallasProducto.DisplayMember = "nombre";
        //    //        }
        //    //        else
        //    //        {
        //    //            MessageBox.Show("La talla maxima debe ser mayor a la minima", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        MessageBox.Show("Debes seleccionar tallas", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    //    }
        //    //}
        //}

        private void CargaFotografia()
        {
            clsEntFotografia entfoto = new clsEntFotografia();
            entfoto = admfotografia.CargaFotografia(Convert.ToInt32(txtCodProducto.Text));
            if (entfoto != null)
            {
                pbFoto.Image = entfoto.Fotografia;
                pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;
            }            
        }

        private void CargaTemporada()
        {
            cbTemporada.DataSource = admTemporada.MuestraTemporada();
            cbTemporada.DisplayMember = "descripcion";
            cbTemporada.ValueMember = "codigo";
            cbTemporada.SelectedIndex = -1;
        }

        private void cargaTipoCierre()
        {
            cbTipoCierre.DataSource = admtipoCierre.MuestraTipoCierre();
            cbTipoCierre.DisplayMember = "descripcion";
            cbTipoCierre.ValueMember = "codigo";
            cbTipoCierre.SelectedIndex = -1;
        }

        private void CargaControlStock()
        {
            cbControlStock.DataSource = admControl.CargaControlStock();
            cbControlStock.DisplayMember = "descripcion";
            cbControlStock.ValueMember = "codigo";
            cbControlStock.SelectedIndex = 0; 
        }

        private void CargaProducto()
        {
            //ocultamos campos de precios porque solo se edita desde unidades equivalentes
            txtPrecioCom.Visible = false;
            txtPrecioVen.Visible = false;
            label16.Visible = false;
            lbPrecioCosto.Visible = false;

            pro = admProducto.CargaProducto(pro.CodProducto, frmLogin.iCodAlmacen);
            txtCodProducto.Text = pro.CodProducto.ToString();
            txtCodCompraProducto.Text = pro.CodCompraProducto;
            txtNombre.Text = pro.Descripcion;
            txtReferencia.ReadOnly = false;
            txtReferencia.Text = pro.Referencia;
            txtDetCodProd1.Text = pro.detCodProducto1;
            txtDetCodProd2.Text = pro.detCodProducto2;
            txtDetCodProd3.Text = pro.detCodProducto3;

            txtPrecioCata.Text = pro.PrecioCatalogo.ToString();
            
            cbFamilia.SelectedValue = pro.CodFamilia;

            CargarLinea();
            cbLinea.Enabled = true;
            cbLinea.SelectedValue = pro.CodLinea;
            //cbSeriePro.SelectedValue = pro.codSerieProducto;
            //if (pro.CodLinea == 1)
            //{
            //    grpCalzado.Visible = true;
            //    CargaTipoTalla();
            //    cmbTipoTalla.SelectedValue = pro.TipoTalla;
            //    cbTaco.SelectedValue = pro.codTaco;
            //    cargaTipoCierre();
            //    cbTipoCierre.SelectedValue = pro.CodTipoCierre;
            //    ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //    btnAddTalla.Enabled = true;
            //    btnAddTalla.PerformClick();
            //}
            //else if (pro.CodLinea == 10)
            //{                
               
            //}
            CargaGrupo();
            cbCategoria.Enabled = true;
            cbCategoria.SelectedValue = pro.CodGrupo;
            cbMarca.SelectedValue = pro.CodMarca;
            cbTipoArticulo.SelectedValue = pro.CodTipoArticulo;
            cbModelo.SelectedValue = pro.codModelo;
            cbCorte.SelectedValue = pro.codCorte;
            //cbTaco.SelectedValue = pro.codTaco;
            cbColorPrimario.SelectedValue = pro.codColorPrimario;
            cbColorSecundario.SelectedValue = pro.codColorSecundario;
            cbUnidadBase.SelectedValue = pro.CodUnidadMedida;
            cbControlStock.SelectedValue = pro.CodControlStock;
            txtAnnio.Text = pro.Annio.ToString();
            //ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            cbIgv.Checked = pro.ConIgv;
            cbTemporada.SelectedValue = pro.CodTemporada;
        }

       

        private void CargaSerieProducto()
        {
            
            cbSeriePro.DataSource = admSerieP.MuestraSerieProductos();
            cbSeriePro.DisplayMember = "descripcion";
            cbSeriePro.ValueMember = "codSerieProducto";
            cbSeriePro.SelectedIndex = -1;
        }

        private void CargaTipoTalla()
        {
            try
            {
                cmbTipoTalla.DataSource = admTipoTalla.MuestraTipoTalla(Convert.ToInt32(1));
                cmbTipoTalla.DisplayMember = "descripcion";
                cmbTipoTalla.ValueMember = "codTipoTalla";
                cmbTipoTalla.SelectedIndex = 0;                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cargar tipo de tallas..." + ex.Message);
            }
        }

        private void CargarUnidad()
        {
            cbUnidadBase.DataSource = admUnidad.MuestraUnidades();
            cbUnidadBase.DisplayMember = "descripcion";
            cbUnidadBase.ValueMember = "codUnidadMedida";
            cbUnidadBase.SelectedIndex = -1;
        }

        private void CargarColorPrimario()
        {
            cbColorPrimario.DataSource = admColor.MuestraColorPrimario();
            cbColorPrimario.DisplayMember = "descripcion";
            cbColorPrimario.ValueMember = "codColorPrimario";
            cbColorPrimario.SelectedIndex = -1;
        }

        private void CargarColorSecundario()
        {
            cbColorSecundario.DataSource = admColor.MuestraColorSecundario();
            cbColorSecundario.DisplayMember = "descripcion";
            cbColorSecundario.ValueMember = "codColorSecundario";
            cbColorSecundario.SelectedIndex = -1;
        }

        private void CargarLinea()
        {
            cbLinea.DataSource = admLinea.MuestraLineas(Convert.ToInt32(cbFamilia.SelectedValue));
            cbLinea.DisplayMember = "descripcion";
            cbLinea.ValueMember = "codLinea";
            cbLinea.SelectedIndex = -1;
            
        }

        private void CargarTaco()
        { 
            cbTaco.DataSource = admTaco.MuestraTaco();
            cbTaco.DisplayMember = "descripcion";
            cbTaco.ValueMember = "codTaco";
            cbTaco.SelectedIndex = -1;

        }

        private void CargarCorte()
        {
            cbCorte.DataSource = admCorte.MuestraCorte();
            cbCorte.DisplayMember = "descripcion";
            cbCorte.ValueMember = "codCorte";
            cbCorte.SelectedIndex = -1;
        }

        private void CargaModelo()
        {
            cbModelo.DataSource = admModelo.MuestraModelo();
            cbModelo.DisplayMember = "descripcion";
            cbModelo.ValueMember = "codModelo";
            cbModelo.SelectedIndex = -1;
        }

        private void CargaMarcas()
        {
            cbMarca.DataSource = admMarca.MuestraMarcas();
            cbMarca.DisplayMember = "descripcion";
            cbMarca.ValueMember = "codMarca";
            cbMarca.SelectedIndex = -1;
        }

        private void CargaFamilias()
        {
            cbFamilia.DataSource = admFamilia.MuestraFamilias();
            cbFamilia.DisplayMember = "descripcion";
            cbFamilia.ValueMember = "codFamilia";
            cbFamilia.SelectedIndex = -1;
        }

        private void CargaTipoArticulos()
        {
            cbTipoArticulo.DataSource = admTip.MuestraTipoArticulos();
            cbTipoArticulo.DisplayMember = "descripcion";
            cbTipoArticulo.ValueMember = "codTipoArticulo";
            cbTipoArticulo.SelectedIndex = -1;
        }



        private void btnAddTalla_Click(object sender, EventArgs e)
        {

            try
            {
                clsTalla talla = admtallas.CargaTallaCod(Convert.ToInt32(cmbTallasProducto.SelectedValue));

                if (dgvTalla.Rows.Count != 0)
                {

                    int coincide = 0; // para verificar si la talla ya esta agregada
                    foreach (DataGridViewRow row in dgvTalla.Rows)
                    {
                        if (Convert.ToInt32(row.Cells[codtalla1.Name].Value) == talla.codTalla)
                        {
                            coincide++; //agrego 1 al contador de coincidencias
                        }
                    }
                    if (coincide == 0) // si la talla no se encuentra, hago la insercion de la talla
                    {
                        dgvTalla.Rows.Add(1); //a�ado una nueva fila para nueva talla
                        int filas = dgvTalla.Rows.Count;
                        dgvTalla.Rows[filas - 1].Cells[codtalla1.Name].Value = talla.codTalla;//a�ado la talla en el datagrid
                        dgvTalla.Rows[filas - 1].Cells[Talla1.Name].Value = talla.Valor;

                        //agregamos la talla en la bd
                        clsTallaProducto tallaproducto = new clsTallaProducto();
                        tallaproducto.IdTalla = talla.codTalla;
                        tallaproducto.IdProducto = pro.CodProducto;
                        tallaproducto.IdAlmacen = frmLogin.iCodAlmacen;
                        admtallaprod.Insert(tallaproducto);
                    }
                    else
                    {
                        MessageBox.Show("Talla ya se encuentra agregada...");
                    }

                }
                else
                {
                    //si es la primera fila en insertarse
                    dgvTalla.Rows.Add(1);
                    dgvTalla.Rows[0].Cells[Talla1.Name].Value = talla.Valor; // a�ado la talla en el datagrid
                    dgvTalla.Rows[0].Cells[codtalla1.Name].Value = talla.codTalla;


                    //a�ado la talla en la bd
                    clsTallaProducto tallaproducto = new clsTallaProducto();
                    tallaproducto.IdTalla = talla.codTalla;
                    tallaproducto.IdProducto = pro.CodProducto;
                    tallaproducto.IdAlmacen = frmLogin.iCodAlmacen;
                    admtallaprod.Insert(tallaproducto);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido el siguiente error " + ex.ToString(), "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void CargaGrupo()
        {
            try
            {
                cbCategoria.DataSource = admGrupo.MuestraGrupos(Convert.ToInt32(cbLinea.SelectedValue));
                cbCategoria.DisplayMember = "descripcion";
                cbCategoria.ValueMember = "codGrupo";
                cbCategoria.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("hubo un error..." + ex.Message);
            }
        }

        private void LimpiarCalzado()
        {
            dgvTalla.Rows.Clear();
            dgvTalla.Refresh();
            
        }

        private void cmbTipoTalla_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cbLinea.SelectedValue) != 0 && cbSeriePro.SelectedIndex != -1 && cmbTipoTalla.SelectedIndex != -1)
                {
                    ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
                    btnAddTalla.Enabled = true;
                    ProcessTabKey(true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido el siguiente error " + ex.ToString(), "Mensaj", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteTalla_Click(object sender, EventArgs e)
        {
            if (dgvTalla.Rows.Count > 0)
            {
                clsTallaProducto tallaproducto = new clsTallaProducto();
                tallaproducto.IdTalla = Convert.ToInt32(dgvTalla.Rows[dgvTalla.CurrentRow.Index].Cells[codtalla1.Name].Value);
                tallaproducto.IdProducto = pro.CodProducto;
                admtallaprod.Delete(tallaproducto);
                dgvTalla.Rows.RemoveAt(dgvTalla.CurrentRow.Index);
            }
        }

        private void cbLinea_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*e.KeyChar = char.ToUpper(e.KeyChar);
            cbLinea.Text.ToUpper();*/
        }

        private void cbLinea_KeyDown(object sender, KeyEventArgs e)
        {
            /*if (e.KeyCode == Keys.Enter)
            {
                ProcessTabKey(true);
            }*/
        }

        private void cbSeriePro_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //LimpiarCalzado();
            //try
            //{
            //    string nombre = cbLinea.GetItemText(cbLinea.SelectedItem);
            //    if (nombre.Equals("CALZADO"))
            //    {

            //        ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //        btnAddTalla.Enabled = true;
            //        btnAddTalla.PerformClick();


            //    }else if(nombre.Equals("ACCESORIOS"))
            //    {
            //        btnAddTalla.Enabled = false;
            //    }

            //    //switch (Convert.ToInt32(cbLinea.SelectedValue))
            //    //{

            //    //    case 1:

            //    //        break;
            //    //    case 8:
            //    //        ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //    //        btnAddTalla.Enabled = true;
            //    //        btnAddTalla.PerformClick();
            //    //        break;
            //    //    case 12:
            //    //        ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //    //        btnAddTalla.Enabled = true;
            //    //        btnAddTalla.PerformClick();
            //    //        break;
            //    //    case 13:
            //    //        ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //    //        btnAddTalla.Enabled = true;
            //    //        btnAddTalla.PerformClick();
            //    //        break;
            //    //    case 18:
            //    //        ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //    //        btnAddTalla.Enabled = true;
            //    //        btnAddTalla.PerformClick();
            //    //        break;
            //    //    case 19:
            //    //        ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
            //    //        btnAddTalla.Enabled = true;
            //    //        btnAddTalla.PerformClick();
            //    //        break;
            //    //    default:
            //    //        break;
            //    //}

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Se ha producido el siguiente error " + ex.ToString(), "Mensaj", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void btnLinea_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmLineas"] != null)
            {
                Application.OpenForms["frmLineas"].Activate();
            }
            else
            {
                frmLineas frm = new frmLineas();
                frm.fam = admFamilia.CargaFamilia(Convert.ToInt32(cbFamilia.SelectedValue));
                frm.ShowDialog();
                
                CargarLinea();
                
            }
        }

        private void btnFamilia_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmFamilias"] != null)
            {
                Application.OpenForms["frmFamilias"].Activate();
            }
            else
            {
                frmFamilias form = new frmFamilias();
                //form.MdiParent = this;
                form.ShowDialog();
                CargaFamilias();
            }
        }

        private void btnMarca_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmMarcas"] != null)
                {
                    Application.OpenForms["frmMarcas"].Activate();
                }
                else
                {
                    frmMarcas form = new frmMarcas();
                    //form.MdiParent = this;
                    form.ShowDialog();
                    CargaMarcas();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void cbSeriePro_KeyDown(object sender, KeyEventArgs e)
        {
           /* if (e.KeyCode == Keys.Return)
            {
                ProcessTabKey(true);
            }*/
        }

        private void cbSeriePro_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*e.KeyChar = char.ToUpper(e.KeyChar);
            cbSeriePro.Text.ToUpper();*/
        }

        

        private bool validacionesCaracteristicas()
        {
            Boolean estado = true;
            if (Convert.ToInt32(cbLinea.SelectedValue) == 1) //calzado
            {
                if (cbMarca.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbMarca.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;
                }

                if (cbFamilia.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbFamilia.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                if (cbSeriePro.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbSeriePro.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                if (cbTipoArticulo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbTipoArticulo.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

               

                if (cbCorte.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbCorte.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                if (cbModelo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbModelo.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                if (cbColorPrimario.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbColorPrimario.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;
                }

                if (cmbTipoTalla.SelectedValue != null)
                {
                    if (Convert.ToInt32(cmbTipoTalla.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;
                }
            }

            
            
            return estado;
        }

        

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (superValidator1.Validate())
                {
                    pro.Codtalla = Convert.ToInt32(cmbTallasProducto.SelectedValue);
                    pro.valorTalla = cmbTallasProducto.GetItemText(cmbTallasProducto.SelectedItem);
                    pro.CodUsuario = frmLogin.iCodUser;
                    pro.CodLinea = Convert.ToInt32(cbLinea.SelectedValue);
                    pro.CodFamilia = Convert.ToInt32(cbFamilia.SelectedValue);
                    //pro.codSerieProducto = Convert.ToInt32(cbSeriePro.SelectedValue);
                    pro.CodTipoArticulo = Convert.ToInt32(cbTipoArticulo.SelectedValue);
                    pro.CodMarca = Convert.ToInt32(cbMarca.SelectedValue);
                    pro.codModelo = Convert.ToInt32(cbModelo.SelectedValue);
                    pro.codCorte = Convert.ToInt32(cbCorte.SelectedValue);
                    //pro.codTaco = Convert.ToInt32(cbTaco.SelectedValue);
                    pro.codColorPrimario = Convert.ToInt32(cbColorPrimario.SelectedValue);
                    pro.codColorSecundario = Convert.ToInt32(cbColorSecundario.SelectedValue);
                    pro.CodUnidadMedida = Convert.ToInt32(cbUnidadBase.SelectedValue);
                    pro.CodControlStock = Convert.ToInt32(cbControlStock.SelectedValue);
                    //pro.TipoTalla = Convert.ToInt32(cmbTipoTalla.SelectedValue);
                    pro.Referencia = txtReferencia.Text;
                    pro.Descripcion = txtNombre.Text;
                    pro.Igv = cbIgv.Checked;
                    pro.ConIgv = cbIgv.Checked;
                    pro.Estado = cbEstado.Checked;
                    pro.CodTemporada = Convert.ToInt32(cbTemporada.SelectedValue);
                    pro.CodTipoCierre = 3;
                    pro.CodGrupo = Convert.ToInt32(cbCategoria.SelectedValue);
                    pro.CodCuello = 0;
                    pro.CodManga = 0;
                    String[] DetCodProducto = new String[3];
                    DetCodProducto[0] = txtDetCodProd1.Text.Trim();
                    DetCodProducto[1] = txtDetCodProd2.Text.Trim();
                    DetCodProducto[2] = txtDetCodProd3.Text.Trim();
                   // pro.Tallas = new List<clsTalla>();
                    int cont = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        if (DetCodProducto[i] != "") { cont++; }
                    }

                    
                    if (cbIgv.Checked)
                    {
                        pro.Igv = true;
                    }
                    else
                    {
                        pro.Igv = false;
                    }
                    pro.CodDise�o = 0;
                    pro.CodTipoDise�o = 0;
                    pro.Annio = Convert.ToInt32(txtAnnio.Text);

                    if (txtPrecioCata.Text == "") { txtPrecioCata.Text = "0"; pro.PrecioCatalogo = 0; }
                    
                    if (txtPrecioCata.Text != "") { pro.PrecioCatalogo = Convert.ToDecimal(txtPrecioCata.Text); }
                    //if (txtPrecioCata.Text == "" && txtPrecioCompra.Text == "") { pro.PrecioCatalogo = 0; }
                    Boolean estadoxtalla = true;
                    if (Proceso == 1)
                    {
                        if (validacionescaracteristicas())
                        {
                            if (txtPrecioCata.Text != "")
                            {
                                if (Convert.ToInt32(cbLinea.SelectedValue) != -1)
                                {
                                    if (dgvTalla.Rows.Count > 0)
                                    {
                                        //pro.Tallas = new List<clsTalla>();
                                        pro.CodCompraProducto = txtCodCompraProducto.Text + "T" + pro.valorTalla;
                                        foreach (DataGridViewRow row in dgvTalla.Rows)
                                        {
                                            if (row.Cells[codtalla1.Name].Value != null)
                                            {
                                                //pro.valorTalla = row.Cells[codtalla1.Name].Value.ToString();
                                                talla = admTalla.DevolverCodTalla((String)row.Cells[codtalla1.Name].Value, Convert.ToInt32(cmbTipoTalla.SelectedValue));
                                                pro.Tallas.Add(talla);
                                                //pro.Codtalla = talla.codTalla;
                                            }
 
                                        }
                                            if (cont == 0)
                                            {
                                                int rows = admProducto.insert(pro);
                                                if(rows>0)
                                                {
                                                    btnGuardar.Enabled = false;
                                                    linkConfiguraUnidadesEquivalentes.Visible = true;
                                                    txtCodProducto.Text = pro.CodProducto.ToString();
                                                    //pro = admProducto.CargaReferenciaProducto(pro.CodProducto);
                                                    //txtReferencia.Text = pro.Referencia;
                                                    //txtNombre.Text = pro.Descripcion;
                                                    if (pbFoto.Image != null)
                                                    {
                                                        GuardaFotografia();
                                                    }
                                                    GuardaUnidadesCompra();
                                                    GuardaUnidadesVenta();
                                                    GuardaUnidadesEquivalencia();

                                                    //foreach (clsTalla t in pro.Tallas)
                                                    //{
                                                    //    clsTallaProducto tallaproducto = new clsTallaProducto();
                                                    //    tallaproducto.IdTalla = t.codTalla;
                                                    //    tallaproducto.IdProducto = pro.CodProducto;
                                                    //    tallaproducto.IdAlmacen = frmLogin.iCodAlmacen;
                                                    //    admtallaprod.Insert(tallaproducto);
                                                    //}
                                                }
                                                else
                                                {
                                                    estadoxtalla = false;
                                                }
                                            }
                                            else 
                                            {
                                                if (admProducto.insertDetCodProducto(pro,DetCodProducto))
                                                {

                                                    btnGuardar.Enabled = false;
                                                    linkConfiguraUnidadesEquivalentes.Visible = true;
                                                    txtCodProducto.Text = pro.CodProducto.ToString();
                                                    //pro = admProducto.CargaReferenciaProducto(pro.CodProducto);
                                                    //txtReferencia.Text = pro.Referencia;
                                                    //txtNombre.Text = pro.Descripcion;
                                                    if (pbFoto.Image != null)
                                                    {
                                                        GuardaFotografia();
                                                    }
                                                    GuardaUnidadesCompra();
                                                    GuardaUnidadesVenta();
                                                    GuardaUnidadesEquivalencia();

                                                    //foreach (clsTalla t in pro.Tallas)
                                                    //{
                                                    //    clsTallaProducto tallaproducto = new clsTallaProducto();
                                                    //    tallaproducto.IdTalla = t.codTalla;
                                                    //    tallaproducto.IdProducto = pro.CodProducto;
                                                    //    tallaproducto.IdAlmacen = frmLogin.iCodAlmacen;
                                                    //    admtallaprod.Insert(tallaproducto);
                                                    //}
                                                }
                                                else
                                                {
                                                    estadoxtalla = false;
                                                }
                                            
                                            }

                                        
                                    }
                                   
                                    if (dgvTalla.Rows.Count <= 0)
                                    {
                                        pro.CodCompraProducto = txtCodCompraProducto.Text;

                                        if (cont == 0)
                                        {
                                            int rows = admProducto.insert(pro);
                                            if (rows>0)
                                            {

                                                btnGuardar.Enabled = false;
                                                linkConfiguraUnidadesEquivalentes.Visible = true;
                                                txtCodProducto.Text = pro.CodProducto.ToString();
                                                //pro = admProducto.CargaReferenciaProducto(pro.CodProducto);
                                                //txtReferencia.Text = pro.Referencia;
                                                //txtNombre.Text = pro.Descripcion;
                                                if (pbFoto.Image != null)
                                                {
                                                    GuardaFotografia();
                                                }
                                                GuardaUnidadesCompra();
                                                GuardaUnidadesVenta();
                                                GuardaUnidadesEquivalencia();
                                            }
                                            else
                                            {
                                                estadoxtalla = false;
                                            }
                                        }
                                        else
                                        {
                                            if (admProducto.insertDetCodProducto(pro, DetCodProducto))
                                            {

                                                btnGuardar.Enabled = false;
                                                linkConfiguraUnidadesEquivalentes.Visible = true;
                                                txtCodProducto.Text = pro.CodProducto.ToString();
                                                //pro = admProducto.CargaReferenciaProducto(pro.CodProducto);
                                                //txtReferencia.Text = pro.Referencia;
                                                //txtNombre.Text = pro.Descripcion;
                                                if (pbFoto.Image != null)
                                                {
                                                    GuardaFotografia();
                                                }
                                                GuardaUnidadesCompra();
                                                GuardaUnidadesVenta();
                                                GuardaUnidadesEquivalencia();
                                            }
                                            else
                                            {
                                                estadoxtalla = false;
                                            }

                                        }                                      
                                    }

                                    if (estadoxtalla)
                                    {
                                        MessageBox.Show("Los Datos se guardaron correctamente", "Gestion Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    else
                                    {
                                    }
                                }
                                else
                                {
                                    //es otro insert de producto
                                }
                            }
                        }

                        else
                        {
                            MessageBox.Show("Complete Los datos");
                        }
                    }
                    if (Proceso == 2)
                    {
                        pro.CodProducto = Convert.ToInt32(txtCodProducto.Text);
                        pro.CodCompraProducto = txtCodCompraProducto.Text;
                        //if (Convert.ToInt32(cbLinea.SelectedValue) == 1)
                        //{
                        //    foreach (DataGridViewRow row in dgvTalla.Rows)
                        //    {
                        //        if (row.Cells[codtalla1.Name].Value != null)
                        //        {
                        //            //pro.valorTalla = row.Cells[codtalla1.Name].Value.ToString();
                        //            //talla = admTalla.DevolverCodTalla(pro.valorTalla, Convert.ToInt32(cmbTipoTalla.SelectedValue));
                        //            //pro.Codtalla = talla.codTalla;
                        //            talla = admTalla.DevolverCodTalla((String)row.Cells[codtalla1.Name].Value, Convert.ToInt32(cmbTipoTalla.SelectedValue));
                        //            pro.Tallas.Add(talla);
                        //        }
                        //        else
                        //        {
                        //            MessageBox.Show("Seleccione Talla...");
                        //            return;
                        //        }
                        //    }
                        //}
                        int rows = admProducto.update(pro);
                        if (rows>0)
                        {
                            if (cont == 0)
                            {
                                if (admProducto.DeleteDetCodProducto(pro))
                                {
                                    if (pbFoto.Image != null)
                                    {
                                        GuardaFotografia();
                                    }

                                    MessageBox.Show("Los datos se guardaron correctamente", "Gestion Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    this.Close();
                                }

                            }
                            else
                            {
                                if (admProducto.updateDetCodProducto(pro,DetCodProducto))
                                {
                                    if (pbFoto.Image != null)
                                    {
                                        GuardaFotografia();
                                    }
                                    
                                    MessageBox.Show("Los datos se guardaron correctamente", "Gestion Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    this.Close();
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Hubo un error al actualizar...");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardaUnidadesEquivalencia()
        {
            equi.CodProducto = Convert.ToInt32(txtCodProducto.Text);
            equi.CodEquivalente = Convert.ToInt32(cbUnidadBase.SelectedValue);
            equi.Precio = 0;
            equi.Factor = 1;
            equi.CodUser = frmLogin.iCodUser;
            equi.Tipo = Convert.ToInt32(9);
            equi.CodAlmacen = frmLogin.iCodAlmacen;
            equi.CompraVenta = 2;            
            equi.ICodMoneda = Convert.ToInt32(1);
            if (admProducto.insertunidadequivalente(equi))
            {                                
            }
        }

        private void GuardaUnidadesVenta()
        {
            equi.CodUnidad = Convert.ToInt32(cbUnidadBase.SelectedValue);            
            equi.CodProducto = Convert.ToInt32(txtCodProducto.Text);
            equi.Tipo = Convert.ToInt32(10);
            equi.Precio = Convert.ToDecimal(txtPrecioVen.Text);
            equi.CodUser = frmLogin.iCodUser;
            equi.CodAlmacen = frmLogin.iCodAlmacen;
            equi.CompraVenta = 1;            
            equi.ICodMoneda = Convert.ToInt32(1);         
            if (admProducto.insertunidadequivalente(equi))
            {                
                
            }
        }

        private void GuardaUnidadesCompra()
        {
            equi.CodUnidad = Convert.ToInt32(cbUnidadBase.SelectedValue);            
            equi.CodProducto = Convert.ToInt32(txtCodProducto.Text);
            equi.Precio = Convert.ToDecimal(txtPrecioCom.Text);
            equi.CodUser = frmLogin.iCodUser;
            equi.Tipo = 9;
            equi.CodAlmacen = frmLogin.iCodAlmacen;
            equi.CompraVenta = 0;            
            equi.ICodMoneda = 1;
            if (admProducto.insertunidadequivalente(equi))
            {
            }
        }

        private void GuardaFotografia()
        {
            entfotografia.Codrelacion = Convert.ToInt32(txtCodProducto.Text);
            entfotografia.CodAlmacen = frmLogin.iCodAlmacen;
            if (pbFoto.Image != null)
            {
                entfotografia.Fotografia = pbFoto.Image;
            }
            else
            {
                entfotografia.Fotografia = null;
            }
            entfotografia.Codusuario = frmLogin.iCodUser;
            if (Proceso != 0)
            {
                if (Proceso == 1)
                {
                    if (admfotografia.AgregarFotografia(entfotografia))
                    {
                        
                    }
                }
                else if (Proceso == 2)
                {
                    if (admfotografia.ActualizarFotografia(entfotografia))
                    {
                        
                    }
                }
            }
        }

        private bool validacionescaracteristicas()
        {
            Boolean estado = true;
            if (Convert.ToInt32(cbLinea.SelectedValue) == 1)
            {
                if (cbMarca.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbMarca.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;
                }

                if (cbFamilia.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbFamilia.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                //if (cbSeriePro.SelectedValue != null)
                //{
                //    if (Convert.ToInt32(cbSeriePro.SelectedValue) == 0)
                //    {
                //        estado = false;
                //    }
                //    else
                //    {
                //        estado = true;
                //    }
                //}
                //else
                //{
                //    estado = false;

                //}

                if (cbTipoArticulo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbTipoArticulo.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                

                if (cbCorte.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbCorte.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                if (cbModelo.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbModelo.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;

                }

                if (cbColorPrimario.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbColorPrimario.SelectedValue) == 0)
                    {
                        estado = false;
                    }
                    else
                    {
                        estado = true;
                    }
                }
                else
                {
                    estado = false;
                }


                

                //if (cmbTipoTalla.SelectedValue != null)
                //{
                //    if (Convert.ToInt32(cmbTipoTalla.SelectedValue) == 0)
                //    {
                //        estado = false;
                //    }
                //    else
                //    {
                //        estado = true;
                //    }
                //}
                //else
                //{
                //    estado = false;
                //}
            }
            return estado;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            /*if (txtPrecioCata.Text != "")
            {
                if (cbIgv.Checked == true)
                {
                    double precio = Convert.ToDouble(txtPrecioCata.Text);
                    double precioxigv = Convert.ToDouble(precio * 0.18);
                    double suma = precio + precioxigv;
                    txtPrecioCompra.Text = suma.ToString();
                }
                if (cbIgv.Checked == false)
                {
                    txtPrecioCompra.Text = txtPrecioCata.Text;
                }
            }
            else
            {
                MessageBox.Show("Ingrese Precio Catalago");
            }*/
        }

        //para que me selleccione el combo dentro del datagridview
       

        private void LastColumnComboSelectionChanged(object sender, EventArgs e)
        {
            //var currentcell = dgvTalla.CurrentCellAddress;
            //var sendingCB = sender as DataGridViewComboBoxEditingControl;
            //DataGridViewTextBoxCell cel = (DataGridViewTextBoxCell)dgvTalla.Rows[currentcell.Y].Cells[0];
            //cel.Value = sendingCB.EditingControlFormattedValue.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmModelo"] != null)
                {
                    Application.OpenForms["frmModelo"].Activate();
                }
                else
                {
                    frmModelo modelo = new frmModelo();
                    modelo.ShowDialog();
                    CargaModelo();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnTipoArticulo_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmTipoArticulos"] != null)
            {
                Application.OpenForms["frmTipoArticulos"].Activate();
            }
            else
            {
                frmTipoArticulos frm = new frmTipoArticulos();
                frm.ShowDialog();

                CargaTipoArticulos();

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTaco"] != null)
                {
                    Application.OpenForms["frmTaco"].Activate();
                }
                else
                {
                    frmTaco frm = new frmTaco();
                    frm.ShowDialog();
                    CargarTaco();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnUnidad_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmUnidades"] != null)
                {
                    Application.OpenForms["frmUnidades"].Activate();
                }
                else
                {
                    frmUnidades frm = new frmUnidades();
                    frm.ShowDialog();
                    CargarUnidad();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cargar unidades..." + ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void cbFamilia_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                CargarLinea();
                cbLinea.Enabled = true;
                cbCategoria.DataSource=null;
                cbCategoria.Items.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al seleccionar familia..." + ex);
            }
        }

        private void linkConfiguraUnidadesEquivalentes_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                frmUnidadEquivalente frm = new frmUnidadEquivalente();
                frm.codProd = Convert.ToInt32(txtCodProducto.Text);
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
            }
        }

        private void txtPrecioCata_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtCodProducto_TextChanged(object sender, EventArgs e)
        {
            if (txtCodProducto.Text != "")
            {
                
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["frmTipoCierre"] != null)
                {
                    Application.OpenForms["frmTipoCierre"].Activate();
                }
                else
                {
                    frmTipoCierre form = new frmTipoCierre();
                    //form.MdiParent = this;
                    form.ShowDialog();
                    cargaTipoCierre();
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void cbLinea_SelectedValueChanged(object sender, EventArgs e)
        {          
            
        }      
        

        private void button7_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmCuello"] != null)
            {
                Application.OpenForms["frmCuello"].Activate();
            }
            else
            {
                frmCuello form = new frmCuello();
                //form.MdiParent = this;
                form.ShowDialog();
                
            }
        }


        private void linkGuardarImagen_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmGuardaImagenProducto frm = new frmGuardaImagenProducto();
            frm.codProd = Convert.ToInt32(txtCodProducto.Text);
            frm.Proceso = 1;
            frm.ShowDialog();
        }

        private void btnAdd_Fotografia_Click(object sender, EventArgs e)
        {
            try
            {
                //flag_foto = true;
                OpenFileDialog BuscarImagen = new OpenFileDialog();
                BuscarImagen.Filter = "Archivos de Imagen|*.jpg";
                BuscarImagen.FileName = "";
                BuscarImagen.Title = "Titulo del Dialog";
                BuscarImagen.InitialDirectory = "D:\\";
                if (BuscarImagen.ShowDialog() == DialogResult.OK)
                {
                    String direccion = BuscarImagen.FileName;
                    pbFoto.ImageLocation = direccion;
                    pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDel_Fotografia_Click(object sender, EventArgs e)
        {
            try
            {
                pbFoto.Image = null;
                if (txtCodProducto.Text != "")
                {                    
                    if (admfotografia.EliminarFotografia(Convert.ToInt32(txtCodProducto.Text), Convert.ToInt32(frmLogin.iCodAlmacen)))
                    {
                        MessageBox.Show("La Fotograf�a se elimin� correctamente", "Gestion Relaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnGuardar.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbFamilia_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void cbFamilia_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void cbLinea_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //CargaTipoTalla();
            CargaGrupo();

            CargaTalla();
  
        }

        private void CargaTalla()
        {
            if (cbLinea.SelectedIndex != -1)
            {
                int linea = Convert.ToInt32(cbLinea.SelectedValue);

                cmbTallasProducto.DataSource = admtallas.ListaTallaporLinea(linea, 1, 100, 0);
                cmbTallasProducto.DisplayMember = "nombre";
                cmbTallasProducto.ValueMember = "codTalla";
                cmbTallasProducto.SelectedIndex = -1;
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmGrupo"] != null)
            {
                Application.OpenForms["frmGrupo"].Activate();
            }
            else
            {
                frmGrupo form = new frmGrupo();
                form.lin.CodLinea = Convert.ToInt32(cbLinea.SelectedValue); 
                //form.MdiParent = this;
                form.ShowDialog();
                CargaGrupo();

            }
        }

        private void cbCategoria_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbCategoria.Text == "MOCHILAS")
            {
                grpCalzado.Enabled = false;
            }
        }

        private void dgvTalla_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //if (dgvTalla.CurrentCell.ColumnIndex == 1 && e.Control is ComboBox)
            //{
            //    ComboBox comboBox = e.Control as ComboBox;
            //    //comboBox.SelectedIndexChanged += LastColumnComboSelectionChanged;
            //    comboBox.SelectedValueChanged += LastColumnComboSelectionChanged;
            //}
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmSerieProducto"] != null)
            {
                Application.OpenForms["frmSerieProducto"].Activate();
            }
            else
            {
                frmSerieProducto frm = new frmSerieProducto();
                frm.ShowDialog();
                CargaSerieProducto();
            }
        }

        private void txtDetCodProd1_KeyPress(object sender, KeyPressEventArgs e)
        {
                if(e.KeyChar==13 && txtDetCodProd1.Text != "")
                {
                    txtDetCodProd2.Enabled = true;
                    txtDetCodProd2.Focus();
                }

        }

        private void txtDetCodProd1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDetCodProd2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 && txtDetCodProd2.Text != "" && txtDetCodProd1.Text != "")
            {
                txtDetCodProd3.Enabled = true;
                txtDetCodProd3.Focus();
            }
        }

        private void cbSeriePro_SelectedValueChanged(object sender, EventArgs e)
        {
            LimpiarCalzado();
            try
            {
                string nombre = cbLinea.GetItemText(cbLinea.SelectedItem);
                if (nombre.Equals("CALZADO"))
                {

                    //ser = admSerieP.ListaTallaxSerie(Convert.ToInt32(cbSeriePro.SelectedValue));
                    //btnAddTalla.Enabled = true;
                    //cargaCmbTallas();
                    //cargaTallasxproducto();

                }
                else if (nombre.Equals("ACCESORIOS"))
                {
                    btnAddTalla.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmMaterial"] != null)
            {
                Application.OpenForms["frmMaterial"].Activate();
            }
            else
            {
                frmMaterial form = new frmMaterial();
                //form.MdiParent = this;
                form.ShowDialog();
                CargarCorte();

            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            
        }

        private void btnColorp_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmColorPrimario"] != null)
            {
                Application.OpenForms["frmColorPrimario"].Activate();
            }
            else
            {
                frmColorPrimario form = new frmColorPrimario();
                //form.MdiParent = this;
                form.ShowDialog();
                CargarColorPrimario();

            }
        }

        private void btnColors_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmColorSecundario"] != null)
            {
                Application.OpenForms["frmColorSecundario"].Activate();
            }
            else
            {
                frmColorSecundario form = new frmColorSecundario();
                //form.MdiParent = this;
                form.ShowDialog();
                CargarColorSecundario();

            }
        }
    }
}