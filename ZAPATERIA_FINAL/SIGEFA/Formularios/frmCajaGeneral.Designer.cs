﻿namespace SIGEFA.Formularios
{
    partial class frmCajaGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCajaGeneral));
            this.dgvMovimientos = new Telerik.WinControls.UI.RadGridView();
            this.dtpFinicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.dtpFfin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.lbldep = new Telerik.WinControls.UI.RadLabel();
            this.lblndep = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.btnBuscar = new Telerik.WinControls.UI.RadButton();
            this.btnReporte = new Telerik.WinControls.UI.RadButton();
            this.btnAccion = new Telerik.WinControls.UI.RadButton();
            this.lblsaldo = new Telerik.WinControls.UI.RadLabel();
            this.lblgasto = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbldep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblndep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblsaldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgasto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMovimientos
            // 
            this.dgvMovimientos.AutoSizeRows = true;
            this.dgvMovimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMovimientos.GroupExpandAnimationType = Telerik.WinControls.UI.GridExpandAnimationType.Slide;
            this.dgvMovimientos.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.dgvMovimientos.MasterTemplate.AllowAddNewRow = false;
            this.dgvMovimientos.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "idcgeneral";
            gridViewTextBoxColumn1.HeaderText = "idGeneral";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "idcgeneral";
            gridViewTextBoxColumn1.Width = 70;
            gridViewTextBoxColumn2.FieldName = "idalmacen";
            gridViewTextBoxColumn2.HeaderText = "idAlmacen";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "idalmacen";
            gridViewTextBoxColumn3.FieldName = "almacen";
            gridViewTextBoxColumn3.HeaderText = "Origen";
            gridViewTextBoxColumn3.Name = "almacen";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 125;
            gridViewTextBoxColumn4.FieldName = "fecharegistro";
            gridViewTextBoxColumn4.HeaderText = "Fecha Registro";
            gridViewTextBoxColumn4.Name = "fecharegistro";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 123;
            gridViewTextBoxColumn5.FieldName = "monto";
            gridViewTextBoxColumn5.HeaderText = "Monto";
            gridViewTextBoxColumn5.Name = "monto";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 132;
            gridViewTextBoxColumn6.FieldName = "nombre";
            gridViewTextBoxColumn6.HeaderText = "Nombre";
            gridViewTextBoxColumn6.Name = "nombre";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 146;
            gridViewTextBoxColumn7.FieldName = "dni";
            gridViewTextBoxColumn7.HeaderText = "DNI";
            gridViewTextBoxColumn7.Name = "dni";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 83;
            gridViewTextBoxColumn8.FieldName = "usuario";
            gridViewTextBoxColumn8.HeaderText = "Usuario";
            gridViewTextBoxColumn8.Name = "usuario";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 103;
            gridViewTextBoxColumn9.FieldName = "tipo";
            gridViewTextBoxColumn9.HeaderText = "Tipo";
            gridViewTextBoxColumn9.Name = "tipo";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 103;
            gridViewTextBoxColumn10.FieldName = "tipomov";
            gridViewTextBoxColumn10.HeaderText = "Tipo Mov.";
            gridViewTextBoxColumn10.Name = "tipomov";
            gridViewTextBoxColumn10.Width = 78;
            gridViewTextBoxColumn11.FieldName = "depositado";
            gridViewTextBoxColumn11.HeaderText = "Depositado?";
            gridViewTextBoxColumn11.Name = "depositado";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn11.Width = 120;
            this.dgvMovimientos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.dgvMovimientos.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgvMovimientos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgvMovimientos.Name = "dgvMovimientos";
            this.dgvMovimientos.ReadOnly = true;
            this.dgvMovimientos.Size = new System.Drawing.Size(1013, 323);
            this.dgvMovimientos.TabIndex = 0;
            this.dgvMovimientos.Text = "radGridView1";
            this.dgvMovimientos.ThemeName = "Material";
            // 
            // dtpFinicio
            // 
            this.dtpFinicio.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFinicio.Location = new System.Drawing.Point(780, 21);
            this.dtpFinicio.Name = "dtpFinicio";
            this.dtpFinicio.Size = new System.Drawing.Size(146, 36);
            this.dtpFinicio.TabIndex = 1;
            this.dtpFinicio.TabStop = false;
            this.dtpFinicio.Text = "02/07/2019";
            this.dtpFinicio.ThemeName = "Material";
            this.dtpFinicio.Value = new System.DateTime(2019, 7, 2, 12, 56, 54, 846);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(689, 29);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(85, 21);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Fecha Inicio";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(705, 72);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(69, 21);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Fecha Fin";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            // 
            // dtpFfin
            // 
            this.dtpFfin.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFfin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFfin.Location = new System.Drawing.Point(780, 65);
            this.dtpFfin.Name = "dtpFfin";
            this.dtpFfin.Size = new System.Drawing.Size(145, 36);
            this.dtpFfin.TabIndex = 3;
            this.dtpFfin.TabStop = false;
            this.dtpFfin.Text = "02/07/2019";
            this.dtpFfin.ThemeName = "Material";
            this.dtpFfin.Value = new System.DateTime(2019, 7, 2, 12, 56, 54, 846);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.radLabel3.Location = new System.Drawing.Point(432, 21);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(116, 23);
            this.radLabel3.TabIndex = 8;
            this.radLabel3.Text = "CAJA GENERAL";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel3.ThemeName = "Material";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.dgvMovimientos);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 177);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1017, 343);
            this.radGroupBox1.TabIndex = 9;
            this.radGroupBox1.ThemeName = "Material";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radGroupBox2.Controls.Add(this.radLabel10);
            this.radGroupBox2.Controls.Add(this.lblsaldo);
            this.radGroupBox2.Controls.Add(this.lbldep);
            this.radGroupBox2.Controls.Add(this.lblgasto);
            this.radGroupBox2.Controls.Add(this.lblndep);
            this.radGroupBox2.Controls.Add(this.radLabel8);
            this.radGroupBox2.Controls.Add(this.radLabel5);
            this.radGroupBox2.Controls.Add(this.radLabel9);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.dtpFinicio);
            this.radGroupBox2.Controls.Add(this.radLabel1);
            this.radGroupBox2.Controls.Add(this.btnBuscar);
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.btnReporte);
            this.radGroupBox2.Controls.Add(this.btnAccion);
            this.radGroupBox2.Controls.Add(this.dtpFfin);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox2.HeaderText = "";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(1017, 171);
            this.radGroupBox2.TabIndex = 10;
            this.radGroupBox2.ThemeName = "Material";
            // 
            // lbldep
            // 
            this.lbldep.Location = new System.Drawing.Point(154, 92);
            this.lbldep.Name = "lbldep";
            this.lbldep.Size = new System.Drawing.Size(52, 21);
            this.lbldep.TabIndex = 6;
            this.lbldep.Text = "0.0000";
            this.lbldep.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lbldep.ThemeName = "Material";
            // 
            // lblndep
            // 
            this.lblndep.Location = new System.Drawing.Point(154, 65);
            this.lblndep.Name = "lblndep";
            this.lblndep.Size = new System.Drawing.Size(52, 21);
            this.lblndep.TabIndex = 5;
            this.lblndep.Text = "0.0000";
            this.lblndep.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lblndep.ThemeName = "Material";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(15, 92);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(121, 21);
            this.radLabel5.TabIndex = 4;
            this.radLabel5.Text = "Total Depositado:";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel5.ThemeName = "Material";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 65);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(124, 21);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Total S/Depositar:";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel4.ThemeName = "Material";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Image = global::SIGEFA.Properties.Resources.buscar24;
            this.btnBuscar.Location = new System.Drawing.Point(580, 120);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(109, 36);
            this.btnBuscar.TabIndex = 5;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.ThemeName = "Material";
            this.btnBuscar.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // btnReporte
            // 
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.Location = new System.Drawing.Point(853, 120);
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(114, 36);
            this.btnReporte.TabIndex = 6;
            this.btnReporte.Text = "Copiar";
            this.btnReporte.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReporte.ThemeName = "Material";
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnAccion
            // 
            this.btnAccion.Image = global::SIGEFA.Properties.Resources.operaciones24;
            this.btnAccion.Location = new System.Drawing.Point(705, 120);
            this.btnAccion.Name = "btnAccion";
            this.btnAccion.Size = new System.Drawing.Size(132, 36);
            this.btnAccion.TabIndex = 7;
            this.btnAccion.Text = "Transferir";
            this.btnAccion.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAccion.ThemeName = "Material";
            this.btnAccion.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // lblsaldo
            // 
            this.lblsaldo.Location = new System.Drawing.Point(335, 92);
            this.lblsaldo.Name = "lblsaldo";
            this.lblsaldo.Size = new System.Drawing.Size(52, 21);
            this.lblsaldo.TabIndex = 10;
            this.lblsaldo.Text = "0.0000";
            this.lblsaldo.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lblsaldo.ThemeName = "Material";
            // 
            // lblgasto
            // 
            this.lblgasto.Location = new System.Drawing.Point(155, 124);
            this.lblgasto.Name = "lblgasto";
            this.lblgasto.Size = new System.Drawing.Size(52, 21);
            this.lblgasto.TabIndex = 9;
            this.lblgasto.Text = "0.0000";
            this.lblgasto.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lblgasto.ThemeName = "Material";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(268, 92);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(48, 21);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "Saldo:";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel8.ThemeName = "Material";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(15, 124);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(93, 21);
            this.radLabel9.TabIndex = 7;
            this.radLabel9.Text = "Total Gastos:";
            this.radLabel9.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel9.ThemeName = "Material";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(10, 107);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(201, 21);
            this.radLabel10.TabIndex = 9;
            this.radLabel10.Text = "-----------------------------------------";
            this.radLabel10.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel10.ThemeName = "Material";
            // 
            // frmCajaGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1017, 520);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.MaximizeBox = false;
            this.Name = "frmCajaGeneral";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Caja General";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmCajaGeneral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbldep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblndep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblsaldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgasto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView dgvMovimientos;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFinicio;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFfin;
        private Telerik.WinControls.UI.RadButton btnBuscar;
        private Telerik.WinControls.UI.RadButton btnReporte;
        private Telerik.WinControls.UI.RadButton btnAccion;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel lbldep;
        private Telerik.WinControls.UI.RadLabel lblndep;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel lblsaldo;
        private Telerik.WinControls.UI.RadLabel lblgasto;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
    }
}
