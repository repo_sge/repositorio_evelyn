﻿namespace SIGEFA.Formularios
{
    partial class frmEstatusCtas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.windows8Theme1 = new Telerik.WinControls.Themes.Windows8Theme();
            this.dgvCtas = new Telerik.WinControls.UI.RadGridView();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.VisualStudio2012LightTheme();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCtas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCtas.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCtas
            // 
            this.dgvCtas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvCtas.Location = new System.Drawing.Point(0, 67);
            // 
            // 
            // 
            this.dgvCtas.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dgvCtas.MasterTemplate.AllowAddNewRow = false;
            this.dgvCtas.MasterTemplate.AllowDragToGroup = false;
            this.dgvCtas.MasterTemplate.AutoGenerateColumns = false;
            this.dgvCtas.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "codcuentacorriente";
            gridViewTextBoxColumn1.HeaderText = "idCta";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "codcuentacorriente";
            gridViewTextBoxColumn2.FieldName = "cuentacorriente";
            gridViewTextBoxColumn2.HeaderText = "Cuenta";
            gridViewTextBoxColumn2.Name = "cuentacorriente";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 175;
            gridViewTextBoxColumn3.FieldName = "saldo";
            gridViewTextBoxColumn3.HeaderText = "Saldo Actual";
            gridViewTextBoxColumn3.Name = "saldo";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 276;
            this.dgvCtas.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.dgvCtas.MasterTemplate.EnableGrouping = false;
            this.dgvCtas.MasterTemplate.ShowFilteringRow = false;
            this.dgvCtas.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgvCtas.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgvCtas.Name = "dgvCtas";
            this.dgvCtas.ReadOnly = true;
            this.dgvCtas.ShowGroupPanel = false;
            this.dgvCtas.Size = new System.Drawing.Size(451, 256);
            this.dgvCtas.TabIndex = 0;
            this.dgvCtas.Text = "radGridView1";
            this.dgvCtas.ThemeName = "Material";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.radLabel1.Location = new System.Drawing.Point(142, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(148, 25);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Cuentas Bancarias";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // radButton1
            // 
            this.radButton1.Image = global::SIGEFA.Properties.Resources.actualizar24;
            this.radButton1.Location = new System.Drawing.Point(372, 12);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(59, 36);
            this.radButton1.TabIndex = 2;
            this.radButton1.ThemeName = "Material";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // frmEstatusCtas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 323);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.dgvCtas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmEstatusCtas";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estado Cuentas";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmEstatusCtas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCtas.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCtas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Windows8Theme windows8Theme1;
        private Telerik.WinControls.UI.RadGridView dgvCtas;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.Themes.VisualStudio2012LightTheme visualStudio2012LightTheme1;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadButton radButton1;
    }
}
