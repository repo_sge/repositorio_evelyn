﻿namespace SIGEFA.Formularios
{
    partial class frmExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            this.telerikMetroTouchTheme1 = new Telerik.WinControls.Themes.TelerikMetroTouchTheme();
            this.dgproductos = new Telerik.WinControls.UI.RadGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblarchivo = new Telerik.WinControls.UI.RadLabel();
            this.btnOpenfile = new Telerik.WinControls.UI.RadButton();
            this.btnCarga = new Telerik.WinControls.UI.RadButton();
            this.rbcaballeros = new Telerik.WinControls.UI.RadRadioButton();
            this.rbdamas = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.cboHojas = new Telerik.WinControls.UI.RadDropDownList();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.waitingBarIndicatorElement1 = new Telerik.WinControls.UI.WaitingBarIndicatorElement();
            this.waitingBarIndicatorElement2 = new Telerik.WinControls.UI.WaitingBarIndicatorElement();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.rbInsert = new Telerik.WinControls.UI.RadRadioButton();
            this.rbUpdate = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rbStock = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.rbCompras = new Telerik.WinControls.UI.RadRadioButton();
            this.rbTransfer = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgproductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgproductos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblarchivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenfile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCarga)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbcaballeros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbdamas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHojas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbInsert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbStock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbCompras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgproductos
            // 
            this.dgproductos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgproductos.EnableCustomDrawing = true;
            this.dgproductos.Location = new System.Drawing.Point(0, 146);
            // 
            // 
            // 
            this.dgproductos.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dgproductos.MasterTemplate.AllowAddNewRow = false;
            this.dgproductos.MasterTemplate.AllowDragToGroup = false;
            this.dgproductos.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.dgproductos.MasterTemplate.EnableGrouping = false;
            this.dgproductos.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgproductos.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.dgproductos.Name = "dgproductos";
            this.dgproductos.ReadOnly = true;
            this.dgproductos.Size = new System.Drawing.Size(1029, 347);
            this.dgproductos.TabIndex = 0;
            this.dgproductos.Text = "radGridView1";
            this.dgproductos.ThemeName = "Material";
            this.dgproductos.RowPaint += new Telerik.WinControls.UI.GridViewRowPaintEventHandler(this.dgproductos_RowPaint);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblarchivo
            // 
            this.lblarchivo.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.lblarchivo.Location = new System.Drawing.Point(108, 80);
            this.lblarchivo.Name = "lblarchivo";
            this.lblarchivo.Size = new System.Drawing.Size(155, 23);
            this.lblarchivo.TabIndex = 1;
            this.lblarchivo.Text = "Seleccione archivo....";
            this.lblarchivo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblarchivo.ThemeName = "Material";
            // 
            // btnOpenfile
            // 
            this.btnOpenfile.Image = global::SIGEFA.Properties.Resources.folder24;
            this.btnOpenfile.ImageAlignment = System.Drawing.ContentAlignment.BottomLeft;
            this.btnOpenfile.Location = new System.Drawing.Point(12, 73);
            this.btnOpenfile.Name = "btnOpenfile";
            this.btnOpenfile.Size = new System.Drawing.Size(90, 36);
            this.btnOpenfile.TabIndex = 2;
            this.btnOpenfile.Text = "Abrir";
            this.btnOpenfile.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpenfile.ThemeName = "Material";
            this.btnOpenfile.Click += new System.EventHandler(this.btnOpenfile_Click);
            // 
            // btnCarga
            // 
            this.btnCarga.Enabled = false;
            this.btnCarga.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnCarga.Location = new System.Drawing.Point(858, 32);
            this.btnCarga.Name = "btnCarga";
            this.btnCarga.Size = new System.Drawing.Size(136, 36);
            this.btnCarga.TabIndex = 3;
            this.btnCarga.Text = "Cargar Datos";
            this.btnCarga.ThemeName = "Material";
            this.btnCarga.Click += new System.EventHandler(this.btnCarga_Click);
            // 
            // rbcaballeros
            // 
            this.rbcaballeros.Location = new System.Drawing.Point(92, 25);
            this.rbcaballeros.Name = "rbcaballeros";
            this.rbcaballeros.Size = new System.Drawing.Size(95, 22);
            this.rbcaballeros.TabIndex = 4;
            this.rbcaballeros.TabStop = false;
            this.rbcaballeros.Text = "Caballeros";
            this.rbcaballeros.ThemeName = "Material";
            this.rbcaballeros.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbcaballeros_ToggleStateChanged);
            // 
            // rbdamas
            // 
            this.rbdamas.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbdamas.Location = new System.Drawing.Point(5, 27);
            this.rbdamas.Name = "rbdamas";
            this.rbdamas.Size = new System.Drawing.Size(71, 22);
            this.rbdamas.TabIndex = 5;
            this.rbdamas.Text = "Damas";
            this.rbdamas.ThemeName = "Material";
            this.rbdamas.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.rbdamas.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbdamas_ToggleStateChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel1.Location = new System.Drawing.Point(505, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(94, 18);
            this.radLabel1.TabIndex = 7;
            this.radLabel1.Text = "Nombre de Hoja";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // cboHojas
            // 
            this.cboHojas.Location = new System.Drawing.Point(478, 30);
            this.cboHojas.Name = "cboHojas";
            this.cboHojas.Size = new System.Drawing.Size(151, 37);
            this.cboHojas.TabIndex = 8;
            this.cboHojas.ThemeName = "Material";
            this.cboHojas.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cboHojas_SelectedIndexChanged);
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.Location = new System.Drawing.Point(6, 116);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(1021, 24);
            this.radWaitingBar1.TabIndex = 9;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.ThemeName = "Material";
            this.radWaitingBar1.Visible = false;
            this.radWaitingBar1.WaitingIndicators.Add(this.waitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingIndicators.Add(this.waitingBarIndicatorElement2);
            this.radWaitingBar1.WaitingIndicatorSize = new System.Drawing.Size(100, 14);
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Throbber;
            // 
            // waitingBarIndicatorElement1
            // 
            this.waitingBarIndicatorElement1.Name = "waitingBarIndicatorElement1";
            this.waitingBarIndicatorElement1.StretchHorizontally = false;
            // 
            // waitingBarIndicatorElement2
            // 
            this.waitingBarIndicatorElement2.Name = "waitingBarIndicatorElement2";
            this.waitingBarIndicatorElement2.StretchHorizontally = false;
            // 
            // rbInsert
            // 
            this.rbInsert.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rbInsert.Location = new System.Drawing.Point(5, 25);
            this.rbInsert.Name = "rbInsert";
            this.rbInsert.Size = new System.Drawing.Size(76, 22);
            this.rbInsert.TabIndex = 10;
            this.rbInsert.Text = "Insertar";
            this.rbInsert.ThemeName = "Material";
            this.rbInsert.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.rbInsert.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbInsert_ToggleStateChanged);
            // 
            // rbUpdate
            // 
            this.rbUpdate.Location = new System.Drawing.Point(84, 25);
            this.rbUpdate.Name = "rbUpdate";
            this.rbUpdate.Size = new System.Drawing.Size(91, 22);
            this.rbUpdate.TabIndex = 11;
            this.rbUpdate.TabStop = false;
            this.rbUpdate.Text = "Actualizar";
            this.rbUpdate.ThemeName = "Material";
            this.rbUpdate.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rbUpdate_ToggleStateChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.rbTransfer);
            this.radGroupBox1.Controls.Add(this.rbCompras);
            this.radGroupBox1.Controls.Add(this.rbStock);
            this.radGroupBox1.Controls.Add(this.rbInsert);
            this.radGroupBox1.Controls.Add(this.rbUpdate);
            this.radGroupBox1.HeaderText = "Tipo Operacion";
            this.radGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(451, 55);
            this.radGroupBox1.TabIndex = 12;
            this.radGroupBox1.Text = "Tipo Operacion";
            this.radGroupBox1.ThemeName = "Material";
            // 
            // rbStock
            // 
            this.rbStock.Location = new System.Drawing.Point(178, 25);
            this.rbStock.Name = "rbStock";
            this.rbStock.Size = new System.Drawing.Size(90, 22);
            this.rbStock.TabIndex = 12;
            this.rbStock.TabStop = false;
            this.rbStock.Text = "Ins. Stock";
            this.rbStock.ThemeName = "Material";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.rbdamas);
            this.radGroupBox2.Controls.Add(this.rbcaballeros);
            this.radGroupBox2.HeaderText = "Sexo";
            this.radGroupBox2.Location = new System.Drawing.Point(647, 14);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(192, 54);
            this.radGroupBox2.TabIndex = 13;
            this.radGroupBox2.Text = "Sexo";
            this.radGroupBox2.ThemeName = "Material";
            // 
            // radLabel2
            // 
            this.radLabel2.BackColor = System.Drawing.Color.Red;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel2.ForeColor = System.Drawing.Color.White;
            this.radLabel2.Location = new System.Drawing.Point(887, 12);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(71, 18);
            this.radLabel2.TabIndex = 14;
            this.radLabel2.Text = "Lista Errores";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            this.radLabel2.Visible = false;
            this.radLabel2.Click += new System.EventHandler(this.radLabel2_Click);
            // 
            // rbCompras
            // 
            this.rbCompras.Location = new System.Drawing.Point(274, 25);
            this.rbCompras.Name = "rbCompras";
            this.rbCompras.Size = new System.Drawing.Size(77, 22);
            this.rbCompras.TabIndex = 13;
            this.rbCompras.TabStop = false;
            this.rbCompras.Text = "Compra";
            this.rbCompras.ThemeName = "Material";
            // 
            // rbTransfer
            // 
            this.rbTransfer.Location = new System.Drawing.Point(357, 25);
            this.rbTransfer.Name = "rbTransfer";
            this.rbTransfer.Size = new System.Drawing.Size(85, 22);
            this.rbTransfer.TabIndex = 14;
            this.rbTransfer.TabStop = false;
            this.rbTransfer.Text = "Transfer.";
            this.rbTransfer.ThemeName = "Material";
            // 
            // frmExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1029, 493);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radWaitingBar1);
            this.Controls.Add(this.cboHojas);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.btnCarga);
            this.Controls.Add(this.btnOpenfile);
            this.Controls.Add(this.lblarchivo);
            this.Controls.Add(this.dgproductos);
            this.Name = "frmExcel";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carga Productos";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmExcel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgproductos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgproductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblarchivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenfile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCarga)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbcaballeros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbdamas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHojas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbInsert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbStock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbCompras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTouchTheme telerikMetroTouchTheme1;
        private Telerik.WinControls.UI.RadGridView dgproductos;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Telerik.WinControls.UI.RadLabel lblarchivo;
        private Telerik.WinControls.UI.RadButton btnOpenfile;
        private Telerik.WinControls.UI.RadButton btnCarga;
        private Telerik.WinControls.UI.RadRadioButton rbcaballeros;
        private Telerik.WinControls.UI.RadRadioButton rbdamas;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList cboHojas;
        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.WaitingBarIndicatorElement waitingBarIndicatorElement1;
        private Telerik.WinControls.UI.WaitingBarIndicatorElement waitingBarIndicatorElement2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Telerik.WinControls.UI.RadRadioButton rbInsert;
        private Telerik.WinControls.UI.RadRadioButton rbUpdate;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadRadioButton rbStock;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadRadioButton rbTransfer;
        private Telerik.WinControls.UI.RadRadioButton rbCompras;
    }
}
