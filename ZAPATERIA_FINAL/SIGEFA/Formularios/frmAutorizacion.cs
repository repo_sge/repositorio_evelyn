﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmAutorizacion : Telerik.WinControls.UI.RadForm
    {
        clsAdmUsuario admusu = new clsAdmUsuario();

        public clsUsuario autorizador { get; set; }

        public frmAutorizacion()
        {
            InitializeComponent();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            string usuario = txtUsuario.Text;
            string clave = txtClave.Text;

            autorizador = admusu.cargaAutorizador(usuario, clave);

            if (autorizador != null)
            {
                 this.DialogResult= DialogResult.OK;
                this.Close();
                this.Dispose();
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;

                this.Close();
                this.Dispose();
            }
        }

        private void frmAutorizacion_Load(object sender, EventArgs e)
        {
            txtUsuario.Focus();
            autorizador = new clsUsuario();
        }

        private void txtClave_TextChanged(object sender, EventArgs e)
        {

        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            this.Dispose();
        }
    }
}
