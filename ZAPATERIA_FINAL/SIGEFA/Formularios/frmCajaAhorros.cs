﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;
using Telerik.WinControls.UI;

namespace SIGEFA.Formularios
{
    public partial class frmCajaAhorros : Telerik.WinControls.UI.RadForm
    {
        clsAdmCajaAhorros admahorros = new clsAdmCajaAhorros();

        public frmCajaAhorros()
        {
            InitializeComponent();
        }

        private void frmCajaAhorros_Load(object sender, EventArgs e)
        {
            var diasmes = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            dtpFinicio.Value = DateTime.Now.AddDays(-DateTime.Now.Day+1);
            dtpFfin.Value = DateTime.Now.AddDays(diasmes-DateTime.Now.Day);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
            dgvMovimientos.DataSource = admahorros.listaCajaAhorros(dtpFinicio.Value.Date, dtpFfin.Value.Date);
            dgvMovimientos.ClearSelection();
            calculaTotal();
        }

        public void calculaTotal()
        {
            decimal totalin = 0;
            decimal totalout = 0;
            foreach(GridViewRowInfo r in dgvMovimientos.Rows)
            {
                if (r.Cells["tipo"].Value.Equals("INGRESO"))
                {
                    totalin += Convert.ToDecimal(r.Cells["monto"].Value);
                }else if (r.Cells["tipo"].Value.Equals("EGRESO"))
                {
                    totalout += Convert.ToDecimal(r.Cells["monto"].Value);
                }
                
            }

            lblTotal.Text = String.Format("{0:#,##0.0000}", totalin);
            lblGastos.Text = String.Format("{0:#,##0.0000}", totalout);
        }

        private void btnRetiro_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmPagoTesoreria"] != null)
            {
                Application.OpenForms["frmPagoTesoreria"].Activate();
            }
            else
            {
                frmPagoTesoreria form = new frmPagoTesoreria();
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Proceso = 2;
                form.ShowDialog();

                if (form.DialogResult == DialogResult.OK)
                {
                    btnBuscar.PerformClick();
                }
                

            }
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            dgvMovimientos.MultiSelect = true;
            dgvMovimientos.SelectAll();
            dgvMovimientos.ClipboardCopyMode = GridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            DataObject dataObj = dgvMovimientos.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);

            dgvMovimientos.MultiSelect = false;
        }
    }
}
