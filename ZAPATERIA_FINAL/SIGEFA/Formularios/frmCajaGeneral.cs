﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;
using Telerik.WinControls.UI;

namespace SIGEFA.Formularios
{
    public partial class frmCajaGeneral : Telerik.WinControls.UI.RadForm
    {
        clsAdmCajaGeneral admcgeneral = new clsAdmCajaGeneral();

        public int Codcta { get; set; }
        public string Operacion { get; set; }

        public frmCajaGeneral()
        {
            InitializeComponent();
        }

        private void frmCajaGeneral_Load(object sender, EventArgs e)
        {
            var diasmes = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            dtpFinicio.Value = DateTime.Now.AddDays(-DateTime.Now.Day + 1);
            dtpFfin.Value = DateTime.Now.AddDays(diasmes - DateTime.Now.Day);
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            listar();
            
        }

        public void listar()
        {
            dgvMovimientos.DataSource = admcgeneral.listaCajaGeneral(dtpFinicio.Value.Date, dtpFfin.Value.Date);
            dgvMovimientos.ClearSelection();

            calculaTotales();
        }

        private void calculaTotales()
        {
            decimal totalndep = 0;
            decimal totaldep = 0;
            decimal gastos = 0;
            decimal saldo = 0;
            foreach (GridViewRowInfo r in dgvMovimientos.Rows)
            {
                if (r.Cells["tipo"].Value.Equals("INGRESO"))
                {
                    if (r.Cells["depositado"].Value.Equals("Si"))
                    {
                        totaldep += Convert.ToDecimal(r.Cells["monto"].Value);
                    }
                    else if (r.Cells["depositado"].Value.Equals("No"))
                    {
                        totalndep += Convert.ToDecimal(r.Cells["monto"].Value);
                    }
                }else
                {
                   gastos += Convert.ToDecimal(r.Cells["monto"].Value);
                }

            }

            lblndep.Text = String.Format("{0:#,##0.00}", totalndep);
            lbldep.Text = String.Format("{0:#,##0.00}", totaldep);
            lblgasto.Text= String.Format("{0:#,##0.00}", gastos);
            saldo = totalndep - gastos;
            lblsaldo.Text= String.Format("{0:#,##0.00}", saldo);
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            
            if (dgvMovimientos.SelectedRows.Count>0) {

                if (Application.OpenForms["frmSeleccionCuenta"] != null)
                {
                    Application.OpenForms["frmSeleccionCuenta"].Activate();
                }
                else
                {
                    string env = Convert.ToString(dgvMovimientos.SelectedRows[0].Cells["depositado"].Value);
                    if (env.Equals("No"))
                    {
                        frmSeleccionCuenta form = new frmSeleccionCuenta();
                        form.StartPosition = FormStartPosition.CenterScreen;
                        form.ShowDialog();

                        if (form.DialogResult == DialogResult.OK)
                        {
                            Codcta = form.Codcta;
                            Operacion = form.Operacion;
                            int cod = Convert.ToInt32(dgvMovimientos.SelectedRows[0].Cells["idcgeneral"].Value);

                            bool result = admcgeneral.transferirCGeneral(cod, Codcta, Operacion, frmLogin.iCodUser);

                            if (result)
                            {
                                MessageBox.Show("Movimiento transferido correctamente.");
                            }
                            else
                            {
                                MessageBox.Show("Hubo un error en la transferencia.");
                            }

                            listar();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Este movimiento ya ha sido transferido.");
                    }
                }
            }
            else
            {
                MessageBox.Show("Seleccione un movimiento");
            }
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            dgvMovimientos.MultiSelect = true ;
            dgvMovimientos.SelectAll();
            dgvMovimientos.ClipboardCopyMode = GridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            DataObject dataObj = dgvMovimientos.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);

            dgvMovimientos.MultiSelect = false;

        }
    }
}
