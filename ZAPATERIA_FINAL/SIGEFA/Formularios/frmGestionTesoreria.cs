﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;
using Telerik.WinControls.UI;

namespace SIGEFA.Formularios
{
    public partial class frmGestionTesoreria : Telerik.WinControls.UI.RadForm
    {
        clsAdmTesoreria admteso = new clsAdmTesoreria();

        public frmGestionTesoreria()
        {
            InitializeComponent();
        }

        private void frmGestionTesoreria_Load(object sender, EventArgs e)
        {
            var diasmes = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            dtpFinicio.Value = DateTime.Now.AddDays(-DateTime.Now.Day + 1);
            dtpFfin.Value = DateTime.Now.AddDays(diasmes - DateTime.Now.Day);
        }

        private void btnPago_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["frmPagoTesoreria"] != null)
            {
                Application.OpenForms["frmPagoTesoreria"].Activate();
            }
            else
            {
                frmPagoTesoreria form = new frmPagoTesoreria();
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Proceso = 1;
                form.Show();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dgvlista.ClearSelection();
            dgvlista.DataSource=admteso.listarTesoreria(dtpFinicio.Value, dtpFfin.Value);
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            dgvlista.MultiSelect = true;
            dgvlista.SelectAll();
            dgvlista.ClipboardCopyMode = GridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            DataObject dataObj = dgvlista.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);

            dgvlista.MultiSelect = false;


            MessageBox.Show("Puede copiar los datos a excel");
        }
    }
}
