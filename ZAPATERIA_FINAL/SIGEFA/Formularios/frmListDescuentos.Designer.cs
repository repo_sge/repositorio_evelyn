﻿namespace SIGEFA.Formularios
{
    partial class frmListDescuentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.dgvDescuentos = new Telerik.WinControls.UI.RadGridView();
            this.cmbTipo = new Telerik.WinControls.UI.RadDropDownList();
            this.btnBuscar = new Telerik.WinControls.UI.RadButton();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.dtpFinicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpFfin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDescuentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDescuentos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDescuentos
            // 
            this.dgvDescuentos.AutoSizeRows = true;
            this.dgvDescuentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDescuentos.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.dgvDescuentos.MasterTemplate.AllowAddNewRow = false;
            this.dgvDescuentos.MasterTemplate.AllowColumnReorder = false;
            this.dgvDescuentos.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "idDescuento";
            gridViewTextBoxColumn1.HeaderText = "idDescuento";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "idDescuento";
            gridViewTextBoxColumn1.Width = 78;
            gridViewTextBoxColumn2.FieldName = "usuario";
            gridViewTextBoxColumn2.HeaderText = "Usuario";
            gridViewTextBoxColumn2.Name = "usuario";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 60;
            gridViewTextBoxColumn3.FieldName = "autorizador";
            gridViewTextBoxColumn3.HeaderText = "Autorizado Por";
            gridViewTextBoxColumn3.Name = "autorizador";
            gridViewTextBoxColumn3.Width = 89;
            gridViewTextBoxColumn4.FieldName = "producto";
            gridViewTextBoxColumn4.HeaderText = "Producto";
            gridViewTextBoxColumn4.Name = "producto";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 402;
            gridViewTextBoxColumn5.FieldName = "monto";
            gridViewTextBoxColumn5.HeaderText = "Monto Original";
            gridViewTextBoxColumn5.Name = "monto";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 133;
            gridViewTextBoxColumn6.FieldName = "montom";
            gridViewTextBoxColumn6.HeaderText = "Modificacion";
            gridViewTextBoxColumn6.Name = "montom";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 132;
            gridViewTextBoxColumn7.FieldName = "tipo";
            gridViewTextBoxColumn7.HeaderText = "Tipo";
            gridViewTextBoxColumn7.Name = "tipo";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 162;
            this.dgvDescuentos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.dgvDescuentos.MasterTemplate.EnableGrouping = false;
            this.dgvDescuentos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgvDescuentos.Name = "dgvDescuentos";
            this.dgvDescuentos.ReadOnly = true;
            this.dgvDescuentos.ShowGroupPanel = false;
            this.dgvDescuentos.Size = new System.Drawing.Size(1026, 268);
            this.dgvDescuentos.TabIndex = 0;
            this.dgvDescuentos.Text = "radGridView1";
            this.dgvDescuentos.ThemeName = "Material";
            // 
            // cmbTipo
            // 
            this.cmbTipo.Location = new System.Drawing.Point(70, 74);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(133, 37);
            this.cmbTipo.TabIndex = 1;
            this.cmbTipo.ThemeName = "Material";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Image = global::SIGEFA.Properties.Resources.buscar24;
            this.btnBuscar.Location = new System.Drawing.Point(715, 73);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(111, 36);
            this.btnBuscar.TabIndex = 2;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.ThemeName = "Material";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(231, 73);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(58, 21);
            this.radLabel2.TabIndex = 5;
            this.radLabel2.Text = "F. Inicio";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(470, 74);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(42, 21);
            this.radLabel3.TabIndex = 6;
            this.radLabel3.Text = "F. Fin";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel3.ThemeName = "Material";
            // 
            // dtpFinicio
            // 
            this.dtpFinicio.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFinicio.Location = new System.Drawing.Point(295, 73);
            this.dtpFinicio.Name = "dtpFinicio";
            this.dtpFinicio.Size = new System.Drawing.Size(145, 36);
            this.dtpFinicio.TabIndex = 7;
            this.dtpFinicio.TabStop = false;
            this.dtpFinicio.Text = "17/07/2019";
            this.dtpFinicio.ThemeName = "Material";
            this.dtpFinicio.Value = new System.DateTime(2019, 7, 17, 19, 33, 19, 963);
            // 
            // dtpFfin
            // 
            this.dtpFfin.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFfin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFfin.Location = new System.Drawing.Point(518, 74);
            this.dtpFfin.Name = "dtpFfin";
            this.dtpFfin.Size = new System.Drawing.Size(145, 36);
            this.dtpFfin.TabIndex = 8;
            this.dtpFfin.TabStop = false;
            this.dtpFfin.Text = "17/07/2019";
            this.dtpFfin.ThemeName = "Material";
            this.dtpFfin.Value = new System.DateTime(2019, 7, 17, 19, 33, 19, 963);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.dgvDescuentos);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radGroupBox1.HeaderPosition = Telerik.WinControls.UI.HeaderPosition.Left;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 138);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1030, 288);
            this.radGroupBox1.TabIndex = 9;
            this.radGroupBox1.ThemeName = "Material";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.radLabel1.Location = new System.Drawing.Point(353, 12);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(259, 23);
            this.radLabel1.TabIndex = 9;
            this.radLabel1.Text = "Modificaciones de Precio en Ventas";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radGroupBox2.Controls.Add(this.cmbTipo);
            this.radGroupBox2.Controls.Add(this.radButton1);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.radLabel1);
            this.radGroupBox2.Controls.Add(this.dtpFinicio);
            this.radGroupBox2.Controls.Add(this.dtpFfin);
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.btnBuscar);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox2.HeaderText = "";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(1030, 126);
            this.radGroupBox2.TabIndex = 10;
            this.radGroupBox2.ThemeName = "Material";
            // 
            // radButton1
            // 
            this.radButton1.Image = global::SIGEFA.Properties.Resources.reporte24;
            this.radButton1.Location = new System.Drawing.Point(853, 73);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(113, 36);
            this.radButton1.TabIndex = 4;
            this.radButton1.Text = "Copiar";
            this.radButton1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton1.ThemeName = "Material";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(70, 47);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(124, 21);
            this.radLabel4.TabIndex = 6;
            this.radLabel4.Text = "Tipo Modificacion";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel4.ThemeName = "Material";
            // 
            // frmListDescuentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1030, 426);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmListDescuentos";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Reporte Descuentos";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmListDescuentos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDescuentos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDescuentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView dgvDescuentos;
        private Telerik.WinControls.UI.RadDropDownList cmbTipo;
        private Telerik.WinControls.UI.RadButton btnBuscar;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFinicio;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFfin;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton radButton1;
    }
}
