﻿namespace SIGEFA.Formularios
{
    partial class frmGestionTesoreria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.dtpFfin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpFinicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.dgvlista = new Telerik.WinControls.UI.RadGridView();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.btnBuscar = new Telerik.WinControls.UI.RadButton();
            this.btnPago = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlista.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radGroupBox1.Controls.Add(this.radButton1);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.dtpFfin);
            this.radGroupBox1.Controls.Add(this.dtpFinicio);
            this.radGroupBox1.Controls.Add(this.btnBuscar);
            this.radGroupBox1.Controls.Add(this.btnPago);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Datos Tesoreria";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1018, 113);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Datos Tesoreria";
            this.radGroupBox1.ThemeName = "Material";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(210, 35);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(42, 21);
            this.radLabel2.TabIndex = 5;
            this.radLabel2.Text = "F. Fin";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(24, 35);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(58, 21);
            this.radLabel1.TabIndex = 4;
            this.radLabel1.Text = "F. Inicio";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // dtpFfin
            // 
            this.dtpFfin.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFfin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFfin.Location = new System.Drawing.Point(210, 62);
            this.dtpFfin.Name = "dtpFfin";
            this.dtpFfin.Size = new System.Drawing.Size(147, 36);
            this.dtpFfin.TabIndex = 3;
            this.dtpFfin.TabStop = false;
            this.dtpFfin.Text = "20/07/2019";
            this.dtpFfin.ThemeName = "Material";
            this.dtpFfin.Value = new System.DateTime(2019, 7, 20, 10, 10, 57, 395);
            // 
            // dtpFinicio
            // 
            this.dtpFinicio.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFinicio.Location = new System.Drawing.Point(24, 62);
            this.dtpFinicio.Name = "dtpFinicio";
            this.dtpFinicio.Size = new System.Drawing.Size(149, 36);
            this.dtpFinicio.TabIndex = 2;
            this.dtpFinicio.TabStop = false;
            this.dtpFinicio.Text = "20/07/2019";
            this.dtpFinicio.ThemeName = "Material";
            this.dtpFinicio.Value = new System.DateTime(2019, 7, 20, 10, 10, 57, 395);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radGroupBox2.Controls.Add(this.dgvlista);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radGroupBox2.HeaderText = "";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 121);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(1018, 420);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.ThemeName = "Material";
            // 
            // dgvlista
            // 
            this.dgvlista.AutoSizeRows = true;
            this.dgvlista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvlista.GroupExpandAnimationType = Telerik.WinControls.UI.GridExpandAnimationType.Accordion;
            this.dgvlista.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.dgvlista.MasterTemplate.AllowAddNewRow = false;
            this.dgvlista.MasterTemplate.AllowColumnReorder = false;
            this.dgvlista.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "idtesoreria";
            gridViewTextBoxColumn1.HeaderText = "idtesoreria";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "idtesoreria";
            gridViewTextBoxColumn1.Width = 1006;
            gridViewTextBoxColumn2.FieldName = "tipopago";
            gridViewTextBoxColumn2.HeaderText = "Tipo Pago";
            gridViewTextBoxColumn2.Name = "tipopago";
            gridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn2.Width = 184;
            gridViewTextBoxColumn3.FieldName = "formapago";
            gridViewTextBoxColumn3.HeaderText = "Metodo Pago";
            gridViewTextBoxColumn3.Name = "formapago";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 137;
            gridViewTextBoxColumn4.Expression = "";
            gridViewTextBoxColumn4.FieldName = "montopago";
            gridViewTextBoxColumn4.HeaderText = "Monto Pago";
            gridViewTextBoxColumn4.Name = "montopago";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 126;
            gridViewTextBoxColumn5.FieldName = "cod";
            gridViewTextBoxColumn5.HeaderText = "Origen Dinero";
            gridViewTextBoxColumn5.Name = "cod";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 95;
            gridViewTextBoxColumn6.FieldName = "descripcion";
            gridViewTextBoxColumn6.HeaderText = "Obs.";
            gridViewTextBoxColumn6.Name = "descripcion";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 472;
            this.dgvlista.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6});
            this.dgvlista.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgvlista.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgvlista.Name = "dgvlista";
            this.dgvlista.ReadOnly = true;
            this.dgvlista.Size = new System.Drawing.Size(1014, 400);
            this.dgvlista.TabIndex = 0;
            this.dgvlista.Text = "radGridView1";
            this.dgvlista.ThemeName = "Material";
            // 
            // radButton1
            // 
            this.radButton1.Image = global::SIGEFA.Properties.Resources.reporte24;
            this.radButton1.Location = new System.Drawing.Point(692, 59);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(120, 36);
            this.radButton1.TabIndex = 6;
            this.radButton1.Text = "Copiar";
            this.radButton1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton1.ThemeName = "Material";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Image = global::SIGEFA.Properties.Resources.buscar24;
            this.btnBuscar.Location = new System.Drawing.Point(559, 59);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(108, 36);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.ThemeName = "Material";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnPago
            // 
            this.btnPago.Image = global::SIGEFA.Properties.Resources.pago24;
            this.btnPago.Location = new System.Drawing.Point(829, 59);
            this.btnPago.Name = "btnPago";
            this.btnPago.Size = new System.Drawing.Size(153, 36);
            this.btnPago.TabIndex = 0;
            this.btnPago.Text = "Realizar Pago";
            this.btnPago.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPago.ThemeName = "Material";
            this.btnPago.Click += new System.EventHandler(this.btnPago_Click);
            // 
            // frmGestionTesoreria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 541);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Name = "frmGestionTesoreria";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Tesoreria";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmGestionTesoreria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvlista.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvlista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadButton btnPago;
        private Telerik.WinControls.UI.RadGridView dgvlista;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFfin;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFinicio;
        private Telerik.WinControls.UI.RadButton btnBuscar;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton radButton1;
    }
}
