﻿
using ExcelDataReader;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using System.Linq;
using System.ComponentModel;

namespace SIGEFA.Formularios
{
    public partial class frmExcel : Telerik.WinControls.UI.RadForm
    {
        clsAdmTipoArticulo admTip = new clsAdmTipoArticulo();
        clsAdmFamilia admFamilia = new clsAdmFamilia();
        clsAdmModelo admModelo = new clsAdmModelo();
        clsAdmMarca admMarca = new clsAdmMarca();
        clsAdmCorte admCorte = new clsAdmCorte();
        clsAdmTaco admTaco = new clsAdmTaco();
        clsAdmCuello admCuello = new clsAdmCuello();
        clsAdmLinea admLinea = new clsAdmLinea();
        clsAdmColor admColor = new clsAdmColor();
        clsAdmUnidad admUnidad = new clsAdmUnidad();
        cldAdmTipoTalla admTipoTalla = new cldAdmTipoTalla();
        clsAdmTalla admTalla = new clsAdmTalla();
        clsAdmSerieProducto admSerieP = new clsAdmSerieProducto();
        clsAdmProducto admProducto = new clsAdmProducto();
        clsAdmGrupo admGrupo = new clsAdmGrupo();
        clsAdmControlStock admControl = new clsAdmControlStock();
        clsAdmTipoCierre admtipoCierre = new clsAdmTipoCierre();
        clsAdmTemporada admTemporada = new clsAdmTemporada();
        clsAdmTipoCambio tc = new clsAdmTipoCambio();
        clsEntFotografia entfotografia = new clsEntFotografia();
        clsAdmFotografia admfotografia = new clsAdmFotografia();
        clsUnidadEquivalente equi = new clsUnidadEquivalente();
        clsSerieProducto ser = new clsSerieProducto();
        clsAdmTallaProducto admtallaprod = new clsAdmTallaProducto();
        clsAdmTalla admtallas = new clsAdmTalla();
        clsAdmNotaIngreso admnota = new clsAdmNotaIngreso();
        clsAdmFactura admfactura = new clsAdmFactura();
        clsAdmProveedor admprov = new clsAdmProveedor();
        clsAdmFormaPago admpago = new clsAdmFormaPago();
        clsAdmTransferencia admtransfer = new clsAdmTransferencia();
        clsAdmSerie admserie = new clsAdmSerie();
        clsSerie serie = new clsSerie();


        DataTable temporadas = new DataTable();
        DataTable marcas = new DataTable();
        DataTable familias = new DataTable();
        DataTable tiposarticulo = new DataTable();
        DataTable cortes = new DataTable();
        DataTable tacos = new DataTable();
        DataTable unidades = new DataTable();
        DataTable grupos = new DataTable();
        DataTable lineas = new DataTable();
        DataTable colorp = new DataTable();
        DataTable colors = new DataTable();

        List<int> posiciones;

        clsTransferencia transfer;
        clsNotaIngreso nota;
        clsFactura factura;

        DataTable detallefactura;
        DataTable detallenota;
        DataTable detalletrasnfer; //productos para la transferencia

        DataTable codigosbarra;

        DataTable productostable; //productos nota de ingreso
        DataTable productosupdate; //productos actualizar
        

        DataSet resultado = new DataSet();

        int cantinsertar;

        public frmExcel()
        {

            InitializeComponent();
            
        }


        private void btnOpenfile_Click(object sender, EventArgs e)
        {
           
            try
            {
                using (OpenFileDialog openfile=new OpenFileDialog() {Filter= "Excel 2013|*.xlsx", ValidateNames=true })
                {
                    if (openfile.ShowDialog() == DialogResult.OK)
                    {

                        using (var stream = File.Open(openfile.FileName, FileMode.Open, FileAccess.Read))
                        {
                            using (var reader = ExcelReaderFactory.CreateReader(stream))
                            {
                                lblarchivo.Text = openfile.FileName;
                                resultado = reader.AsDataSet(new ExcelDataSetConfiguration()
                                {
                                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                    {
                                        UseHeaderRow = true
                                    }
                                });

                                btnCarga.Enabled = true;
                                radLabel2.Visible = false;
                                dgproductos.DataSource = null;

                                cboHojas.Items.Clear();
                                foreach (DataTable dt in resultado.Tables)
                                {
                                    cboHojas.Items.Add(dt.TableName);
                                }


                            }
                        }
                        
                    }
                }       
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            
        }

        private void CargaMarcas()
        {
            marcas = admMarca.MuestraMarcas();
        }

        private void CargaTemporada()
        {
            temporadas = admTemporada.MuestraTemporada();
        }

        private void CargaFamilias()
        {
            familias = admFamilia.MuestraFamilias();
        }

        private void CargaTipoArticulos()
        {
            tiposarticulo = admTip.MuestraTipoArticulos();
        }

        private void CargarCorte()
        {
            cortes = admCorte.MuestraCorte();
        }

        private void CargarTaco()
        {
            tacos = admTaco.MuestraTaco();
        }

        private void CargarUnidad()
        {
            unidades = admUnidad.MuestraUnidades();
        }

        private void CargaGrupo(int codlinea)
        {
            grupos = admGrupo.MuestraGrupos(codlinea);
        }

        private void CargarLinea(int codfamilia)
        {
            lineas = admLinea.MuestraLineas(codfamilia);
        }

        private void CargarColorPrimario()
        {
            colorp = admColor.MuestraColorPrimario();
        }

        private void CargarColorSecundario()
        {
            colors = admColor.MuestraColorSecundario();
        }

        private void Cargacodigobarras()
        {
            codigosbarra = new DataTable();

            codigosbarra = admProducto.listaCodigobarras();
        }

        private void btnCarga_Click(object sender, EventArgs e)
        {
            radWaitingBar1.Visible = true;
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
                btnCarga.Enabled = false;
                btnOpenfile.Enabled = false;
                radWaitingBar1.StartWaiting();
            }
        }

        public int cargaPrincipal(int n, BackgroundWorker worker, DoWorkEventArgs e)
        {
            try
            {
                cantinsertar = 0;
                int result = 0;
                if (dgproductos.Rows.Count > 0)
                {
                    posiciones = new List<int>();

                    productostable = new DataTable();
                    productosupdate = new DataTable();

                    creaFilas(productostable);
                    creaFilas(productosupdate);

                    DataRow prod;
                    DataRow produ;

                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;

                    }
                    else
                    {
                        CargaFamilias();
                        CargaMarcas();
                        CargaTemporada();
                        CargarColorPrimario();
                        CargarColorSecundario();
                        CargarTaco();
                        CargarCorte();
                        CargarUnidad();
                        CargaTipoArticulos();
                        //se recorre el grid de productos

                        if (!rbStock.IsChecked && !rbCompras.IsChecked && !rbTransfer.IsChecked)
                        {
                            foreach (GridViewRowInfo row in dgproductos.Rows)
                            {
                                if (rbInsert.IsChecked) //si esta marcado insertar
                                {
                                    if (Convert.ToInt32(row.Cells[6].Value) == 0) //si la columna donde se guarda el id del producto insertado es 0, osea que no este insertado
                                    {
                                        prod = productostable.NewRow();
                                        setDatos(prod, row);
                                        if (Convert.ToInt32(prod["error"]) == 0) //si el producto no tiene errores lo agrega
                                        {
                                            productostable.Rows.Add(prod);
                                        }

                                        if (Convert.ToInt32(row.Cells[9].Value) > 0)
                                        {
                                            cantinsertar++;
                                        }
                                    }
                                }
                                else if (rbUpdate.IsChecked) //si esta marcado actualizar
                                {
                                    if (Convert.ToInt32(row.Cells[6].Value) != 0) //que tengan codigo de insercion
                                    {
                                        produ = productosupdate.NewRow();
                                        setDatos(produ, row);
                                        if (Convert.ToInt32(produ["error"]) == 0)
                                        {
                                            productosupdate.Rows.Add(produ);
                                        }
                                    }
                                }
                            }
                        }

                        if (rbInsert.IsChecked)
                        {
                            if (productostable.Rows.Count > 0)
                            {
                                admProducto.insertMasivo(productostable);

                                verificaProductosInsertados();

                                if (cantinsertar > 0)
                                {
                                    nota = new clsNotaIngreso();
                                    detallenota = new DataTable();
                                    creaFilasDetNI(detallenota);

                                    armaCabecera();

                                    ingresoNI(nota);
                                }
                            }
                        }
                        else if (rbUpdate.IsChecked)
                        {
                            if (productosupdate.Rows.Count > 0)
                            {
                                admProducto.updateMasivo(productosupdate);
                            }

                            verificaProductosInsertados();
                        }else if (rbStock.IsChecked)
                        {
                            nota = new clsNotaIngreso();
                            detallenota = new DataTable();
                            creaFilasDetNI(detallenota);

                            armaCabecera();

                            ingresoNI(nota);
                        }else if (rbCompras.IsChecked)
                        {
                            //registro de la nota de ingreso
                            nota = new clsNotaIngreso();
                            detallenota = new DataTable();
                            //para registro de la compra
                            factura = new clsFactura();
                            detallefactura = new DataTable();

                            creaFilasDetNI(detallenota);
                            creaFilasDetFactura(detallefactura);

                            armaCabecera();
                            armaCabeceraFactura();

                            ingresoNI(nota);
                            ingresoFactura(factura);
                        }else if (rbTransfer.IsChecked)
                        {
                            transfer = new clsTransferencia();
                            detalletrasnfer = new DataTable();

                            creaFilasDetTransfer(detalletrasnfer);

                            armaCabeceraTransfer();

                            IngresoTransfer(transfer);


                        }

                    }
                    result = 1;
                }
                return result;
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        private void armaCabecera()
        {
            decimal total = 0;

            if (rbInsert.IsChecked || rbStock.IsChecked)
            {
                serie = admserie.BuscaSeriexDocumento(10, frmLogin.iCodAlmacen);
                if (serie != null)
                {
                    nota.CodTipoTransaccion = 4;
                    nota.CodProveedor = 0;
                    nota.CodTipoDocumento = 10;
                    nota.NumDoc = serie.Numeracion.ToString().PadLeft(8, '0');
                    nota.FormaPago = 0;
                    nota.FechaIngreso = DateTime.Now.Date;
                    nota.FechaPago = DateTime.Now.Date;
                    nota.CodSerie = serie.CodSerie;
                    nota.Serie = serie.Serie;
                    nota.Comentario = "Nota Ingreso";
                }
                else
                {
                    MessageBox.Show("No se ha encontrado serie registrada para nota de ingreso");
                    return;
                }
            }
            else if (rbCompras.IsChecked)
            {
                nota.CodTipoTransaccion = 1;

                clsProveedor prov = new clsProveedor();
                prov = admprov.BuscaProveedor(dgproductos.Rows[0].Cells[1].Value.ToString());
                if (prov != null)
                {
                    nota.CodProveedor = prov.CodProveedor;
                }
                else
                {
                    prov = new clsProveedor();
                    prov.Ruc = dgproductos.Rows[0].Cells[1].Value.ToString().Trim();
                    prov.RazonSocial = dgproductos.Rows[0].Cells[2].Value.ToString().Trim();
                    prov.Direccion = "";
                    prov.Telefono = "";
                    prov.Fax = "";
                    prov.Representante = "";
                    prov.Contacto = "";
                    prov.Comentario = "";
                    prov.Banco = "";
                    prov.CtaCte = "";
                    prov.FrecuenciaVisita = 0;
                    prov.Margen = 0;
                    prov.Estado = true;
                    prov.CodUser = frmLogin.iCodUser;
                    prov.FechaRegistro = DateTime.Now.Date;


                    if (admprov.insert(prov))
                    {
                        nota.CodProveedor = prov.CodProveedorNuevo;
                    }
                    else
                    {
                        MessageBox.Show("Error al guardar proveedor","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                        return;
                    }

                }
                nota.CodTipoDocumento = 2;
                String[] doc = dgproductos.Rows[0].Cells[4].Value.ToString().Split('-');
                nota.NumDoc = doc[2];
                nota.Comentario = "Ingreso por compra";

                DataTable data = admpago.CargaFormaPagos(0);

                var a = data.AsEnumerable()
                            .Where(x => x["descripcion"].Equals(dgproductos.Rows[0].Cells[5].Value.ToString().Trim()))
                            .Select(x => x["codFormaPago"]).ToList();

                nota.FormaPago = Convert.ToInt32(a[0]);

                clsFormaPago fpago = admpago.CargaFormaPago(Convert.ToInt32(a[0]));


                if (fpago.Dias>0)
                {
                    nota.FechaPago = Convert.ToDateTime(dgproductos.Rows[0].Cells[3].Value).AddDays(fpago.Dias);
                }
                else
                {
                    nota.FechaPago = Convert.ToDateTime(dgproductos.Rows[0].Cells[3].Value);
                }
                    
                nota.FechaIngreso = Convert.ToDateTime(dgproductos.Rows[0].Cells[3].Value);
                nota.CodSerie = 0;
                nota.Serie = "";
            }

            nota.CodAlmacen = frmLogin.iCodAlmacen;
            nota.CodUser = frmLogin.iCodUser;
            nota.CodOrdenCompra = 0;
            nota.codalmacenemisor = 0;
            nota.Flete = 0;

            nota.Moneda = 1;
            nota.TipoCambio = tc.CargaTipoCambio(DateTime.Now.Date, 2).Compra;
            nota.CodUser = frmLogin.iCodUser;
            nota.Estado = 1;
            nota.Codtransferencia = 0;
            nota.Recibido = 0;
            nota.Cancelado = 0;
                
            nota.Motivo = "";
            nota.Aplicada = 0;

            if (rbStock.IsChecked || rbCompras.IsChecked)
            {
                foreach (GridViewRowInfo row in dgproductos.Rows)
                {
                    //valido que el stock del producto a insertar stock sea mayor a 0
                    if (Convert.ToInt32(row.Cells[9].Value) > 0)
                    {
                        //valido que el codigo de producto sea diferente de 0, osea que este registrado en la BD
                        if (Convert.ToInt32(row.Cells[6].Value) != 0)
                        {
                            total += (Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value)));
                        }
                    }
                }
            }
            else
            {
                foreach (GridViewRowInfo row in dgproductos.Rows)
                {
                    bool existe = productostable.AsEnumerable()
                            .Where(x => x["codcomprapro"].ToString().Trim().Equals(row.Cells[0].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                            .Any();

                    if (existe)
                    {
                        //valido que el stock del producto a insertar stock sea mayor a 0
                        if (Convert.ToInt32(row.Cells[9].Value) > 0)
                        {
                            //valido que el codigo de producto sea diferente de 0, osea que este registrado en la BD
                            if (Convert.ToInt32(row.Cells[6].Value) != 0)
                            {
                                total += (Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value)));
                            }
                        }
                    }
                }
            }

            nota.MontoBruto = total;
            nota.MontoDscto = 0;
            nota.Flete = 0;
            nota.Igv = (total / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18);
            nota.Total = total;
 
        }

        private void armaCabeceraFactura()
        {
            decimal total = 0;

            factura.CodNotaIngreso = 0;
            factura.CodAlmacen = frmLogin.iCodAlmacen;
            factura.CodTipoTransaccion = 1;
            clsProveedor prov = new clsProveedor();
            prov = admprov.BuscaProveedor(dgproductos.Rows[0].Cells[1].Value.ToString());
            if (prov != null)
            {
                factura.CodProveedor = prov.CodProveedor;
            }
            else
            {
                prov = new clsProveedor();
                prov.Ruc = dgproductos.Rows[0].Cells[1].Value.ToString().Trim();
                prov.RazonSocial = dgproductos.Rows[0].Cells[2].Value.ToString().Trim();
                if (admprov.insert(prov))
                {
                    factura.CodProveedor = prov.CodProveedorNuevo;
                }
                else
                {
                    MessageBox.Show("Error al guardar proveedor", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            factura.CodTipoDocumento = 2;
            factura.DocumentoFactura = dgproductos.Rows[0].Cells[4].Value.ToString();
            factura.Comentario = "Factura Compra";
            DataTable data = admpago.CargaFormaPagos(0);

            var a = data.AsEnumerable()
                        .Where(x => x["descripcion"].Equals(dgproductos.Rows[0].Cells[5].Value.ToString().Trim()))
                        .Select(x => x["codFormaPago"]).ToList();

            factura.FormaPago = Convert.ToInt32(a[0]);

            clsFormaPago fpago = admpago.CargaFormaPago(Convert.ToInt32(a[0]));

            if (fpago.Dias > 0)
            {
                factura.FechaPago = Convert.ToDateTime(dgproductos.Rows[0].Cells[3].Value).AddDays(fpago.Dias);
            }
            else
            {
                factura.FechaPago = Convert.ToDateTime(dgproductos.Rows[0].Cells[3].Value);
            }

            factura.FechaIngreso = Convert.ToDateTime(dgproductos.Rows[0].Cells[3].Value);
            factura.Cancelado = 0;
            factura.CodUser = frmLogin.iCodUser;
            factura.Moneda = 1;
            factura.TipoCambio = tc.CargaTipoCambio(DateTime.Now.Date, 2).Compra;
            factura.Estado = 1;

            foreach (GridViewRowInfo row in dgproductos.Rows)
            {
                if (Convert.ToInt32(row.Cells[9].Value) > 0)
                {
                    if (Convert.ToInt32(row.Cells[6].Value) != 0)
                    {
                        total += (Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value)));
                    }
                }
            }


            factura.MontoBruto = total;
            factura.MontoDscto = 0;
            factura.Flete = 0;
            factura.Igv = (total / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18);
            factura.Total = total;
            
        }

        private void armaCabeceraTransfer()
        {
            decimal total = 0;

            serie = admserie.BuscaSeriexDocumento(22, frmLogin.iCodAlmacen);

            String[] destino = dgproductos.Rows[0].Cells[1].Value.ToString().Split('-');

            transfer.CodAlmacenOrigen = frmLogin.iCodAlmacen;
            transfer.CodAlmacenDestino = Convert.ToInt32(destino[0]);
            transfer.CodTipoDocumento = 22; // Documento COTIZACION INTERNA (documento solicitado por carsalsi)
            transfer.Moneda = 1;
            transfer.FechaEnvio = DateTime.Now.Date;
            transfer.FechaEntrega = DateTime.Now.Date;
            transfer.FormaPago = 0;
            transfer.FechaPago = DateTime.Now.Date;
            transfer.CodListaPrecio = 0;
            transfer.Comentario = dgproductos.Rows[0].Cells[2].Value.ToString();
            transfer.DescripcionRechazo = "";
            
            transfer.CodUser = frmLogin.iCodUser;
            transfer.Estado = 1;
            transfer.Codserie = serie.CodSerie;
            transfer.Serie = serie.Serie;
            transfer.Numerodocumento = serie.Numeracion.ToString().PadLeft(8, '0');

            foreach (GridViewRowInfo row in dgproductos.Rows)
            {
                if (Convert.ToInt32(row.Cells[9].Value) > 0)
                {
                    if (Convert.ToInt32(row.Cells[6].Value) != 0)
                    {
                        total += (Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value)));
                    }
                }
            }

            transfer.MontoBruto = total;
            transfer.MontoDscto = 0.00m;
            transfer.Igv = (total / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18);
            transfer.Total = total;
        }

        public void setDatos(DataRow prod, GridViewRowInfo row)
        {
            int err = 0;
            //insercion de codigo de barras////////////////////////////////////////////////////

            if (row.Cells[0].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna codigo barras en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["codcomprapro"] = row.Cells[0].Value.ToString().Trim();
            }

            //////////////////////////////////////////////////////////////////////////////////

            //insercion del año/////////////////////////////////////////////////////////////
            if (row.Cells[2].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna fecha en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["anio"] = Convert.ToDateTime(row.Cells[2].Value).Year;
            }
            ////////////////////////////////////////////////////////////////////

            //insercion de la temporada//////////////////////////////////////////////////////////////

            if (row.Cells[3].Value == DBNull.Value)
            {
                err++;
            }
            else
            {
                var camp = temporadas.AsEnumerable()
                            .Where(x => x["descripcion"].ToString().Trim().Equals(row.Cells[3].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                            .Select(x => x["codigo"]).ToList();

                if (camp.Count > 0)
                {
                    prod["codtemp"] = camp[0];
                }
                else
                {
                    if (row.Cells[3].Value.ToString().Length > 0)
                    {
                        clsTemporada m = new clsTemporada();
                        m.Descripcion = row.Cells[3].Value.ToString().Trim();
                        m.CodUser = frmLogin.iCodUser;
                        admTemporada.insertTemporada(m);
                        prod["codtemp"] = Convert.ToInt32(m.CodTemporada);
                        CargaTemporada();
                    }
                    else
                    {
                        prod["codtemp"] = 0;
                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////


            //insercion de la marca/////////////////////////////////////////////////////////////

            if (row.Cells[4].Value == DBNull.Value)
            {
                err++;
            }
            else
            {
                var marca = marcas.AsEnumerable()
                            .Where(x => x["descripcion"].ToString().Trim().Equals(row.Cells[4].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                            .Select(x => x["codMarca"]).ToList();

                if (marca.Count > 0)
                {
                    prod["codmar"] = marca[0];
                }
                else
                {
                    if (row.Cells[4].Value.ToString().Length > 0)
                    {
                        clsMarca m = new clsMarca();
                        m.Descripcion = row.Cells[4].Value.ToString().Trim();
                        m.CodUser = frmLogin.iCodUser;
                        admMarca.insert(m);
                        prod["codmar"] = Convert.ToInt32(m.CodMarcaNuevo);
                        CargaMarcas();
                    }
                    else
                    {
                        prod["codmar"] = 0;
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////

            ///insercion de referencia///////////////////////////////////////////////////
            
            if (row.Cells[5].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna referencia en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["referencia"] = row.Cells[5].Value.ToString().Trim();
            }
            /////////////////////////////////////////////////////////////////////////////

            //insercion de color///////////////////////////////////////////////////////////

            if (row.Cells[7].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna color en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                if (!row.Cells[7].Value.ToString().Trim().Contains("/"))
                {

                    var colorprim = colorp.AsEnumerable()
                                .Where(x => x["descripcion"].ToString().Trim().Equals(row.Cells[7].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                                .Select(x => x["codColorPrimario"]).ToList();

                    if (colorprim.Count > 0)
                    {
                        prod["codcolorp"] = colorprim[0];
                    }
                    else
                    {
                        clsColorPrimario c = new clsColorPrimario();
                        c.Descripcion = row.Cells[7].Value.ToString().Trim();
                        admColor.insertPrimario(c);
                        prod["codcolorp"] = Convert.ToInt32(c.CodColorPrimario);
                        CargarColorPrimario();
                    }

                    prod["codcolors"] = 0;
                }
                else
                {
                    String[] colores = row.Cells[7].Value.ToString().Trim().Split('/');

                    var colorprim = colorp.AsEnumerable()
                                .Where(x => x["descripcion"].ToString().Trim().Equals(colores[0].Trim(), StringComparison.OrdinalIgnoreCase))
                                .Select(x => x["codColorPrimario"]).ToList();


                    if (colorprim.Count > 0)
                    {
                        prod["codcolorp"] = colorprim[0];
                    }
                    else
                    {
                        clsColorPrimario c = new clsColorPrimario();
                        c.Descripcion = colores[0].Trim();
                        admColor.insertPrimario(c);
                        prod["codcolorp"] = Convert.ToInt32(c.CodColorPrimario);
                        CargarColorPrimario();
                    }

                    var colorsec = colors.AsEnumerable()
                                .Where(x => x["descripcion"].ToString().Trim().Equals(colores[1].Trim(), StringComparison.OrdinalIgnoreCase))
                                .Select(x => x["codColorSecundario"]).ToList();

                    if (colorsec.Count > 0)
                    {
                        prod["codcolors"] = colorsec[0];
                    }
                    else
                    {
                        clsColorSecundario c2 = new clsColorSecundario();
                        c2.Descripcion = colores[1].Trim();
                        admColor.insertSecundario(c2);
                        prod["codcolors"] = Convert.ToInt32(c2.CodColorSecundario);
                        CargarColorSecundario();
                    }

                }
            }
            //////////////////////////////////////////////////////////////////////

            //insercion de talla/////////////////////////////////////////////
            if (row.Cells[8].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna talla en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["vtalla"] = row.Cells[8].Value.ToString().Trim();
            }

            ///////////////////////////////////////////////////////////

            //insercion de talla/////////////////////////////////////////////

            if (row.Cells[9].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna cantidad en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                int cant = Convert.ToInt32(row.Cells[9].Value);
            }
            ////////////////////////////////////////////////////////////////

            ////insercion de materiales//////////////////////////////////////////

            if (row.Cells[10].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna material en la fila " + (row.Index + 1));
                err++;
            }
            else
            {

                var corte = cortes.AsEnumerable()
                            .Where(x => x["descripcion"].ToString().Trim().Equals(row.Cells[10].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                            .Select(x => x["codCorte"]).ToList();

                if (corte.Count > 0)
                {
                    prod["codcor"] = corte[0];
                }
                else
                {
                    clsCorte cor = new clsCorte();
                    cor.Descripcion = row.Cells[10].Value.ToString().Trim();
                    cor.Referencia = row.Cells[10].Value.ToString().Trim();
                    cor.CodUser = frmLogin.iCodUser;
                    admCorte.insertCorte(cor);
                    prod["codcor"] = Convert.ToInt32(cor.CodCorte);
                    CargarCorte();
                }
            }
            ////////////////////////////////////////////////////////////////////////////////

            //insercion de precio de compra/////////////////////////////////////////
            if (row.Cells[11].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna precio costo en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["preciocompra"] = Convert.ToDecimal(row.Cells[11].Value);
            }
            ///////////////////////////////////////////////////////////////////////

            //insercion de precio de venta/////////////////////////////////////////
            if (row.Cells[12].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna precio venta en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["precioca"] = Convert.ToDecimal(row.Cells[12].Value);
            }

            ///////////////////////////////////////////////////////////////////////
            
            //insercion de precio de director/////////////////////////////////////////
            if (row.Cells[13].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna precio venta en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["preciodir"] = Convert.ToDecimal(row.Cells[13].Value);
            }

            ///////////////////////////////////////////////////////////////////////
            
            //insercion de precio de promotor/////////////////////////////////////////
            if (row.Cells[14].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna precio venta en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                prod["preciopro"] = Convert.ToDecimal(row.Cells[14].Value);
            }

            ///////////////////////////////////////////////////////////////////////

            ////seleccion de familia////////////////////////////////////////////////////

            string fam = rbdamas.IsChecked ? rbdamas.Text.Trim() : rbcaballeros.Text.Trim();

            var familia = familias.AsEnumerable()
                            .Where(x => x["descripcion"].ToString().Trim().Equals(fam, StringComparison.OrdinalIgnoreCase))
                            .Select(x => x["codFamilia"]).ToList();

            prod["codfam"] = familia[0];
            ///////////////////////////////////////////////////////////////////////////

            //seleccion de linea//////////////////////////////////////////////////////

            CargarLinea(Convert.ToInt32(prod["codfam"]));

            prod["codlin"] = Convert.ToInt32(lineas.Rows[0][0]);

            //////////////////////////////////////////////////////////////////////////
            CargaGrupo(Convert.ToInt32(prod["codlin"]));

            //insercion de grupos//////////////////////////////////////////////////////////

            if (row.Cells[15].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna categoria venta en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                var grup = grupos.AsEnumerable()
                               .Where(x => x["descripcion"].ToString().Trim().Equals(row.Cells[15].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                               .Select(x => x["codGrupo"]).ToList();

                if (grup.Count > 0)
                {
                    prod["codgru"] = grup[0];
                }
                else
                {
                    if (row.Cells[13].Value.ToString().Length > 0)
                    {
                        clsGrupo grupo = new clsGrupo();
                        grupo.CodLinea = Convert.ToInt32(prod["codlin"]);
                        grupo.Descripcion = row.Cells[15].Value.ToString().Trim();
                        grupo.Referencia = row.Cells[15].Value.ToString().Trim();
                        grupo.CodUser = frmLogin.iCodUser;
                        admGrupo.insert(grupo);
                        prod["codgru"] = Convert.ToInt32(grupo.CodGrupoNuevo);
                    }
                    else
                    {
                        prod["codgru"] = 0;
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////

            /////////insercion de taco//////////////////////////////////////////////////////////////
            if (row.Cells[16].Value == DBNull.Value)
            {
                //MessageBox.Show("Revise columna taco venta en la fila " + (row.Index + 1));
                err++;
            }
            else
            {
                var tac = tacos.AsEnumerable()
                           .Where(x => x["descripcion"].ToString().Trim().Equals(row.Cells[16].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                           .Select(x => x["codTaco"]).ToList();

                if (tac.Count > 0)
                {
                    prod["codta"] = tac[0];
                }
                else
                {
                    clsTaco taco = new clsTaco();
                    taco.Descripcion = row.Cells[16].Value.ToString().Trim();
                    admTaco.insert(taco);
                    prod["codta"] = Convert.ToInt32(taco.CodTaco);
                    CargarTaco();
                }
            }
            //////////////////////////////////////////////////////////////////


            ///////datos adicionales//////////////////////////////////////////
            prod["codtip"] = 10;

            prod["descripcion"] = "";
            prod["coddeta"] = 0;

            prod["codtipota"] = 0;

            prod["codmo"] = 23;

            prod["codserp"] = 0;

            prod["coduni"] = 2;

            prod["control"] = true;

            prod["igv"] = true;
            prod["precioconigv"] = true;
            prod["estado"] = true;

            prod["CodTipoCierre"] = 0;
            prod["codcuello"] = 0;
            prod["codmanga"] = 0;
            prod["coddiseño"] = 0;
            prod["codtipodiseño"] = 0;
            prod["codusu"] = frmLogin.iCodUser;
            prod["codalmacen"] = frmLogin.iCodAlmacen;
            if (Convert.ToInt32(row.Cells[6].Value) != 0)
            {
                prod["codpro"] = Convert.ToInt32(row.Cells[6].Value);
            }
            prod["newid"] = 0;
            prod["error"] = 0; //ser utiliza para verificar si la fila del producto tiene errores (datos vacios o mal formato)
            if (err > 0)
            {
                posiciones.Add(row.Index); //se guarda la posicion del error
                prod["error"] = err; //se guarda dato de la cantidad de errores
            }
            //////////////////////////////////////////////////////////////////////////////
        }

        private void rbcaballeros_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            //rbcaballeros.IsChecked = true;
            //rbdamas.IsChecked = false;
        }

        private void rbdamas_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            //rbcaballeros.IsChecked = false;
            //rbdamas.IsChecked = true;
        }

        private void radDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void cboHojas_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (cboHojas.SelectedIndex != -1)
            {
                //dgproductos.Rows.Clear();
                dgproductos.DataSource = null;
                dgproductos.DataSource = resultado.Tables[cboHojas.SelectedIndex];
                verificaProductosInsertados();
            }
        }

        private void frmExcel_Load(object sender, EventArgs e)
        {
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int n = Convert.ToInt32(e.Argument);
            e.Result = cargaPrincipal(n,worker, e);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            radWaitingBar1.StopWaiting();
            radWaitingBar1.ResetWaiting();
            btnCarga.Enabled = true;
            btnOpenfile.Enabled = true;

            if (e.Cancelled)
            {
                MessageBox.Show("Se cancelo la operacion");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Hubo un Error....");
            }
            else
            {
                

                if (posiciones.Count > 0)
                {
                    MessageBox.Show("Finalizo carga con errores, revise log de errores");
                    radLabel2.Visible = true;

                }
                else
                {
                    MessageBox.Show("Finalizo carga satisfactoriamente");
                    radLabel2.Visible = false;

                }
            }
            radWaitingBar1.Visible = false;
        }

        private void verificaProductosInsertados()
        {
            Cargacodigobarras();


            foreach(GridViewRowInfo dr in dgproductos.Rows)
            {
                var existe = codigosbarra.AsEnumerable().Where(x => x.Field<string>("codcompraproducto").Trim().Equals(dr.Cells[0].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase)).Any();

                if (existe)
                {
                    var a = codigosbarra.AsEnumerable()
                            .Where(x => x.Field<string>("codcompraproducto").Trim().Equals(dr.Cells[0].Value.ToString().Trim(),StringComparison.OrdinalIgnoreCase))
                            .Select(x => x.Field<int>("codproducto")).ToList()[0];
                    
                    dr.Cells[6].Value = a.ToString().Trim();
                }
                else
                {
                    dr.Cells[6].Value = 0;   
                }
            }
        }

        public void creaFilas(DataTable tabla)
        {
            tabla.Columns.Add(new DataColumn("codusu", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codtalla", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("vtalla", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("codlin", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codfam", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("coduni", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codtip", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codmar", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("control", typeof(System.Boolean)));
            tabla.Columns.Add(new DataColumn("referencia", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("descripcion", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("igv", typeof(System.Boolean)));
            tabla.Columns.Add(new DataColumn("precioconigv", typeof(System.Boolean)));
            tabla.Columns.Add(new DataColumn("estado", typeof(System.Boolean)));
            tabla.Columns.Add(new DataColumn("precioca", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("preciocompra", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("codmo", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("coddeta", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codcor", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codta", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codcolorp", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codcolors", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codtipota", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codgru", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codserp", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codcomprapro", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("CodTipoCierre", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codtemp", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codcuello", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codmanga", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("coddiseño", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codtipodiseño", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("anio", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codalmacen", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codpro", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("newid", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("error", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("preciodir", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("preciopro", typeof(System.Decimal)));
        }

        public void creaFilasDetNI(DataTable tabla)
        {
            tabla.Columns.Add(new DataColumn("codpro", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codnota", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codalma", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("moneda", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("unidad", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("serielote", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("cantidad", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("precio", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("subtotal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto1", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto2", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto3", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("montodscto", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("igv", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("flete", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("importe", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("precioreal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("valoreal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("fecha", typeof(System.DateTime)));
            tabla.Columns.Add(new DataColumn("codusu", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("valorrealS", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("codrequer", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("bonific", typeof(System.Boolean)));
            tabla.Columns.Add(new DataColumn("newid", typeof(System.Int32)));
        }

        public void creaFilasDetFactura(DataTable tabla)
        {
            tabla.Columns.Add(new DataColumn("codpro", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codnota", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("codfactura", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codalma", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("moneda", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("unidad", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("serielote", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("cantidad", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("precio", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("subtotal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto1", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto2", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto3", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("montodscto", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("igv", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("flete", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("importe", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("precioreal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("valoreal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("fecha", typeof(System.DateTime)));
            tabla.Columns.Add(new DataColumn("codusu", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("valorrealS", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("codrequer", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("bonific", typeof(System.Int16)));

        }

        public void creaFilasDetTransfer(DataTable tabla)
        {
            tabla.Columns.Add(new DataColumn("codpro", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codtrans", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codalmaorig", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("codalmadest", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("unidad", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("serielote", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("cantidad", typeof(System.String)));
            tabla.Columns.Add(new DataColumn("precio", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("subtotal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto1", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto2", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("dscto3", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("montodscto", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("igv", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("importe", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("precioreal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("cantidadp", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("codprov", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("valoreal", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("precioigv", typeof(System.Decimal)));
            tabla.Columns.Add(new DataColumn("codusu", typeof(System.Int32)));
            tabla.Columns.Add(new DataColumn("promedio", typeof(System.Decimal)));

        }

        public void ingresoNI(clsNotaIngreso nota)
        {
            if (admnota.insert(nota))
            {
                DataRow dn;

                if (rbStock.IsChecked || rbCompras.IsChecked)
                {
                    foreach (GridViewRowInfo row in dgproductos.Rows)
                    {   
                        //valido que la columna de codigo de producto sea diferente de 0, osea que este registrado en la BD
                        if (Convert.ToDecimal(row.Cells[6].Value) != 0)
                        {   
                            //valido que la columna stock del producto a insertar stock sea mayor a 0
                            if (Convert.ToDecimal(row.Cells[9].Value) > 0)
                            {
                                dn = detallenota.NewRow();
                                añadedetalleNI(dn, row);
                                detallenota.Rows.Add(dn);
                            }
                        }
                    }
                }
                else
                {
                    foreach (GridViewRowInfo row in dgproductos.Rows)
                    {
                        bool existe = productostable.AsEnumerable()
                            .Where(x => x["codcomprapro"].ToString().Trim().Equals(row.Cells[0].Value.ToString().Trim(), StringComparison.OrdinalIgnoreCase))
                            .Any();

                        if (existe)
                        {
                            //valido que la columna de codigo de producto sea diferente de 0, osea que este registrado en la BD
                            if (Convert.ToDecimal(row.Cells[6].Value) != 0)
                            {
                                //valido que el producto no este en la lista de productos con errores
                                if (posiciones.IndexOf(row.Index) == -1)
                                {
                                    //valido que la columna stock del producto a insertar stock sea mayor a 0
                                    if (Convert.ToDecimal(row.Cells[9].Value) > 0)
                                    {
                                        dn = detallenota.NewRow();
                                        añadedetalleNI(dn, row);
                                        detallenota.Rows.Add(dn);
                                    }
                                }
                            }
                        }
                    }

                }

                if (detallenota.Rows.Count > 0)
                {
                    if (admnota.insertdetalleMasivo(detallenota))
                    {

                    }
                }
            }            
        }

        public void ingresoFactura(clsFactura fac)
        {
            fac.CodNotaIngreso = Convert.ToInt32(nota.CodNotaIngreso);

            if (admfactura.insert(fac))
            {
                DataRow dn;

                foreach (GridViewRowInfo row in dgproductos.Rows)
                {
                    if (Convert.ToDecimal(row.Cells[6].Value) != 0)
                    {
                        if (Convert.ToDecimal(row.Cells[9].Value) > 0)
                        {
                            dn = detallefactura.NewRow();
                            añadedetalleFactura(dn, row);
                            detallefactura.Rows.Add(dn);
                        }
                    }
                }
                
                if (detallefactura.Rows.Count > 0)
                {
                    if (admfactura.insertdetalleMasivo(detallefactura))
                    {
                        
                    }
                }
            }
        }

        public void IngresoTransfer(clsTransferencia transfer)
        {

            if (admtransfer.insert(transfer))
            {
                DataRow dn;

                foreach (GridViewRowInfo row in dgproductos.Rows)
                {
                    if (Convert.ToDecimal(row.Cells[6].Value) != 0)
                    {
                        if (Convert.ToDecimal(row.Cells[9].Value) > 0)
                        {
                            dn = detalletrasnfer.NewRow();
                            añadedetalleFactura(dn, row);
                            detalletrasnfer.Rows.Add(dn);
                        }
                    }
                }

                if (detalletrasnfer.Rows.Count > 0)
                {
                    if (admtransfer.insertdetalleMasivo(detalletrasnfer))
                    {

                    }
                }
            }
        }

        private void añadedetalleNI(DataRow dn, GridViewRowInfo row)
        {

            dn["codpro"]= Convert.ToInt32(row.Cells[6].Value);
            dn["codnota"]= Convert.ToInt32(nota.CodNotaIngreso);
            dn["codalma"]= frmLogin.iCodAlmacen;
            dn["moneda"] = 1;
            dn["unidad"] = 2;
            dn["serielote"] = 0;
            dn["cantidad"]= Convert.ToDecimal(row.Cells[9].Value);
            dn["precio"]= Convert.ToDecimal(row.Cells[11].Value);
            dn["subtotal"]= Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value));
            dn["dscto1"]= Convert.ToDecimal(0);
            dn["dscto2"] = Convert.ToDecimal(0);
            dn["dscto3"] = Convert.ToDecimal(0);
            dn["montodscto"] = Convert.ToDecimal(0);
            dn["igv"]= (Convert.ToDecimal(dn["subtotal"]) / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18);
            dn["flete"]= Convert.ToDecimal(0);
            dn["importe"]= Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value));
            dn["precioreal"]= Convert.ToDecimal(row.Cells[11].Value);
            dn["valoreal"]= Convert.ToDecimal(row.Cells[11].Value) - ((Convert.ToDecimal(row.Cells[11].Value) / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18));
            dn["fecha"]= DateTime.Now.Date;
            dn["codusu"]= frmLogin.iCodUser;
            dn["valorrealS"]= Convert.ToDecimal(row.Cells[11].Value) - ((Convert.ToDecimal(row.Cells[11].Value) / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18));
            dn["codrequer"] = 0;
            dn["bonific"] = false;
 
        }

        private void añadedetalleFactura(DataRow dn, GridViewRowInfo row)
        {

            dn["codpro"] = Convert.ToInt32(row.Cells[6].Value);
            dn["codtrans"] = Convert.ToInt32(transfer.CodTransDir);
            dn["codalmaorig"] = frmLogin.iCodAlmacen;
            dn["codalmadest"] = transfer.CodAlmacenDestino;
            dn["unidad"] = 2;
            dn["serielote"] = 0;
            dn["cantidad"] = Convert.ToDecimal(row.Cells[9].Value);
            dn["precio"] = Convert.ToDecimal(row.Cells[11].Value);
            dn["subtotal"] = Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value));
            dn["dscto1"] = Convert.ToDecimal(0);
            dn["dscto2"] = Convert.ToDecimal(0);
            dn["dscto3"] = Convert.ToDecimal(0);
            dn["montodscto"] = Convert.ToDecimal(0);
            dn["igv"] = (Convert.ToDecimal(dn["subtotal"]) / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18);
            dn["importe"] = Convert.ToDecimal(row.Cells[9].Value) * (Convert.ToDecimal(row.Cells[11].Value));
            dn["precioreal"] = Convert.ToDecimal(row.Cells[11].Value);
            dn["cantidadp"] = Convert.ToDecimal(row.Cells[9].Value);
            dn["codprov"] = 0;
            dn["valoreal"] = Convert.ToDecimal(row.Cells[11].Value) - ((Convert.ToDecimal(row.Cells[11].Value) / Convert.ToDecimal(1.18)) * Convert.ToDecimal(0.18));
            dn["precioigv"] = 0;
            dn["codusu"] = frmLogin.iCodUser;
            dn["promedio"] = dn["valoreal"];

        }

        private void dgproductos_RowPaint(object sender, GridViewRowPaintEventArgs e)
        {
            try
            {
                if (e.Row != null)
                {
                    GridDataRowElement dataRow = e.Row as GridDataRowElement;
                    if (dataRow != null)
                    {
                        double? value = 0;
                        if (dataRow.RowInfo.Cells[6].Value != DBNull.Value)
                        {
                            value = Convert.ToDouble(dataRow.RowInfo.Cells[6].Value);
                        }

                        if (value == 0)
                        {
                            Pen pen = Pens.Red;
                            Size rowSize = dataRow.Size;
                            rowSize.Height -= 5;
                            rowSize.Width -= 5;
                            //e.Row.BackColor = Color.LightGreen;
                            e.Graphics.DrawRectangle(pen, new Rectangle(new Point(2, 2), rowSize));
                        }
                        else
                        {
                            Pen pen = Pens.RoyalBlue;
                            Size rowSize = dataRow.Size;
                            rowSize.Height -= 5;
                            rowSize.Width -= 5;
                            //e.Row.BackColor = Color.LightGreen;
                            e.Graphics.DrawRectangle(pen, new Rectangle(new Point(2, 2), rowSize));
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void rbInsert_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
        }

        private void rbUpdate_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
        }

        private void radLabel2_Click(object sender, EventArgs e)
        {
            frmErrorExcel listaerrores = new frmErrorExcel();
            listaerrores.errores = posiciones;
            listaerrores.ShowDialog();
        }
    }
}
