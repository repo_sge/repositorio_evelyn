﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
    public partial class frmRegeneracionArchivos : DevComponents.DotNetBar.Office2007Form
    {

        //SIGEFA.SunatFacElec.Conexion conex = new SunatFacElec.Conexion();
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
        clsAdmNotaCredito admnc = new clsAdmNotaCredito();
        clsAdmCliente AdmCli = new clsAdmCliente();
        clsAdmSerie Admser = new clsAdmSerie();
        clsAdmTransaccion AdmTran = new clsAdmTransaccion();
        clsAdmGuiaRemision AdmGuia = new clsAdmGuiaRemision();
        clsAdmDocumentoIdentidad AdmDocumentoIdentidad = new clsAdmDocumentoIdentidad();
        clsAdmAlmacen admalma = new clsAdmAlmacen();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;
        clsFacturaVenta venta = new clsFacturaVenta();
        clsNotaCredito nc;

        clsSerie ser = new clsSerie();
        clsTransaccion tran = new clsTransaccion();
        clsFacturaVenta ventaConsultada;
        clsCliente cli = new clsCliente();
        clsGuiaRemision guia = new clsGuiaRemision();
        public List<clsDetalleFacturaVenta> ListaDetalleVenta = new List<clsDetalleFacturaVenta>();
        public List<clsDetalleNotaCredito> listadetalleNC = new List<clsDetalleNotaCredito>();
        DataTable detalleTableVenta;
        DataTable detalleNC;
        clsAdmTipoDocumento admTipoDocumento = new clsAdmTipoDocumento();

        public Byte[] firmadigital { get; set; }

        /**
		 * Variables de tipos primitivos
		 */
        Int32 NumeroDocumento;

        /*
		 * Clase utilizada para la version UBL 2.1
		 */
        SIGEFA.SunatFacElec.Facturacion facturacion = new SunatFacElec.Facturacion();


        public frmRegeneracionArchivos()
        {
            InitializeComponent();
        }

        private void CargaLista()
        {
            if (Convert.ToInt32(cmbTipoDoc.SelectedValue) != 4)
            {
                dgvVentaSinRepositorio.ClearSelection();
                dgvVentaSinRepositorio.AutoGenerateColumns = false;
                dgvVentaSinRepositorio.DataSource = data;
                data.DataSource = AdmVenta.VentaSinRepositorio(frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value, Convert.ToInt32(cmbTipoDoc.SelectedValue));
                data.Filter = String.Empty;
                filtro = String.Empty;
                if (dgvVentaSinRepositorio.Rows.Count > 0)
                {
                    btnGenerarArchivos.Enabled = true;
                }
                else
                {
                    btnGenerarArchivos.Enabled = false;
                }
            }
            else
            {
                dgvVentaSinRepositorio.ClearSelection();
                dgvVentaSinRepositorio.AutoGenerateColumns = false;
                dgvVentaSinRepositorio.DataSource = data;
                data.DataSource = admnc.NCsinRepositorio(frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value);
                data.Filter = String.Empty;
                filtro = String.Empty;
                if (dgvVentaSinRepositorio.Rows.Count > 0)
                {
                    btnGenerarArchivos.Enabled = true;
                }
                else
                {
                    btnGenerarArchivos.Enabled = false;
                }
            }
            
            
            dgvVentaSinRepositorio.Focus();
        }

        private async void btnGenerarArchivos_Click(object sender, EventArgs e)
        {
            try
            {
                if(dgvVentaSinRepositorio.Rows.Count > 0 && dgvVentaSinRepositorio.CurrentRow != null)
                {
                    foreach (DataGridViewRow row in dgvVentaSinRepositorio.Rows)
                    {
                        /**
						 * Cargar datos de cliente, venta y detalle venta
						 */

                        if (Convert.ToInt32(cmbTipoDoc.SelectedValue) == 4)
                        {
                            nc = new clsNotaCredito();
                            nc = admnc.CargaNotaCredito_Regeneracion(Convert.ToInt32(row.Cells[codFactura.Name].Value));

                            if (nc != null)
                            {
                                CargaTransaccion(nc.CodTipoTransaccion);

                                cli = AdmCli.MuestraCliente(nc.CodCliente);

                                //nc.DocumentoIdentidad = AdmDocumentoIdentidad.MuestraDocumentoIdentidad(Convert.ToInt32(ventaConsultada.CodigoDocumentoIdentidad));

                                CargaDetalleNC();
                                RecorreDetalleNC();

                                /*
                                 * Agregar codigo empresa
                                 */
                                //nc.CodEmpresa = frmLogin.iCodEmpresa;

                                await facturacion.DatosNCredito(cli, nc, listadetalleNC);
                                //con.DatosNCredito(cli, nc, detalleNotaCredito);
                                firmadigital = facturacion.LogoEmp;

                                //string tipoDocumento = (ventaConsultada.CodTipoDocumento == 1) ? "B" : "F";

                                //MessageBox.Show("Los archivos XML y PDF para el comprobante " +
                                //                tipoDocumento + ventaConsultada.Serie +
                                //                "-" + ventaConsultada.NumDoc +
                                //                " fueron generados correctamente",
                                //                "Venta", MessageBoxButtons.OK,
                                //                MessageBoxIcon.Information);


                                listadetalleNC.Clear();
                                row.DefaultCellStyle.BackColor = Color.Green;
                            }
                            else
                            {
                                MessageBox.Show("El documento solicitado no existe", "Venta",
                                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            ventaConsultada = new clsFacturaVenta();
                            ventaConsultada = AdmVenta.CargaFacturaVenta_Regeneracion(Convert.ToInt32(row.Cells[codFactura.Name].Value));

                            if (ventaConsultada != null)
                            {
                                CargaTransaccion(ventaConsultada.CodTipoTransaccion);

                                cli = AdmCli.MuestraCliente(ventaConsultada.CodCliente);

                                if (cli.Tipodocidentidad.Idtipodocumentoidentidad == 1)
                                {
                                    cli.Tipodocidentidad.Codsunat = "1";
                                }
                                if (cli.Tipodocidentidad.Idtipodocumentoidentidad == 2)
                                {
                                    cli.Tipodocidentidad.Codsunat = "6";
                                }

                                ventaConsultada.DocumentoIdentidad = AdmDocumentoIdentidad.MuestraDocumentoIdentidad(Convert.ToInt32(ventaConsultada.CodigoDocumentoIdentidad));

                                CargaDetalle();
                                RecorreDetalle();

                                /*
                                 * Agregar codigo empresa
                                 */
                                //ventaConsultada.CodEmpresa = frmLogin.iCodEmpresa;

                                await facturacion.GeneraDocumento(cli, ventaConsultada, ListaDetalleVenta);
                                firmadigital = facturacion.LogoEmp;

                                //string tipoDocumento = (ventaConsultada.CodTipoDocumento == 1) ? "B" : "F";

                                //MessageBox.Show("Los archivos XML y PDF para el comprobante " +
                                //                tipoDocumento + ventaConsultada.Serie +
                                //                "-" + ventaConsultada.NumDoc +
                                //                " fueron generados correctamente",
                                //                "Venta", MessageBoxButtons.OK,
                                //                MessageBoxIcon.Information);


                                ListaDetalleVenta.Clear();
                                row.DefaultCellStyle.BackColor = Color.Green;
                            }
                            else
                            {
                                MessageBox.Show("El documento solicitado no existe", "Venta",
                                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                            

                        

                    }
                    CargaLista();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error: " + ex.Message, "Venta",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void RecorreDetalleNC()
        {
            if (detalleNC.Rows.Count > 0)
            {
                foreach (DataRow row in detalleNC.Rows)
                {
                    añadedetalleNC(row);
                }
            }
        }

        private void CargaDetalleNC()
        {
            detalleNC = admnc.CargaDetalle(Convert.ToInt32(nc.CodNotaCredito));
        }

        private void añadedetalleNC(DataRow fila)
        {

            clsDetalleNotaCredito detafac = new clsDetalleNotaCredito();
            detafac.CodNotaCredito = Convert.ToInt32(nc.CodNotaCredito);
            detafac.CodProducto = Convert.ToInt32(fila["codproducto"]);
            detafac.CodAlmacen = frmLogin.iCodAlmacen;
            detafac.UnidadIngresada = Convert.ToInt32(fila["codUnidadMedida"]);
            detafac.SerieLote = "0";
            detafac.Cantidad = Convert.ToDouble(fila["cantidad"]);
            detafac.PrecioUnitario = Convert.ToDouble(fila["preciounitario"]);
            detafac.Subtotal = Convert.ToDouble(fila["subtotal"]);
            detafac.Descuento1 = Convert.ToDouble(fila["descuento1"]);
            detafac.Descuento2 = Convert.ToDouble(fila["descuento2"]);
            detafac.Descuento3 = Convert.ToDouble(fila["descuento3"]);
            detafac.MontoDescuento = Convert.ToDouble(fila["montodscto"]);
            detafac.Igv = Convert.ToDouble(fila["igv"]);
            detafac.Importe = Convert.ToDouble(fila["importe"]);
            detafac.PrecioReal = Convert.ToDouble(fila["precioreal"]);
            detafac.ValoReal = Convert.ToDouble(fila["valoreal"]);
            detafac.FechaIngreso = Convert.ToDateTime(fila["fechaingreso"]);
            detafac.DescripcionNC = "";
            detafac.Moneda = Convert.ToInt32(fila["moneda"]);
            detafac.CodUser = frmLogin.iCodUser;
            detafac.TipoImpuesto = "10";
            listadetalleNC.Add(detafac);
        }

        private void dgvVentaSinRepositorio_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            //if (dgvVentaSinRepositorio.Rows.Count >= 1 && e.Row.Selected)
            //{
            //    venta.CodFacturaVenta = e.Row.Cells[codFactura.Name].Value.ToString();
            //    NumeroDocumento = Convert.ToInt32(e.Row.Cells[codcliente.Name].Value);
            //}
        }

        private void CargaDetalle()
        {
            detalleTableVenta = AdmVenta.CargaDetalle_Regeneracion(Convert.ToInt32(ventaConsultada.CodFacturaVenta), frmLogin.iCodAlmacen);
        }

        private void RecorreDetalle()
        {
            if (detalleTableVenta.Rows.Count > 0)
            {
                foreach (DataRow row in detalleTableVenta.Rows)
                {
                    añadedetalle(row);
                }
            }
        }

        private void añadedetalle(DataRow fila)
        {
            clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
            deta.CodProducto = Convert.ToInt32(fila["codProducto"]);
            deta.CodVenta = Convert.ToInt32(ventaConsultada.CodFacturaVenta);
            deta.CodAlmacen = frmLogin.iCodAlmacen;
            deta.UnidadIngresada = Convert.ToInt32(fila["codUnidadMedida"]);
            deta.SerieLote = "";
            deta.Cantidad = Convert.ToDecimal(fila["cantidad"]);
            deta.PrecioUnitario = Convert.ToDecimal(fila["preciounitario"]);
            deta.Subtotal = Convert.ToDecimal(fila["subtotal"]);
            deta.Descuento1 = Convert.ToDecimal(fila["descuento1"]);
            deta.MontoDescuento = Convert.ToDecimal(fila["montodscto"]);
            deta.Igv = Convert.ToDecimal(fila["igv"]);
            deta.Importe = Convert.ToDecimal(fila["importe"]);
            deta.PrecioReal = Convert.ToDecimal(fila["precioreal"]);
            deta.ValoReal = Convert.ToDecimal(fila["valoreal"]);
            deta.CodUser = frmLogin.iCodUser;
            deta.CantidadPendiente = Convert.ToDecimal(fila["cantidad"]);
            deta.Moneda = 1;
            deta.Descripcion = fila["producto"].ToString();
            deta.CodTipoArticulo = 1;
            deta.Impuesto = fila["tipoimpuesto"].ToString();
            deta.TipoUnidad = 1;
            deta.CodDetalleCotizacion = 0;
            deta.CodDetallePedido = Convert.ToInt32(fila["codDetalle"]);

            ListaDetalleVenta.Add(deta);
        }

        private void CargaTransaccion(Int32 CodTransaccion)
        {
            tran = AdmTran.MuestraTransaccion(CodTransaccion);
            tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
        }
        
        private void btnBuscarDocumentos_Click(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dgvVentaSinRepositorio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                btnGenerarArchivos.PerformClick();
            }
        }

        private void frmRegeneracionArchivos_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void frmRegeneracionArchivos_Load(object sender, EventArgs e)
        {
            cargaTiposDoc();
            dtpDesde.Focus();
        }

        private void cargaTiposDoc()
        {
            cmbTipoDoc.ValueMember = "codTipoDocumento";
            cmbTipoDoc.DisplayMember = "descripcion";
            cmbTipoDoc.DataSource = admTipoDocumento.CargaTipoDocumento_repo();
            cmbTipoDoc.SelectedValue = 1;
        }

        private void dtpDesde_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B)
            {
                btnBuscarDocumentos.PerformClick();
            }
        }

        private void dtpHasta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B)
            {
                btnBuscarDocumentos.PerformClick();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
