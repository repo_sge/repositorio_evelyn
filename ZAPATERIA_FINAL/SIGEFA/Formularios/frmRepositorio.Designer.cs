﻿namespace SIGEFA.Formularios
{
    partial class frmRepositorio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRepositorio));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.cbSerie = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.cbEstadodoc = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.btnEnviarrepo = new DevComponents.DotNetBar.ButtonX();
            this.cbTipocomprobante = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.cbFinicio = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.cbFfin = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.btnSalir = new DevComponents.DotNetBar.ButtonX();
            this.btnBuscar = new DevComponents.DotNetBar.ButtonX();
            this.dgv_repositorio = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.idrepositorio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idAlmacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idEmpresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSucursal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idfacturaventa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipocomprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idtipocomprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.correlativo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaemision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecharegistro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.monto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadosunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mensajesunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombredocxml = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rutaxml = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombredocpdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rutapdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.node1 = new DevComponents.Tree.Node();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbFinicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_repositorio)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.dgv_repositorio);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(1, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1297, 468);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 4;
            this.groupPanel1.Text = "Repositorio";
            this.groupPanel1.Click += new System.EventHandler(this.groupPanel1_Click);
            // 
            // cbSerie
            // 
            this.cbSerie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbSerie.DisplayMember = "Text";
            this.cbSerie.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbSerie.FormattingEnabled = true;
            this.cbSerie.ItemHeight = 14;
            this.cbSerie.Items.AddRange(new object[] {
            this.comboItem6,
            this.comboItem7});
            this.cbSerie.Location = new System.Drawing.Point(51, 527);
            this.cbSerie.Name = "cbSerie";
            this.cbSerie.Size = new System.Drawing.Size(144, 20);
            this.cbSerie.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbSerie.TabIndex = 17;
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "BLC";
            this.comboItem6.Value = "1";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "FCC";
            this.comboItem7.Value = "2";
            // 
            // labelX5
            // 
            this.labelX5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(12, 524);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(31, 23);
            this.labelX5.TabIndex = 16;
            this.labelX5.Text = "Serie";
            // 
            // cbEstadodoc
            // 
            this.cbEstadodoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbEstadodoc.DisplayMember = "Text";
            this.cbEstadodoc.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbEstadodoc.FormattingEnabled = true;
            this.cbEstadodoc.ItemHeight = 14;
            this.cbEstadodoc.Items.AddRange(new object[] {
            this.comboItem3,
            this.comboItem4,
            this.comboItem5});
            this.cbEstadodoc.Location = new System.Drawing.Point(266, 498);
            this.cbEstadodoc.Name = "cbEstadodoc";
            this.cbEstadodoc.Size = new System.Drawing.Size(144, 20);
            this.cbEstadodoc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbEstadodoc.TabIndex = 15;
            this.cbEstadodoc.SelectionChangeCommitted += new System.EventHandler(this.cbEstadodoc_SelectionChangeCommitted);
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "No Enviado";
            this.comboItem3.Value = "-1";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "Enviado";
            this.comboItem4.Value = "0";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "Todos";
            this.comboItem5.Value = "3";
            // 
            // labelX4
            // 
            this.labelX4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(221, 495);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(39, 23);
            this.labelX4.TabIndex = 14;
            this.labelX4.Text = "Estado";
            // 
            // btnEnviarrepo
            // 
            this.btnEnviarrepo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnEnviarrepo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnviarrepo.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnEnviarrepo.Location = new System.Drawing.Point(1122, 512);
            this.btnEnviarrepo.Name = "btnEnviarrepo";
            this.btnEnviarrepo.Size = new System.Drawing.Size(75, 23);
            this.btnEnviarrepo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnEnviarrepo.TabIndex = 13;
            this.btnEnviarrepo.Text = "Enviar";
            this.btnEnviarrepo.Click += new System.EventHandler(this.btnEnviarrepo_Click);
            // 
            // cbTipocomprobante
            // 
            this.cbTipocomprobante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbTipocomprobante.DisplayMember = "Text";
            this.cbTipocomprobante.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbTipocomprobante.FormattingEnabled = true;
            this.cbTipocomprobante.ItemHeight = 14;
            this.cbTipocomprobante.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2});
            this.cbTipocomprobante.Location = new System.Drawing.Point(51, 497);
            this.cbTipocomprobante.Name = "cbTipocomprobante";
            this.cbTipocomprobante.Size = new System.Drawing.Size(144, 20);
            this.cbTipocomprobante.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbTipocomprobante.TabIndex = 12;
            this.cbTipocomprobante.SelectedIndexChanged += new System.EventHandler(this.cbTipocomprobante_SelectedIndexChanged);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "BLC";
            this.comboItem1.Value = "1";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "FCC";
            this.comboItem2.Value = "2";
            // 
            // labelX3
            // 
            this.labelX3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(13, 493);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(31, 23);
            this.labelX3.TabIndex = 11;
            this.labelX3.Text = "Tipo";
            // 
            // labelX2
            // 
            this.labelX2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(427, 521);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(49, 23);
            this.labelX2.TabIndex = 10;
            this.labelX2.Text = "F. Fin";
            // 
            // labelX1
            // 
            this.labelX1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(427, 495);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(49, 23);
            this.labelX1.TabIndex = 9;
            this.labelX1.Text = "F. Inicio";
            // 
            // cbFinicio
            // 
            this.cbFinicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.cbFinicio.BackgroundStyle.Class = "DateTimeInputBackground";
            this.cbFinicio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFinicio.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.cbFinicio.ButtonDropDown.Visible = true;
            this.cbFinicio.IsPopupCalendarOpen = false;
            this.cbFinicio.Location = new System.Drawing.Point(482, 498);
            // 
            // 
            // 
            // 
            // 
            // 
            this.cbFinicio.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFinicio.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.cbFinicio.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.cbFinicio.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFinicio.MonthCalendar.DisplayMonth = new System.DateTime(2018, 11, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.cbFinicio.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.cbFinicio.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.cbFinicio.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.cbFinicio.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFinicio.MonthCalendar.TodayButtonVisible = true;
            this.cbFinicio.Name = "cbFinicio";
            this.cbFinicio.Size = new System.Drawing.Size(126, 20);
            this.cbFinicio.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbFinicio.TabIndex = 8;
            this.cbFinicio.Click += new System.EventHandler(this.cbFinicio_Click);
            // 
            // cbFfin
            // 
            this.cbFfin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            // 
            // 
            // 
            this.cbFfin.BackgroundStyle.Class = "DateTimeInputBackground";
            this.cbFfin.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFfin.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.cbFfin.ButtonDropDown.Visible = true;
            this.cbFfin.IsPopupCalendarOpen = false;
            this.cbFfin.Location = new System.Drawing.Point(482, 524);
            // 
            // 
            // 
            // 
            // 
            // 
            this.cbFfin.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFfin.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.cbFfin.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.cbFfin.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFfin.MonthCalendar.DisplayMonth = new System.DateTime(2018, 11, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.cbFfin.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.cbFfin.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.cbFfin.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.cbFfin.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.cbFfin.MonthCalendar.TodayButtonVisible = true;
            this.cbFfin.Name = "cbFfin";
            this.cbFfin.Size = new System.Drawing.Size(126, 20);
            this.cbFfin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbFfin.TabIndex = 7;
            this.cbFfin.Click += new System.EventHandler(this.cbFfin_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSalir.Location = new System.Drawing.Point(1213, 512);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSalir.TabIndex = 2;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnBuscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuscar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnBuscar.Location = new System.Drawing.Point(1032, 512);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dgv_repositorio
            // 
            this.dgv_repositorio.AllowUserToAddRows = false;
            this.dgv_repositorio.AllowUserToDeleteRows = false;
            this.dgv_repositorio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dgv_repositorio.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_repositorio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_repositorio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idrepositorio,
            this.idAlmacen,
            this.idEmpresa,
            this.idSucursal,
            this.idfacturaventa,
            this.tipocomprobante,
            this.idtipocomprobante,
            this.comprobante,
            this.serie,
            this.correlativo,
            this.fechaemision,
            this.fecharegistro,
            this.monto,
            this.estadosunat,
            this.mensajesunat,
            this.nombredocxml,
            this.rutaxml,
            this.nombredocpdf,
            this.rutapdf});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_repositorio.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_repositorio.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_repositorio.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgv_repositorio.Location = new System.Drawing.Point(3, 1);
            this.dgv_repositorio.Name = "dgv_repositorio";
            this.dgv_repositorio.ReadOnly = true;
            this.dgv_repositorio.RowHeadersVisible = false;
            this.dgv_repositorio.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_repositorio.Size = new System.Drawing.Size(1281, 446);
            this.dgv_repositorio.TabIndex = 0;
            // 
            // idrepositorio
            // 
            this.idrepositorio.DataPropertyName = "idrepositorio";
            this.idrepositorio.HeaderText = "COD";
            this.idrepositorio.Name = "idrepositorio";
            this.idrepositorio.ReadOnly = true;
            this.idrepositorio.Width = 55;
            // 
            // idAlmacen
            // 
            this.idAlmacen.DataPropertyName = "idAlmacen";
            this.idAlmacen.HeaderText = "CODALMACEN";
            this.idAlmacen.Name = "idAlmacen";
            this.idAlmacen.ReadOnly = true;
            this.idAlmacen.Visible = false;
            this.idAlmacen.Width = 106;
            // 
            // idEmpresa
            // 
            this.idEmpresa.DataPropertyName = "idEmpresa";
            this.idEmpresa.HeaderText = "CODEMPRESA";
            this.idEmpresa.Name = "idEmpresa";
            this.idEmpresa.ReadOnly = true;
            this.idEmpresa.Visible = false;
            this.idEmpresa.Width = 107;
            // 
            // idSucursal
            // 
            this.idSucursal.DataPropertyName = "idSucursal";
            this.idSucursal.HeaderText = "CODSUCURSAL";
            this.idSucursal.Name = "idSucursal";
            this.idSucursal.ReadOnly = true;
            this.idSucursal.Visible = false;
            this.idSucursal.Width = 113;
            // 
            // idfacturaventa
            // 
            this.idfacturaventa.DataPropertyName = "idfacturaventa";
            this.idfacturaventa.HeaderText = "CODFACTURAVENTA";
            this.idfacturaventa.Name = "idfacturaventa";
            this.idfacturaventa.ReadOnly = true;
            this.idfacturaventa.Visible = false;
            this.idfacturaventa.Width = 141;
            // 
            // tipocomprobante
            // 
            this.tipocomprobante.DataPropertyName = "tipocomprobante";
            this.tipocomprobante.HeaderText = "TIPO COMPROBANTE";
            this.tipocomprobante.Name = "tipocomprobante";
            this.tipocomprobante.ReadOnly = true;
            this.tipocomprobante.Width = 143;
            // 
            // idtipocomprobante
            // 
            this.idtipocomprobante.DataPropertyName = "idtipocomprobante";
            this.idtipocomprobante.HeaderText = "CODTIPOCOMP";
            this.idtipocomprobante.Name = "idtipocomprobante";
            this.idtipocomprobante.ReadOnly = true;
            this.idtipocomprobante.Visible = false;
            this.idtipocomprobante.Width = 111;
            // 
            // comprobante
            // 
            this.comprobante.DataPropertyName = "comprobante";
            this.comprobante.HeaderText = "COMPROBANTE";
            this.comprobante.Name = "comprobante";
            this.comprobante.ReadOnly = true;
            this.comprobante.Width = 115;
            // 
            // serie
            // 
            this.serie.DataPropertyName = "serie";
            this.serie.HeaderText = "SERIE";
            this.serie.Name = "serie";
            this.serie.ReadOnly = true;
            this.serie.Width = 64;
            // 
            // correlativo
            // 
            this.correlativo.DataPropertyName = "correlativo";
            this.correlativo.HeaderText = "CORRELATIVO";
            this.correlativo.Name = "correlativo";
            this.correlativo.ReadOnly = true;
            this.correlativo.Width = 108;
            // 
            // fechaemision
            // 
            this.fechaemision.DataPropertyName = "fechaemision";
            this.fechaemision.HeaderText = "FECHAEMISION";
            this.fechaemision.Name = "fechaemision";
            this.fechaemision.ReadOnly = true;
            this.fechaemision.Width = 112;
            // 
            // fecharegistro
            // 
            this.fecharegistro.DataPropertyName = "fecharegistro";
            this.fecharegistro.HeaderText = "FECHA REGISTRO";
            this.fecharegistro.Name = "fecharegistro";
            this.fecharegistro.ReadOnly = true;
            this.fecharegistro.Width = 126;
            // 
            // monto
            // 
            this.monto.DataPropertyName = "monto";
            this.monto.HeaderText = "MONTO";
            this.monto.Name = "monto";
            this.monto.ReadOnly = true;
            this.monto.Width = 72;
            // 
            // estadosunat
            // 
            this.estadosunat.DataPropertyName = "estadosunat";
            this.estadosunat.HeaderText = "ESTADO SUNAT";
            this.estadosunat.Name = "estadosunat";
            this.estadosunat.ReadOnly = true;
            this.estadosunat.Width = 116;
            // 
            // mensajesunat
            // 
            this.mensajesunat.DataPropertyName = "mensajesunat";
            this.mensajesunat.HeaderText = "MENSAJE SUNAT";
            this.mensajesunat.Name = "mensajesunat";
            this.mensajesunat.ReadOnly = true;
            this.mensajesunat.Width = 122;
            // 
            // nombredocxml
            // 
            this.nombredocxml.DataPropertyName = "nombredocxml";
            this.nombredocxml.HeaderText = "NOMBRE XML";
            this.nombredocxml.Name = "nombredocxml";
            this.nombredocxml.ReadOnly = true;
            this.nombredocxml.Width = 104;
            // 
            // rutaxml
            // 
            this.rutaxml.DataPropertyName = "rutaxml";
            this.rutaxml.HeaderText = "RUTA XML";
            this.rutaxml.Name = "rutaxml";
            this.rutaxml.ReadOnly = true;
            this.rutaxml.Width = 87;
            // 
            // nombredocpdf
            // 
            this.nombredocpdf.DataPropertyName = "nombredocpdf";
            this.nombredocpdf.HeaderText = "NOMBRE PDF";
            this.nombredocpdf.Name = "nombredocpdf";
            this.nombredocpdf.ReadOnly = true;
            this.nombredocpdf.Width = 103;
            // 
            // rutapdf
            // 
            this.rutapdf.DataPropertyName = "rutapdf";
            this.rutapdf.HeaderText = "RUTA PDF";
            this.rutapdf.Name = "rutapdf";
            this.rutapdf.ReadOnly = true;
            this.rutapdf.Width = 86;
            // 
            // node1
            // 
            this.node1.Name = "node1";
            this.node1.Text = "node1";
            // 
            // frmRepositorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 559);
            this.Controls.Add(this.btnEnviarrepo);
            this.Controls.Add(this.cbSerie);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.labelX5);
            this.Controls.Add(this.cbFfin);
            this.Controls.Add(this.cbEstadodoc);
            this.Controls.Add(this.cbFinicio);
            this.Controls.Add(this.labelX4);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.labelX2);
            this.Controls.Add(this.cbTipocomprobante);
            this.Controls.Add(this.labelX3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRepositorio";
            this.Text = "Repositorio";
            this.Load += new System.EventHandler(this.frmRepositorio_Load);
            this.groupPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbFinicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_repositorio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput cbFinicio;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput cbFfin;
        private DevComponents.DotNetBar.ButtonX btnSalir;
        private DevComponents.DotNetBar.ButtonX btnBuscar;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgv_repositorio;
        private DevComponents.Tree.Node node1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbTipocomprobante;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.ButtonX btnEnviarrepo;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbEstadodoc;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbSerie;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.DotNetBar.LabelX labelX5;
        private System.Windows.Forms.DataGridViewTextBoxColumn idrepositorio;
        private System.Windows.Forms.DataGridViewTextBoxColumn idAlmacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn idEmpresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSucursal;
        private System.Windows.Forms.DataGridViewTextBoxColumn idfacturaventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipocomprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtipocomprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn comprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn correlativo;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaemision;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecharegistro;
        private System.Windows.Forms.DataGridViewTextBoxColumn monto;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadosunat;
        private System.Windows.Forms.DataGridViewTextBoxColumn mensajesunat;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombredocxml;
        private System.Windows.Forms.DataGridViewTextBoxColumn rutaxml;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombredocpdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn rutapdf;
    }
}