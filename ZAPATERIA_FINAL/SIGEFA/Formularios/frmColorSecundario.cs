﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace SIGEFA.Formularios
{
    public partial class frmColorSecundario : Telerik.WinControls.UI.RadForm
    {
        clsAdmColor admcolor = new clsAdmColor();
        public clsColorSecundario color = new clsColorSecundario();
        public Int32 Proceso = 0; //(1) Nuevo (2)Editar (3)CONSULTA F1
        public Int32 Procedencia = 0; //(1)Nota Ingreso (2)Nota Salida 

        clsValidar ok = new clsValidar();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;


        public frmColorSecundario()
        {
            InitializeComponent();
        }

        private void frmColorSecundario_Load(object sender, EventArgs e)
        {
            CargaLista();
            label2.Text = "Descripcion";
            label3.Text = "descripcion";
            if (Proceso == 3)
            {
                bloquearbotones();
            }
        }

        private void bloquearbotones()
        {
            btnNuevo.Visible = false;
            btnEditar.Visible = false;
            btnEliminar.Visible = false;
            btnGuardar.Text = "Aceptar";
            //btnGuardar.ImageIndex = 6;
        }

        private void CargaLista()
        {
            dgvColorSecundario.DataSource = data;
            data.DataSource = admcolor.MuestraColorSecundario();
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvColorSecundario.ClearSelection();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            groupBox3.Text = "Registro Nuevo";
            Proceso = 1;
        }

        private void CambiarEstados(Boolean Estado)
        {
            groupBox1.Visible = Estado;
            groupBox3.Visible = !Estado;
            btnGuardar.Enabled = !Estado;
            btnNuevo.Enabled = Estado;
            btnEditar.Enabled = Estado;
            btnEliminar.Enabled = Estado;
            txtDescripcion.Text = "";
            superValidator1.Validate();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            CambiarEstados(false);
            groupBox3.Text = "Editar Registro";
            Proceso = 2;
            txtCodigo.Text = color.CodColorSecundario.ToString();
            txtDescripcion.Text = color.Descripcion;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvColorSecundario.CurrentRow.Index != -1 && color.CodColorSecundario != 0)
            {
                DialogResult dlgResult = MessageBox.Show("Esta seguro que desea eliminar los datos definitivamente", "Colores", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    if (admcolor.deleteSecundario(color.CodColorSecundario))
                    {
                        MessageBox.Show("Los datos han sido eliminado correctamente", "Colores", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CargaLista();
                    }
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (groupBox1.Visible)
            {
                this.Close();
            }
            else
            {
                Proceso = 0;
                CambiarEstados(true);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (btnGuardar.Text == "Guardar")
            {
                if (superValidator1.Validate())
                {
                    if (Proceso != 0 && txtDescripcion.Text != "")
                    {
                        color.Descripcion = txtDescripcion.Text;

                        if (Proceso == 1)
                        {
                            if (admcolor.insertSecundario(color))
                            {
                                MessageBox.Show("Los datos se guardaron correctamente", "Moneda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CambiarEstados(true);
                                CargaLista();
                            }
                        }
                        else if (Proceso == 2)
                        {
                            if (admcolor.updateSecundario(color))
                            {
                                MessageBox.Show("Los datos se guardaron correctamente", "Moneda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CambiarEstados(true);
                                CargaLista();
                            }
                        }
                        Proceso = 0;
                    }
                }
            }
            else if (btnGuardar.Text == "Aceptar")
            {
                if (Proceso == 3)
                {
                    this.Close();
                }
            }
        }

        private void dgvColorPrimario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Proceso == 3)
            {
                //frmNotaSalida form = (frmNotaSalida)Application.OpenForms["frmNotaSalida"];
                //form.CodDocumento = doc.CodFormaPago;
                //form.txtDocRef.Text = doc.Sigla;
                this.Close();
            }
        }

        private void dgvColorPrimario_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            label3.Text = dgvColorSecundario.Columns[e.ColumnIndex].HeaderText;
            label2.Text = dgvColorSecundario.Columns[e.ColumnIndex].DataPropertyName;
        }

        private void dgvColorPrimario_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvColorSecundario.Rows.Count >= 1 && e.Row.Selected)
            {
                color.CodColorSecundario = Convert.ToInt32(e.Row.Cells[codcolorsecundario.Name].Value);
                color.Descripcion = e.Row.Cells[descripcion.Name].Value.ToString();
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
                if (Proceso == 3)
                {
                    btnGuardar.Enabled = true;
                }
            }
            else if (dgvColorSecundario.SelectedRows.Count == 0)
            {
                btnEditar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }

        private void frmColorSecundario_Shown(object sender, EventArgs e)
        {
            CargaLista();
            txtFiltro.Focus();
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtFiltro.Text.Length >= 2)
                {
                    data.Filter = String.Format("[{0}] like '*{1}*'", label2.Text.Trim(), txtFiltro.Text.Trim());
                }
                else
                {
                    data.Filter = String.Empty;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

       

        private void dgvColorSecundario_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                color.CodColorSecundario = Convert.ToInt32(dgvColorSecundario.Rows[dgvColorSecundario.CurrentCell.RowIndex].Cells[codcolorsecundario.Name].Value);
                color.Descripcion = dgvColorSecundario.Rows[dgvColorSecundario.CurrentCell.RowIndex].Cells[descripcion.Name].Value.ToString();
                btnEditar.Enabled = true;
                btnEliminar.Enabled = true;
                if (Proceso == 3)
                {
                    btnGuardar.Enabled = true;
                }
            }
            else if (dgvColorSecundario.SelectedRows.Count == 0)
            {
                btnEditar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }
    }
}
