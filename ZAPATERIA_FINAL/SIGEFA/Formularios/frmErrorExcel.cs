﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace SIGEFA.Formularios
{
    public partial class frmErrorExcel : Telerik.WinControls.UI.RadForm
    {
        public frmErrorExcel()
        {
            InitializeComponent();
        }

        public List<int> errores { get; set; }

        private void errorExcel_Load(object sender, EventArgs e)
        {
           
        }

        private void frmErrorExcel_Shown(object sender, EventArgs e)
        {
            foreach (int i in errores)
            {
                string error = "-> Hubo error en la fila " + (i + 1);
                lstErrores.Items.Add(error);
            }
        }
    }
}
