﻿namespace SIGEFA
{
    partial class mdi_Menu
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mdi_Menu));
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btArqueo = new DevComponents.DotNetBar.ButtonItem();
            this.biStockMinimos = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.biConsulta = new DevComponents.DotNetBar.ButtonItem();
            this.biModificar = new DevComponents.DotNetBar.ButtonItem();
            this.biAnular = new DevComponents.DotNetBar.ButtonItem();
            this.biEliminar = new DevComponents.DotNetBar.ButtonItem();
            this.rbOperaciones = new DevComponents.DotNetBar.RibbonBar();
            this.biNotadeIngreso = new DevComponents.DotNetBar.ButtonItem();
            this.biNotadeSalida = new DevComponents.DotNetBar.ButtonItem();
            this.biTransferencia = new DevComponents.DotNetBar.ButtonItem();
            this.biTransferenciasPendientes = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel7 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar15 = new DevComponents.DotNetBar.RibbonBar();
            this.biCajaAhorros = new DevComponents.DotNetBar.ButtonItem();
            this.biCajaGeneral = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar8 = new DevComponents.DotNetBar.RibbonBar();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.biCtas = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar13 = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar14 = new DevComponents.DotNetBar.RibbonBar();
            this.biMovimientosCaja = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem18 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar9 = new DevComponents.DotNetBar.RibbonBar();
            this.BiAperturaCaja = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel5 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar6 = new DevComponents.DotNetBar.RibbonBar();
            this.biPedidoVenta = new DevComponents.DotNetBar.ButtonItem();
            this.biPedidosPendientes = new DevComponents.DotNetBar.ButtonItem();
            this.biCotizacion = new DevComponents.DotNetBar.ButtonItem();
            this.biCotizacionesVigentes = new DevComponents.DotNetBar.ButtonItem();
            this.biCotizacionesAprobadas = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar3 = new DevComponents.DotNetBar.RibbonBar();
            this.biCobros = new DevComponents.DotNetBar.ButtonItem();
            this.btnMasivo = new DevComponents.DotNetBar.ButtonItem();
            this.biStockAlmacenes = new DevComponents.DotNetBar.ButtonItem();
            this.rbVentas = new DevComponents.DotNetBar.RibbonBar();
            this.biVenta = new DevComponents.DotNetBar.ButtonItem();
            this.biMuestraVentas = new DevComponents.DotNetBar.ButtonItem();
            this.biVentaSeparacion = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.biGuia = new DevComponents.DotNetBar.ButtonItem();
            this.biGuias = new DevComponents.DotNetBar.ButtonItem();
            this.biBuscarGuia = new DevComponents.DotNetBar.ButtonItem();
            this.biNotaCredito = new DevComponents.DotNetBar.ButtonItem();
            this.ciNotasdeCredito = new DevComponents.DotNetBar.ButtonItem();
            this.biNotaDebito = new DevComponents.DotNetBar.ButtonItem();
            this.ciNotasdeDebito = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel6 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbCompras = new DevComponents.DotNetBar.RibbonBar();
            this.biPedidoCompra = new DevComponents.DotNetBar.ButtonItem();
            this.btnVerCompras = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem16 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem17 = new DevComponents.DotNetBar.ButtonItem();
            this.biPagos = new DevComponents.DotNetBar.ButtonItem();
            this.btnRequerimiento = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
            this.biHistorialRequerimiento = new DevComponents.DotNetBar.ButtonItem();
            this.biConsolidado = new DevComponents.DotNetBar.ButtonItem();
            this.biOrdenCompra = new DevComponents.DotNetBar.ButtonItem();
            this.biOrdenesCompras = new DevComponents.DotNetBar.ButtonItem();
            this.biCompraOrden = new DevComponents.DotNetBar.ButtonItem();
            this.biHistorialFacturaciones = new DevComponents.DotNetBar.ButtonItem();
            this.biGuiasSinFacturar = new DevComponents.DotNetBar.ButtonItem();
            this.biNotaCreditoCompra = new DevComponents.DotNetBar.ButtonItem();
            this.biNotasCreditoCompras = new DevComponents.DotNetBar.ButtonItem();
            this.btnNotaDebitoC = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.riReportesGeneral = new DevComponents.DotNetBar.RibbonBar();
            this.btnReporte = new DevComponents.DotNetBar.ButtonItem();
            this.biDescuentos = new DevComponents.DotNetBar.ButtonItem();
            this.rbReportes = new DevComponents.DotNetBar.RibbonBar();
            this.biInventario = new DevComponents.DotNetBar.ButtonItem();
            this.biKardex = new DevComponents.DotNetBar.ButtonItem();
            this.biRotacionProducto = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar7 = new DevComponents.DotNetBar.RibbonBar();
            this.bitermetro = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.biBackup = new DevComponents.DotNetBar.ButtonItem();
            this.biImport = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar4 = new DevComponents.DotNetBar.RibbonBar();
            this.biTablas = new DevComponents.DotNetBar.ButtonItem();
            this.biUnidades = new DevComponents.DotNetBar.ButtonItem();
            this.biFamilias = new DevComponents.DotNetBar.ButtonItem();
            this.biMarcas = new DevComponents.DotNetBar.ButtonItem();
            this.biTipoArticulo = new DevComponents.DotNetBar.ButtonItem();
            this.biCaracteristica = new DevComponents.DotNetBar.ButtonItem();
            this.biDocumentos = new DevComponents.DotNetBar.ButtonItem();
            this.biTransacciones = new DevComponents.DotNetBar.ButtonItem();
            this.biTipoCambio = new DevComponents.DotNetBar.ButtonItem();
            this.biAutorizado = new DevComponents.DotNetBar.ButtonItem();
            this.biFormaPago = new DevComponents.DotNetBar.ButtonItem();
            this.biMetodoPago = new DevComponents.DotNetBar.ButtonItem();
            this.biListasPrecios = new DevComponents.DotNetBar.ButtonItem();
            this.biVehiculosTransporte = new DevComponents.DotNetBar.ButtonItem();
            this.biConductores = new DevComponents.DotNetBar.ButtonItem();
            this.biEmpresasTransporte = new DevComponents.DotNetBar.ButtonItem();
            this.biZonas = new DevComponents.DotNetBar.ButtonItem();
            this.biVendedores = new DevComponents.DotNetBar.ButtonItem();
            this.biDestaques = new DevComponents.DotNetBar.ButtonItem();
            this.biBancos = new DevComponents.DotNetBar.ButtonItem();
            this.biCuentasCorrientes = new DevComponents.DotNetBar.ButtonItem();
            this.biTarjetaPago = new DevComponents.DotNetBar.ButtonItem();
            this.biTipoEgresoCaja = new DevComponents.DotNetBar.ButtonItem();
            this.biParametros = new DevComponents.DotNetBar.ButtonItem();
            this.biVigenciaCotizaciones = new DevComponents.DotNetBar.ButtonItem();
            this.rbConfigurar = new DevComponents.DotNetBar.RibbonBar();
            this.biEmpresa = new DevComponents.DotNetBar.ButtonItem();
            this.biSucursal = new DevComponents.DotNetBar.ButtonItem();
            this.biAlmacen = new DevComponents.DotNetBar.ButtonItem();
            this.biUsuarios = new DevComponents.DotNetBar.ButtonItem();
            this.biRepositorio = new DevComponents.DotNetBar.ButtonItem();
            this.biRegeneracion = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbNegocio = new DevComponents.DotNetBar.RibbonBar();
            this.biProductos = new DevComponents.DotNetBar.ButtonItem();
            this.biCatalogo = new DevComponents.DotNetBar.ButtonItem();
            this.biClientes = new DevComponents.DotNetBar.ButtonItem();
            this.biProveedores = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel10 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar10 = new DevComponents.DotNetBar.RibbonBar();
            this.biRegistroCompras = new DevComponents.DotNetBar.ButtonItem();
            this.biRegistroVentas = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonPanel11 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar12 = new DevComponents.DotNetBar.RibbonBar();
            this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonBar11 = new DevComponents.DotNetBar.RibbonBar();
            this.Reporte = new DevComponents.DotNetBar.ButtonItem();
            this.biLogout = new DevComponents.DotNetBar.ButtonItem();
            this.rtVentas = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtCompras = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtOperaciones = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtEntidades = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtReportes = new DevComponents.DotNetBar.RibbonTabItem();
            this.rtAdministrador = new DevComponents.DotNetBar.RibbonTabItem();
            this.rbCaja = new DevComponents.DotNetBar.RibbonTabItem();
            this.Libros = new DevComponents.DotNetBar.RibbonTabItem();
            this.ribbonTabItem1 = new DevComponents.DotNetBar.RibbonTabItem();
            this.rbLibrosElectronicos = new DevComponents.DotNetBar.RibbonTabItem();
            this.liTipodeCambio = new DevComponents.DotNetBar.LabelItem();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.sEmpresa = new System.Windows.Forms.ToolStripStatusLabel();
            this.sAlmacen = new System.Windows.Forms.ToolStripStatusLabel();
            this.sIP = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabStrip1 = new DevComponents.DotNetBar.TabStrip();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel7.SuspendLayout();
            this.ribbonPanel5.SuspendLayout();
            this.ribbonPanel6.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.ribbonPanel10.SuspendLayout();
            this.ribbonPanel11.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel7);
            this.ribbonControl1.Controls.Add(this.ribbonPanel10);
            this.ribbonControl1.Controls.Add(this.ribbonPanel5);
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Controls.Add(this.ribbonPanel6);
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.ribbonPanel4);
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Controls.Add(this.ribbonPanel11);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.ForeColor = System.Drawing.Color.Black;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biLogout,
            this.rtVentas,
            this.rtCompras,
            this.rtOperaciones,
            this.rtEntidades,
            this.rtReportes,
            this.rtAdministrador,
            this.rbCaja,
            this.Libros,
            this.ribbonTabItem1,
            this.rbLibrosElectronicos,
            this.liTipodeCambio});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.Size = new System.Drawing.Size(640, 151);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 4;
            this.ribbonControl1.Text = "ribbonControl1";
            this.ribbonControl1.Click += new System.EventHandler(this.ribbonControl1_Click);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel2.Controls.Add(this.ribbonBar5);
            this.ribbonPanel2.Controls.Add(this.ribbonBar1);
            this.ribbonPanel2.Controls.Add(this.rbOperaciones);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 2;
            this.ribbonPanel2.Visible = false;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.DragDropSupport = true;
            this.ribbonBar5.Images = this.imageList1;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btArqueo,
            this.biStockMinimos});
            this.ribbonBar5.Location = new System.Drawing.Point(461, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(147, 93);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar5.TabIndex = 2;
            this.ribbonBar5.Text = "Arqueo";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.TitleVisible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "products.png");
            this.imageList1.Images.SetKeyName(1, "cliente.png");
            this.imageList1.Images.SetKeyName(2, "1211811759.png");
            this.imageList1.Images.SetKeyName(3, "user-icon-512.png");
            this.imageList1.Images.SetKeyName(4, "1325228163.jpg");
            this.imageList1.Images.SetKeyName(5, "cal3png.png");
            this.imageList1.Images.SetKeyName(6, "Get Document.ico");
            this.imageList1.Images.SetKeyName(7, "Send Document.ico");
            this.imageList1.Images.SetKeyName(8, "Transfer Document.ico");
            this.imageList1.Images.SetKeyName(9, "compras_a_proveedores_01.png");
            this.imageList1.Images.SetKeyName(10, "boxes copy_thumb.png");
            this.imageList1.Images.SetKeyName(11, "TarjetasKardex-1-PNG.png");
            this.imageList1.Images.SetKeyName(12, "a-propos-bleu-utilisateur-icone-7595-96.png");
            this.imageList1.Images.SetKeyName(13, "inventarios1.jpg");
            this.imageList1.Images.SetKeyName(14, "d9dc81882f20a4fb51dadd294dd1b4d5.png");
            this.imageList1.Images.SetKeyName(15, "Almacen.png");
            this.imageList1.Images.SetKeyName(16, "proveedor.png");
            this.imageList1.Images.SetKeyName(17, "company_256.png");
            this.imageList1.Images.SetKeyName(18, "iEngrenages.png");
            this.imageList1.Images.SetKeyName(19, "bag.png");
            this.imageList1.Images.SetKeyName(20, "venta.png");
            this.imageList1.Images.SetKeyName(21, "boleta-link.png");
            this.imageList1.Images.SetKeyName(22, "cotizacion.png");
            this.imageList1.Images.SetKeyName(23, "factura-icon.jpg");
            this.imageList1.Images.SetKeyName(24, "icon_shippingbox_withcalendar.png");
            this.imageList1.Images.SetKeyName(25, "images (1).jpg");
            this.imageList1.Images.SetKeyName(26, "pedido.png");
            this.imageList1.Images.SetKeyName(27, "pedidos.png");
            this.imageList1.Images.SetKeyName(28, "DocumentSearch.png");
            this.imageList1.Images.SetKeyName(29, "editar-una-pluma-para-escribir-icono-6827-96.png");
            this.imageList1.Images.SetKeyName(30, "Icono-Borrar-Anuncio.gif");
            this.imageList1.Images.SetKeyName(31, "lista-de-regalos.png");
            this.imageList1.Images.SetKeyName(32, "database-backup-cd-512.png");
            this.imageList1.Images.SetKeyName(33, "database-backup-icon-512.png");
            this.imageList1.Images.SetKeyName(34, "pagos.png");
            this.imageList1.Images.SetKeyName(35, "pagossol.png");
            this.imageList1.Images.SetKeyName(36, "lista-de-regalos.png");
            this.imageList1.Images.SetKeyName(37, "ICONO-INVENTARIO.jpg");
            this.imageList1.Images.SetKeyName(38, "Porcentaje (1).png");
            this.imageList1.Images.SetKeyName(39, "DeleteRed.png");
            this.imageList1.Images.SetKeyName(40, "credit-note.png");
            this.imageList1.Images.SetKeyName(41, "Report.ico");
            this.imageList1.Images.SetKeyName(42, "stocks-icon.png");
            this.imageList1.Images.SetKeyName(43, "icon-requerimiento.ico");
            this.imageList1.Images.SetKeyName(44, "logog.png");
            this.imageList1.Images.SetKeyName(45, "ReporteProblemas.png");
            this.imageList1.Images.SetKeyName(46, "sucursales.png");
            this.imageList1.Images.SetKeyName(47, "stock.jpg");
            this.imageList1.Images.SetKeyName(48, "stock2.png");
            this.imageList1.Images.SetKeyName(49, "productoscarga.jpg");
            this.imageList1.Images.SetKeyName(50, "Creditos.png");
            this.imageList1.Images.SetKeyName(51, "reutilizar.jpg");
            this.imageList1.Images.SetKeyName(52, "codebarz.png");
            this.imageList1.Images.SetKeyName(53, "deposito2.png");
            this.imageList1.Images.SetKeyName(54, "report.jpg");
            this.imageList1.Images.SetKeyName(55, "estadistica.png");
            this.imageList1.Images.SetKeyName(56, "gastos.png");
            // 
            // btArqueo
            // 
            this.btArqueo.ImageIndex = 36;
            this.btArqueo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btArqueo.Name = "btArqueo";
            this.btArqueo.SubItemsExpandWidth = 14;
            this.btArqueo.Tag = "37";
            this.btArqueo.Text = "Arqueo";
            this.btArqueo.Click += new System.EventHandler(this.btArqueo_Click);
            // 
            // biStockMinimos
            // 
            this.biStockMinimos.ImageIndex = 48;
            this.biStockMinimos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biStockMinimos.Name = "biStockMinimos";
            this.biStockMinimos.SubItemsExpandWidth = 14;
            this.biStockMinimos.Tag = "38";
            this.biStockMinimos.Text = "Reporte Stock Minimo";
            this.biStockMinimos.Click += new System.EventHandler(this.biStockMinimos_Click);
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.DragDropSupport = true;
            this.ribbonBar1.Images = this.imageList1;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biConsulta,
            this.biModificar,
            this.biAnular,
            this.biEliminar});
            this.ribbonBar1.Location = new System.Drawing.Point(219, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(242, 93);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar1.TabIndex = 1;
            this.ribbonBar1.Text = "Operaciones";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.TitleVisible = false;
            // 
            // biConsulta
            // 
            this.biConsulta.ImageIndex = 28;
            this.biConsulta.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biConsulta.Name = "biConsulta";
            this.biConsulta.SubItemsExpandWidth = 14;
            this.biConsulta.Tag = "36";
            this.biConsulta.Text = "Consulta";
            this.biConsulta.Click += new System.EventHandler(this.biConsulta_Click);
            // 
            // biModificar
            // 
            this.biModificar.Enabled = false;
            this.biModificar.ImageIndex = 29;
            this.biModificar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biModificar.Name = "biModificar";
            this.biModificar.SubItemsExpandWidth = 14;
            this.biModificar.Tag = "38";
            this.biModificar.Text = "Modificar";
            this.biModificar.Visible = false;
            this.biModificar.Click += new System.EventHandler(this.biModificar_Click);
            // 
            // biAnular
            // 
            this.biAnular.Enabled = false;
            this.biAnular.ImageIndex = 39;
            this.biAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biAnular.Name = "biAnular";
            this.biAnular.SubItemsExpandWidth = 14;
            this.biAnular.Tag = "39";
            this.biAnular.Text = "Anular";
            this.biAnular.Visible = false;
            this.biAnular.Click += new System.EventHandler(this.biAnular_Click);
            // 
            // biEliminar
            // 
            this.biEliminar.Enabled = false;
            this.biEliminar.ImageIndex = 30;
            this.biEliminar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biEliminar.Name = "biEliminar";
            this.biEliminar.SubItemsExpandWidth = 14;
            this.biEliminar.Tag = "40";
            this.biEliminar.Text = "Eliminar";
            this.biEliminar.Visible = false;
            this.biEliminar.Click += new System.EventHandler(this.biEliminar_Click);
            // 
            // rbOperaciones
            // 
            this.rbOperaciones.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbOperaciones.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbOperaciones.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbOperaciones.ContainerControlProcessDialogKey = true;
            this.rbOperaciones.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbOperaciones.DragDropSupport = true;
            this.rbOperaciones.Images = this.imageList1;
            this.rbOperaciones.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biNotadeIngreso,
            this.biNotadeSalida,
            this.biTransferencia});
            this.rbOperaciones.Location = new System.Drawing.Point(3, 0);
            this.rbOperaciones.Name = "rbOperaciones";
            this.rbOperaciones.Size = new System.Drawing.Size(216, 93);
            this.rbOperaciones.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rbOperaciones.TabIndex = 0;
            this.rbOperaciones.Text = "Transacciones";
            // 
            // 
            // 
            this.rbOperaciones.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbOperaciones.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbOperaciones.TitleVisible = false;
            // 
            // biNotadeIngreso
            // 
            this.biNotadeIngreso.ImageIndex = 6;
            this.biNotadeIngreso.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biNotadeIngreso.Name = "biNotadeIngreso";
            this.biNotadeIngreso.SubItemsExpandWidth = 14;
            this.biNotadeIngreso.Tag = "32";
            this.biNotadeIngreso.Text = "Nota de Ingreso";
            this.biNotadeIngreso.Click += new System.EventHandler(this.biNotadeIngreso_Click);
            // 
            // biNotadeSalida
            // 
            this.biNotadeSalida.ImageIndex = 7;
            this.biNotadeSalida.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biNotadeSalida.Name = "biNotadeSalida";
            this.biNotadeSalida.SubItemsExpandWidth = 14;
            this.biNotadeSalida.Tag = "33";
            this.biNotadeSalida.Text = "Nota de Salida";
            this.biNotadeSalida.Click += new System.EventHandler(this.biNotadeSalida_Click);
            // 
            // biTransferencia
            // 
            this.biTransferencia.ImageIndex = 8;
            this.biTransferencia.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biTransferencia.Name = "biTransferencia";
            this.biTransferencia.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biTransferenciasPendientes});
            this.biTransferencia.SubItemsExpandWidth = 14;
            this.biTransferencia.Tag = "34";
            this.biTransferencia.Text = "Tranferencia  Directa";
            this.biTransferencia.Click += new System.EventHandler(this.biTransferencia_Click);
            // 
            // biTransferenciasPendientes
            // 
            this.biTransferenciasPendientes.Name = "biTransferenciasPendientes";
            this.biTransferenciasPendientes.Tag = "35";
            this.biTransferenciasPendientes.Text = "Transferencias Pendientes";
            this.biTransferenciasPendientes.Click += new System.EventHandler(this.biTransferenciasPendientes_Click);
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel7.Controls.Add(this.ribbonBar15);
            this.ribbonPanel7.Controls.Add(this.ribbonBar8);
            this.ribbonPanel7.Controls.Add(this.ribbonBar13);
            this.ribbonPanel7.Controls.Add(this.ribbonBar14);
            this.ribbonPanel7.Controls.Add(this.ribbonBar9);
            this.ribbonPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel7.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel7.Name = "ribbonPanel7";
            this.ribbonPanel7.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel7.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel7.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel7.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel7.TabIndex = 7;
            // 
            // ribbonBar15
            // 
            this.ribbonBar15.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar15.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar15.ContainerControlProcessDialogKey = true;
            this.ribbonBar15.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar15.DragDropSupport = true;
            this.ribbonBar15.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biCajaAhorros,
            this.biCajaGeneral});
            this.ribbonBar15.Location = new System.Drawing.Point(488, 0);
            this.ribbonBar15.Name = "ribbonBar15";
            this.ribbonBar15.Size = new System.Drawing.Size(126, 93);
            this.ribbonBar15.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar15.TabIndex = 6;
            // 
            // 
            // 
            this.ribbonBar15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar15.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // biCajaAhorros
            // 
            this.biCajaAhorros.Image = global::SIGEFA.Properties.Resources.ahorros;
            this.biCajaAhorros.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biCajaAhorros.Name = "biCajaAhorros";
            this.biCajaAhorros.SubItemsExpandWidth = 14;
            this.biCajaAhorros.Tag = "81";
            this.biCajaAhorros.Text = "Caja Ahorros";
            this.biCajaAhorros.Click += new System.EventHandler(this.biCajaAhorros_Click);
            // 
            // biCajaGeneral
            // 
            this.biCajaGeneral.Image = global::SIGEFA.Properties.Resources.caja_general1;
            this.biCajaGeneral.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biCajaGeneral.Name = "biCajaGeneral";
            this.biCajaGeneral.SubItemsExpandWidth = 14;
            this.biCajaGeneral.Tag = "82";
            this.biCajaGeneral.Text = "Caja General";
            this.biCajaGeneral.Click += new System.EventHandler(this.buttonItem20_Click);
            // 
            // ribbonBar8
            // 
            this.ribbonBar8.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ContainerControlProcessDialogKey = true;
            this.ribbonBar8.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar8.DragDropSupport = true;
            this.ribbonBar8.Images = this.imageList2;
            this.ribbonBar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem4,
            this.buttonItem12,
            this.buttonItem13});
            this.ribbonBar8.Location = new System.Drawing.Point(262, 0);
            this.ribbonBar8.Name = "ribbonBar8";
            this.ribbonBar8.Size = new System.Drawing.Size(226, 93);
            this.ribbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar8.TabIndex = 5;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.TitleVisible = false;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "products.png");
            this.imageList2.Images.SetKeyName(1, "cliente.png");
            this.imageList2.Images.SetKeyName(2, "1211811759.png");
            this.imageList2.Images.SetKeyName(3, "user-icon-512.png");
            this.imageList2.Images.SetKeyName(4, "1325228163.jpg");
            this.imageList2.Images.SetKeyName(5, "cal3png.png");
            this.imageList2.Images.SetKeyName(6, "Get Document.ico");
            this.imageList2.Images.SetKeyName(7, "Send Document.ico");
            this.imageList2.Images.SetKeyName(8, "Transfer Document.ico");
            this.imageList2.Images.SetKeyName(9, "compras_a_proveedores_01.png");
            this.imageList2.Images.SetKeyName(10, "boxes copy_thumb.png");
            this.imageList2.Images.SetKeyName(11, "TarjetasKardex-1-PNG.png");
            this.imageList2.Images.SetKeyName(12, "a-propos-bleu-utilisateur-icone-7595-96.png");
            this.imageList2.Images.SetKeyName(13, "inventarios1.jpg");
            this.imageList2.Images.SetKeyName(14, "d9dc81882f20a4fb51dadd294dd1b4d5.png");
            this.imageList2.Images.SetKeyName(15, "Almacen.png");
            this.imageList2.Images.SetKeyName(16, "proveedor.png");
            this.imageList2.Images.SetKeyName(17, "company_256.png");
            this.imageList2.Images.SetKeyName(18, "iEngrenages.png");
            this.imageList2.Images.SetKeyName(19, "bag.png");
            this.imageList2.Images.SetKeyName(20, "venta.png");
            this.imageList2.Images.SetKeyName(21, "boleta-link.png");
            this.imageList2.Images.SetKeyName(22, "cotizacion.png");
            this.imageList2.Images.SetKeyName(23, "factura-icon.jpg");
            this.imageList2.Images.SetKeyName(24, "icon_shippingbox_withcalendar.png");
            this.imageList2.Images.SetKeyName(25, "images (1).jpg");
            this.imageList2.Images.SetKeyName(26, "pedido.png");
            this.imageList2.Images.SetKeyName(27, "pedidos.png");
            this.imageList2.Images.SetKeyName(28, "DocumentSearch.png");
            this.imageList2.Images.SetKeyName(29, "editar-una-pluma-para-escribir-icono-6827-96.png");
            this.imageList2.Images.SetKeyName(30, "Icono-Borrar-Anuncio.gif");
            this.imageList2.Images.SetKeyName(31, "lista-de-regalos.png");
            this.imageList2.Images.SetKeyName(32, "database-backup-cd-512.png");
            this.imageList2.Images.SetKeyName(33, "database-backup-icon-512.png");
            this.imageList2.Images.SetKeyName(34, "pagos.png");
            this.imageList2.Images.SetKeyName(35, "pagossol.png");
            this.imageList2.Images.SetKeyName(36, "lista-de-regalos.png");
            this.imageList2.Images.SetKeyName(37, "ICONO-INVENTARIO.jpg");
            this.imageList2.Images.SetKeyName(38, "Porcentaje (1).png");
            this.imageList2.Images.SetKeyName(39, "DeleteRed.png");
            this.imageList2.Images.SetKeyName(40, "credit-note.png");
            this.imageList2.Images.SetKeyName(41, "ventass.png");
            this.imageList2.Images.SetKeyName(42, "reporte.png");
            this.imageList2.Images.SetKeyName(43, "control_asistencia.png");
            this.imageList2.Images.SetKeyName(44, "caja-fuerte.png");
            this.imageList2.Images.SetKeyName(45, "1407444313_3294.ico");
            this.imageList2.Images.SetKeyName(46, "1407444370_3472.png");
            this.imageList2.Images.SetKeyName(47, "1407444353_17840.ico");
            this.imageList2.Images.SetKeyName(48, "recycle_bin_full.ico");
            this.imageList2.Images.SetKeyName(49, "nissan_qashqai_2008.png");
            this.imageList2.Images.SetKeyName(50, "3606.png");
            this.imageList2.Images.SetKeyName(51, "folder_black_configure_88705.png");
            this.imageList2.Images.SetKeyName(52, "AndroMoneyss.png");
            this.imageList2.Images.SetKeyName(53, "DSCN3324s.png");
            this.imageList2.Images.SetKeyName(54, "9544028-banco-render-3dss.png");
            this.imageList2.Images.SetKeyName(55, "ReporteProblemas2.png");
            this.imageList2.Images.SetKeyName(56, "pedidos2.png");
            this.imageList2.Images.SetKeyName(57, "400_F_2.png");
            this.imageList2.Images.SetKeyName(58, "caja-chica-pymess2.png");
            this.imageList2.Images.SetKeyName(59, "cajaFuertes.png");
            this.imageList2.Images.SetKeyName(60, "caja_registradora2.png");
            this.imageList2.Images.SetKeyName(61, "caja_registradora.png");
            this.imageList2.Images.SetKeyName(62, "iconos_software_pro_.png");
            this.imageList2.Images.SetKeyName(63, "programacion2.png");
            this.imageList2.Images.SetKeyName(64, "calculadora2.png");
            this.imageList2.Images.SetKeyName(65, "compras2.png");
            this.imageList2.Images.SetKeyName(66, "shopping2.png");
            this.imageList2.Images.SetKeyName(67, "buck up generar.png");
            this.imageList2.Images.SetKeyName(68, "buck up guarda.png");
            this.imageList2.Images.SetKeyName(69, "configurationes.png");
            this.imageList2.Images.SetKeyName(70, "configuration1.png");
            this.imageList2.Images.SetKeyName(71, "Kyo-Tux-Aeon-Folder-Black-Configure.ico");
            this.imageList2.Images.SetKeyName(72, "control_panel1.png");
            this.imageList2.Images.SetKeyName(73, "cajachicaotros.png");
            this.imageList2.Images.SetKeyName(74, "libro.png");
            this.imageList2.Images.SetKeyName(75, "LE.png");
            this.imageList2.Images.SetKeyName(76, "logout1.png");
            // 
            // buttonItem4
            // 
            this.buttonItem4.ImageIndex = 34;
            this.buttonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlI);
            this.buttonItem4.SubItemsExpandWidth = 14;
            this.buttonItem4.Tag = "78";
            this.buttonItem4.Text = "Tesorería";
            this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click_2);
            // 
            // buttonItem12
            // 
            this.buttonItem12.ImageIndex = 43;
            this.buttonItem12.ImagePaddingHorizontal = 20;
            this.buttonItem12.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biCtas});
            this.buttonItem12.SubItemsExpandWidth = 14;
            this.buttonItem12.Tag = "79";
            this.buttonItem12.Text = "Movimientos Bancarios";
            this.buttonItem12.Click += new System.EventHandler(this.buttonItem12_Click);
            // 
            // biCtas
            // 
            this.biCtas.Name = "biCtas";
            this.biCtas.Tag = "79";
            this.biCtas.Text = "Estado de las Ctas.";
            this.biCtas.Click += new System.EventHandler(this.biCtas_Click);
            // 
            // buttonItem13
            // 
            this.buttonItem13.ImageIndex = 36;
            this.buttonItem13.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.SubItemsExpandWidth = 14;
            this.buttonItem13.Tag = "80";
            this.buttonItem13.Text = "Conciliacion Bancaria";
            this.buttonItem13.Click += new System.EventHandler(this.buttonItem13_Click);
            // 
            // ribbonBar13
            // 
            this.ribbonBar13.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar13.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar13.ContainerControlProcessDialogKey = true;
            this.ribbonBar13.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar13.DragDropSupport = true;
            this.ribbonBar13.Images = this.imageList2;
            this.ribbonBar13.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem1,
            this.buttonItem3});
            this.ribbonBar13.Location = new System.Drawing.Point(136, 0);
            this.ribbonBar13.Name = "ribbonBar13";
            this.ribbonBar13.Size = new System.Drawing.Size(126, 93);
            this.ribbonBar13.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar13.TabIndex = 4;
            // 
            // 
            // 
            this.ribbonBar13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar13.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar13.TitleVisible = false;
            // 
            // buttonItem1
            // 
            this.buttonItem1.ImageIndex = 58;
            this.buttonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItemsExpandWidth = 14;
            this.buttonItem1.Tag = "77";
            this.buttonItem1.Text = "Caja Chica";
            this.buttonItem1.Click += new System.EventHandler(this.buttonItem1_Click_5);
            // 
            // buttonItem3
            // 
            this.buttonItem3.Enabled = false;
            this.buttonItem3.ImageIndex = 73;
            this.buttonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.SubItemsExpandWidth = 14;
            this.buttonItem3.Text = "Otros Gastos";
            this.buttonItem3.Visible = false;
            this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click_5);
            // 
            // ribbonBar14
            // 
            this.ribbonBar14.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar14.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar14.ContainerControlProcessDialogKey = true;
            this.ribbonBar14.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar14.DragDropSupport = true;
            this.ribbonBar14.Images = this.imageList2;
            this.ribbonBar14.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biMovimientosCaja});
            this.ribbonBar14.Location = new System.Drawing.Point(57, 0);
            this.ribbonBar14.Name = "ribbonBar14";
            this.ribbonBar14.Size = new System.Drawing.Size(79, 93);
            this.ribbonBar14.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar14.TabIndex = 3;
            // 
            // 
            // 
            this.ribbonBar14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar14.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar14.TitleVisible = false;
            // 
            // biMovimientosCaja
            // 
            this.biMovimientosCaja.ImageIndex = 35;
            this.biMovimientosCaja.ImagePaddingHorizontal = 20;
            this.biMovimientosCaja.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biMovimientosCaja.Name = "biMovimientosCaja";
            this.biMovimientosCaja.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem18});
            this.biMovimientosCaja.SubItemsExpandWidth = 14;
            this.biMovimientosCaja.Tag = "75";
            this.biMovimientosCaja.Text = "Movimientos Caja";
            this.biMovimientosCaja.Click += new System.EventHandler(this.biMovimientosCaja_Click);
            // 
            // buttonItem18
            // 
            this.buttonItem18.Name = "buttonItem18";
            this.buttonItem18.Tag = "76";
            this.buttonItem18.Text = "Cajas Historicas";
            this.buttonItem18.Click += new System.EventHandler(this.buttonItem18_Click);
            // 
            // ribbonBar9
            // 
            this.ribbonBar9.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.ContainerControlProcessDialogKey = true;
            this.ribbonBar9.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar9.DragDropSupport = true;
            this.ribbonBar9.Images = this.imageList2;
            this.ribbonBar9.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.BiAperturaCaja});
            this.ribbonBar9.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar9.Name = "ribbonBar9";
            this.ribbonBar9.Size = new System.Drawing.Size(54, 93);
            this.ribbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar9.TabIndex = 1;
            this.ribbonBar9.Text = "Egresos";
            // 
            // 
            // 
            this.ribbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.TitleVisible = false;
            this.ribbonBar9.ItemClick += new System.EventHandler(this.ribbonBar9_ItemClick);
            // 
            // BiAperturaCaja
            // 
            this.BiAperturaCaja.ImageIndex = 60;
            this.BiAperturaCaja.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.BiAperturaCaja.Name = "BiAperturaCaja";
            this.BiAperturaCaja.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlI);
            this.BiAperturaCaja.SubItemsExpandWidth = 14;
            this.BiAperturaCaja.Tag = "74";
            this.BiAperturaCaja.Text = "Apertura Caja";
            this.BiAperturaCaja.Click += new System.EventHandler(this.BiAperturaCaja_Click_1);
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.AutoSize = true;
            this.ribbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel5.Controls.Add(this.ribbonBar6);
            this.ribbonPanel5.Controls.Add(this.ribbonBar3);
            this.ribbonPanel5.Controls.Add(this.rbVentas);
            this.ribbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel5.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel5.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel5.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarCaptionInactiveBackground2;
            this.ribbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel5.TabIndex = 5;
            this.ribbonPanel5.Visible = false;
            // 
            // ribbonBar6
            // 
            this.ribbonBar6.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.ContainerControlProcessDialogKey = true;
            this.ribbonBar6.DragDropSupport = true;
            this.ribbonBar6.Images = this.imageList1;
            this.ribbonBar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biPedidoVenta,
            this.biCotizacion});
            this.ribbonBar6.Location = new System.Drawing.Point(478, 0);
            this.ribbonBar6.Name = "ribbonBar6";
            this.ribbonBar6.Size = new System.Drawing.Size(136, 94);
            this.ribbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar6.TabIndex = 3;
            this.ribbonBar6.Text = "ribbonBar6";
            // 
            // 
            // 
            this.ribbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.TitleVisible = false;
            // 
            // biPedidoVenta
            // 
            this.biPedidoVenta.ImageIndex = 26;
            this.biPedidoVenta.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biPedidoVenta.Name = "biPedidoVenta";
            this.biPedidoVenta.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlP);
            this.biPedidoVenta.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biPedidosPendientes});
            this.biPedidoVenta.SubItemsExpandWidth = 14;
            this.biPedidoVenta.Tag = "20";
            this.biPedidoVenta.Text = "Pedidos";
            this.biPedidoVenta.Visible = false;
            this.biPedidoVenta.Click += new System.EventHandler(this.biPedidoVenta_Click);
            // 
            // biPedidosPendientes
            // 
            this.biPedidosPendientes.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biPedidosPendientes.Name = "biPedidosPendientes";
            this.biPedidosPendientes.SubItemsExpandWidth = 14;
            this.biPedidosPendientes.Tag = "21";
            this.biPedidosPendientes.Text = "Pedidos Pendientes";
            this.biPedidosPendientes.Click += new System.EventHandler(this.biPedidosPendientes_Click);
            // 
            // biCotizacion
            // 
            this.biCotizacion.ImageIndex = 22;
            this.biCotizacion.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biCotizacion.Name = "biCotizacion";
            this.biCotizacion.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biCotizacionesVigentes,
            this.biCotizacionesAprobadas});
            this.biCotizacion.SubItemsExpandWidth = 14;
            this.biCotizacion.Tag = "22";
            this.biCotizacion.Text = "Cotizaciones";
            this.biCotizacion.Visible = false;
            this.biCotizacion.Click += new System.EventHandler(this.biCotizacion_Click);
            // 
            // biCotizacionesVigentes
            // 
            this.biCotizacionesVigentes.Name = "biCotizacionesVigentes";
            this.biCotizacionesVigentes.Tag = "23";
            this.biCotizacionesVigentes.Text = "Historial de Cotizaciones";
            this.biCotizacionesVigentes.Click += new System.EventHandler(this.biCotizacionesVigentes_Click);
            // 
            // biCotizacionesAprobadas
            // 
            this.biCotizacionesAprobadas.Name = "biCotizacionesAprobadas";
            this.biCotizacionesAprobadas.Tag = "24";
            this.biCotizacionesAprobadas.Text = "Cotizaciones Aprobadas";
            this.biCotizacionesAprobadas.Visible = false;
            this.biCotizacionesAprobadas.Click += new System.EventHandler(this.biCotizacionesAprobadas_Click);
            // 
            // ribbonBar3
            // 
            this.ribbonBar3.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ContainerControlProcessDialogKey = true;
            this.ribbonBar3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar3.DragDropSupport = true;
            this.ribbonBar3.Images = this.imageList1;
            this.ribbonBar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biCobros,
            this.biStockAlmacenes});
            this.ribbonBar3.Location = new System.Drawing.Point(334, 0);
            this.ribbonBar3.Name = "ribbonBar3";
            this.ribbonBar3.Size = new System.Drawing.Size(142, 93);
            this.ribbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar3.TabIndex = 2;
            this.ribbonBar3.Text = "ribbonBar3";
            // 
            // 
            // 
            this.ribbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.TitleVisible = false;
            // 
            // biCobros
            // 
            this.biCobros.ImageIndex = 34;
            this.biCobros.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biCobros.Name = "biCobros";
            this.biCobros.ShowSubItems = false;
            this.biCobros.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMasivo});
            this.biCobros.SubItemsExpandWidth = 14;
            this.biCobros.Tag = "18";
            this.biCobros.Text = "Por Cobrar";
            this.biCobros.Click += new System.EventHandler(this.biCobros_Click);
            // 
            // btnMasivo
            // 
            this.btnMasivo.Name = "btnMasivo";
            this.btnMasivo.Text = "Cobro Masivo";
            this.btnMasivo.Visible = false;
            this.btnMasivo.Click += new System.EventHandler(this.btnMasivo_Click);
            // 
            // biStockAlmacenes
            // 
            this.biStockAlmacenes.ImageIndex = 48;
            this.biStockAlmacenes.ImagePaddingHorizontal = 15;
            this.biStockAlmacenes.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biStockAlmacenes.Name = "biStockAlmacenes";
            this.biStockAlmacenes.SubItemsExpandWidth = 14;
            this.biStockAlmacenes.Tag = "19";
            this.biStockAlmacenes.Text = "Stock de Almacenes";
            this.biStockAlmacenes.Click += new System.EventHandler(this.biStockAlmacenes_Click);
            // 
            // rbVentas
            // 
            this.rbVentas.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbVentas.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbVentas.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbVentas.ContainerControlProcessDialogKey = true;
            this.rbVentas.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbVentas.DragDropSupport = true;
            this.rbVentas.Images = this.imageList1;
            this.rbVentas.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biVenta,
            this.biVentaSeparacion,
            this.biGuia,
            this.biNotaCredito,
            this.biNotaDebito});
            this.rbVentas.Location = new System.Drawing.Point(3, 0);
            this.rbVentas.Name = "rbVentas";
            this.rbVentas.Size = new System.Drawing.Size(331, 93);
            this.rbVentas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rbVentas.TabIndex = 0;
            this.rbVentas.Text = "Documentos de Ventas";
            // 
            // 
            // 
            this.rbVentas.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbVentas.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbVentas.TitleVisible = false;
            // 
            // biVenta
            // 
            this.biVenta.ImageIndex = 20;
            this.biVenta.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biVenta.Name = "biVenta";
            this.biVenta.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biMuestraVentas});
            this.biVenta.SubItemsExpandWidth = 14;
            this.biVenta.Tag = "8";
            this.biVenta.Text = "Venta";
            this.biVenta.Click += new System.EventHandler(this.biVenta_Click);
            // 
            // biMuestraVentas
            // 
            this.biMuestraVentas.Name = "biMuestraVentas";
            this.biMuestraVentas.Tag = "9";
            this.biMuestraVentas.Text = "Ventas";
            this.biMuestraVentas.Click += new System.EventHandler(this.biMuestraVentas_Click);
            // 
            // biVentaSeparacion
            // 
            this.biVentaSeparacion.ImageIndex = 53;
            this.biVentaSeparacion.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biVentaSeparacion.Name = "biVentaSeparacion";
            this.biVentaSeparacion.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem7});
            this.biVentaSeparacion.SubItemsExpandWidth = 14;
            this.biVentaSeparacion.Tag = "10";
            this.biVentaSeparacion.Text = "Venta Separacion";
            this.biVentaSeparacion.Visible = false;
            this.biVentaSeparacion.Click += new System.EventHandler(this.biVentaSeparacion_Click);
            // 
            // buttonItem7
            // 
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.Tag = "11";
            this.buttonItem7.Text = "Ventas Separacion";
            this.buttonItem7.Click += new System.EventHandler(this.buttonItem7_Click);
            // 
            // biGuia
            // 
            this.biGuia.ImageIndex = 24;
            this.biGuia.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biGuia.Name = "biGuia";
            this.biGuia.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biGuias,
            this.biBuscarGuia});
            this.biGuia.SubItemsExpandWidth = 14;
            this.biGuia.Tag = "12";
            this.biGuia.Text = "Guias";
            this.biGuia.Visible = false;
            this.biGuia.Click += new System.EventHandler(this.biGuia_Click);
            // 
            // biGuias
            // 
            this.biGuias.Name = "biGuias";
            this.biGuias.Tag = "13";
            this.biGuias.Text = "Guias de Remision";
            this.biGuias.Click += new System.EventHandler(this.biGuias_Click);
            // 
            // biBuscarGuia
            // 
            this.biBuscarGuia.Name = "biBuscarGuia";
            this.biBuscarGuia.Tag = "14";
            this.biBuscarGuia.Text = "Buscar Guias";
            this.biBuscarGuia.Click += new System.EventHandler(this.biBuscarGuia_Click);
            // 
            // biNotaCredito
            // 
            this.biNotaCredito.ImageIndex = 40;
            this.biNotaCredito.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biNotaCredito.Name = "biNotaCredito";
            this.biNotaCredito.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ciNotasdeCredito});
            this.biNotaCredito.SubItemsExpandWidth = 14;
            this.biNotaCredito.Tag = "15";
            this.biNotaCredito.Text = "Notas de Crédito";
            this.biNotaCredito.Click += new System.EventHandler(this.biNotaCredito_Click);
            // 
            // ciNotasdeCredito
            // 
            this.ciNotasdeCredito.Name = "ciNotasdeCredito";
            this.ciNotasdeCredito.Tag = "16";
            this.ciNotasdeCredito.Text = "Notas de Crédito";
            this.ciNotasdeCredito.Click += new System.EventHandler(this.ciNotasdeCredito_Click);
            // 
            // biNotaDebito
            // 
            this.biNotaDebito.Enabled = false;
            this.biNotaDebito.ImageIndex = 40;
            this.biNotaDebito.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biNotaDebito.Name = "biNotaDebito";
            this.biNotaDebito.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ciNotasdeDebito});
            this.biNotaDebito.SubItemsExpandWidth = 14;
            this.biNotaDebito.Tag = "17";
            this.biNotaDebito.Text = "Nota de Débito";
            this.biNotaDebito.Visible = false;
            this.biNotaDebito.Click += new System.EventHandler(this.biNotaDebito_Click);
            // 
            // ciNotasdeDebito
            // 
            this.ciNotasdeDebito.Name = "ciNotasdeDebito";
            this.ciNotasdeDebito.Text = "Notas de Débito";
            this.ciNotasdeDebito.Click += new System.EventHandler(this.ciNotasdeDebito_Click);
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel6.Controls.Add(this.rbCompras);
            this.ribbonPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel6.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel6.Name = "ribbonPanel6";
            this.ribbonPanel6.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel6.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel6.TabIndex = 6;
            this.ribbonPanel6.Visible = false;
            // 
            // rbCompras
            // 
            this.rbCompras.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbCompras.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbCompras.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbCompras.ContainerControlProcessDialogKey = true;
            this.rbCompras.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbCompras.DragDropSupport = true;
            this.rbCompras.Images = this.imageList1;
            this.rbCompras.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biPedidoCompra,
            this.buttonItem16,
            this.buttonItem17,
            this.biPagos,
            this.btnRequerimiento,
            this.biConsolidado,
            this.biOrdenCompra,
            this.biCompraOrden,
            this.biNotaCreditoCompra,
            this.btnNotaDebitoC});
            this.rbCompras.Location = new System.Drawing.Point(3, 0);
            this.rbCompras.Name = "rbCompras";
            this.rbCompras.Size = new System.Drawing.Size(644, 93);
            this.rbCompras.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rbCompras.TabIndex = 3;
            this.rbCompras.Text = "Documentos de venta";
            // 
            // 
            // 
            this.rbCompras.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbCompras.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbCompras.TitleVisible = false;
            // 
            // biPedidoCompra
            // 
            this.biPedidoCompra.ImageIndex = 20;
            this.biPedidoCompra.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biPedidoCompra.Name = "biPedidoCompra";
            this.biPedidoCompra.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnVerCompras});
            this.biPedidoCompra.SubItemsExpandWidth = 14;
            this.biPedidoCompra.Tag = "25";
            this.biPedidoCompra.Text = "Compra Directa";
            this.biPedidoCompra.Click += new System.EventHandler(this.biPedidoCompra_Click);
            // 
            // btnVerCompras
            // 
            this.btnVerCompras.Name = "btnVerCompras";
            this.btnVerCompras.Tag = "26";
            this.btnVerCompras.Text = "Ver Compras";
            this.btnVerCompras.Click += new System.EventHandler(this.btnVerCompras_Click);
            // 
            // buttonItem16
            // 
            this.buttonItem16.Enabled = false;
            this.buttonItem16.ImageIndex = 26;
            this.buttonItem16.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem16.Name = "buttonItem16";
            this.buttonItem16.SubItemsExpandWidth = 14;
            this.buttonItem16.Text = "Pedidos";
            this.buttonItem16.Visible = false;
            // 
            // buttonItem17
            // 
            this.buttonItem17.Enabled = false;
            this.buttonItem17.ImageIndex = 24;
            this.buttonItem17.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem17.Name = "buttonItem17";
            this.buttonItem17.SubItemsExpandWidth = 14;
            this.buttonItem17.Tag = "24";
            this.buttonItem17.Text = "Guias";
            this.buttonItem17.Visible = false;
            // 
            // biPagos
            // 
            this.biPagos.ImageIndex = 35;
            this.biPagos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biPagos.Name = "biPagos";
            this.biPagos.SubItemsExpandWidth = 14;
            this.biPagos.Tag = "27";
            this.biPagos.Text = "Por Pagar";
            this.biPagos.Click += new System.EventHandler(this.biPagos_Click);
            // 
            // btnRequerimiento
            // 
            this.btnRequerimiento.Enabled = false;
            this.btnRequerimiento.ImageIndex = 44;
            this.btnRequerimiento.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnRequerimiento.Name = "btnRequerimiento";
            this.btnRequerimiento.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem9,
            this.biHistorialRequerimiento});
            this.btnRequerimiento.SubItemsExpandWidth = 14;
            this.btnRequerimiento.Tag = "28";
            this.btnRequerimiento.Text = "Requerimiento";
            this.btnRequerimiento.Visible = false;
            this.btnRequerimiento.Click += new System.EventHandler(this.btnRequerimiento_Click);
            // 
            // buttonItem9
            // 
            this.buttonItem9.Name = "buttonItem9";
            this.buttonItem9.Tag = "29";
            this.buttonItem9.Text = "Requerimientos Borradores";
            this.buttonItem9.Click += new System.EventHandler(this.buttonItem9_Click);
            // 
            // biHistorialRequerimiento
            // 
            this.biHistorialRequerimiento.Name = "biHistorialRequerimiento";
            this.biHistorialRequerimiento.Tag = "79";
            this.biHistorialRequerimiento.Text = "Historial Requerimientos";
            this.biHistorialRequerimiento.Click += new System.EventHandler(this.biHistorialRequerimiento_Click);
            // 
            // biConsolidado
            // 
            this.biConsolidado.Enabled = false;
            this.biConsolidado.ImageIndex = 10;
            this.biConsolidado.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biConsolidado.Name = "biConsolidado";
            this.biConsolidado.SubItemsExpandWidth = 14;
            this.biConsolidado.Tag = "90";
            this.biConsolidado.Text = "Consolidado de Requerimientos";
            this.biConsolidado.Visible = false;
            this.biConsolidado.Click += new System.EventHandler(this.biConsolidado_Click);
            // 
            // biOrdenCompra
            // 
            this.biOrdenCompra.ImageIndex = 45;
            this.biOrdenCompra.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biOrdenCompra.Name = "biOrdenCompra";
            this.biOrdenCompra.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biOrdenesCompras});
            this.biOrdenCompra.SubItemsExpandWidth = 14;
            this.biOrdenCompra.Tag = "28";
            this.biOrdenCompra.Text = "Orden Compra";
            this.biOrdenCompra.Visible = false;
            this.biOrdenCompra.Click += new System.EventHandler(this.biOrdenCompra_Click);
            // 
            // biOrdenesCompras
            // 
            this.biOrdenesCompras.Name = "biOrdenesCompras";
            this.biOrdenesCompras.Tag = "29";
            this.biOrdenesCompras.Text = "Ordenes Compra";
            this.biOrdenesCompras.Click += new System.EventHandler(this.biOrdenesCompras_Click);
            // 
            // biCompraOrden
            // 
            this.biCompraOrden.ImageIndex = 19;
            this.biCompraOrden.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biCompraOrden.Name = "biCompraOrden";
            this.biCompraOrden.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biHistorialFacturaciones,
            this.biGuiasSinFacturar});
            this.biCompraOrden.SubItemsExpandWidth = 14;
            this.biCompraOrden.Tag = "30";
            this.biCompraOrden.Text = "Ingreso de Factura";
            this.biCompraOrden.Visible = false;
            this.biCompraOrden.Click += new System.EventHandler(this.buttonItem2_Click);
            // 
            // biHistorialFacturaciones
            // 
            this.biHistorialFacturaciones.Name = "biHistorialFacturaciones";
            this.biHistorialFacturaciones.Tag = "31";
            this.biHistorialFacturaciones.Text = "Historial Facturaciones";
            this.biHistorialFacturaciones.Click += new System.EventHandler(this.biHistorialFacturaciones_Click);
            // 
            // biGuiasSinFacturar
            // 
            this.biGuiasSinFacturar.Enabled = false;
            this.biGuiasSinFacturar.Name = "biGuiasSinFacturar";
            this.biGuiasSinFacturar.Tag = "91";
            this.biGuiasSinFacturar.Text = "Guias Sin Facturar";
            this.biGuiasSinFacturar.Visible = false;
            this.biGuiasSinFacturar.Click += new System.EventHandler(this.biGuiasSinFacturar_Click);
            // 
            // biNotaCreditoCompra
            // 
            this.biNotaCreditoCompra.Enabled = false;
            this.biNotaCreditoCompra.ImageIndex = 40;
            this.biNotaCreditoCompra.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biNotaCreditoCompra.Name = "biNotaCreditoCompra";
            this.biNotaCreditoCompra.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biNotasCreditoCompras});
            this.biNotaCreditoCompra.SubItemsExpandWidth = 14;
            this.biNotaCreditoCompra.Tag = "26";
            this.biNotaCreditoCompra.Text = "Notas de Crédito";
            this.biNotaCreditoCompra.Visible = false;
            this.biNotaCreditoCompra.Click += new System.EventHandler(this.biNotaCreditoCompra_Click);
            // 
            // biNotasCreditoCompras
            // 
            this.biNotasCreditoCompras.Name = "biNotasCreditoCompras";
            this.biNotasCreditoCompras.Tag = "27";
            this.biNotasCreditoCompras.Text = "Notas de Crédito";
            this.biNotasCreditoCompras.Click += new System.EventHandler(this.biNotasCreditoCompras_Click);
            // 
            // btnNotaDebitoC
            // 
            this.btnNotaDebitoC.Enabled = false;
            this.btnNotaDebitoC.ImageIndex = 40;
            this.btnNotaDebitoC.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnNotaDebitoC.Name = "btnNotaDebitoC";
            this.btnNotaDebitoC.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2});
            this.btnNotaDebitoC.SubItemsExpandWidth = 14;
            this.btnNotaDebitoC.Tag = "93";
            this.btnNotaDebitoC.Text = "Nota de Débito";
            this.btnNotaDebitoC.Visible = false;
            this.btnNotaDebitoC.Click += new System.EventHandler(this.btnNotaDebitoC_Click);
            // 
            // buttonItem2
            // 
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Tag = "94";
            this.buttonItem2.Text = "Notas de Débito";
            this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click_1);
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel3.Controls.Add(this.riReportesGeneral);
            this.ribbonPanel3.Controls.Add(this.rbReportes);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 3;
            this.ribbonPanel3.Visible = false;
            // 
            // riReportesGeneral
            // 
            this.riReportesGeneral.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.riReportesGeneral.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.riReportesGeneral.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.riReportesGeneral.ContainerControlProcessDialogKey = true;
            this.riReportesGeneral.Dock = System.Windows.Forms.DockStyle.Left;
            this.riReportesGeneral.DragDropSupport = true;
            this.riReportesGeneral.Images = this.imageList1;
            this.riReportesGeneral.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnReporte,
            this.biDescuentos});
            this.riReportesGeneral.Location = new System.Drawing.Point(204, 0);
            this.riReportesGeneral.Name = "riReportesGeneral";
            this.riReportesGeneral.Size = new System.Drawing.Size(284, 93);
            this.riReportesGeneral.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.riReportesGeneral.TabIndex = 1;
            this.riReportesGeneral.Text = "ribbonBar5";
            // 
            // 
            // 
            this.riReportesGeneral.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.riReportesGeneral.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.riReportesGeneral.TitleVisible = false;
            // 
            // btnReporte
            // 
            this.btnReporte.ImageIndex = 41;
            this.btnReporte.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.SubItemsExpandWidth = 14;
            this.btnReporte.Tag = "45";
            this.btnReporte.Text = "Reportes";
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // biDescuentos
            // 
            this.biDescuentos.Image = global::SIGEFA.Properties.Resources.report32;
            this.biDescuentos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biDescuentos.Name = "biDescuentos";
            this.biDescuentos.SubItemsExpandWidth = 14;
            this.biDescuentos.Text = "Descuentos Ventas";
            this.biDescuentos.Click += new System.EventHandler(this.biDescuentos_Click);
            // 
            // rbReportes
            // 
            this.rbReportes.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbReportes.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbReportes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbReportes.ContainerControlProcessDialogKey = true;
            this.rbReportes.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbReportes.DragDropSupport = true;
            this.rbReportes.Images = this.imageList1;
            this.rbReportes.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biInventario,
            this.biKardex,
            this.biRotacionProducto});
            this.rbReportes.Location = new System.Drawing.Point(3, 0);
            this.rbReportes.Name = "rbReportes";
            this.rbReportes.Size = new System.Drawing.Size(201, 93);
            this.rbReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rbReportes.TabIndex = 0;
            this.rbReportes.Text = "Reportes";
            // 
            // 
            // 
            this.rbReportes.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbReportes.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbReportes.TitleVisible = false;
            // 
            // biInventario
            // 
            this.biInventario.ImageIndex = 10;
            this.biInventario.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biInventario.Name = "biInventario";
            this.biInventario.SubItemsExpandWidth = 14;
            this.biInventario.Tag = "43";
            this.biInventario.Text = "Inventario";
            this.biInventario.Click += new System.EventHandler(this.biInventario_Click);
            // 
            // biKardex
            // 
            this.biKardex.ImageIndex = 11;
            this.biKardex.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biKardex.Name = "biKardex";
            this.biKardex.SubItemsExpandWidth = 14;
            this.biKardex.Tag = "44";
            this.biKardex.Text = "Kardex";
            this.biKardex.Click += new System.EventHandler(this.biKardex_Click);
            // 
            // biRotacionProducto
            // 
            this.biRotacionProducto.Enabled = false;
            this.biRotacionProducto.ImageIndex = 49;
            this.biRotacionProducto.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biRotacionProducto.Name = "biRotacionProducto";
            this.biRotacionProducto.SubItemsExpandWidth = 14;
            this.biRotacionProducto.Tag = "101";
            this.biRotacionProducto.Text = "Rotacion de Productos";
            this.biRotacionProducto.Visible = false;
            this.biRotacionProducto.Click += new System.EventHandler(this.biRotacionProducto_Click);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel4.Controls.Add(this.ribbonBar7);
            this.ribbonPanel4.Controls.Add(this.ribbonBar2);
            this.ribbonPanel4.Controls.Add(this.ribbonBar4);
            this.ribbonPanel4.Controls.Add(this.rbConfigurar);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel4.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 4;
            this.ribbonPanel4.Visible = false;
            // 
            // ribbonBar7
            // 
            this.ribbonBar7.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.ContainerControlProcessDialogKey = true;
            this.ribbonBar7.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar7.DragDropSupport = true;
            this.ribbonBar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.bitermetro});
            this.ribbonBar7.Location = new System.Drawing.Point(610, 0);
            this.ribbonBar7.Name = "ribbonBar7";
            this.ribbonBar7.Size = new System.Drawing.Size(100, 93);
            this.ribbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar7.TabIndex = 4;
            this.ribbonBar7.Text = "ribbonBar7";
            // 
            // 
            // 
            this.ribbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.TitleVisible = false;
            this.ribbonBar7.Visible = false;
            // 
            // bitermetro
            // 
            this.bitermetro.Enabled = false;
            this.bitermetro.ImageIndex = 42;
            this.bitermetro.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.bitermetro.Name = "bitermetro";
            this.bitermetro.SubItemsExpandWidth = 14;
            this.bitermetro.Text = "Termometro de ventas";
            this.bitermetro.Visible = false;
            this.bitermetro.Click += new System.EventHandler(this.buttonItem1_Click_1);
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.DragDropSupport = true;
            this.ribbonBar2.Images = this.imageList1;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biBackup,
            this.biImport});
            this.ribbonBar2.Location = new System.Drawing.Point(472, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(138, 93);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar2.TabIndex = 3;
            this.ribbonBar2.Text = "ribbonBar2";
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.TitleVisible = false;
            // 
            // biBackup
            // 
            this.biBackup.ImageIndex = 32;
            this.biBackup.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biBackup.Name = "biBackup";
            this.biBackup.SubItemsExpandWidth = 14;
            this.biBackup.Tag = "72";
            this.biBackup.Text = "Generar Backup";
            this.biBackup.Click += new System.EventHandler(this.biBackup_Click);
            // 
            // biImport
            // 
            this.biImport.ImageIndex = 33;
            this.biImport.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biImport.Name = "biImport";
            this.biImport.SubItemsExpandWidth = 14;
            this.biImport.Tag = "73";
            this.biImport.Text = "Importar BD";
            this.biImport.Click += new System.EventHandler(this.biImport_Click);
            // 
            // ribbonBar4
            // 
            this.ribbonBar4.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.ContainerControlProcessDialogKey = true;
            this.ribbonBar4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar4.DragDropSupport = true;
            this.ribbonBar4.Images = this.imageList2;
            this.ribbonBar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biTablas,
            this.biParametros});
            this.ribbonBar4.Location = new System.Drawing.Point(334, 0);
            this.ribbonBar4.Name = "ribbonBar4";
            this.ribbonBar4.Size = new System.Drawing.Size(138, 93);
            this.ribbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar4.TabIndex = 2;
            this.ribbonBar4.Text = "Configuración";
            // 
            // 
            // 
            this.ribbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.TitleVisible = false;
            // 
            // biTablas
            // 
            this.biTablas.ImageIndex = 18;
            this.biTablas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biTablas.Name = "biTablas";
            this.biTablas.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biUnidades,
            this.biFamilias,
            this.biMarcas,
            this.biTipoArticulo,
            this.biCaracteristica,
            this.biDocumentos,
            this.biTransacciones,
            this.biTipoCambio,
            this.biAutorizado,
            this.biFormaPago,
            this.biMetodoPago,
            this.biListasPrecios,
            this.biVehiculosTransporte,
            this.biConductores,
            this.biEmpresasTransporte,
            this.biZonas,
            this.biVendedores,
            this.biDestaques,
            this.biBancos,
            this.biCuentasCorrientes,
            this.biTarjetaPago,
            this.biTipoEgresoCaja});
            this.biTablas.SubItemsExpandWidth = 14;
            this.biTablas.Tag = "51";
            this.biTablas.Text = "Tablas";
            // 
            // biUnidades
            // 
            this.biUnidades.Name = "biUnidades";
            this.biUnidades.Tag = "52";
            this.biUnidades.Text = "Unidades";
            this.biUnidades.Click += new System.EventHandler(this.buttonItem26_Click);
            // 
            // biFamilias
            // 
            this.biFamilias.Name = "biFamilias";
            this.biFamilias.Tag = "53";
            this.biFamilias.Text = "Familias";
            this.biFamilias.Click += new System.EventHandler(this.buttonItem27_Click);
            // 
            // biMarcas
            // 
            this.biMarcas.Name = "biMarcas";
            this.biMarcas.Tag = "54";
            this.biMarcas.Text = "Marcas";
            this.biMarcas.Click += new System.EventHandler(this.buttonItem28_Click);
            // 
            // biTipoArticulo
            // 
            this.biTipoArticulo.Name = "biTipoArticulo";
            this.biTipoArticulo.Tag = "55";
            this.biTipoArticulo.Text = "Tipo de Articulos";
            this.biTipoArticulo.Click += new System.EventHandler(this.buttonItem29_Click);
            // 
            // biCaracteristica
            // 
            this.biCaracteristica.Name = "biCaracteristica";
            this.biCaracteristica.Tag = "56";
            this.biCaracteristica.Text = "Caracteristicas";
            this.biCaracteristica.Click += new System.EventHandler(this.buttonItem30_Click);
            // 
            // biDocumentos
            // 
            this.biDocumentos.Name = "biDocumentos";
            this.biDocumentos.Tag = "57";
            this.biDocumentos.Text = "Documentos";
            this.biDocumentos.Click += new System.EventHandler(this.biDocumentos_Click);
            // 
            // biTransacciones
            // 
            this.biTransacciones.Name = "biTransacciones";
            this.biTransacciones.Tag = "58";
            this.biTransacciones.Text = "Transacciones";
            this.biTransacciones.Click += new System.EventHandler(this.biTransacciones_Click);
            // 
            // biTipoCambio
            // 
            this.biTipoCambio.Name = "biTipoCambio";
            this.biTipoCambio.Tag = "59";
            this.biTipoCambio.Text = "Tipo de Cambio";
            this.biTipoCambio.Click += new System.EventHandler(this.biTipoCambio_Click);
            // 
            // biAutorizado
            // 
            this.biAutorizado.Name = "biAutorizado";
            this.biAutorizado.Tag = "60";
            this.biAutorizado.Text = "Autorizado";
            this.biAutorizado.Click += new System.EventHandler(this.biAutorizado_Click);
            // 
            // biFormaPago
            // 
            this.biFormaPago.Name = "biFormaPago";
            this.biFormaPago.Tag = "61";
            this.biFormaPago.Text = "Forma de Pago";
            this.biFormaPago.Click += new System.EventHandler(this.biFormaPago_Click);
            // 
            // biMetodoPago
            // 
            this.biMetodoPago.Name = "biMetodoPago";
            this.biMetodoPago.Tag = "62";
            this.biMetodoPago.Text = "Metodos de Pago";
            this.biMetodoPago.Click += new System.EventHandler(this.biMetodoPago_Click);
            // 
            // biListasPrecios
            // 
            this.biListasPrecios.Name = "biListasPrecios";
            this.biListasPrecios.Tag = "63";
            this.biListasPrecios.Text = "Tipo Precios";
            this.biListasPrecios.Click += new System.EventHandler(this.biListasPrecios_Click);
            // 
            // biVehiculosTransporte
            // 
            this.biVehiculosTransporte.Name = "biVehiculosTransporte";
            this.biVehiculosTransporte.Tag = "64";
            this.biVehiculosTransporte.Text = "Vehiculos Transporte";
            this.biVehiculosTransporte.Visible = false;
            this.biVehiculosTransporte.Click += new System.EventHandler(this.biVehiculosTransporte_Click);
            // 
            // biConductores
            // 
            this.biConductores.Name = "biConductores";
            this.biConductores.Tag = "65";
            this.biConductores.Text = "Conductores";
            this.biConductores.Visible = false;
            this.biConductores.Click += new System.EventHandler(this.biConductores_Click);
            // 
            // biEmpresasTransporte
            // 
            this.biEmpresasTransporte.Name = "biEmpresasTransporte";
            this.biEmpresasTransporte.Tag = "66";
            this.biEmpresasTransporte.Text = "Empresas Transporte";
            this.biEmpresasTransporte.Visible = false;
            this.biEmpresasTransporte.Click += new System.EventHandler(this.biEmpresasTransporte_Click);
            // 
            // biZonas
            // 
            this.biZonas.Name = "biZonas";
            this.biZonas.Tag = "67";
            this.biZonas.Text = "Zonas";
            this.biZonas.Click += new System.EventHandler(this.biZonas_Click);
            // 
            // biVendedores
            // 
            this.biVendedores.Name = "biVendedores";
            this.biVendedores.Tag = "68";
            this.biVendedores.Text = "Vendedores";
            this.biVendedores.Click += new System.EventHandler(this.biVendedores_Click);
            // 
            // biDestaques
            // 
            this.biDestaques.Name = "biDestaques";
            this.biDestaques.Tag = "69";
            this.biDestaques.Text = "Destaques";
            this.biDestaques.Click += new System.EventHandler(this.biDestaques_Click);
            // 
            // biBancos
            // 
            this.biBancos.Name = "biBancos";
            this.biBancos.Tag = "72";
            this.biBancos.Text = "Bancos";
            this.biBancos.Click += new System.EventHandler(this.biBancos_Click);
            // 
            // biCuentasCorrientes
            // 
            this.biCuentasCorrientes.Name = "biCuentasCorrientes";
            this.biCuentasCorrientes.Tag = "81";
            this.biCuentasCorrientes.Text = "Cuentas Bancarias";
            this.biCuentasCorrientes.Click += new System.EventHandler(this.biCuentasCorrientes_Click);
            // 
            // biTarjetaPago
            // 
            this.biTarjetaPago.Name = "biTarjetaPago";
            this.biTarjetaPago.Tag = "82";
            this.biTarjetaPago.Text = "Tarjetas de Pago";
            this.biTarjetaPago.Click += new System.EventHandler(this.biTarjetaPago_Click);
            // 
            // biTipoEgresoCaja
            // 
            this.biTipoEgresoCaja.Name = "biTipoEgresoCaja";
            this.biTipoEgresoCaja.Tag = "95";
            this.biTipoEgresoCaja.Text = "Tipo de Egreso Caja";
            this.biTipoEgresoCaja.Click += new System.EventHandler(this.biTipoEgresoCaja_Click);
            // 
            // biParametros
            // 
            this.biParametros.ImageFixedSize = new System.Drawing.Size(48, 48);
            this.biParametros.ImageIndex = 72;
            this.biParametros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Default;
            this.biParametros.ImagePaddingHorizontal = 10;
            this.biParametros.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biParametros.Name = "biParametros";
            this.biParametros.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biVigenciaCotizaciones});
            this.biParametros.SubItemsExpandWidth = 14;
            this.biParametros.Tag = "70";
            this.biParametros.Text = "Parametros";
            this.biParametros.Click += new System.EventHandler(this.biParametros_Click_1);
            // 
            // biVigenciaCotizaciones
            // 
            this.biVigenciaCotizaciones.Name = "biVigenciaCotizaciones";
            this.biVigenciaCotizaciones.Tag = "71";
            this.biVigenciaCotizaciones.Text = "Vigencia de Cotizaciones";
            this.biVigenciaCotizaciones.Click += new System.EventHandler(this.biVigenciaCotizaciones_Click);
            // 
            // rbConfigurar
            // 
            this.rbConfigurar.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbConfigurar.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbConfigurar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbConfigurar.ContainerControlProcessDialogKey = true;
            this.rbConfigurar.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbConfigurar.DragDropSupport = true;
            this.rbConfigurar.Images = this.imageList1;
            this.rbConfigurar.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biEmpresa,
            this.biSucursal,
            this.biAlmacen,
            this.biUsuarios,
            this.biRepositorio});
            this.rbConfigurar.Location = new System.Drawing.Point(3, 0);
            this.rbConfigurar.Name = "rbConfigurar";
            this.rbConfigurar.Size = new System.Drawing.Size(331, 93);
            this.rbConfigurar.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rbConfigurar.TabIndex = 0;
            this.rbConfigurar.Text = "Configurar";
            // 
            // 
            // 
            this.rbConfigurar.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbConfigurar.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbConfigurar.TitleVisible = false;
            // 
            // biEmpresa
            // 
            this.biEmpresa.ImageIndex = 17;
            this.biEmpresa.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biEmpresa.Name = "biEmpresa";
            this.biEmpresa.SubItemsExpandWidth = 14;
            this.biEmpresa.Tag = "46";
            this.biEmpresa.Text = "Empresa";
            this.biEmpresa.Click += new System.EventHandler(this.buttonItem22_Click);
            // 
            // biSucursal
            // 
            this.biSucursal.ImageIndex = 46;
            this.biSucursal.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biSucursal.Name = "biSucursal";
            this.biSucursal.SubItemsExpandWidth = 14;
            this.biSucursal.Tag = "47";
            this.biSucursal.Text = "Sucursales";
            this.biSucursal.Click += new System.EventHandler(this.biSucursal_Click);
            // 
            // biAlmacen
            // 
            this.biAlmacen.ImageIndex = 15;
            this.biAlmacen.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biAlmacen.Name = "biAlmacen";
            this.biAlmacen.SubItemsExpandWidth = 14;
            this.biAlmacen.Tag = "48";
            this.biAlmacen.Text = "Almacen";
            this.biAlmacen.Click += new System.EventHandler(this.buttonItem23_Click);
            // 
            // biUsuarios
            // 
            this.biUsuarios.ImageIndex = 12;
            this.biUsuarios.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biUsuarios.Name = "biUsuarios";
            this.biUsuarios.SubItemsExpandWidth = 14;
            this.biUsuarios.Tag = "49";
            this.biUsuarios.Text = "Usuarios";
            this.biUsuarios.Click += new System.EventHandler(this.biUsuarios_Click);
            // 
            // biRepositorio
            // 
            this.biRepositorio.ImageIndex = 26;
            this.biRepositorio.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biRepositorio.Name = "biRepositorio";
            this.biRepositorio.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biRegeneracion});
            this.biRepositorio.SubItemsExpandWidth = 14;
            this.biRepositorio.Tag = "50";
            this.biRepositorio.Text = "Repositorio";
            this.biRepositorio.Click += new System.EventHandler(this.biRepositorio_Click);
            // 
            // biRegeneracion
            // 
            this.biRegeneracion.Name = "biRegeneracion";
            this.biRegeneracion.Text = "Regeneracion Archivos";
            this.biRegeneracion.Click += new System.EventHandler(this.biRegeneracion_Click);
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel1.Controls.Add(this.rbNegocio);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            this.ribbonPanel1.Visible = false;
            // 
            // rbNegocio
            // 
            this.rbNegocio.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbNegocio.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbNegocio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbNegocio.ContainerControlProcessDialogKey = true;
            this.rbNegocio.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbNegocio.DragDropSupport = true;
            this.rbNegocio.Images = this.imageList1;
            this.rbNegocio.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biProductos,
            this.biClientes,
            this.biProveedores});
            this.rbNegocio.Location = new System.Drawing.Point(3, 0);
            this.rbNegocio.Name = "rbNegocio";
            this.rbNegocio.Size = new System.Drawing.Size(216, 93);
            this.rbNegocio.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.rbNegocio.TabIndex = 0;
            this.rbNegocio.Text = "Negocio";
            // 
            // 
            // 
            this.rbNegocio.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbNegocio.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbNegocio.TitleVisible = false;
            // 
            // biProductos
            // 
            this.biProductos.ImageIndex = 0;
            this.biProductos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biProductos.Name = "biProductos";
            this.biProductos.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biCatalogo});
            this.biProductos.SubItemsExpandWidth = 14;
            this.biProductos.Tag = "39";
            this.biProductos.Text = "Productos";
            this.biProductos.Click += new System.EventHandler(this.buttonItem1_Click);
            // 
            // biCatalogo
            // 
            this.biCatalogo.Name = "biCatalogo";
            this.biCatalogo.Tag = "40";
            this.biCatalogo.Text = "Catalogo";
            this.biCatalogo.Click += new System.EventHandler(this.biCatalogo_Click);
            // 
            // biClientes
            // 
            this.biClientes.ImageIndex = 4;
            this.biClientes.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biClientes.Name = "biClientes";
            this.biClientes.SubItemsExpandWidth = 14;
            this.biClientes.Tag = "41";
            this.biClientes.Text = "Clientes";
            this.biClientes.Click += new System.EventHandler(this.biClientes_Click);
            // 
            // biProveedores
            // 
            this.biProveedores.ImageIndex = 5;
            this.biProveedores.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biProveedores.Name = "biProveedores";
            this.biProveedores.SubItemsExpandWidth = 14;
            this.biProveedores.Tag = "42";
            this.biProveedores.Text = "Proveedores";
            this.biProveedores.Click += new System.EventHandler(this.biProveedores_Click);
            // 
            // ribbonPanel10
            // 
            this.ribbonPanel10.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel10.Controls.Add(this.ribbonBar10);
            this.ribbonPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel10.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel10.Name = "ribbonPanel10";
            this.ribbonPanel10.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel10.Size = new System.Drawing.Size(640, 96);
            // 
            // 
            // 
            this.ribbonPanel10.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel10.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel10.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel10.TabIndex = 10;
            this.ribbonPanel10.Visible = false;
            // 
            // ribbonBar10
            // 
            this.ribbonBar10.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar10.ContainerControlProcessDialogKey = true;
            this.ribbonBar10.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar10.DragDropSupport = true;
            this.ribbonBar10.Images = this.imageList2;
            this.ribbonBar10.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.biRegistroCompras,
            this.biRegistroVentas});
            this.ribbonBar10.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar10.Name = "ribbonBar10";
            this.ribbonBar10.Size = new System.Drawing.Size(200, 93);
            this.ribbonBar10.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar10.TabIndex = 2;
            // 
            // 
            // 
            this.ribbonBar10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar10.TitleVisible = false;
            // 
            // biRegistroCompras
            // 
            this.biRegistroCompras.ImageIndex = 74;
            this.biRegistroCompras.ImagePaddingHorizontal = 20;
            this.biRegistroCompras.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biRegistroCompras.Name = "biRegistroCompras";
            this.biRegistroCompras.SubItemsExpandWidth = 14;
            this.biRegistroCompras.Tag = "110";
            this.biRegistroCompras.Text = "Registro de Compras";
            this.biRegistroCompras.Click += new System.EventHandler(this.biRegistroCompras_Click);
            // 
            // biRegistroVentas
            // 
            this.biRegistroVentas.ImageIndex = 75;
            this.biRegistroVentas.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.biRegistroVentas.Name = "biRegistroVentas";
            this.biRegistroVentas.SubItemsExpandWidth = 14;
            this.biRegistroVentas.Tag = "111";
            this.biRegistroVentas.Text = "Registro de Ventas";
            // 
            // ribbonPanel11
            // 
            this.ribbonPanel11.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonPanel11.Controls.Add(this.ribbonBar12);
            this.ribbonPanel11.Controls.Add(this.ribbonBar11);
            this.ribbonPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel11.Location = new System.Drawing.Point(0, 53);
            this.ribbonPanel11.Name = "ribbonPanel11";
            this.ribbonPanel11.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel11.Size = new System.Drawing.Size(706, 96);
            // 
            // 
            // 
            this.ribbonPanel11.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel11.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel11.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel11.TabIndex = 11;
            this.ribbonPanel11.Visible = false;
            // 
            // ribbonBar12
            // 
            this.ribbonBar12.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar12.ContainerControlProcessDialogKey = true;
            this.ribbonBar12.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar12.DragDropSupport = true;
            this.ribbonBar12.Images = this.imageList1;
            this.ribbonBar12.ImagesLarge = this.imageList1;
            this.ribbonBar12.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem6});
            this.ribbonBar12.Location = new System.Drawing.Point(80, 0);
            this.ribbonBar12.Name = "ribbonBar12";
            this.ribbonBar12.Size = new System.Drawing.Size(97, 93);
            this.ribbonBar12.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar12.TabIndex = 1;
            this.ribbonBar12.Text = "Gastos al Mes";
            // 
            // 
            // 
            this.ribbonBar12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar12.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // buttonItem6
            // 
            this.buttonItem6.ImageIndex = 56;
            this.buttonItem6.ImagePaddingVertical = 8;
            this.buttonItem6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Bottom;
            this.buttonItem6.Name = "buttonItem6";
            this.buttonItem6.NotificationMarkOffset = new System.Drawing.Point(3, 4);
            this.buttonItem6.SubItemsExpandWidth = 14;
            this.buttonItem6.Text = "Gastos Mes";
            this.buttonItem6.Click += new System.EventHandler(this.buttonItem6_Click_1);
            // 
            // ribbonBar11
            // 
            this.ribbonBar11.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar11.ContainerControlProcessDialogKey = true;
            this.ribbonBar11.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar11.DragDropSupport = true;
            this.ribbonBar11.Images = this.imageList1;
            this.ribbonBar11.ImagesLarge = this.imageList1;
            this.ribbonBar11.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.Reporte});
            this.ribbonBar11.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar11.Name = "ribbonBar11";
            this.ribbonBar11.Size = new System.Drawing.Size(77, 93);
            this.ribbonBar11.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.ribbonBar11.TabIndex = 0;
            this.ribbonBar11.Text = "Ventas Mes";
            // 
            // 
            // 
            this.ribbonBar11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar11.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // Reporte
            // 
            this.Reporte.ImageIndex = 55;
            this.Reporte.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.Reporte.Name = "Reporte";
            this.Reporte.SubItemsExpandWidth = 14;
            this.Reporte.Text = "Reporte";
            this.Reporte.Click += new System.EventHandler(this.Reporte_Click);
            // 
            // biLogout
            // 
            this.biLogout.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.biLogout.Checked = true;
            this.biLogout.ForeColor = System.Drawing.Color.Maroon;
            this.biLogout.Image = ((System.Drawing.Image)(resources.GetObject("biLogout.Image")));
            this.biLogout.ImageIndex = 51;
            this.biLogout.Name = "biLogout";
            this.biLogout.Text = "Log Out";
            this.biLogout.Click += new System.EventHandler(this.biLogout_Click);
            // 
            // rtVentas
            // 
            this.rtVentas.Name = "rtVentas";
            this.rtVentas.Panel = this.ribbonPanel5;
            this.rtVentas.Tag = "1";
            this.rtVentas.Text = "Ventas";
            this.rtVentas.Click += new System.EventHandler(this.rtVentas_Click);
            // 
            // rtCompras
            // 
            this.rtCompras.Name = "rtCompras";
            this.rtCompras.Panel = this.ribbonPanel6;
            this.rtCompras.Tag = "2";
            this.rtCompras.Text = "Compras";
            this.rtCompras.Click += new System.EventHandler(this.rtCompras_Click);
            // 
            // rtOperaciones
            // 
            this.rtOperaciones.Name = "rtOperaciones";
            this.rtOperaciones.Panel = this.ribbonPanel2;
            this.rtOperaciones.Tag = "3";
            this.rtOperaciones.Text = "Almacen";
            this.rtOperaciones.Click += new System.EventHandler(this.rtOperaciones_Click);
            // 
            // rtEntidades
            // 
            this.rtEntidades.Name = "rtEntidades";
            this.rtEntidades.Panel = this.ribbonPanel1;
            this.rtEntidades.Tag = "4";
            this.rtEntidades.Text = "Entidades";
            this.rtEntidades.Click += new System.EventHandler(this.rtEntidades_Click);
            // 
            // rtReportes
            // 
            this.rtReportes.Name = "rtReportes";
            this.rtReportes.Panel = this.ribbonPanel3;
            this.rtReportes.Tag = "5";
            this.rtReportes.Text = "Reportes";
            this.rtReportes.Click += new System.EventHandler(this.rtReportes_Click);
            // 
            // rtAdministrador
            // 
            this.rtAdministrador.Name = "rtAdministrador";
            this.rtAdministrador.Panel = this.ribbonPanel4;
            this.rtAdministrador.Tag = "6";
            this.rtAdministrador.Text = "Administrador";
            this.rtAdministrador.Click += new System.EventHandler(this.rtAdministrador_Click);
            // 
            // rbCaja
            // 
            this.rbCaja.Checked = true;
            this.rbCaja.Name = "rbCaja";
            this.rbCaja.Panel = this.ribbonPanel7;
            this.rbCaja.Tag = "83";
            this.rbCaja.Text = "CajayBancos";
            this.rbCaja.Click += new System.EventHandler(this.rbCaja_Click);
            // 
            // Libros
            // 
            this.Libros.Name = "Libros";
            this.Libros.Panel = this.ribbonPanel10;
            this.Libros.Tag = "109";
            this.Libros.Text = "LibrosElectronicos";
            this.Libros.Visible = false;
            // 
            // ribbonTabItem1
            // 
            this.ribbonTabItem1.Name = "ribbonTabItem1";
            this.ribbonTabItem1.Panel = this.ribbonPanel11;
            this.ribbonTabItem1.Text = "ReporteVentas";
            this.ribbonTabItem1.Visible = false;
            // 
            // rbLibrosElectronicos
            // 
            this.rbLibrosElectronicos.Name = "rbLibrosElectronicos";
            this.rbLibrosElectronicos.Visible = false;
            // 
            // liTipodeCambio
            // 
            this.liTipodeCambio.BackColor = System.Drawing.Color.Transparent;
            this.liTipodeCambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.liTipodeCambio.ForeColor = System.Drawing.Color.Maroon;
            this.liTipodeCambio.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.liTipodeCambio.Name = "liTipodeCambio";
            this.liTipodeCambio.PaddingBottom = 5;
            this.liTipodeCambio.SingleLineColor = System.Drawing.SystemColors.WindowFrame;
            this.liTipodeCambio.Text = "Tipo de Cambio";
            // 
            // comboItem1
            // 
            this.comboItem1.ForeColor = System.Drawing.Color.White;
            this.comboItem1.Text = "comboItem1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sUsuario,
            this.sEmpresa,
            this.sAlmacen,
            this.sIP});
            this.statusStrip1.Location = new System.Drawing.Point(5, 423);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(640, 24);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sUsuario
            // 
            this.sUsuario.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.sUsuario.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.sUsuario.Name = "sUsuario";
            this.sUsuario.Size = new System.Drawing.Size(156, 19);
            this.sUsuario.Spring = true;
            this.sUsuario.Text = "Usuario";
            // 
            // sEmpresa
            // 
            this.sEmpresa.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.sEmpresa.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.sEmpresa.Name = "sEmpresa";
            this.sEmpresa.Size = new System.Drawing.Size(156, 19);
            this.sEmpresa.Spring = true;
            this.sEmpresa.Text = "Empresa";
            // 
            // sAlmacen
            // 
            this.sAlmacen.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.sAlmacen.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.sAlmacen.Name = "sAlmacen";
            this.sAlmacen.Size = new System.Drawing.Size(156, 19);
            this.sAlmacen.Spring = true;
            this.sAlmacen.Text = "Almacen";
            // 
            // sIP
            // 
            this.sIP.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.sIP.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.sIP.Name = "sIP";
            this.sIP.Size = new System.Drawing.Size(156, 19);
            this.sIP.Spring = true;
            this.sIP.Text = "IP";
            // 
            // tabStrip1
            // 
            this.tabStrip1.AutoHideSystemBox = true;
            this.tabStrip1.AutoSelectAttachedControl = true;
            this.tabStrip1.CanReorderTabs = true;
            this.tabStrip1.CloseButtonOnTabsAlwaysDisplayed = false;
            this.tabStrip1.CloseButtonVisible = false;
            this.tabStrip1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tabStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabStrip1.ForeColor = System.Drawing.Color.Black;
            this.tabStrip1.Location = new System.Drawing.Point(5, 400);
            this.tabStrip1.MdiForm = this;
            this.tabStrip1.MdiNoFormActivateFlicker = false;
            this.tabStrip1.MdiTabbedDocuments = true;
            this.tabStrip1.Name = "tabStrip1";
            this.tabStrip1.SelectedTab = null;
            this.tabStrip1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabStrip1.Size = new System.Drawing.Size(640, 23);
            this.tabStrip1.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabStrip1.TabIndex = 9;
            this.tabStrip1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabStrip1.Text = "tabStrip1";
            this.tabStrip1.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tabStrip1_SelectedTabChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "sigefa.sql";
            this.saveFileDialog1.Title = "Exportar backup";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Title = "Importar BD";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // mdi_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(650, 449);
            this.Controls.Add(this.tabStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ribbonControl1);
            this.EnableGlass = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(650, 450);
            this.Name = "mdi_Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SGE - SYSTEM\'S ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mdi_Menu_FormClosed);
            this.Load += new System.EventHandler(this.mdi_Menu_Load);
            this.MdiChildActivate += new System.EventHandler(this.mdi_Menu_MdiChildActivate);
            this.Shown += new System.EventHandler(this.mdi_Menu_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mdi_Menu_KeyDown);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel7.ResumeLayout(false);
            this.ribbonPanel5.ResumeLayout(false);
            this.ribbonPanel6.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.ribbonPanel10.ResumeLayout(false);
            this.ribbonPanel11.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonBar rbNegocio;
        private DevComponents.DotNetBar.ButtonItem biProductos;
        private DevComponents.DotNetBar.ButtonItem biClientes;
        private DevComponents.DotNetBar.RibbonTabItem rtEntidades;
        private DevComponents.DotNetBar.RibbonTabItem rtOperaciones;
        private DevComponents.DotNetBar.ButtonItem biProveedores;
        private DevComponents.DotNetBar.RibbonBar rbOperaciones;
        private DevComponents.DotNetBar.ButtonItem biNotadeIngreso;
        private DevComponents.DotNetBar.ButtonItem biNotadeSalida;
        private DevComponents.DotNetBar.ButtonItem biTransferencia;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonTabItem rtReportes;
        private DevComponents.DotNetBar.RibbonTabItem rtAdministrador;
        private DevComponents.DotNetBar.RibbonBar rbReportes;
        private DevComponents.DotNetBar.ButtonItem biInventario;
        private DevComponents.DotNetBar.ButtonItem biKardex;
        private DevComponents.DotNetBar.RibbonBar rbConfigurar;
        private DevComponents.DotNetBar.ButtonItem biEmpresa;
        private DevComponents.DotNetBar.ButtonItem biAlmacen;
        private DevComponents.DotNetBar.RibbonBar ribbonBar4;
        private DevComponents.DotNetBar.ButtonItem biTablas;
        private DevComponents.DotNetBar.ButtonItem biUnidades;
        private DevComponents.DotNetBar.ButtonItem biFamilias;
        private DevComponents.DotNetBar.ButtonItem biMarcas;
        private DevComponents.DotNetBar.ButtonItem biTipoArticulo;
        private DevComponents.DotNetBar.ButtonItem biCaracteristica;
        private DevComponents.Editors.ComboItem comboItem1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private DevComponents.DotNetBar.TabStrip tabStrip1;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private System.Windows.Forms.ToolStripStatusLabel sEmpresa;
        private System.Windows.Forms.ToolStripStatusLabel sAlmacen;
        private System.Windows.Forms.ToolStripStatusLabel sUsuario;
        private System.Windows.Forms.ToolStripStatusLabel sIP;
        private DevComponents.DotNetBar.ButtonItem biDocumentos;
        private DevComponents.DotNetBar.RibbonTabItem rtVentas;
        private DevComponents.DotNetBar.RibbonTabItem rtCompras;
        private DevComponents.DotNetBar.ButtonItem biTransacciones;
        private DevComponents.DotNetBar.RibbonBar rbVentas;
        private DevComponents.DotNetBar.ButtonItem biVenta;
        private DevComponents.DotNetBar.RibbonBar rbCompras;
        private DevComponents.DotNetBar.ButtonItem biPedidoCompra;
        private DevComponents.DotNetBar.ButtonItem buttonItem17;
        private DevComponents.DotNetBar.ButtonItem biTipoCambio;
        private DevComponents.DotNetBar.ButtonItem biAutorizado;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.ButtonItem biConsulta;
        private DevComponents.DotNetBar.ButtonItem biModificar;
        private DevComponents.DotNetBar.ButtonItem biEliminar;
        private DevComponents.DotNetBar.ButtonItem biUsuarios;
        private DevComponents.DotNetBar.ButtonItem biFormaPago;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ButtonItem biBackup;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevComponents.DotNetBar.ButtonItem biImport;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar3;
        private DevComponents.DotNetBar.ButtonItem biCobros;
        private DevComponents.DotNetBar.ButtonItem biPagos;
        private DevComponents.DotNetBar.ButtonItem biMetodoPago;
        private DevComponents.DotNetBar.ButtonItem biGuia;
        private DevComponents.DotNetBar.RibbonBar riReportesGeneral;
        private DevComponents.DotNetBar.ButtonItem btnReporte;
        private DevComponents.DotNetBar.ButtonItem biListasPrecios;
        private DevComponents.DotNetBar.ButtonItem biVehiculosTransporte;
        private DevComponents.DotNetBar.ButtonItem biConductores;
        private DevComponents.DotNetBar.ButtonItem biEmpresasTransporte;
        private DevComponents.DotNetBar.ButtonItem biZonas;
        private DevComponents.DotNetBar.ButtonItem biVendedores;
        private DevComponents.DotNetBar.ButtonItem biDestaques;
        private DevComponents.DotNetBar.RibbonBar ribbonBar5;
        private DevComponents.DotNetBar.ButtonItem btArqueo;
        private DevComponents.DotNetBar.ButtonItem biGuias;
        private DevComponents.DotNetBar.ButtonItem biAnular;
        private DevComponents.DotNetBar.ButtonItem biNotaCredito;
        private DevComponents.DotNetBar.ButtonItem ciNotasdeCredito;
        private DevComponents.DotNetBar.ButtonItem biMuestraVentas;
        private DevComponents.DotNetBar.RibbonBar ribbonBar6;
        private DevComponents.DotNetBar.ButtonItem biCotizacion;
        private DevComponents.DotNetBar.ButtonItem biCotizacionesVigentes;
        private DevComponents.DotNetBar.ButtonItem biPedidoVenta;
        private DevComponents.DotNetBar.ButtonItem biPedidosPendientes;
        private DevComponents.DotNetBar.ButtonItem biCatalogo;
        private DevComponents.DotNetBar.ButtonItem buttonItem16;
        private DevComponents.DotNetBar.ButtonItem biBancos;
        private DevComponents.DotNetBar.RibbonBar ribbonBar7;
        private DevComponents.DotNetBar.ButtonItem bitermetro;
        private DevComponents.DotNetBar.ButtonItem biBuscarGuia;
        private DevComponents.DotNetBar.ButtonItem btnRequerimiento;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private DevComponents.DotNetBar.ButtonItem biOrdenCompra;
        private DevComponents.DotNetBar.ButtonItem biOrdenesCompras;
        private DevComponents.DotNetBar.ButtonItem biCompraOrden;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel6;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel5;
        private DevComponents.DotNetBar.ButtonItem biSucursal;
        private DevComponents.DotNetBar.ButtonItem biTransferenciasPendientes;
        private DevComponents.DotNetBar.ButtonItem biHistorialRequerimiento;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel7;
        private DevComponents.DotNetBar.RibbonTabItem rbCaja;
        private DevComponents.DotNetBar.RibbonBar ribbonBar14;
        private DevComponents.DotNetBar.ButtonItem biMovimientosCaja;
        private DevComponents.DotNetBar.RibbonBar ribbonBar9;
        private System.Windows.Forms.ImageList imageList2;
        private DevComponents.DotNetBar.ButtonItem biHistorialFacturaciones;
        private DevComponents.DotNetBar.ButtonItem biCotizacionesAprobadas;
        private DevComponents.DotNetBar.ButtonItem biConsolidado;
        private DevComponents.DotNetBar.ButtonItem biCuentasCorrientes;
        private DevComponents.DotNetBar.ButtonItem biTarjetaPago;
        private DevComponents.DotNetBar.ButtonItem biParametros;
        private DevComponents.DotNetBar.ButtonItem biVigenciaCotizaciones;
        private DevComponents.DotNetBar.ButtonItem biGuiasSinFacturar;
        private DevComponents.DotNetBar.ButtonItem biStockAlmacenes;
        private DevComponents.DotNetBar.ButtonItem biNotaCreditoCompra;
        private DevComponents.DotNetBar.ButtonItem biNotasCreditoCompras;
        private DevComponents.DotNetBar.ButtonItem btnNotaDebitoC;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private DevComponents.DotNetBar.ButtonItem biTipoEgresoCaja;
        private DevComponents.DotNetBar.ButtonItem biNotaDebito;
        private DevComponents.DotNetBar.ButtonItem ciNotasdeDebito;
        private DevComponents.DotNetBar.ButtonItem btnMasivo;
        private DevComponents.DotNetBar.ButtonItem biRotacionProducto;
        private DevComponents.DotNetBar.ButtonItem BiAperturaCaja;
        private DevComponents.DotNetBar.ButtonItem biStockMinimos;
        private DevComponents.DotNetBar.RibbonTabItem rbLibrosElectronicos;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel10;
        private DevComponents.DotNetBar.RibbonBar ribbonBar10;
        private DevComponents.DotNetBar.ButtonItem biRegistroCompras;
        private DevComponents.DotNetBar.ButtonItem biRegistroVentas;
        private DevComponents.DotNetBar.RibbonTabItem Libros;
        private DevComponents.DotNetBar.ButtonItem biLogout;
        private DevComponents.DotNetBar.ButtonItem btnVerCompras;
        private DevComponents.DotNetBar.ButtonItem biVentaSeparacion;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private System.Windows.Forms.ImageList imageList1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel11;
        private DevComponents.DotNetBar.RibbonBar ribbonBar11;
        private DevComponents.DotNetBar.RibbonTabItem ribbonTabItem1;
        private DevComponents.DotNetBar.ButtonItem Reporte;
        private DevComponents.DotNetBar.RibbonBar ribbonBar13;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.RibbonBar ribbonBar8;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.RibbonBar ribbonBar12;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.LabelItem liTipodeCambio;
        private DevComponents.DotNetBar.ButtonItem biRepositorio;
        private DevComponents.DotNetBar.ButtonItem buttonItem18;
        private DevComponents.DotNetBar.RibbonBar ribbonBar15;
        private DevComponents.DotNetBar.ButtonItem biCajaAhorros;
        private DevComponents.DotNetBar.ButtonItem biCajaGeneral;
        private DevComponents.DotNetBar.ButtonItem biCtas;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem biRegeneracion;
        private DevComponents.DotNetBar.ButtonItem biDescuentos;
    }
}



