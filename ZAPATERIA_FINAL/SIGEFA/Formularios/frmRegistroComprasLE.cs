﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmRegistroComprasLE : DevComponents.DotNetBar.Office2007Form
    {
        clsValidar val = new clsValidar();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
        clsAdmFactura admCompras = new clsAdmFactura();

        public frmRegistroComprasLE()
        {
            InitializeComponent();
        }       
               
        private void Limpiar()
        {
            
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

            if (txtnombrelibro.Text != "" && txtnombrelibro.Text != ".")
            {
                //se crea un objeto de tipo savefiledialog que nos servira para guardar el archivo
                SaveFileDialog Save = new SaveFileDialog();
                System.IO.StreamWriter myStreamWriter = null;
                //al igual que para abrir el tipo de documentos aqui se especifica en que extenciones se puede guardar el archivo
                Save.FileName = txtnombrelibro.Text;
                Save.Filter = "Text (*.txt)|*.txt|HTML(*.html*)|*.html|All files(*.*)|*.*";
                Save.CheckPathExists = true;
                Save.Title = "Guardar como";
                Save.ShowDialog(this);
                try
                {
                    //este codigo se utiliza para guardar el archivo de nuestro editor
                    myStreamWriter = System.IO.File.AppendText(Save.FileName);
                    

                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable("ListaGuias");
                    // Columnas
                    foreach (DataGridViewColumn column in dgvVentas.Columns)
                    {
                        DataColumn dc = new DataColumn(column.Name.ToString());
                        dt.Columns.Add(dc);
                    }
                    // Datos
                    for (int i = 0; i < dgvVentas.Rows.Count; i++)
                    {
                        DataGridViewRow row = dgvVentas.Rows[i];
                        DataRow dr = dt.NewRow();
                        for (int j = 0; j < dgvVentas.Columns.Count; j++)
                        {
                            dr[j] = (row.Cells[j].Value == null) ? "" : row.Cells[j].Value.ToString().Trim();
                        }
                        dt.Rows.Add(dr);
                    }

                    ds.Tables.Add(dt);

                   
                    String cad = "";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            if (j == (ds.Tables[0].Columns.Count - 1))
                            {
                                cad = cad + ((ds.Tables[0].Rows[i][j]).ToString() + "\t");
                                myStreamWriter.WriteLine(cad);
                                cad = "";
                            }
                            else
                            { cad = cad + ((ds.Tables[0].Rows[i][j]).ToString() + "|"); }
                        }
                    }

                    //myStreamWriter.Write(cad);
                    myStreamWriter.Flush();
                    

                }
                catch (Exception) { }
            }
            
        }

        private void biSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMontoLiquidar_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.SOLONumeros(sender, e);
        }

        private void biAprobar_Click(object sender, EventArgs e)
        {
            frmGestionNombreLE form = new frmGestionNombreLE();
            form.Hoy = dtpHasta.Value;
            if (form.ShowDialog() == DialogResult.OK) 
            {
                txtnombrelibro.Text = form.GetNombre();
                btnGuardar.Enabled = true;
            }            
        }

        private void frmRegistroComprasLE_Load(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void CargaLista()
        {
            dgvVentas.DataSource = data;
            data.DataSource = admCompras.MuestraFactura(dtpDesde.Value, dtpHasta.Value, frmLogin.iCodAlmacen);
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvVentas.ClearSelection();
        }

        private void dtpDesde_ValueChanged(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dtpHasta_ValueChanged(object sender, EventArgs e)
        {
            CargaLista();
        }

        
    }
}
