﻿namespace SIGEFA.Formularios
{
    partial class frmGestionEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGestionEmpresa));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.customValidator1 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.customValidator2 = new DevComponents.DotNetBar.Validator.CustomValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.txtDepartamento = new Telerik.WinControls.UI.RadTextBox();
            this.txtProvincia = new Telerik.WinControls.UI.RadTextBox();
            this.txtDistrito = new Telerik.WinControls.UI.RadTextBox();
            this.txtUsuariosol = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtClavesol = new Telerik.WinControls.UI.RadTextBox();
            this.btnAceptar = new Telerik.WinControls.UI.RadButton();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.btnCancelar = new Telerik.WinControls.UI.RadButton();
            this.txtUbigeo = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtNombrecer = new Telerik.WinControls.UI.RadTextBox();
            this.txtClavecer = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.txtUrl = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.txtTelefono = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.txtDireccion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.txtRazonSocial = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.txtRUC = new Telerik.WinControls.UI.RadTextBox();
            this.cbActivoE = new Telerik.WinControls.UI.RadCheckBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.txtCodEmpresa = new Telerik.WinControls.UI.RadTextBox();
            this.requiredFieldValidator1 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.requiredFieldValidator3 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProvincia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistrito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuariosol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClavesol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUbigeo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombrecer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClavecer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUrl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDireccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActivoE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodEmpresa)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "cross.png");
            this.imageList1.Images.SetKeyName(1, "tick.png");
            this.imageList1.Images.SetKeyName(2, "Clear Green Button.ico");
            // 
            // superValidator1
            // 
            this.superValidator1.ContainerControl = this;
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // customValidator1
            // 
            this.customValidator1.ErrorMessage = "El RUC ingresado no es valido";
            this.customValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator1.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator1_ValidateValue);
            // 
            // customValidator2
            // 
            this.customValidator2.ErrorMessage = "Ingrese la Razon Social";
            this.customValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            this.customValidator2.ValidateValue += new DevComponents.DotNetBar.Validator.ValidateValueEventHandler(this.customValidator2_ValidateValue);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel1.Location = new System.Drawing.Point(5, 27);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(79, 15);
            this.radLabel1.TabIndex = 12;
            this.radLabel1.Text = "Departamento:";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel2.Location = new System.Drawing.Point(5, 54);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(54, 15);
            this.radLabel2.TabIndex = 13;
            this.radLabel2.Text = "Provincia:";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel3.Location = new System.Drawing.Point(5, 83);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(43, 15);
            this.radLabel3.TabIndex = 14;
            this.radLabel3.Text = "Distrito:";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel3.ThemeName = "Material";
            // 
            // txtDepartamento
            // 
            this.txtDepartamento.Location = new System.Drawing.Point(99, 25);
            this.txtDepartamento.Name = "txtDepartamento";
            this.txtDepartamento.Size = new System.Drawing.Size(100, 20);
            this.txtDepartamento.TabIndex = 15;
            // 
            // txtProvincia
            // 
            this.txtProvincia.Location = new System.Drawing.Point(99, 52);
            this.txtProvincia.Name = "txtProvincia";
            this.txtProvincia.Size = new System.Drawing.Size(100, 20);
            this.txtProvincia.TabIndex = 16;
            // 
            // txtDistrito
            // 
            this.txtDistrito.Location = new System.Drawing.Point(99, 81);
            this.txtDistrito.Name = "txtDistrito";
            this.txtDistrito.Size = new System.Drawing.Size(100, 20);
            this.txtDistrito.TabIndex = 16;
            // 
            // txtUsuariosol
            // 
            this.txtUsuariosol.Location = new System.Drawing.Point(118, 25);
            this.txtUsuariosol.Name = "txtUsuariosol";
            this.txtUsuariosol.Size = new System.Drawing.Size(123, 20);
            this.txtUsuariosol.TabIndex = 18;
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel4.Location = new System.Drawing.Point(5, 27);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 15);
            this.radLabel4.TabIndex = 17;
            this.radLabel4.Text = "Usuario Sol:";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel4.ThemeName = "Material";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel6.Location = new System.Drawing.Point(247, 29);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(56, 15);
            this.radLabel6.TabIndex = 18;
            this.radLabel6.Text = "Clave Sol:";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel6.ThemeName = "Material";
            // 
            // txtClavesol
            // 
            this.txtClavesol.Location = new System.Drawing.Point(346, 26);
            this.txtClavesol.Name = "txtClavesol";
            this.txtClavesol.Size = new System.Drawing.Size(114, 20);
            this.txtClavesol.TabIndex = 19;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(537, 209);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(93, 23);
            this.btnAceptar.TabIndex = 20;
            this.btnAceptar.Text = "Guardar";
            this.btnAceptar.ThemeName = "Material";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(537, 255);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(93, 23);
            this.btnCancelar.TabIndex = 21;
            this.btnCancelar.Text = "Salir";
            this.btnCancelar.ThemeName = "Material";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtUbigeo
            // 
            this.txtUbigeo.Location = new System.Drawing.Point(99, 109);
            this.txtUbigeo.Name = "txtUbigeo";
            this.txtUbigeo.Size = new System.Drawing.Size(100, 20);
            this.txtUbigeo.TabIndex = 18;
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel9.Location = new System.Drawing.Point(5, 111);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(44, 15);
            this.radLabel9.TabIndex = 17;
            this.radLabel9.Text = "Ubigeo:";
            this.radLabel9.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel9.ThemeName = "Material";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.txtUbigeo);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.txtDepartamento);
            this.radGroupBox1.Controls.Add(this.txtProvincia);
            this.radGroupBox1.Controls.Add(this.txtDistrito);
            this.radGroupBox1.HeaderText = "Localidad";
            this.radGroupBox1.Location = new System.Drawing.Point(483, 12);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(204, 159);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Localidad";
            this.radGroupBox1.ThemeName = "Material";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.txtUrl);
            this.radGroupBox2.Controls.Add(this.radLabel10);
            this.radGroupBox2.Controls.Add(this.txtClavecer);
            this.radGroupBox2.Controls.Add(this.radLabel8);
            this.radGroupBox2.Controls.Add(this.txtNombrecer);
            this.radGroupBox2.Controls.Add(this.radLabel5);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.txtUsuariosol);
            this.radGroupBox2.Controls.Add(this.radLabel6);
            this.radGroupBox2.Controls.Add(this.txtClavesol);
            this.radGroupBox2.HeaderText = "Datos Sunat";
            this.radGroupBox2.Location = new System.Drawing.Point(12, 199);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(465, 123);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.Text = "Datos Sunat";
            this.radGroupBox2.ThemeName = "Material";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel5.Location = new System.Drawing.Point(5, 56);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(104, 15);
            this.radLabel5.TabIndex = 18;
            this.radLabel5.Text = "Nombre Certificado:";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel5.ThemeName = "Material";
            // 
            // txtNombrecer
            // 
            this.txtNombrecer.Location = new System.Drawing.Point(118, 54);
            this.txtNombrecer.Name = "txtNombrecer";
            this.txtNombrecer.Size = new System.Drawing.Size(123, 20);
            this.txtNombrecer.TabIndex = 19;
            // 
            // txtClavecer
            // 
            this.txtClavecer.Location = new System.Drawing.Point(346, 55);
            this.txtClavecer.Name = "txtClavecer";
            this.txtClavecer.Size = new System.Drawing.Size(114, 20);
            this.txtClavecer.TabIndex = 21;
            // 
            // radLabel8
            // 
            this.radLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel8.Location = new System.Drawing.Point(247, 57);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(93, 15);
            this.radLabel8.TabIndex = 20;
            this.radLabel8.Text = "Clave Certificado:";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel8.ThemeName = "Material";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel10.Location = new System.Drawing.Point(5, 91);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(75, 15);
            this.radLabel10.TabIndex = 19;
            this.radLabel10.Text = "URL de envio:";
            this.radLabel10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel10.ThemeName = "Material";
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(118, 89);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(342, 20);
            this.txtUrl.TabIndex = 20;
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.txtCodEmpresa);
            this.radGroupBox3.Controls.Add(this.radLabel15);
            this.radGroupBox3.Controls.Add(this.cbActivoE);
            this.radGroupBox3.Controls.Add(this.txtTelefono);
            this.radGroupBox3.Controls.Add(this.radLabel7);
            this.radGroupBox3.Controls.Add(this.radLabel11);
            this.radGroupBox3.Controls.Add(this.txtDireccion);
            this.radGroupBox3.Controls.Add(this.radLabel12);
            this.radGroupBox3.Controls.Add(this.radLabel13);
            this.radGroupBox3.Controls.Add(this.txtRazonSocial);
            this.radGroupBox3.Controls.Add(this.radLabel14);
            this.radGroupBox3.Controls.Add(this.txtRUC);
            this.radGroupBox3.HeaderText = "Datos Generales";
            this.radGroupBox3.Location = new System.Drawing.Point(12, 12);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(465, 181);
            this.radGroupBox3.TabIndex = 22;
            this.radGroupBox3.Text = "Datos Generales";
            this.radGroupBox3.ThemeName = "Material";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(94, 147);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(208, 20);
            this.txtTelefono.TabIndex = 20;
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel7.Location = new System.Drawing.Point(4, 151);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(52, 15);
            this.radLabel7.TabIndex = 19;
            this.radLabel7.Text = "Telefono:";
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabel7.ThemeName = "Material";
            // 
            // radLabel11
            // 
            this.radLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel11.Location = new System.Drawing.Point(346, 30);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(43, 15);
            this.radLabel11.TabIndex = 20;
            this.radLabel11.Text = "Estado:";
            this.radLabel11.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel11.ThemeName = "Material";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(94, 117);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(208, 20);
            this.txtDireccion.TabIndex = 19;
            // 
            // radLabel12
            // 
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel12.Location = new System.Drawing.Point(4, 121);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(55, 15);
            this.radLabel12.TabIndex = 18;
            this.radLabel12.Text = "Direccion:";
            this.radLabel12.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel12.ThemeName = "Material";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel13.Location = new System.Drawing.Point(3, 67);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(74, 15);
            this.radLabel13.TabIndex = 17;
            this.radLabel13.Text = "Razon Social:";
            this.radLabel13.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel13.ThemeName = "Material";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Location = new System.Drawing.Point(94, 63);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(208, 20);
            this.txtRazonSocial.TabIndex = 18;
            this.superValidator1.SetValidator1(this.txtRazonSocial, this.requiredFieldValidator1);
            // 
            // radLabel14
            // 
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel14.Location = new System.Drawing.Point(4, 94);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(33, 15);
            this.radLabel14.TabIndex = 18;
            this.radLabel14.Text = "RUC:";
            this.radLabel14.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel14.ThemeName = "Material";
            // 
            // txtRUC
            // 
            this.txtRUC.Location = new System.Drawing.Point(94, 90);
            this.txtRUC.Name = "txtRUC";
            this.txtRUC.Size = new System.Drawing.Size(208, 20);
            this.txtRUC.TabIndex = 19;
            this.superValidator1.SetValidator1(this.txtRUC, this.requiredFieldValidator3);
            // 
            // cbActivoE
            // 
            this.cbActivoE.Location = new System.Drawing.Point(346, 53);
            this.cbActivoE.Name = "cbActivoE";
            this.cbActivoE.Size = new System.Drawing.Size(65, 19);
            this.cbActivoE.TabIndex = 21;
            this.cbActivoE.Text = "Activo";
            this.cbActivoE.ThemeName = "Material";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(12, 328);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(288, 23);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = "(*) Presione F2 para verificar los datos con la SUNAT";
            this.labelX1.TextLineAlignment = System.Drawing.StringAlignment.Near;
            this.labelX1.Visible = false;
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.radLabel15.Location = new System.Drawing.Point(5, 40);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(76, 15);
            this.radLabel15.TabIndex = 18;
            this.radLabel15.Text = "Cod. Empresa";
            this.radLabel15.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel15.ThemeName = "Material";
            // 
            // txtCodEmpresa
            // 
            this.txtCodEmpresa.Enabled = false;
            this.txtCodEmpresa.Location = new System.Drawing.Point(94, 36);
            this.txtCodEmpresa.Name = "txtCodEmpresa";
            this.txtCodEmpresa.Size = new System.Drawing.Size(208, 20);
            this.txtCodEmpresa.TabIndex = 19;
            // 
            // requiredFieldValidator1
            // 
            this.requiredFieldValidator1.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator3
            // 
            this.requiredFieldValidator3.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator3.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // frmGestionEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 351);
            this.Controls.Add(this.radGroupBox3);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.labelX1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmGestionEmpresa";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion Empresa";
            this.Load += new System.EventHandler(this.frmGestionEmpresa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProvincia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistrito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuariosol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClavesol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUbigeo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombrecer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClavecer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUrl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDireccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRUC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbActivoE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodEmpresa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList1;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator1;
        private DevComponents.DotNetBar.Validator.CustomValidator customValidator2;
        private Telerik.WinControls.UI.RadButton btnCancelar;
        private Telerik.WinControls.UI.RadButton btnAceptar;
        private Telerik.WinControls.UI.RadTextBox txtClavesol;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox txtUsuariosol;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txtDistrito;
        private Telerik.WinControls.UI.RadTextBox txtProvincia;
        private Telerik.WinControls.UI.RadTextBox txtDepartamento;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadTextBox txtUbigeo;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadTextBox txtClavecer;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox txtNombrecer;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox txtUrl;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadTextBox txtTelefono;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadTextBox txtDireccion;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadTextBox txtRazonSocial;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadTextBox txtRUC;
        private Telerik.WinControls.UI.RadCheckBox cbActivoE;
        private DevComponents.DotNetBar.LabelX labelX1;
        private Telerik.WinControls.UI.RadTextBox txtCodEmpresa;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator1;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator3;
    }
}