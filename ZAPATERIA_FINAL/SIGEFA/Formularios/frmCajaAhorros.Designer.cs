﻿namespace SIGEFA.Formularios
{
    partial class frmCajaAhorros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCajaAhorros));
            this.dgvMovimientos = new Telerik.WinControls.UI.RadGridView();
            this.dtpFinicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.dtpFfin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.lblTotal = new Telerik.WinControls.UI.RadLabel();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.lblGastos = new Telerik.WinControls.UI.RadLabel();
            this.btnReporte = new Telerik.WinControls.UI.RadButton();
            this.btnRetiro = new Telerik.WinControls.UI.RadButton();
            this.btnBuscar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGastos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRetiro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMovimientos
            // 
            this.dgvMovimientos.AutoSizeRows = true;
            this.dgvMovimientos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMovimientos.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.dgvMovimientos.MasterTemplate.AllowAddNewRow = false;
            this.dgvMovimientos.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "idahorro";
            gridViewTextBoxColumn1.HeaderText = "idahorro";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "idahorro";
            gridViewTextBoxColumn2.FieldName = "idalmacen";
            gridViewTextBoxColumn2.HeaderText = "idalmacen";
            gridViewTextBoxColumn2.IsVisible = false;
            gridViewTextBoxColumn2.Name = "idalmacen";
            gridViewTextBoxColumn3.FieldName = "almacen";
            gridViewTextBoxColumn3.HeaderText = "Origen";
            gridViewTextBoxColumn3.Name = "almacen";
            gridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn3.Width = 142;
            gridViewTextBoxColumn4.FieldName = "fecharegistro";
            gridViewTextBoxColumn4.HeaderText = "Fecha Registro";
            gridViewTextBoxColumn4.Name = "fecharegistro";
            gridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn4.Width = 170;
            gridViewTextBoxColumn5.FieldName = "monto";
            gridViewTextBoxColumn5.HeaderText = "Monto";
            gridViewTextBoxColumn5.Name = "monto";
            gridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn5.Width = 141;
            gridViewTextBoxColumn6.FieldName = "nombre";
            gridViewTextBoxColumn6.HeaderText = "Nombre";
            gridViewTextBoxColumn6.Name = "nombre";
            gridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn6.Width = 168;
            gridViewTextBoxColumn7.FieldName = "dni";
            gridViewTextBoxColumn7.HeaderText = "Dni";
            gridViewTextBoxColumn7.Name = "dni";
            gridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn7.Width = 84;
            gridViewTextBoxColumn8.FieldName = "usuario";
            gridViewTextBoxColumn8.HeaderText = "Usuario";
            gridViewTextBoxColumn8.Name = "usuario";
            gridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn8.Width = 158;
            gridViewTextBoxColumn9.FieldName = "tipo";
            gridViewTextBoxColumn9.HeaderText = "Tipo";
            gridViewTextBoxColumn9.MinWidth = 10;
            gridViewTextBoxColumn9.Name = "tipo";
            gridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn9.Width = 101;
            gridViewTextBoxColumn10.FieldName = "tipoegreso";
            gridViewTextBoxColumn10.HeaderText = "Tipo Egreso";
            gridViewTextBoxColumn10.Name = "tipoegreso";
            gridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn10.Width = 104;
            this.dgvMovimientos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.dgvMovimientos.MasterTemplate.ShowRowHeaderColumn = false;
            this.dgvMovimientos.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgvMovimientos.Name = "dgvMovimientos";
            this.dgvMovimientos.ReadOnly = true;
            this.dgvMovimientos.Size = new System.Drawing.Size(1068, 335);
            this.dgvMovimientos.TabIndex = 0;
            this.dgvMovimientos.Text = "radGridView1";
            this.dgvMovimientos.ThemeName = "Material";
            // 
            // dtpFinicio
            // 
            this.dtpFinicio.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFinicio.Location = new System.Drawing.Point(102, 88);
            this.dtpFinicio.Name = "dtpFinicio";
            this.dtpFinicio.Size = new System.Drawing.Size(147, 36);
            this.dtpFinicio.TabIndex = 1;
            this.dtpFinicio.TabStop = false;
            this.dtpFinicio.Text = "4/07/2019";
            this.dtpFinicio.ThemeName = "Material";
            this.dtpFinicio.Value = new System.DateTime(2019, 7, 4, 9, 20, 23, 276);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(38, 88);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(58, 21);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "F. Inicio";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(290, 88);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(42, 21);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "F. Fin";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            // 
            // dtpFfin
            // 
            this.dtpFfin.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFfin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFfin.Location = new System.Drawing.Point(338, 90);
            this.dtpFfin.Name = "dtpFfin";
            this.dtpFfin.Size = new System.Drawing.Size(147, 36);
            this.dtpFfin.TabIndex = 2;
            this.dtpFfin.TabStop = false;
            this.dtpFfin.Text = "4/07/2019";
            this.dtpFfin.ThemeName = "Material";
            this.dtpFfin.Value = new System.DateTime(2019, 7, 4, 9, 20, 23, 276);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.radLabel3.Location = new System.Drawing.Point(433, 12);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(122, 23);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "CAJA AHORROS";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel3.ThemeName = "Material";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(807, 19);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(104, 21);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Total Ingresos:";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel4.ThemeName = "Material";
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(924, 19);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(52, 21);
            this.lblTotal.TabIndex = 4;
            this.lblTotal.Text = "0.0000";
            this.lblTotal.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTotal.ThemeName = "Material";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.dgvMovimientos);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 152);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(1072, 355);
            this.radGroupBox1.TabIndex = 7;
            this.radGroupBox1.ThemeName = "Material";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radGroupBox2.Controls.Add(this.radLabel5);
            this.radGroupBox2.Controls.Add(this.lblGastos);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.lblTotal);
            this.radGroupBox2.Controls.Add(this.dtpFinicio);
            this.radGroupBox2.Controls.Add(this.radLabel1);
            this.radGroupBox2.Controls.Add(this.btnReporte);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.dtpFfin);
            this.radGroupBox2.Controls.Add(this.btnRetiro);
            this.radGroupBox2.Controls.Add(this.btnBuscar);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox2.HeaderText = "";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(1072, 146);
            this.radGroupBox2.TabIndex = 8;
            this.radGroupBox2.ThemeName = "Material";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(807, 46);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(93, 21);
            this.radLabel5.TabIndex = 5;
            this.radLabel5.Text = "Total Gastos:";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel5.ThemeName = "Material";
            // 
            // lblGastos
            // 
            this.lblGastos.Location = new System.Drawing.Point(924, 46);
            this.lblGastos.Name = "lblGastos";
            this.lblGastos.Size = new System.Drawing.Size(52, 21);
            this.lblGastos.TabIndex = 6;
            this.lblGastos.Text = "0.0000";
            this.lblGastos.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGastos.ThemeName = "Material";
            // 
            // btnReporte
            // 
            this.btnReporte.Image = ((System.Drawing.Image)(resources.GetObject("btnReporte.Image")));
            this.btnReporte.Location = new System.Drawing.Point(848, 88);
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.Size = new System.Drawing.Size(121, 36);
            this.btnReporte.TabIndex = 5;
            this.btnReporte.Text = "Copiar";
            this.btnReporte.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReporte.ThemeName = "Material";
            this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
            // 
            // btnRetiro
            // 
            this.btnRetiro.Image = global::SIGEFA.Properties.Resources.retiro24;
            this.btnRetiro.Location = new System.Drawing.Point(679, 88);
            this.btnRetiro.Name = "btnRetiro";
            this.btnRetiro.Size = new System.Drawing.Size(150, 36);
            this.btnRetiro.TabIndex = 6;
            this.btnRetiro.Text = "Operaciones";
            this.btnRetiro.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRetiro.ThemeName = "Material";
            this.btnRetiro.Click += new System.EventHandler(this.btnRetiro_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Image = global::SIGEFA.Properties.Resources.buscar24;
            this.btnBuscar.Location = new System.Drawing.Point(539, 88);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(111, 36);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.ThemeName = "Material";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // frmCajaAhorros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1072, 507);
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupBox1);
            this.MaximizeBox = false;
            this.Name = "frmCajaAhorros";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Caja Ahorros";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmCajaAhorros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMovimientos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFinicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFfin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblGastos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRetiro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView dgvMovimientos;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFinicio;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFfin;
        private Telerik.WinControls.UI.RadButton btnBuscar;
        private Telerik.WinControls.UI.RadButton btnReporte;
        private Telerik.WinControls.UI.RadButton btnRetiro;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel lblTotal;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lblGastos;
    }
}
