﻿namespace SIGEFA.Formularios
{
    partial class frmErrorExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstErrores = new Telerik.WinControls.UI.RadListView();
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            ((System.ComponentModel.ISupportInitialize)(this.lstErrores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // lstErrores
            // 
            this.lstErrores.AllowEdit = false;
            this.lstErrores.AllowRemove = false;
            this.lstErrores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstErrores.Location = new System.Drawing.Point(0, 0);
            this.lstErrores.Name = "lstErrores";
            this.lstErrores.Size = new System.Drawing.Size(299, 353);
            this.lstErrores.TabIndex = 0;
            this.lstErrores.Text = "radListView1";
            this.lstErrores.ThemeName = "TelerikMetroBlue";
            // 
            // frmErrorExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 353);
            this.Controls.Add(this.lstErrores);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(307, 388);
            this.MinimumSize = new System.Drawing.Size(307, 388);
            this.Name = "frmErrorExcel";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(307, 388);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Errores Excel";
            this.ThemeName = "TelerikMetroBlue";
            this.Shown += new System.EventHandler(this.frmErrorExcel_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.lstErrores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadListView lstErrores;
        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
    }
}
