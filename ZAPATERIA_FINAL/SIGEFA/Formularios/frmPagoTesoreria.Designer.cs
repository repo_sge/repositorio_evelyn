﻿namespace SIGEFA.Formularios
{
    partial class frmPagoTesoreria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMonto = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.lblcompra = new Telerik.WinControls.UI.RadLabel();
            this.gbUsuario = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.txtDni = new Telerik.WinControls.UI.RadTextBox();
            this.txtNombre = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.lblsaldo = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtobs = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.cmbfpago = new Telerik.WinControls.UI.RadDropDownList();
            this.cmbTipoPago = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.cmborigen = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.btnGuardar = new Telerik.WinControls.UI.RadButton();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.lblnombre = new Telerik.WinControls.UI.RadLabel();
            this.lblnmonto = new Telerik.WinControls.UI.RadLabel();
            this.lblmonto = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtMonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblcompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbUsuario)).BeginInit();
            this.gbUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblsaldo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtobs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbfpago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmborigen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGuardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblnombre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblnmonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblmonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMonto
            // 
            this.txtMonto.Location = new System.Drawing.Point(191, 322);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(218, 36);
            this.txtMonto.TabIndex = 0;
            this.txtMonto.ThemeName = "Material";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(101, 328);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(53, 21);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Monto:";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel1.ThemeName = "Material";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(66, 36);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(90, 21);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Forma Pago:";
            this.radLabel2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel2.ThemeName = "Material";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(57, 79);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(99, 21);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Origen Dinero:";
            this.radLabel3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel3.ThemeName = "Material";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radGroupBox1.Controls.Add(this.lblmonto);
            this.radGroupBox1.Controls.Add(this.lblnmonto);
            this.radGroupBox1.Controls.Add(this.lblnombre);
            this.radGroupBox1.Controls.Add(this.lblcompra);
            this.radGroupBox1.Controls.Add(this.gbUsuario);
            this.radGroupBox1.Controls.Add(this.lblsaldo);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.txtobs);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.cmbfpago);
            this.radGroupBox1.Controls.Add(this.cmbTipoPago);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.cmborigen);
            this.radGroupBox1.Controls.Add(this.radButton1);
            this.radGroupBox1.Controls.Add(this.txtMonto);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.btnGuardar);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "Datos Deposito";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(490, 556);
            this.radGroupBox1.TabIndex = 4;
            this.radGroupBox1.Text = "Datos Deposito";
            this.radGroupBox1.ThemeName = "Material";
            // 
            // lblcompra
            // 
            this.lblcompra.Location = new System.Drawing.Point(107, 198);
            this.lblcompra.Name = "lblcompra";
            this.lblcompra.Size = new System.Drawing.Size(60, 21);
            this.lblcompra.TabIndex = 5;
            this.lblcompra.Text = "Factura:";
            this.lblcompra.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblcompra.ThemeName = "Material";
            this.lblcompra.Visible = false;
            // 
            // gbUsuario
            // 
            this.gbUsuario.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gbUsuario.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gbUsuario.Controls.Add(this.radLabel7);
            this.gbUsuario.Controls.Add(this.txtDni);
            this.gbUsuario.Controls.Add(this.txtNombre);
            this.gbUsuario.Controls.Add(this.radLabel8);
            this.gbUsuario.HeaderText = "";
            this.gbUsuario.Location = new System.Drawing.Point(79, 371);
            this.gbUsuario.Name = "gbUsuario";
            this.gbUsuario.Size = new System.Drawing.Size(350, 100);
            this.gbUsuario.TabIndex = 5;
            this.gbUsuario.ThemeName = "Material";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(25, 21);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(63, 21);
            this.radLabel7.TabIndex = 3;
            this.radLabel7.Text = "Nombre:";
            this.radLabel7.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel7.ThemeName = "Material";
            // 
            // txtDni
            // 
            this.txtDni.Location = new System.Drawing.Point(113, 57);
            this.txtDni.Name = "txtDni";
            this.txtDni.Size = new System.Drawing.Size(218, 36);
            this.txtDni.TabIndex = 2;
            this.txtDni.ThemeName = "Material";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(113, 15);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(218, 36);
            this.txtNombre.TabIndex = 2;
            this.txtNombre.ThemeName = "Material";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(25, 63);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(35, 21);
            this.radLabel8.TabIndex = 3;
            this.radLabel8.Text = "DNI:";
            this.radLabel8.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel8.ThemeName = "Material";
            // 
            // lblsaldo
            // 
            this.lblsaldo.Location = new System.Drawing.Point(261, 115);
            this.lblsaldo.Name = "lblsaldo";
            this.lblsaldo.Size = new System.Drawing.Size(36, 21);
            this.lblsaldo.TabIndex = 5;
            this.lblsaldo.Text = "0.00";
            this.lblsaldo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblsaldo.ThemeName = "Material";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(194, 115);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(48, 21);
            this.radLabel6.TabIndex = 4;
            this.radLabel6.Text = "Saldo:";
            this.radLabel6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel6.ThemeName = "Material";
            // 
            // txtobs
            // 
            this.txtobs.AutoSize = false;
            this.txtobs.Location = new System.Drawing.Point(191, 237);
            this.txtobs.Multiline = true;
            this.txtobs.Name = "txtobs";
            this.txtobs.Size = new System.Drawing.Size(218, 73);
            this.txtobs.TabIndex = 7;
            this.txtobs.ThemeName = "Material";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(49, 237);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(107, 21);
            this.radLabel5.TabIndex = 5;
            this.radLabel5.Text = "Observaciones:";
            this.radLabel5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel5.ThemeName = "Material";
            // 
            // cmbfpago
            // 
            this.cmbfpago.Location = new System.Drawing.Point(191, 29);
            this.cmbfpago.Name = "cmbfpago";
            this.cmbfpago.Size = new System.Drawing.Size(218, 37);
            this.cmbfpago.TabIndex = 6;
            this.cmbfpago.ThemeName = "Material";
            this.cmbfpago.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbfpago_SelectedIndexChanged);
            // 
            // cmbTipoPago
            // 
            this.cmbTipoPago.Location = new System.Drawing.Point(191, 142);
            this.cmbTipoPago.Name = "cmbTipoPago";
            this.cmbTipoPago.Size = new System.Drawing.Size(218, 37);
            this.cmbTipoPago.TabIndex = 5;
            this.cmbTipoPago.ThemeName = "Material";
            this.cmbTipoPago.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbTipoPago_SelectedIndexChanged);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(79, 149);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(77, 21);
            this.radLabel4.TabIndex = 4;
            this.radLabel4.Text = "Tipo Pago:";
            this.radLabel4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabel4.ThemeName = "Material";
            // 
            // cmborigen
            // 
            this.cmborigen.Enabled = false;
            this.cmborigen.Location = new System.Drawing.Point(191, 72);
            this.cmborigen.Name = "cmborigen";
            this.cmborigen.Size = new System.Drawing.Size(218, 37);
            this.cmborigen.TabIndex = 4;
            this.cmborigen.ThemeName = "Material";
            this.cmborigen.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmborigen_SelectedIndexChanged);
            // 
            // radButton1
            // 
            this.radButton1.Image = global::SIGEFA.Properties.Resources.cancel24;
            this.radButton1.Location = new System.Drawing.Point(262, 500);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(118, 36);
            this.radButton1.TabIndex = 3;
            this.radButton1.Text = "Cancelar";
            this.radButton1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton1.ThemeName = "Material";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::SIGEFA.Properties.Resources.save24;
            this.btnGuardar.Location = new System.Drawing.Point(92, 500);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(118, 36);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.ThemeName = "Material";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.Location = new System.Drawing.Point(231, 88);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(125, 37);
            this.radDropDownList1.TabIndex = 6;
            this.radDropDownList1.ThemeName = "Material";
            // 
            // lblnombre
            // 
            this.lblnombre.Location = new System.Drawing.Point(174, 198);
            this.lblnombre.Name = "lblnombre";
            this.lblnombre.Size = new System.Drawing.Size(64, 21);
            this.lblnombre.TabIndex = 6;
            this.lblnombre.Text = "------------";
            this.lblnombre.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblnombre.ThemeName = "Material";
            this.lblnombre.Visible = false;
            // 
            // lblnmonto
            // 
            this.lblnmonto.Location = new System.Drawing.Point(296, 198);
            this.lblnmonto.Name = "lblnmonto";
            this.lblnmonto.Size = new System.Drawing.Size(53, 21);
            this.lblnmonto.TabIndex = 7;
            this.lblnmonto.Text = "Monto:";
            this.lblnmonto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblnmonto.ThemeName = "Material";
            this.lblnmonto.Visible = false;
            // 
            // lblmonto
            // 
            this.lblmonto.Location = new System.Drawing.Point(355, 198);
            this.lblmonto.Name = "lblmonto";
            this.lblmonto.Size = new System.Drawing.Size(54, 21);
            this.lblmonto.TabIndex = 7;
            this.lblmonto.Text = "----------";
            this.lblmonto.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblmonto.ThemeName = "Material";
            this.lblmonto.Visible = false;
            // 
            // frmPagoTesoreria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 556);
            this.Controls.Add(this.radGroupBox1);
            this.MaximizeBox = false;
            this.Name = "frmPagoTesoreria";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Retiro";
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmRetiroNuevo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtMonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblcompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbUsuario)).EndInit();
            this.gbUsuario.ResumeLayout(false);
            this.gbUsuario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblsaldo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtobs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbfpago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTipoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmborigen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGuardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblnombre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblnmonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblmonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton btnGuardar;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTextBox txtMonto;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList cmborigen;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadDropDownList cmbTipoPago;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadDropDownList cmbfpago;
        private Telerik.WinControls.UI.RadTextBox txtobs;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel lblsaldo;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadGroupBox gbUsuario;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadTextBox txtDni;
        private Telerik.WinControls.UI.RadTextBox txtNombre;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel lblcompra;
        private Telerik.WinControls.UI.RadLabel lblnombre;
        private Telerik.WinControls.UI.RadLabel lblmonto;
        private Telerik.WinControls.UI.RadLabel lblnmonto;
    }
}
