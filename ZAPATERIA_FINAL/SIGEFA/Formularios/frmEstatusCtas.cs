﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmEstatusCtas : Telerik.WinControls.UI.RadForm
    {

        clsAdmCtaCte admcta = new clsAdmCtaCte();


        public frmEstatusCtas()
        {
            InitializeComponent();
        }

        private void frmEstatusCtas_Load(object sender, EventArgs e)
        {
            cargaCtas();
        }

        private void cargaCtas()
        {
            dgvCtas.ClearSelection();
            dgvCtas.DataSource = null;
            dgvCtas.DataSource = admcta.ListaCtasSaldo();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            cargaCtas();
        }
    }
}
