using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using SIGEFA.Entidades;
using SIGEFA.Administradores;

namespace SIGEFA.Formularios
{
    public partial class frmGuardaImagenProducto : DevComponents.DotNetBar.OfficeForm
    {
        public frmGuardaImagenProducto()
        {
            InitializeComponent();
        }

        private clsEntFotografia entfotografia = new clsEntFotografia();
        private clsAdmFotografia admfotografia = new clsAdmFotografia();
        public Int32 codProd = 0;
        public Int32 Proceso = 0;

        private void frmGuardaImagenProducto_Load(object sender, EventArgs e)
        {
            CargaFotografia();

        }

        private void CargaFotografia()
        {
            clsEntFotografia entfoto = new clsEntFotografia();
            entfoto = admfotografia.CargaFotografia(codProd);
            if (entfoto != null)
            {
                pbFoto.Image = entfoto.Fotografia;
                pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;
                btnGuardar.Visible = false;
            }
            //else {
            //    MessageBox.Show("No se pudo cargar la fotograf�a", "Gesti�n Relaci�n ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void btnAdd_Fotografia_Click(object sender, EventArgs e)
        {
            try
            {
                //flag_foto = true;
                OpenFileDialog BuscarImagen = new OpenFileDialog();
                BuscarImagen.Filter = "Archivos de Imagen|*.jpg";
                BuscarImagen.FileName = "";
                BuscarImagen.Title = "Titulo del Dialog";
                BuscarImagen.InitialDirectory = "D:\\";
                if (BuscarImagen.ShowDialog() == DialogResult.OK)
                {
                    String direccion = BuscarImagen.FileName;
                    pbFoto.ImageLocation = direccion;
                    pbFoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDel_Fotografia_Click(object sender, EventArgs e)
        {
            try
            {
                pbFoto.Image = null;
                if (admfotografia.EliminarFotografia(codProd, frmLogin.iCodAlmacen))
                {
                    MessageBox.Show("La Fotograf�a se elimin� correctamente", "Gestion Relaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnGuardar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void GuardaFotografia()
        {

            entfotografia.Codrelacion = codProd;
            if (pbFoto.Image != null)
            {
                entfotografia.Fotografia = pbFoto.Image;
            }
            else
            {
                entfotografia.Fotografia = null;
            }
            entfotografia.Codusuario = frmLogin.iCodUser;
            if (Proceso != 0)
            {
                if (Proceso == 1)
                {
                    if (admfotografia.AgregarFotografia(entfotografia))
                    {
                        MessageBox.Show("Los datos de Fotograf�a se guardaron correctamente", "Gestion Relaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnGuardar.Enabled = false;
                    }
                }
                else if (Proceso == 2)
                {
                    if (admfotografia.ActualizarFotografia(entfotografia))
                    {
                        MessageBox.Show("Los datos de Fotograf�a se guardaron correctamente", "Gestion Relaci�n", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardaFotografia();
        }
        
    }
}