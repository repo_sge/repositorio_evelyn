﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;


namespace SIGEFA.Interfaces
{
    interface IModelo
    {
        DataTable ListaModelo();
        DataTable ListraProveedorModelo(Int32 cod);
        Boolean VerificaModelo(clsModelo model);
        DataTable MuestraModeloOC();
        DataTable ListaModeloProveedorZ();

        Boolean Insert(clsModelo mod);

        Boolean Delete(Int32 mod);
        Boolean Update(clsModelo mod);
    }
}
