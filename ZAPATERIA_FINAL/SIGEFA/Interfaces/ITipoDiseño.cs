﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using System.Data;

namespace SIGEFA.Interfaces
{
    interface ITipoDiseño
    {
        Boolean Insert(clsTipoDiseño dise);

        Boolean Update(clsTipoDiseño dise);

        Boolean Delete(int Codmar);

        DataTable ListaTipoDiseño();
        
    }
}
