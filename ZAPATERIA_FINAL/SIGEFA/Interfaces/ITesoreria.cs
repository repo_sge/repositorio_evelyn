﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface ITesoreria
    {

        bool insertPagoTesoreria(int fpago, int origencaja,int origencta, decimal mpago, int tpago, string descripcion, int codalmacen, int coduser,int idcompra);

        DataTable listarTesoreria(DateTime finicio, DateTime ffin);

        decimal saldoCajaAhorros();
        decimal saldoCajaGeneral();
        decimal saldoCtaCte(int codcta);


    }
}
