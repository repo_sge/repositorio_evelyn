﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ICorte
    {
        DataTable ListaCorte();

        Boolean deleteCorte(Int32 codCorte);
        Boolean insertCorte(clsCorte corte);
        Boolean updateCorte(clsCorte corte);

    }
}
