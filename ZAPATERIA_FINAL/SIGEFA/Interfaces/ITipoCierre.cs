﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ITipoCierre
    {
        Boolean Insert(clsTipoCierre NuevaMarca);
        Boolean Update(clsTipoCierre Marca);
        Boolean Delete(Int32 Codigo);

        clsTipoCierre CargaTipoCierre(Int32 Codigo);
        DataTable ListaTipoCierre();
    }
}
