﻿using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface ITipoImpuesto
    {
        List<clsTipoImpuesto> listar_tipoimpuesto_xestado();
        clsTipoImpuesto listar_tipoimpuesto_xid(int id);
    }
}
