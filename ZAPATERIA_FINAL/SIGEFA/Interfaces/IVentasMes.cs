﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace SIGEFA.Interfaces
{
    interface IVentasMes
    {
        Int32 BuscarMes(Int32 Year);
        Boolean GuardaVentas(Int32 mespasado, Int32 anio, Int32 codalmacen, Int32 mesactual, Int32 dia);
        DataTable MostrarVentasMes(Int32 mes, Int32 anio, Int32 codalmacen);
    }
}
