﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ITemporada
    {

        DataTable ListaTemporadas();
        Boolean insertTemporada(clsTemporada temp);
    }
}
