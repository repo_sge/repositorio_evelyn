﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface IDetCodProducto
    {
        Boolean Insert(clsDetCodProducto DetalleCodProducto);
        Boolean Update(clsDetCodProducto DetalleCodProducto);
        Boolean Delete(clsDetCodProducto DetalleCodProducto);
    }
}
