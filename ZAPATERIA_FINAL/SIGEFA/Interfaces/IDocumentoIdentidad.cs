﻿using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface IDocumentoIdentidad
    {

        DataTable ListaDocumentoIdentidad(Int32 codigoTipoDocumento);
        clsDocumentoIdentidad MuestraDocumentoIdentidad(Int32 codigoDocumentoIdentidad);
        clsDocumentoIdentidad ObtenerDocumentoIdentidadDeVenta(Int32 codigoFacturaVenta);
    }
}
