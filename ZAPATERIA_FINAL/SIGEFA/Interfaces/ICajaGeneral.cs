﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface ICajaGeneral
    {

        DataTable listaCajaGeneral(DateTime finicio, DateTime ffin);

        Boolean transferirCGeneral(int codcgeneral,int codcta,string noperacion,int coduser);

    }
}
