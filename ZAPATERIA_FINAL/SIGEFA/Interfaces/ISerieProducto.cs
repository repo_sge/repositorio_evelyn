﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface ISerieProducto
    {

        DataTable ListaSerieProducto();
        clsSerieProducto ListaSerieProductoCod(Int32 codSerie);
        clsSerieProducto ListaTallaxSerie(Int32 codser);

        Boolean Insert(clsSerieProducto ser);

        Boolean Update(clsSerieProducto ser);

        Boolean delete(clsSerieProducto ser);
    }
}
