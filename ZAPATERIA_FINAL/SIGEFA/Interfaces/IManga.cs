﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using System.Data;

namespace SIGEFA.Interfaces
{
    interface IManga
    {
        Boolean Insert(clsManga mar);

        Boolean Update(clsManga mar);

        Boolean Delete(int Codmar);

        DataTable ListaManga();
    }
}
