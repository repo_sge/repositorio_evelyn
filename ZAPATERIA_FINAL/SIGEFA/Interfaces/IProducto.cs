﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using DevComponents.DotNetBar.Controls;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface IProducto
    {
        int Insert(clsProducto ProductoNuevo);

        Boolean InsertMasivo(DataTable Productos);
        Boolean UpdateMasivo(DataTable Productos);

        DataTable listaCodigobarras();

        int Update(clsProducto Producto);
        Boolean Delete(Int32 Codigo);
        Boolean InsertWhitDetalleCod(clsProducto ProductoNuevo, String[] detallecod);
        Boolean UpdateDetCodProducto(clsProducto pro, String[] DetcodProducto);
        Boolean DeleteDetCodProducto(clsProducto pro);

        Boolean InsertProductoAlmacen(clsProducto ProductoAlmacenNuevo);
        Boolean UpdateProductoAlmacen(clsProducto ProductoAlmacen);
        Boolean DeleteProductoAlmacen(Int32 CodProductoAlmacen);

        Boolean InsertCaracteristica(clsCaracteristicaProducto CaracNuevo);
        Boolean DeleteCaracteristica(Int32 Codigo);
        DataTable ListaCaracteristicas(Int32 CodProducto);

        Boolean InsertNota(clsNotaProducto NotaProducto);
        Boolean DeleteNota(Int32 Codigo);
        DataTable ListaNotas(Int32 CodProducto);

        Boolean InsertUnidad(clsUnidadEquivalente NuevaUnidad);
        Boolean UpdateUnidad(clsUnidadEquivalente Unidad);
        Boolean DeleteUnidad(Int32 Codigo);
        clsUnidadEquivalente CargaUnidadEquivalente(Int32 Coduni, Int32 Codpro);
        DataTable ListaUnidadesEquivalentes(Int32 CodigoProducto, Int32 codAlmacen);
        DataTable CargaUnidadesEquivalentes(Int32 CodigoProducto);

        clsProducto CargaProducto(Int32 CodProducto, Int32 CodAlmacen);
        clsProducto CargaProductoDetalle(Int32 CodPro, Int32 CodAlm, Int32 Caso, Int32 Lista);
        clsProducto CargaProductoDetalle1(Int32 CodPro, Int32 CodAlm, Int32 Caso, Int32 Lista);
        clsProducto CargaDatosProductoOrden(Int32 CodPro, Int32 CodAlm, Int32 codusu, Decimal cant);
        clsProducto CargaProductoDetalleR(String Referencia, Int32 CodAlm, Int32 Caso, Int32 Lista);
        DataTable ListaProductos(Int32 nivel, Int32 codigo, Int32 CodAlmacen);
        DataTable CatalogoProductos();
        DataTable ListaProductosReporte(Int32 CodAlmacen, Int32 Tipo, Int32 Inicio);
        DataTable RelacionProductosIngreso(Int32 Tipo, Int32 codalma);
        DataTable RelacionIngresoPorProveedor(Int32 Tipo, Int32 codalma, Int32 codproveedor);
        DataTable RelacionProductosSalida(Int32 Tipo, Int32 codalmacen, Int32 codlista);
        DataTable BuscaProductos(Int32 Criterio, String Filtro);
        DataTable ArbolProductos();
        DataTable StockProductoAlmacenes(Int32 codEmpre, Int32 codPro);
        DataTable MuestraProductosProveedor(Int32 codProducto, Int32 codAlmacen);
        clsProducto MuestraProductosTransferencia(Int32 codProducto, Int32 codAlmacen);
        clsProducto MuestraProductosTransferencia_nuevo(Int32 codProducto, Int32 codAlmacen);
        DataTable RelacionProductosCotizacion(Int32 Tipo, Int32 codAlmacen, Int32 codlista);
        Decimal CargaPrecioProducto(Int32 CodProducto, Int32 CodAlmacen,Int32 codmon);
        DataTable MuestraStockAlmacenes();

        DataTable BuscarProducto(Int32 codProducto);
        DataTable RelacionProductos(Int32 codalma);

        DataTable ListaProductosz(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel);
        clsProducto CargaProductoDetalleZ(Int32 codProb, Int32 CodAlm);
        DataTable ConsultaProductoRR(String referencia, Int32 cod);
        DataTable ArbolProductosZ();
        clsProducto BuscaProductoAlmacenZ(Int32 codalma, String codebar);

        clsProducto BuscaPrecioCompraProductoAnterior(Int32 codpro);
        DataTable ListaArticulosRequerimiento(Int32 cod);
        DataTable TallasxProducto(Int32 codpro);
        DataTable TraeDatosProducto(Int32 codpro);

        DataTable TallasxProducto2(Int32 codseriep, Int32 codtipota, Int32 codpro, Int32 codalma);

        DataTable VerificaProductoIngresado(String refe);

        DataTable BuscaProductoZ2(Int32 codalma, String codigobar);

        clsProducto BuscaProductoZ3(Int32 codalma, String codebar);

        clsNotaIngreso BuscarProductoBar(Int32 codalma, String codebar, Int32 tipo);
        DataTable BuscarProductoDetalleBar(String Codebar, Int32 tipo, Int32 codalma);

        DataTable BuscarProductoDetalleSalBar(String codebar);

        Int32 VerificaCodigoProducto(Int32 codcarac, Int32 cotalla, Int32 coduser, Decimal precio);

        //
        clsProducto BuscaProductoDetalleSerie(Int32 codAlmacen, String codeBar);
        DataTable CargarRangoTallas(Decimal talla1, Decimal talla2, Int32 codTipoTalla, Int32 codLinea);

        //Nuevos Cambios
        DataTable ListaProductosz3(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp);
        DataTable ListaProductosz4(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors);
        DataTable ListaProductosz5(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac);
        DataTable ListaProductosz6(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon);
        DataTable ListaProductosz7(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon, Int32 codtipot);
        Boolean InsertDatosArticulo(clsProducto prod);
        DataTable ListaProductosZ2(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel);
        DataTable MuestraProductoDetalleRR2(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon, Int32 codtipotal);
        clsProducto MuestraProductoParaCantidad(Int32 codPro);
        clsProducto MuestraDatosdeProductoMovimiento(Int32 codpro);

        DataTable CargaUnidadCompra(Int32 codPro, Int32 codAlm, Int32 compra_venta);

        DataTable CargaUnidaEquivalente(Int32 codPro, Int32 codAlm);

        DataTable ListaUnidadesEquivalentesCompra(Int32 CodigoProducto, Int32 codAlmacen);
        DataTable ListaUnidadesEquivalentesVenta(Int32 CodigoProducto, Int32 codAlmacen);
        DataTable ListaUnidadesEquivalentesVenta1(Int32 CodigoProducto, Int32 codAlmacen);
        Int32 UnidadBase(Int32 codPro, Int32 codalma);
        clsProducto CargaReferenciaProducto(Int32 codProducto);
        clsProducto PrecioPromedio(Int32 codProducto, Int32 codalm);
        clsUnidadEquivalente PrecioVenta(Int32 coduni, Int32 codalmacen);
        clsUnidadEquivalente Factor(Int32 codProducto, Int32 codUnidadMedida, Int32 codUnidaEqui);
        Decimal FactorProducto(Int32 codPro, Int32 undBase, Int32 undEqui, Int32 tipo);

        //update totalizado_almacenes
        DataTable totalizadoAlmacenes();

    }

    #region Interfaz de Fotografia
    interface IFotografia
    {
        Boolean AgregarFotografia(clsEntFotografia entfoto);
        Boolean ActualizarFotografia(clsEntFotografia entfoto);
        Boolean EliminarFotografia(Int32 codfotografia, Int32 codalmacen);
        clsEntFotografia CargaFotografia(Int32 codfotografia);
    }
    #endregion
}
