﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface ICajaAhorros
    {

        DataTable listaCajaAhorros(DateTime finicio, DateTime ffin);
        Boolean insertPagoCAhorros(int idalma, decimal monto, int idusuario, string nombre, string dni, int tipoegreso, string observa);
    }
}
