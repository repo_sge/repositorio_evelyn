﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.InterMySql
{
    class MysqlCajaGeneral : ICajaGeneral
    {

        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public DataTable listaCajaGeneral(DateTime finicio, DateTime ffin)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaCajaGeneral", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("finicio", finicio);
                cmd.Parameters.AddWithValue("ffin", ffin);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool transferirCGeneral(int codcgeneral, int codcta, string noperacion, int coduser)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("transferirCGeneral", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("cod", codcgeneral);
                oParam = cmd.Parameters.AddWithValue("codcta", codcta);
                oParam = cmd.Parameters.AddWithValue("noperacion", noperacion);
                oParam = cmd.Parameters.AddWithValue("coduser", coduser);
                int x = cmd.ExecuteNonQuery();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
