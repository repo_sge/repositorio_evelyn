﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.InterMySql
{
    class MysqlRepositorio : IRepositorio
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction tra = null;
        private DataTable tabla = null;

        public int actualizar_repositorio(clsRepositorio repositorio)
        {
            int filas_afectadas = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("actualizar_repositorio", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idrepositorio", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_estadosunat", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_mensajesunat", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_docxml", MySqlDbType.LongBlob));
                cmd.Parameters.Add(new MySqlParameter("@_doczip", MySqlDbType.LongBlob));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));

                cmd.Parameters[0].Value = repositorio.IdRepositorio;
                cmd.Parameters[1].Value = repositorio.Estadosunat;
                cmd.Parameters[2].Value = repositorio.Mensajesunat;
                cmd.Parameters[3].Value = repositorio.Archivo.Xml;
                cmd.Parameters[4].Value = repositorio.Archivo.Zip;
                cmd.Parameters[5].Value = repositorio.ICodUser;

                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        filas_afectadas = Convert.ToInt32(dr["_filas_afectadas"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return filas_afectadas;
            }
            catch (MySqlException ex)
            {
                filas_afectadas = -1;
                tra.Rollback();
                return filas_afectadas;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsRepositorio listar_archivo_xrepositorio(clsRepositorio repositorio)
        {
            clsRepositorio repo = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_archivo_xrepositorio", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_repositorioid", MySqlDbType.Int32));
                cmd.Parameters[0].Value = repositorio.IdRepositorio;

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {

                    while (dr.Read())
                    {
                        repo = new clsRepositorio()
                        {

                            Archivo = new clsArchivo()
                            {

                                Xml = (byte[])dr["docxml"],
                                Pdf = (byte[])dr["docpdf"]
                            },

                        };
                    }
                    dr.Close();
                }
                return repo;
            }
            catch (MySqlException ex)
            {
                return repo;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsRepositorio listar_repositorio_xtscfm(clsRepositorio repositorio)
        {
            clsRepositorio repo = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_repositorio_xtscfm", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_tipocomprobante", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_serie", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_correlativo", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_fechaemision", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_monto", MySqlDbType.Decimal));

                cmd.Parameters[0].Value = repositorio.IdTipodocumento;
                cmd.Parameters[1].Value = repositorio.Serie;
                cmd.Parameters[2].Value = repositorio.Correlativo;
                cmd.Parameters[3].Value = repositorio.Fechaemision;
                cmd.Parameters[4].Value = repositorio.Monto;

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        repo = repositorio;
                        repo.IdRepositorio = (int)dr["repositorioid"];
                    }
                    dr.Close();
                }
                else
                {

                    repo = repositorio;
                    repo.IdRepositorio = 0;
                }
                return repo;
            }
            catch (MySqlException ex)
            {
                return repo;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listar_repositorio_xtsfe(clsSerie numeracion, DateTime inicio, DateTime fin, int idestado)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_repositorio_xtsfe", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idtipocomprobante", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_serie", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_fechaini", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_estadosunat", MySqlDbType.Int32));

                cmd.Parameters[0].Value = numeracion.Tipodocumento.CodTipoDocumento;
                cmd.Parameters[1].Value = numeracion.Serie;
                cmd.Parameters[2].Value = inicio;
                cmd.Parameters[3].Value = fin;
                cmd.Parameters[4].Value = idestado;//numeracion.Estado;

                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public DataTable listar_repositorio_xtsfe_xcomprobante(clsSerie numeracion, clsFacturaVenta comprobante, String inicio, String fin)
        {
            tabla = new DataTable();

            using (con.conectarBD())
            {
                cmd = new MySqlCommand("listar_repositorio_xtsfe_xcomprobante", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_tipocomprobante", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_serie", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_fechaini", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_fechafin", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_comprobante", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_estadosunat", MySqlDbType.Int32));

                cmd.Parameters[0].Value = numeracion.Tipodocumento;
                cmd.Parameters[1].Value = numeracion.Serie;
                cmd.Parameters[2].Value = inicio;
                cmd.Parameters[3].Value = fin;
                cmd.Parameters[4].Value = comprobante.NumDoc;
                cmd.Parameters[5].Value = numeracion.Estado;

                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
            }

            return tabla;
        }

        public clsRepositorio listar_xidrepositorio(int idrepositorio)
        {
            clsRepositorio repo = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("listar_xidrepositorio", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("@_idrepositorio", MySqlDbType.Int32));
                cmd.Parameters[0].Value = idrepositorio;

                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {

                    while (dr.Read())
                    {
                        repo = new clsRepositorio()
                        {
                            IdRepositorio = dr.GetInt32(0),
                            CodFacturaVenta = dr.GetInt32(1),
                            IdTipodocumento = dr.GetInt32(2),
                            Estadosunat = dr.GetInt32(3),
                            Mensajesunat = dr.GetString(4),
                            Nombredocxml = dr.GetString(5),
                            Rutaxml = dr.GetString(6),
                            Archivo = new clsArchivo()
                            {

                                Xml = (byte[])dr[7],
                                Zip = dr[8] == DBNull.Value ? null : (byte[])dr[8]
                            },

                            ICodUser = dr.GetInt32(9),
                            CodSucursal = dr.GetInt32(10),
                            CodAlmacen = dr.GetInt32(11),
                            CodEmpresa=dr.GetInt32(12)
                        };
                    }
                    dr.Close();
                }
                return repo;
            }
            catch (MySqlException ex)
            {
                return repo;
                throw;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int registrar_repositorio(clsRepositorio repositorio)
        {
            int id = -1;

            try
            {
                con.conectarBD();
                tra = con.conector.BeginTransaction();
                cmd = new MySqlCommand("registrar_repositorio", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = tra;
                cmd.Parameters.Add(new MySqlParameter("@_idfacturaventa", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idtipocomprobante", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_comprobante", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_serie", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_correlativo", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_fechaemision", MySqlDbType.Date));
                cmd.Parameters.Add(new MySqlParameter("@_monto", MySqlDbType.Decimal));
                cmd.Parameters.Add(new MySqlParameter("@_estadosunat", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_mensajesunat", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_nombredocxml", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_rutaxml", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_nombredocpdf", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_rutapdf", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_pcorigen", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_usuariopc", MySqlDbType.VarChar));
                cmd.Parameters.Add(new MySqlParameter("@_docxml", MySqlDbType.LongBlob));
                cmd.Parameters.Add(new MySqlParameter("@_docpdf", MySqlDbType.LongBlob));
                cmd.Parameters.Add(new MySqlParameter("@_doczip", MySqlDbType.LongBlob));
                cmd.Parameters.Add(new MySqlParameter("@_idusuario", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idSucursal", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idAlmacen", MySqlDbType.Int32));
                cmd.Parameters.Add(new MySqlParameter("@_idEmpresa", MySqlDbType.Int32));


                cmd.Parameters[0].Value = repositorio.CodFacturaVenta;
                cmd.Parameters[1].Value = repositorio.IdTipodocumento;
                cmd.Parameters[2].Value = repositorio.Nombredoc;
                cmd.Parameters[3].Value = repositorio.Serie;
                cmd.Parameters[4].Value = repositorio.Correlativo;
                cmd.Parameters[5].Value = repositorio.Fechaemision;
                cmd.Parameters[6].Value = repositorio.Monto;
                cmd.Parameters[7].Value = repositorio.Estadosunat;
                cmd.Parameters[8].Value = repositorio.Mensajesunat;
                cmd.Parameters[9].Value = repositorio.Nombredocxml;
                cmd.Parameters[10].Value = repositorio.Rutaxml;
                cmd.Parameters[11].Value = repositorio.Nombredocpdf;
                cmd.Parameters[12].Value = repositorio.Rutapdf;
                cmd.Parameters[15].Value = repositorio.Archivo.Xml;
                cmd.Parameters[16].Value = repositorio.Archivo.Pdf;
                cmd.Parameters[17].Value = repositorio.Archivo.Zip;
                cmd.Parameters[18].Value = repositorio.ICodUser;
                cmd.Parameters[19].Value = repositorio.CodSucursal;
                cmd.Parameters[20].Value = repositorio.CodAlmacen;
                cmd.Parameters[21].Value = repositorio.CodEmpresa;


                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["_id"]);
                    }
                    dr.Close();
                }
                tra.Commit(); tra.Dispose();
                return id;
            }
            catch (MySqlException ex)
            {
                id = -1;
                tra.Rollback();
                return id;
                throw;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int validarEnvio(int codfacturav)
        {
            int id = 0;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("validaEnvioSunat", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("codventa", MySqlDbType.Int32));
                cmd.Parameters[0].Value = codfacturav;

                id = Convert.ToInt32(cmd.ExecuteScalar());

                return id;
            }
            catch (MySqlException ex)
            {
                return id;
                throw;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
