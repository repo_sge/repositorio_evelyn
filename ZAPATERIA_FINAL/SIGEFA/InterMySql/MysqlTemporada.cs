﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.InterMySql
{
    class MysqlTemporada:ITemporada
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataAdapter adap = null;
        MySqlDataReader dr = null;
        DataTable tabla = null;

        public Boolean insertTemporada(clsTemporada temp)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("insertaTemporada", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_descripcion", temp.Descripcion);
                oParam = cmd.Parameters.AddWithValue("_coduser", temp.CodUser);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                temp.CodTemporada = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaTemporadas()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaTemporadas", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
