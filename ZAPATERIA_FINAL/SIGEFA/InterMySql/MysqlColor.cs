﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using SIGEFA.Entidades;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;

namespace SIGEFA.InterMySql
{
    class MysqlColor:IColor
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        #region Implementacion IColorPrimario

        public DataTable ListaColorPrimario()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaColorPrimario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean VerificaColorPrimario(clsColorPrimario colp)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("VerificaColorPrimario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("des", colp.Descripcion);
                oParam = cmd.Parameters.AddWithValue("newid2", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();
                colp.CodColorPrimario = Convert.ToInt32(cmd.Parameters["newid2"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        #endregion

        #region Implementacion IColorSecundario

        public DataTable ListaColorSecundario()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaColorSecundario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean VerificaColorSecundario(clsColorSecundario cols)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("VerificaColorSecundario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("des", cols.Descripcion);
                oParam = cmd.Parameters.AddWithValue("newid2", 0);
                oParam.Direction = ParameterDirection.Output;

                int x = cmd.ExecuteNonQuery();

                cols.CodColorSecundario = Convert.ToInt32(cmd.Parameters["newid2"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable ListaProductoZMarcaColor(Int32 codline, Int32 codmar, Int32 codColorS)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductoZMarcaColorS", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codcolors", codColorS);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool deletePrimario(int codcolor)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("eliminaColorPrimario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_codcolorprimario", codcolor);
                int x = cmd.ExecuteNonQuery();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool insertPrimario(clsColorPrimario ser)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("guardaColorPrimario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_descripcion", ser.Descripcion);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;

                int x = cmd.ExecuteNonQuery();

                ser.CodColorPrimario = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool updatePrimario(clsColorPrimario color)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("actualizarColorPrimario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_codcolorprimario", color.CodColorPrimario);
                oParam = cmd.Parameters.AddWithValue("_descripcion", color.Descripcion);
                int x = cmd.ExecuteNonQuery();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool deleteSecundario(int codcolor)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("eliminaColorSecundario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_codcolorsecundario", codcolor);
                int x = cmd.ExecuteNonQuery();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool insertSecundario(clsColorSecundario ser)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("guardaColorSecundario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_descripcion", ser.Descripcion);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;

                int x = cmd.ExecuteNonQuery();

                ser.CodColorSecundario = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool updateSecundario(clsColorSecundario ser)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("actualizarColorSecundario", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_codcolorsecundario", ser.CodColorSecundario);
                oParam = cmd.Parameters.AddWithValue("_descripcion", ser.Descripcion);
                int x = cmd.ExecuteNonQuery();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        #endregion


    }
}
