﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using SIGEFA.Entidades;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using SIGEFA.Formularios;
using SYS_SGE.Controles.Librerias;
using System.Data.SqlClient;


namespace SIGEFA.InterMySql
{
    class MysqlProducto:IProducto
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        #region Implementacion IProducto

        public int Insert(clsProducto prod)
        {
            int rows = 0;
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codusu", prod.CodUsuario);
                //oParam = cmd.Parameters.AddWithValue("vtalla", prod.valorTalla);
                //oParam = cmd.Parameters.AddWithValue("codtalla", prod.Codtalla);
                if (prod.Codtalla != null) { oParam = cmd.Parameters.AddWithValue("codtalla", prod.Codtalla); } else { oParam = cmd.Parameters.AddWithValue("codtalla", 0); }
                if (prod.valorTalla != null) { oParam = cmd.Parameters.AddWithValue("vtalla", prod.valorTalla); } else { oParam = cmd.Parameters.AddWithValue("vtalla", ""); }
                //if (prod.CodGrupo != 0) { oParam = cmd.Parameters.AddWithValue("codgru", prod.CodGrupo); } else { oParam = cmd.Parameters.AddWithValue("codgru", null); }
                if (prod.CodLinea != 0) { oParam = cmd.Parameters.AddWithValue("codlin", prod.CodLinea); } else { oParam = cmd.Parameters.AddWithValue("codlin", null); }
                oParam = cmd.Parameters.AddWithValue("codfam", prod.CodFamilia);
                oParam = cmd.Parameters.AddWithValue("coduni", prod.CodUnidadMedida);
                oParam = cmd.Parameters.AddWithValue("codtip", prod.CodTipoArticulo);
                if (prod.CodMarca != 0) { oParam = cmd.Parameters.AddWithValue("codmar", prod.CodMarca); } else { oParam = cmd.Parameters.AddWithValue("codmar", null); }
                oParam = cmd.Parameters.AddWithValue("control", prod.CodControlStock);
                oParam = cmd.Parameters.AddWithValue("referencia", "");
                oParam = cmd.Parameters.AddWithValue("descripcion", prod.Descripcion);
                oParam = cmd.Parameters.AddWithValue("igv", prod.Igv);
                oParam = cmd.Parameters.AddWithValue("precioconigv", prod.ConIgv);
                //oParam = cmd.Parameters.AddWithValue("detraccion", prod.Detraccion);
                oParam = cmd.Parameters.AddWithValue("estado", prod.Estado);
                //oParam = cmd.Parameters.AddWithValue("comision", prod.Comision);
                oParam = cmd.Parameters.AddWithValue("precioca", prod.PrecioCatalogo);
                //oParam = cmd.Parameters.AddWithValue("maxPorcDesc", prod.MaxPorcDesc);
                oParam = cmd.Parameters.AddWithValue("codmo", prod.codModelo);
                //oParam = cmd.Parameters.AddWithValue("codfon", prod.codFondo);
                oParam = cmd.Parameters.AddWithValue("coddeta", prod.codDetalle);
                oParam = cmd.Parameters.AddWithValue("codcor", prod.codCorte);
                oParam = cmd.Parameters.AddWithValue("codta", prod.codTaco);
                oParam = cmd.Parameters.AddWithValue("codcolorp", prod.codColorPrimario);
                oParam = cmd.Parameters.AddWithValue("codcolors", prod.codColorSecundario);
                //oParam = cmd.Parameters.AddWithValue("codstipop", prod.codSubTipoArticulo);
                oParam = cmd.Parameters.AddWithValue("codtipota", prod.TipoTalla);
                oParam = cmd.Parameters.AddWithValue("codgrup", prod.CodGrupo);
                oParam = cmd.Parameters.AddWithValue("codserp", prod.codSerieProducto);
                oParam = cmd.Parameters.AddWithValue("codcomprapro", prod.CodCompraProducto);
                oParam = cmd.Parameters.AddWithValue("CodTipoCierre", prod.CodTipoCierre);
                oParam = cmd.Parameters.AddWithValue("codtemp",prod.CodTemporada);
                oParam = cmd.Parameters.AddWithValue("codcuello", prod.CodCuello);
                oParam = cmd.Parameters.AddWithValue("codmanga", prod.CodManga);
                oParam = cmd.Parameters.AddWithValue("coddiseño", prod.CodDiseño);
                oParam = cmd.Parameters.AddWithValue("codtipodiseño", prod.CodTipoDiseño);
                oParam = cmd.Parameters.AddWithValue("anio", prod.Annio);
                //byte[] areglobyte = ImagenAbyte(prod.FotoZapato);
                //oParam = cmd.Parameters.AddWithValue("fotozapato", areglobyte);

                oParam = cmd.Parameters.AddWithValue("newid", 0);
                
                oParam.Direction = ParameterDirection.Output;
                rows = cmd.ExecuteNonQuery();

                prod.CodProducto = Convert.ToInt32(cmd.Parameters["newid"].Value);
                
                return rows;

            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean InsertMasivo(DataTable prods)
        {
            try
            {
                con.conectarBD();
                clsProducto prod = new clsProducto();
                cmd = new MySqlCommand("GuardaProductoMasivo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.UpdatedRowSource = UpdateRowSource.None;

                cmd.Parameters.Add("codusu", MySqlDbType.Int32).SourceColumn = "codusu";
                cmd.Parameters.Add("codtalla", MySqlDbType.Int32).SourceColumn = "codtalla";
                cmd.Parameters.Add("vtalla", MySqlDbType.String).SourceColumn = "vtalla";
                cmd.Parameters.Add("codlin", MySqlDbType.Int32).SourceColumn = "codlin";
                cmd.Parameters.Add("codfam", MySqlDbType.Int32).SourceColumn = "codfam";
                cmd.Parameters.Add("coduni", MySqlDbType.Int32).SourceColumn = "coduni";
                cmd.Parameters.Add("codtip", MySqlDbType.Int32).SourceColumn = "codtip";
                cmd.Parameters.Add("codmar", MySqlDbType.Int32).SourceColumn = "codmar";
                cmd.Parameters.Add("control", MySqlDbType.Bit).SourceColumn = "control";
                cmd.Parameters.Add("referencia", MySqlDbType.String).SourceColumn = "referencia";
                cmd.Parameters.Add("descripcion", MySqlDbType.String).SourceColumn = "descripcion";
                cmd.Parameters.Add("igv", MySqlDbType.Bit).SourceColumn = "igv";
                cmd.Parameters.Add("precioconigv", MySqlDbType.Bit).SourceColumn = "precioconigv";
                cmd.Parameters.Add("estado", MySqlDbType.Bit).SourceColumn = "estado";
                cmd.Parameters.Add("precioca", MySqlDbType.Double).SourceColumn = "precioca";
                cmd.Parameters.Add("codmo", MySqlDbType.Int32).SourceColumn = "codmo";
                cmd.Parameters.Add("coddeta", MySqlDbType.Int32).SourceColumn = "coddeta";
                cmd.Parameters.Add("codcor", MySqlDbType.Int32).SourceColumn = "codcor";
                cmd.Parameters.Add("codta", MySqlDbType.Int32).SourceColumn = "codta";
                cmd.Parameters.Add("codcolorp", MySqlDbType.Int32).SourceColumn = "codcolorp";
                cmd.Parameters.Add("codcolors", MySqlDbType.Int32).SourceColumn = "codcolors";
                cmd.Parameters.Add("codtipota", MySqlDbType.Int32).SourceColumn = "codtipota";
                cmd.Parameters.Add("codgrup", MySqlDbType.Int32).SourceColumn = "codgrup";
                cmd.Parameters.Add("codserp", MySqlDbType.Int32).SourceColumn = "codserp";
                cmd.Parameters.Add("codcomprapro", MySqlDbType.String).SourceColumn = "codcomprapro";
                cmd.Parameters.Add("CodTipoCierre", MySqlDbType.Int32).SourceColumn = "CodTipoCierre";
                cmd.Parameters.Add("codtemp", MySqlDbType.Int32).SourceColumn = "codtemp";
                cmd.Parameters.Add("codcuello", MySqlDbType.Int32).SourceColumn = "codcuello";
                cmd.Parameters.Add("codmanga", MySqlDbType.Int32).SourceColumn = "codmanga";
                cmd.Parameters.Add("coddiseño", MySqlDbType.Int32).SourceColumn = "coddiseño";
                cmd.Parameters.Add("codtipodiseño", MySqlDbType.Int32).SourceColumn = "codtipodiseño";
                cmd.Parameters.Add("anio", MySqlDbType.Int32).SourceColumn = "anio";
                cmd.Parameters.Add("preciocompra", MySqlDbType.Double).SourceColumn = "preciocompra";
                cmd.Parameters.Add("codalmacen", MySqlDbType.Int32).SourceColumn = "codalmacen";
                cmd.Parameters.Add("preciodir", MySqlDbType.Double).SourceColumn = "preciodir";
                cmd.Parameters.Add("preciopro", MySqlDbType.Double).SourceColumn = "preciopro";

                MySqlDataAdapter da = new MySqlDataAdapter();
                cmd.CommandTimeout = 250000000;
                da.InsertCommand = cmd;
                da.UpdateBatchSize = 250;
                int records = da.Update(prods);

                return true;

            }
            catch (MySqlException ex)
            {
                return false;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean UpdateMasivo(DataTable prods)
        {
            try
            {
                con.conectarBD();
                clsProducto prod = new clsProducto();
                cmd = new MySqlCommand("actualizaProductoMasivo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("codpro", MySqlDbType.Int32).SourceColumn = "codpro";
                cmd.Parameters.Add("codgru", MySqlDbType.Int32).SourceColumn = "codgru";
                cmd.Parameters.Add("codlin", MySqlDbType.Int32).SourceColumn = "codlin";
                cmd.Parameters.Add("codfam", MySqlDbType.Int32).SourceColumn = "codfam";
                cmd.Parameters.Add("codmar", MySqlDbType.Int32).SourceColumn = "codmar";
                cmd.Parameters.Add("coduni", MySqlDbType.Int32).SourceColumn = "coduni";
                cmd.Parameters.Add("codtip", MySqlDbType.Int32).SourceColumn = "codtip";
                cmd.Parameters.Add("control", MySqlDbType.Bit).SourceColumn = "control";
                cmd.Parameters.Add("referencia", MySqlDbType.String).SourceColumn = "referencia";
                cmd.Parameters.Add("descripcion", MySqlDbType.String).SourceColumn = "descripcion";
                cmd.Parameters.Add("igv", MySqlDbType.Bit).SourceColumn = "igv";
                cmd.Parameters.Add("precioconigv", MySqlDbType.Bit).SourceColumn = "precioconigv";
                cmd.Parameters.Add("precioca", MySqlDbType.Double).SourceColumn = "precioca";
                cmd.Parameters.Add("vtalla", MySqlDbType.Int32).SourceColumn = "codtalla";
                cmd.Parameters.Add("codcolorp", MySqlDbType.Int32).SourceColumn = "codcolorp";
                cmd.Parameters.Add("codcolors", MySqlDbType.Int32).SourceColumn = "codcolors";
                cmd.Parameters.Add("codcor", MySqlDbType.Int32).SourceColumn = "codcor";
                cmd.Parameters.Add("codta", MySqlDbType.Int32).SourceColumn = "codta";
                cmd.Parameters.Add("codmo", MySqlDbType.Int32).SourceColumn = "codmo";
                cmd.Parameters.Add("codcomprapro", MySqlDbType.String).SourceColumn = "codcomprapro";
                cmd.Parameters.Add("valortalla", MySqlDbType.String).SourceColumn = "vtalla";
                cmd.Parameters.Add("codtemp", MySqlDbType.Int32).SourceColumn = "codtemp";
                cmd.Parameters.Add("codcuello", MySqlDbType.Int32).SourceColumn = "codcuello";
                cmd.Parameters.Add("codmanga", MySqlDbType.Int32).SourceColumn = "codmanga";
                cmd.Parameters.Add("coddiseño", MySqlDbType.Int32).SourceColumn = "coddiseño";
                cmd.Parameters.Add("codtipodiseño", MySqlDbType.Int32).SourceColumn = "codtipodiseño";
                cmd.Parameters.Add("anio", MySqlDbType.Int32).SourceColumn = "anio";
                cmd.Parameters.Add("preciocompra", MySqlDbType.Double).SourceColumn = "preciocompra";
                cmd.Parameters.Add("preciodir", MySqlDbType.Double).SourceColumn = "preciodir";
                cmd.Parameters.Add("preciopro", MySqlDbType.Double).SourceColumn = "preciopro";
                

                MySqlDataAdapter da = new MySqlDataAdapter();

                cmd.CommandTimeout = 250000000;
                da.InsertCommand = cmd;
                da.UpdateCommand = cmd;
                cmd.UpdatedRowSource = UpdateRowSource.None;
                da.UpdateBatchSize = 500;
                int records = da.Update(prods);

                return true;

            }
            catch (MySqlException ex)
            {
                return false;
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listaCodigobarras()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaCodigobarras", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;

                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean InsertWhitDetalleCod(clsProducto prod, String[] detallecod) {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codusu", prod.CodUsuario);

                //oParam = cmd.Parameters.AddWithValue("vtalla", prod.valorTalla);
                if (prod.Codtalla != null) { oParam = cmd.Parameters.AddWithValue("codtalla", prod.Codtalla); } else { oParam = cmd.Parameters.AddWithValue("codtalla", 0); }
                if (prod.valorTalla != null) { oParam = cmd.Parameters.AddWithValue("vtalla", prod.valorTalla); } else { oParam = cmd.Parameters.AddWithValue("vtalla", ""); }
                //if (prod.CodGrupo != 0) { oParam = cmd.Parameters.AddWithValue("codgru", prod.CodGrupo); } else { oParam = cmd.Parameters.AddWithValue("codgru", null); }
                if (prod.CodLinea != 0) { oParam = cmd.Parameters.AddWithValue("codlin", prod.CodLinea); } else { oParam = cmd.Parameters.AddWithValue("codlin", null); }
                oParam = cmd.Parameters.AddWithValue("codfam", prod.CodFamilia);
                oParam = cmd.Parameters.AddWithValue("coduni", prod.CodUnidadMedida);
                oParam = cmd.Parameters.AddWithValue("codtip", prod.CodTipoArticulo);
                if (prod.CodMarca != 0) { oParam = cmd.Parameters.AddWithValue("codmar", prod.CodMarca); } else { oParam = cmd.Parameters.AddWithValue("codmar", null); }
                oParam = cmd.Parameters.AddWithValue("control", prod.CodControlStock);
                oParam = cmd.Parameters.AddWithValue("referencia", "");
                oParam = cmd.Parameters.AddWithValue("descripcion", prod.Descripcion);

                //oParam = cmd.Parameters.AddWithValue("detraccion", prod.Detraccion);
                //oParam = cmd.Parameters.AddWithValue("comision", prod.Comision);
                //oParam = cmd.Parameters.AddWithValue("maxPorcDesc", prod.MaxPorcDesc);
                //oParam = cmd.Parameters.AddWithValue("codfon", prod.codFondo);
                oParam = cmd.Parameters.AddWithValue("igv", prod.Igv);
                oParam = cmd.Parameters.AddWithValue("precioconigv", prod.ConIgv);
                oParam = cmd.Parameters.AddWithValue("estado", prod.Estado);
                oParam = cmd.Parameters.AddWithValue("precioca", prod.PrecioCatalogo);
                oParam = cmd.Parameters.AddWithValue("codmo", prod.codModelo);
                oParam = cmd.Parameters.AddWithValue("coddeta", prod.codDetalle);
                oParam = cmd.Parameters.AddWithValue("codcor", prod.codCorte);
                oParam = cmd.Parameters.AddWithValue("codta", prod.codTaco);
                oParam = cmd.Parameters.AddWithValue("codcolorp", prod.codColorPrimario);
                oParam = cmd.Parameters.AddWithValue("codcolors", prod.codColorSecundario);
                //oParam = cmd.Parameters.AddWithValue("codstipop", prod.codSubTipoArticulo);
                oParam = cmd.Parameters.AddWithValue("codtipota", prod.TipoTalla);
                oParam = cmd.Parameters.AddWithValue("codgrup", prod.CodGrupo);
                oParam = cmd.Parameters.AddWithValue("codserp", prod.codSerieProducto);
                oParam = cmd.Parameters.AddWithValue("codcomprapro", prod.CodCompraProducto);
                oParam = cmd.Parameters.AddWithValue("CodTipoCierre", prod.CodTipoCierre);
                oParam = cmd.Parameters.AddWithValue("codtemp", prod.CodTemporada);
                oParam = cmd.Parameters.AddWithValue("codcuello", prod.CodCuello);
                oParam = cmd.Parameters.AddWithValue("codmanga", prod.CodManga);
                oParam = cmd.Parameters.AddWithValue("coddiseño", prod.CodDiseño);
                oParam = cmd.Parameters.AddWithValue("codtipodiseño", prod.CodTipoDiseño);
                oParam = cmd.Parameters.AddWithValue("anio", prod.Annio);
                //byte[] areglobyte = ImagenAbyte(prod.FotoZapato);
                //oParam = cmd.Parameters.AddWithValue("fotozapato", areglobyte);

                oParam = cmd.Parameters.AddWithValue("newid", 0);

                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                prod.CodProducto = Convert.ToInt32(cmd.Parameters["newid"].Value);



                if (x != 0)
                {
                    MysqlDetCodProducto detcodproducto = new MysqlDetCodProducto();
                    for (int i = 0; i < 3; i++)
                    {
                        if (detallecod[i] != "") 
                        {
                            detcodproducto.Insert(prod.CodProducto, detallecod[i]);
                        }
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean InsertProductoAlmacen(clsProducto prod)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaProductoAlmacen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codusu", prod.CodUsuario);                
                oParam = cmd.Parameters.AddWithValue("valorpromedio", prod.ValorProm);
                oParam = cmd.Parameters.AddWithValue("preciopromedio", prod.PrecioProm);
                oParam = cmd.Parameters.AddWithValue("recargo", prod.Recargo);
                oParam = cmd.Parameters.AddWithValue("precioventa", prod.PrecioVenta);
                oParam = cmd.Parameters.AddWithValue("oferta", prod.Oferta);
                oParam = cmd.Parameters.AddWithValue("descuento", prod.PDescuento);
                oParam = cmd.Parameters.AddWithValue("montodescuento", prod.MontoDscto);
                oParam = cmd.Parameters.AddWithValue("preciooferta", prod.PrecioOferta);
                oParam = cmd.Parameters.AddWithValue("preciovariable", prod.PrecioVariable);
                oParam = cmd.Parameters.AddWithValue("maximodscto", prod.MaximoDscto);
                oParam = cmd.Parameters.AddWithValue("stockminimo", prod.StockMinimo);
                oParam = cmd.Parameters.AddWithValue("stockmaximo", prod.StockMaximo);
                oParam = cmd.Parameters.AddWithValue("stockreposicion", prod.StockReposicion);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                prod.CodProductoAlmacen = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean InsertCaracteristica(clsCaracteristicaProducto carpro)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaCaracteristicaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codpro", carpro.CodProducto);
                oParam = cmd.Parameters.AddWithValue("codcar", carpro.CodCaracteristica);
                oParam = cmd.Parameters.AddWithValue("valor", carpro.Valor);
                oParam = cmd.Parameters.AddWithValue("codusu", carpro.CodUser);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                carpro.CodCaracteristicaProductoNuevo = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean InsertNota(clsNotaProducto notpro)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaNotaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codpro", notpro.CodProducto);                
                oParam = cmd.Parameters.AddWithValue("nota", notpro.Nota);
                oParam = cmd.Parameters.AddWithValue("codusu", notpro.CodUser);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                notpro.CodNotaProducto = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int Update(clsProducto prod)
        {
            int rows = 0;
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaProducto2", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.AddWithValue("codpro", prod.CodProducto);
                cmd.Parameters.AddWithValue("codcomprapro", prod.CodCompraProducto);
                if (prod.CodGrupo != 0) { cmd.Parameters.AddWithValue("codgru", prod.CodGrupo); } else { cmd.Parameters.AddWithValue("codgru", null); }
                if (prod.CodLinea != 0) { cmd.Parameters.AddWithValue("codlin", prod.CodLinea); } else { cmd.Parameters.AddWithValue("codlin", null); }
                cmd.Parameters.AddWithValue("codfam", prod.CodFamilia);
                if (prod.CodMarca != 0) { cmd.Parameters.AddWithValue("codmar", prod.CodMarca); } else { cmd.Parameters.AddWithValue("codmar", null); }
                cmd.Parameters.AddWithValue("coduni", prod.CodUnidadMedida);
                cmd.Parameters.AddWithValue("codtip", prod.CodTipoArticulo);
                cmd.Parameters.AddWithValue("control", prod.CodControlStock);
                cmd.Parameters.AddWithValue("referencia", prod.Referencia);
                cmd.Parameters.AddWithValue("descripcion", prod.Descripcion);                
                cmd.Parameters.AddWithValue("igv", prod.Igv);
                cmd.Parameters.AddWithValue("precioconigv", prod.ConIgv);
                //cmd.Parameters.AddWithValue("detraccion", prod.Detraccion);
                //cmd.Parameters.AddWithValue("estado", prod.Estado);
                cmd.Parameters.AddWithValue("comision", prod.Comision);
                cmd.Parameters.AddWithValue("precioca", prod.PrecioCatalogo);
                //cmd.Parameters.AddWithValue("maxPorcDesc", prod.MaxPorcDesc);
                //nuevos parametros
                cmd.Parameters.AddWithValue("codser", prod.codSerieProducto);
                //cmd.Parameters.AddWithValue("codsubtipo", prod.codSubTipoArticulo);
                cmd.Parameters.AddWithValue("codcor", prod.codCorte);
                cmd.Parameters.AddWithValue("codmo", prod.codModelo);
                cmd.Parameters.AddWithValue("codcolorp", prod.codColorPrimario);
                cmd.Parameters.AddWithValue("codcolors", prod.codColorSecundario);
                //cmd.Parameters.AddWithValue("coddeta", prod.codDetalle);
                cmd.Parameters.AddWithValue("codta", prod.codTaco);
                //cmd.Parameters.AddWithValue("codfon", prod.codFondo);
                cmd.Parameters.AddWithValue("codtipota", prod.TipoTalla);
                cmd.Parameters.AddWithValue("vtalla", prod.Codtalla);
                cmd.Parameters.AddWithValue("valortalla", prod.valorTalla);
                cmd.Parameters.AddWithValue("codtemp", prod.CodTemporada);
                cmd.Parameters.AddWithValue("codcuello", prod.CodCuello);
                cmd.Parameters.AddWithValue("codmanga", prod.CodManga);
                cmd.Parameters.AddWithValue("coddiseño", prod.CodDiseño);
                cmd.Parameters.AddWithValue("codtipodiseño", prod.CodTipoDiseño);
                cmd.Parameters.AddWithValue("anio", prod.Annio);
                //byte[] areglobyte = ImagenAbyte(prod.FotoZapato);

                //cmd.Parameters.AddWithValue("fotozapato", areglobyte);

                rows = cmd.ExecuteNonQuery();

                return rows;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean UpdateProductoAlmacen(clsProducto prod)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaProductoAlmacen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", prod.CodProducto);
                cmd.Parameters.AddWithValue("codalma", prod.CodAlmacen);  
                cmd.Parameters.AddWithValue("valorprom", prod.ValorProm);
                cmd.Parameters.AddWithValue("precioprom", prod.PrecioProm);
                cmd.Parameters.AddWithValue("recargo", prod.Recargo);
                cmd.Parameters.AddWithValue("valorventa", prod.ValorVenta);
                cmd.Parameters.AddWithValue("precioventa", prod.PrecioVenta);
                cmd.Parameters.AddWithValue("oferta", prod.Oferta);
                cmd.Parameters.AddWithValue("descuento", prod.PDescuento);
                cmd.Parameters.AddWithValue("montodescuento", prod.MontoDscto);
                cmd.Parameters.AddWithValue("preciooferta", prod.PrecioOferta);
                cmd.Parameters.AddWithValue("preciovariable", prod.PrecioVariable);
                cmd.Parameters.AddWithValue("maximodscto", prod.MaximoDscto);
                cmd.Parameters.AddWithValue("stockminimo", prod.StockMinimo);
                cmd.Parameters.AddWithValue("stockmaximo", prod.StockMaximo);
                cmd.Parameters.AddWithValue("stockreposicion", prod.StockReposicion);
                cmd.Parameters.AddWithValue("igv", prod.Igv);
                cmd.Parameters.AddWithValue("precioconigv", prod.ConIgv);
                cmd.Parameters.AddWithValue("detraccion", prod.Detraccion);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean DeleteDetCodProducto(clsProducto prod)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarDetalleCodProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("vcodProducto", prod.CodProducto);
                cmd.ExecuteNonQuery();                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        
        }

        public Boolean UpdateDetCodProducto(clsProducto prod, String[] DetcodProducto)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarDetalleCodProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("vcodProducto", prod.CodProducto);
                cmd.ExecuteNonQuery();
                MysqlDetCodProducto detcodproducto = new MysqlDetCodProducto();
                for (int i = 0; i < 3; i++)
                {
                    if (DetcodProducto[i] != "")
                    {
                        detcodproducto.Insert(prod.CodProducto, DetcodProducto[i]);
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Delete(Int32 CodProducto)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codprod", CodProducto);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean DeleteProductoAlmacen(Int32 CodProductoAlmacen)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarProductoAlmacen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codprod", CodProductoAlmacen);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean DeleteCaracteristica(Int32 CodCarPro)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarCaracteristicaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codcarpro", CodCarPro);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean DeleteNota(Int32 CodNotaProducto)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarNotaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codnota", CodNotaProducto);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto CargaProducto(Int32 CodPro, Int32 CodAlm)
         {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProducto", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodPro);
                cmd.Parameters.AddWithValue("codalm", CodAlm);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                        pro.Referencia = dr.GetString(1);
                        pro.Descripcion = dr.GetString(2);
                        pro.CodFamilia = Convert.ToInt32(dr.GetString(6));
                        pro.CodLinea = Convert.ToInt32(dr.GetString(7));
                        pro.CodGrupo = Convert.ToInt32(dr.GetString(8));
                        pro.CodMarca = Convert.ToInt32(dr.GetString(9));
                        pro.codModelo = Convert.ToInt32(dr.GetString(22));
                        pro.codTaco = Convert.ToInt32(dr.GetString(19));
                        pro.codCorte = Convert.ToInt32(dr.GetString(18));
                        pro.codColorPrimario = Convert.ToInt32(dr.GetString(20));
                        pro.codColorSecundario = Convert.ToInt32(dr.GetString(21));
                        pro.codSerieProducto = Convert.ToInt32(dr.GetString(23));
                        pro.CodTipoArticulo = Convert.ToInt32(dr.GetString(5));
                        pro.CodUsuario = Convert.ToInt32(dr.GetInt32(13));
                        pro.CodUnidadMedida = Convert.ToInt32(dr.GetString(11));
                        pro.CodControlStock = Convert.ToInt32(dr.GetString(10));
                        pro.PrecioCatalogo = Convert.ToDecimal(dr.GetString(24));
                        pro.Codtalla = Convert.ToInt32(dr.GetString(25));
                        Int32 cigv = Convert.ToInt32(dr.GetString(26));
                        pro.detCodProducto1 = Convert.ToString(dr.GetString(37));
                        pro.detCodProducto2 = Convert.ToString(dr.GetString(38));
                        pro.detCodProducto3 = Convert.ToString(dr.GetString(39));

                        if (cigv == 1)
                        {
                            pro.ConIgv = true;
                        }
                        else
                        {
                            pro.ConIgv = false;
                        }
                        pro.CodCompraProducto = dr.GetString(27);
                        pro.CodTemporada = Convert.ToInt32(dr.GetString(28));
                        pro.CodCuello = Convert.ToInt32(dr.GetString(29));
                        pro.CodDiseño = Convert.ToInt32(dr.GetString(30));
                        pro.CodManga = Convert.ToInt32(dr.GetString(31));
                        pro.CodTipoDiseño = Convert.ToInt32(dr.GetString(32));
                        pro.CodTipoCierre = Convert.ToInt32(dr.GetString(34));
                        pro.TipoTalla = Convert.ToInt32(dr.GetString(35));
                        pro.Annio = Convert.ToInt32(dr.GetInt32(36));
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto CargaProductoDetalle1(Int32 CodPro, Int32 CodAlm, Int32 Caso, Int32 CodLista)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoDetalle1", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodPro);
                cmd.Parameters.AddWithValue("codalm", CodAlm);
                cmd.Parameters.AddWithValue("caso", Caso);
                cmd.Parameters.AddWithValue("lista", CodLista);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Caso == 1)
                        {
                            pro = new clsProducto();
                            pro.CodProducto = dr.GetInt32(0);
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(4));
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = Convert.ToInt32(dr.GetDecimal(6));
                            pro.ConIgv = dr.GetBoolean(7);
                            pro.Igv = dr.GetBoolean(8);
                            pro.ValorProm = dr.GetDecimal(11);
                            pro.PrecioCompra = dr.GetDecimal(12);
                        }
                        else
                        {
                            pro = new clsProducto();
                            pro.CodProducto = dr.GetInt32(0);
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = dr.GetInt32(4);
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = dr.GetInt32(6);
                            pro.PrecioVenta = dr.GetDecimal(7);
                            pro.PrecioVariable = dr.GetBoolean(8);
                            pro.ConIgv = dr.GetBoolean(9);
                            pro.Igv = dr.GetBoolean(10);
                            pro.ValorProm = dr.GetDecimal(11);
                            pro.PrecioProm = Convert.ToDecimal(dr.GetDecimal(12));
                        }
                    }
                }
                return pro;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto CargaProductoDetalle(Int32 CodPro, Int32 CodAlm, Int32 Caso, Int32 CodLista)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoDetalle", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodPro);
                cmd.Parameters.AddWithValue("codalm", CodAlm);
                cmd.Parameters.AddWithValue("caso", Caso);
                cmd.Parameters.AddWithValue("lista", CodLista);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Caso == 1)
                        {
                            pro = new clsProducto();
                            pro.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(4));
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = Convert.ToInt32(dr.GetDecimal(6));
                            pro.ConIgv = dr.GetBoolean(7);
                            pro.Igv = dr.GetBoolean(8);
                            pro.MaxPorcDesc = dr.GetDecimal(9);
                            pro.PrecioCompra = dr.GetDecimal(10);
                        }
                        else
                        {
                            pro = new clsProducto();
                            pro.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(4));
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = Convert.ToInt32(dr.GetDecimal(6));
                            pro.PrecioVenta = Convert.ToDecimal(dr.GetDecimal(7));
                            pro.PrecioVentaSoles = Convert.ToDecimal(dr.GetDecimal(8));
                            pro.PrecioVariable = dr.GetBoolean(9);
                            pro.Oferta = dr.GetBoolean(10);
                            pro.PDescuento = Convert.ToDecimal(dr.GetDecimal(11));
                            pro.PrecioOferta = Convert.ToDecimal(dr.GetDecimal(12));
                            pro.ConIgv = dr.GetBoolean(13);
                            pro.Igv = dr.GetBoolean(14);
                            pro.MaxPorcDesc = dr.GetDecimal(18);
                            pro.PrecioCatalogo = dr.GetDecimal(19);
                        }
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto CargaProductoDetalleZ(Int32 codProb, Int32 CodAlm)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoDetalleZ", con.conector);
                cmd.Parameters.AddWithValue("codpro", codProb);
                cmd.Parameters.AddWithValue("codalm", CodAlm);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                            pro = new clsProducto();
                            pro.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(4));
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = Convert.ToInt32(dr.GetDecimal(6));
                            pro.ConIgv = dr.GetBoolean(7);
                            pro.Igv = dr.GetBoolean(8);
                            pro.MaxPorcDesc = dr.GetDecimal(9);
                            pro.CodigoBarra = dr.GetString(10);
                            pro.PrecioCatalogo = dr.GetDecimal(11);
                            pro.codSerieProducto = dr.GetInt32(12);
                            pro.PrecioCompra = dr.GetDecimal(13);
                            pro.PorcPromediocompra = dr.GetDecimal(14);
                            pro.ContieneTalla = dr.GetInt32(15);
                            pro.TipoTalla = dr.GetInt32(16);
                            pro.CodLinea = dr.GetInt32(17);

                            if (dr["imagen"] != DBNull.Value && ((byte[])dr["imagen"]).Length > 1)
                            {
                                pro.FotoZapato = ByteArrayToImage((byte[])dr["imagen"]);
                            }
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }


        }

        public clsProducto CargaDatosProductoOrden(Int32 CodPro, Int32 CodAlm, Int32 codusu, Decimal cant)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("CargaDatosProductoOrden", con.conector);
                cmd.Parameters.AddWithValue("alma", CodAlm);
                cmd.Parameters.AddWithValue("usu", codusu);
                cmd.Parameters.AddWithValue("codpro",CodPro);
                cmd.Parameters.AddWithValue("cant", cant);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {                        
                            pro = new clsProducto();
                            pro.Porllegar = Convert.ToInt32(dr.GetDecimal(0));
                            pro.PorAtender = Convert.ToInt32(dr.GetDecimal(1));
                            pro.PorCompletar = Convert.ToInt32(dr.GetDecimal(2));
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
        public clsProducto CargaProductoDetalleR(String Referencia, Int32 CodAlm, Int32 Caso, Int32 Lista)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoDetalleR", con.conector);
                cmd.Parameters.AddWithValue("refe", (Referencia));
                cmd.Parameters.AddWithValue("codalm", CodAlm);
                cmd.Parameters.AddWithValue("caso", Caso);
                cmd.Parameters.AddWithValue("lista", Lista);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Caso == 1)
                        {
                            pro = new clsProducto();
                            pro.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(4)); 
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = Convert.ToInt32(dr.GetDecimal(6));
                            pro.ConIgv = dr.GetBoolean(7);
                            pro.Igv = dr.GetBoolean(8);
                            pro.MaxPorcDesc = dr.GetDecimal(9);
                            pro.PrecioCompra = dr.GetDecimal(10);
                        }
                        else
                        {
                            pro = new clsProducto();
                            pro.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                            pro.Referencia = dr.GetString(1);
                            pro.Descripcion = dr.GetString(2);
                            pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(3));
                            pro.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(4));
                            pro.UnidadDescrip = dr.GetString(5);
                            pro.CodControlStock = Convert.ToInt32(dr.GetDecimal(6));
                            pro.PrecioVenta = Convert.ToDecimal(dr.GetDecimal(7));
                            pro.PrecioVentaSoles = Convert.ToDecimal(dr.GetDecimal(8));
                            pro.PrecioVariable = dr.GetBoolean(9);
                            pro.Oferta = dr.GetBoolean(10);
                            pro.PDescuento = Convert.ToDecimal(dr.GetDecimal(11));
                            pro.PrecioOferta = Convert.ToDecimal(dr.GetDecimal(12));
                            pro.ConIgv = dr.GetBoolean(13);
                            pro.Igv = dr.GetBoolean(14);
                            pro.MaxPorcDesc = dr.GetDecimal(18);
                        }
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
        public DataTable ListaProductos(Int32 nivel, Int32 codigo, Int32 codalmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductos", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("nivel", nivel);
                cmd.Parameters.AddWithValue("codigo", codigo);
                cmd.Parameters.AddWithValue("codalm", codalmacen);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable CatalogoProductos()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CatalogoProductos", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable StockProductoAlmacenes(Int32 codempre, Int32 codpro)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("StockProductoxAlmacen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", codpro);
                cmd.Parameters.AddWithValue("codempre", codempre);                
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaProductosReporte(Int32 codalmacen, Int32 Tipo, Int32 Inicio)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosReporte", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;               
                cmd.Parameters.AddWithValue("codalma", codalmacen);
                cmd.Parameters.AddWithValue("tipo", Tipo);
                cmd.Parameters.AddWithValue("inicio", Inicio);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable RelacionProductosIngreso(Int32 Tipo, Int32 codalma)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("RelacionProductosIngreso", con.conector);
                cmd.Parameters.AddWithValue("tipo", Tipo);
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.CommandType = CommandType.StoredProcedure;                
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable RelacionIngresoPorProveedor(Int32 Tipo, Int32 codalma, Int32 codproveedor)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("RelacionProductosIngresoPorProveedor", con.conector);
                cmd.Parameters.AddWithValue("tipo", Tipo);
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("codprov", codproveedor);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaProductosz(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }



        public DataTable RelacionProductosSalida(Int32 Tipo,Int32 codalmacen, Int32 codlista)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("RelacionProductosSalida", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("tipo", Tipo);
                cmd.Parameters.AddWithValue("codalma", codalmacen);
                cmd.Parameters.AddWithValue("codlista", codlista);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaCaracteristicas(Int32 codigo)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaCaracteristicaProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.AddWithValue("codpro", codigo);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaNotas(Int32 codigo)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaNotasProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", codigo);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable BuscaProductos(Int32 Criterio, String Filtro)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("FiltraProductos", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@criterio", Criterio);
                cmd.Parameters.AddWithValue("@filtro", Filtro);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ArbolProductos()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaArbolProductos", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ArbolProductosZ()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaArbolProductosZ", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
        


        public DataTable MuestraProductosProveedor(Int32 codProducto, Int32 codAlmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductosProveedor", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", codProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

      
        public clsProducto MuestraProductosTransferencia(Int32 codProducto, Int32 codAlmacen)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoTransferencia", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", codProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                            pro = new clsProducto();
                            pro.ValorProm = Convert.ToDecimal(dr.GetDecimal(0));
                            pro.ValorPromsoles = Convert.ToDecimal(dr.GetDecimal(1));
                            pro.PrecioProm = Convert.ToDecimal(dr.GetDecimal(2));
                            pro.StockActual = Convert.ToDecimal(dr.GetDecimal(3));
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto MuestraProductosTransferencia_nuevo(Int32 codProducto, Int32 codAlmacen)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoTransferencia", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", codProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.ValorProm = Convert.ToDecimal(dr.GetDecimal(0));
                        pro.ValorPromsoles = Convert.ToDecimal(dr.GetDecimal(1));
                        pro.PrecioProm = Convert.ToDecimal(dr.GetDecimal(2));
                        pro.StockActual = Convert.ToDecimal(dr.GetDecimal(3));
                        pro.Cantidad = Convert.ToInt32(dr.GetInt32(4));
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable RelacionProductosCotizacion(int Tipo, int codAlmacen, int codlista)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("RelacionProductosCotizacion", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("tipo", Tipo);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.Parameters.AddWithValue("codlista", codlista);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Decimal CargaPrecioProducto(Int32 CodPro, Int32 CodAlm, Int32 codmon)
        {
            Decimal Precio = 0;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("BuscaPrecioProducto", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodPro);
                cmd.Parameters.AddWithValue("codalma", CodAlm);
                cmd.Parameters.AddWithValue("mon", codmon);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Precio= Convert.ToDecimal(dr.GetDecimal(0));
                    }
                }
                return Precio;


            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable BuscarProductoDetalleSalBar(String codebar)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("BuscarProductoDetalleSalBar", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codebar", codebar);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable BuscarProductoDetalleBar(String Codebar, Int32 tipo, Int32 codalma)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("BuscarProductoDetalleBar", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codebar", Codebar);
                cmd.Parameters.AddWithValue("tipo", tipo);
                cmd.Parameters.AddWithValue("codalma", codalma);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsNotaIngreso BuscarProductoBar(Int32 codalma, String codebar, Int32 tipo)
        {
            clsNotaIngreso pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("BuscarProductoBar", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("codebar", codebar);
                cmd.Parameters.AddWithValue("tipo", tipo);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsNotaIngreso();
                        pro.CodReferencia = dr.GetInt32(0);
                        pro.CodNotaIngreso = dr.GetString(1);
                        pro.CodProveedor = dr.GetInt32(2);
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }



        public DataTable MuestraStockAlmacenes()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("consultadinamicastock", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto BuscaProductoAlmacenZ(Int32 codalma, String codebar)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("BuscaProductoZ", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("codebar", codebar);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {

                        pro = new clsProducto();
                        pro.CodProducto = dr.GetInt32(0);
                        pro.Referencia = dr.GetString(1);
                        pro.Descripcion = dr.GetString(2);
                        pro.CodUnidadMedida = dr.GetInt32(3);
                        pro.UnidadDescrip = dr.GetString(4);
                        pro.PrecioVenta = Convert.ToDecimal(dr.GetDecimal(5));
                        pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(6));
                        pro.PrecioProm = Convert.ToDecimal(dr.GetDecimal(8));
                        pro.ValorProm = Convert.ToDecimal(dr.GetDecimal(9));
                    }
                }
                return pro;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto BuscaProductoZ3(Int32 codalma, String codebar)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("BuscaProductoZ3", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("codebar", codebar);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.CodProducto = dr.GetInt32(0);
                        pro.Referencia = dr.GetString(1);
                        pro.Descripcion = dr.GetString(2);
                        pro.CodUnidadMedida = dr.GetInt32(3);
                        pro.UnidadDescrip = dr.GetString(4);
                        pro.PrecioVenta = Convert.ToDecimal(dr.GetDecimal(5));
                        pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(6));
                        pro.PrecioProm = Convert.ToDecimal(dr.GetDecimal(8));
                        pro.ValorProm = Convert.ToDecimal(dr.GetDecimal(9));
                    }
                }
                return pro;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

       public clsProducto BuscaPrecioCompraProductoAnterior(Int32 codpro)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("BuscaPrecioCompraProductoAnterior", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codpro", codpro);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.PrecioCompra = dr.GetDecimal(0);
                    }
                }
                return pro;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable VerificaProductoIngresado(String refe)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("VerificaProductoIngresado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("refe", refe);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        #endregion 

        #region Implementacion IUnidadEquivalente

        public Boolean InsertUnidad(clsUnidadEquivalente uni)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaUnidadEquivalente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codpro", uni.CodProducto);
                oParam = cmd.Parameters.AddWithValue("codAlmacen", frmLogin.iCodAlmacen);
                oParam = cmd.Parameters.AddWithValue("coduni", uni.CodUnidad);
                oParam = cmd.Parameters.AddWithValue("codUndEqui", uni.CodEquivalente);
                oParam = cmd.Parameters.AddWithValue("codTipo", uni.Tipo);
                oParam = cmd.Parameters.AddWithValue("factor", uni.Factor);
                oParam = cmd.Parameters.AddWithValue("codusu", uni.CodUser);
                oParam = cmd.Parameters.AddWithValue("precio", uni.Precio);
                oParam = cmd.Parameters.AddWithValue("compra_venta", uni.CompraVenta);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                uni.CodUnidadEquivalente = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean UpdateUnidad(clsUnidadEquivalente uni)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaUnidadEquivalente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("factor", uni.Factor);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean DeleteUnidad(Int32 CodUnidad)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarUnidadEquivalente", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("coduni", CodUnidad);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsUnidadEquivalente CargaUnidadEquivalente(Int32 Coduni, Int32 Codpro)
        {
            clsUnidadEquivalente uni = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraUnidadEquivalente", con.conector);
                cmd.Parameters.AddWithValue("coduni", Coduni);
                cmd.Parameters.AddWithValue("codpro", Codpro);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        uni = new clsUnidadEquivalente();
                        uni.CodUnidadEquivalente = Convert.ToInt32(dr.GetDecimal(0));
                        uni.CodProducto = Convert.ToInt32(dr.GetDecimal(1));
                        uni.CodUnidad = Convert.ToInt32(dr.GetDecimal(2));
                        //jc       uni.Factor = Convert.ToDecimal(dr.GetDecimal(3));
                        uni.CodUser = Convert.ToInt32(dr.GetDecimal(4));
                        uni.FechaRegistro = dr.GetDateTime(5);// capturo la fecha 
                    }

                }
                return uni;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
        

        public DataTable CargaUnidadesEquivalentes(Int32 CodigoProducto)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaUnidadesEquivalentes", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodigoProducto);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable BuscarProducto(Int32 codProducto)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("BuscarProducto", con.conector);
                cmd.Parameters.AddWithValue("codprod", codProducto);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
        #endregion

        public DataTable RelacionProductos(Int32 codalma)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("RelacionProductos", con.conector);
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public static byte[] ImagenAbyte(Image Imagen)
        {
            MemoryStream memory = new MemoryStream();
            if (Imagen != null)
            {
                Imagen.Save(memory, ImageFormat.Jpeg);
            }
            else
            {
            }
            return memory.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            return Image.FromStream(ms);
        }




        public DataTable ConsultaProductoRR(String referencia, Int32 cod)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoDetalleRR", con.conector);
                cmd.Parameters.AddWithValue("refe", referencia);
                cmd.Parameters.AddWithValue("codalma", cod);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaArticulosRequerimiento(Int32 cod)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaArticulosRequerimiento", con.conector);
                cmd.Parameters.AddWithValue("codalma", cod);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable TallasxProducto(Int32 codpro)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("TallasxProducto", con.conector);
                cmd.Parameters.AddWithValue("codpro", codpro);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }



        public DataTable TraeDatosProducto(Int32 codpro)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("TraeDatosProducto", con.conector);
                cmd.Parameters.AddWithValue("codpro", codpro);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable TallasxProducto2(Int32 codseriep, Int32 codtipota, Int32 codpro, Int32 codalma)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("TallaxProducto2", con.conector);
                cmd.Parameters.AddWithValue("codseriep", codseriep);
                cmd.Parameters.AddWithValue("codtipota", codtipota);
                cmd.Parameters.AddWithValue("codpro", codpro);
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        
        public DataTable BuscaProductoZ2(Int32 codalma, String codigobar)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("BuscaProductoZ2", con.conector);
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("codebar", codigobar);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        //Josue Joshua León Yalta
        public clsProducto BuscaProductoDetalleSerie(Int32 codalma, String codebar)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("BuscaProductoSerie", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("codebar", codebar);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.CodProducto = dr.GetInt32(0);
                        pro.Referencia = dr.GetString(1);
                        pro.Descripcion = dr.GetString(2);
                        pro.CodUnidadMedida = dr.GetInt32(3);
                        pro.UnidadDescrip = dr.GetString(4);
                        pro.PrecioVenta = Convert.ToDecimal(dr.GetDecimal(5));
                        pro.StockDisponible = Convert.ToDecimal(dr.GetDecimal(6));
                        pro.PrecioProm = Convert.ToDecimal(dr.GetDecimal(7));
                        pro.ValorProm = Convert.ToDecimal(dr.GetDecimal(8));
                        pro.codSerieProducto = dr.GetInt32(9);
                        pro.Modelo= dr.GetString(11);
                        pro.Marca= dr.GetString(10);
                        pro.Colorprimario=dr.GetString(12);
                        pro.Colorsecundario= dr.GetString(13);
                        pro.TipoTalla = dr.GetInt32(14);
                        pro.CodLinea = dr.GetInt32(15);
                        pro.PrecioCatalogo = dr.GetDecimal(16);
                        pro.ContieneTalla = dr.GetInt32(17);
                    }   
                }
                return pro;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable CargarRangoTallas(Decimal talla1, Decimal talla2, Int32 codTipoTalla, Int32 codLinea)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ConsultaTallaCompra", con.conector);
                cmd.Parameters.AddWithValue("tal1", talla1);
                cmd.Parameters.AddWithValue("tal2", talla2);
                cmd.Parameters.AddWithValue("codtipo",codTipoTalla);
                cmd.Parameters.AddWithValue("line",codLinea);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Int32 VerificaCodigoProducto(Int32 codcarac, Int32 cotalla, Int32 coduser, Decimal precio)
        {
            try
            {
                int codigo;

                con.conectarBD();
                cmd = new MySqlCommand("VerificaCodigoProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codcaracteristica", codcarac);
                oParam = cmd.Parameters.AddWithValue("talla", cotalla);
                oParam = cmd.Parameters.AddWithValue("coduser", coduser);
                oParam = cmd.Parameters.AddWithValue("precio", precio);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                
                oParam.Direction = ParameterDirection.Output;

                int x = cmd.ExecuteNonQuery();

                codigo = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return codigo;
                }
                else
                {
                    return 0;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        #region Nuevos cambios

        public DataTable ListaProductosZ2(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ2", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable ListaProductosz3(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ3", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.Parameters.AddWithValue("codcolorp", codcolorp);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }



        public DataTable ListaProductosz4(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ4", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.Parameters.AddWithValue("codcolorp", codcolorp);
                cmd.Parameters.AddWithValue("codcolors", codcolors);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }



        public DataTable ListaProductosz5(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ5", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.Parameters.AddWithValue("codcolorp", codcolorp);
                cmd.Parameters.AddWithValue("codcolors", codcolors);
                cmd.Parameters.AddWithValue("codtac", codtac);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable ListaProductosz6(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ6", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.Parameters.AddWithValue("codcolorp", codcolorp);
                cmd.Parameters.AddWithValue("codcolors", codcolors);
                cmd.Parameters.AddWithValue("codtac", codtac);
                cmd.Parameters.AddWithValue("codfon", codfon);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public DataTable ListaProductosz7(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon, Int32 codtipot)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaProductosZ7", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.Parameters.AddWithValue("codcolorp", codcolorp);
                cmd.Parameters.AddWithValue("codcolors", codcolors);
                cmd.Parameters.AddWithValue("codtac", codtac);
                cmd.Parameters.AddWithValue("codfon", codfon);
                cmd.Parameters.AddWithValue("codtipot", codtipot);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean InsertDatosArticulo(clsProducto prod)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("GuardaDatosArticulo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                if (prod.CodLinea != 0) { oParam = cmd.Parameters.AddWithValue("codline", prod.CodLinea); } else { oParam = cmd.Parameters.AddWithValue("codline", null); }
                if (prod.CodMarca != 0) { oParam = cmd.Parameters.AddWithValue("codmar", prod.CodMarca); } else { oParam = cmd.Parameters.AddWithValue("codmar", null); }
                oParam = cmd.Parameters.AddWithValue("codfam", prod.CodFamilia);
                oParam = cmd.Parameters.AddWithValue("codser", prod.codSerieProducto);
                oParam = cmd.Parameters.AddWithValue("codtip", prod.CodTipoArticulo);
                oParam = cmd.Parameters.AddWithValue("codsubt", prod.codSubTipoArticulo);
                oParam = cmd.Parameters.AddWithValue("codcor", prod.codCorte);
                oParam = cmd.Parameters.AddWithValue("codmodel", prod.codModelo);
                oParam = cmd.Parameters.AddWithValue("codcolorp", prod.codColorPrimario);
                oParam = cmd.Parameters.AddWithValue("codcolorsecun", prod.codColorSecundario);
                oParam = cmd.Parameters.AddWithValue("codtac", prod.codTaco);
                oParam = cmd.Parameters.AddWithValue("codfon", prod.codFondo);
                oParam = cmd.Parameters.AddWithValue("codtipot", prod.TipoTalla);
                byte[] areglobyte = ImagenAbyte(prod.FotoZapato);
                oParam = cmd.Parameters.AddWithValue("fotozapato", areglobyte);
                oParam = cmd.Parameters.AddWithValue("codunidm", prod.CodUnidadMedida);
                oParam = cmd.Parameters.AddWithValue("codcontrols", prod.CodControlStock);
                oParam = cmd.Parameters.AddWithValue("newid", 0);

                oParam.Direction = ParameterDirection.Output;

                int x = cmd.ExecuteNonQuery();

                prod.CodProducto = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable MuestraProductoDetalleRR2(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon, Int32 codtipotal)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoDetalleRR2", con.conector);
                cmd.Parameters.AddWithValue("codline", codline);
                cmd.Parameters.AddWithValue("codmar", codmar);
                cmd.Parameters.AddWithValue("codfam", codfam);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.Parameters.AddWithValue("codtip", codtip);
                cmd.Parameters.AddWithValue("codsubt", codsubtip);
                cmd.Parameters.AddWithValue("codcor", codcor);
                cmd.Parameters.AddWithValue("codmodel", codmodel);
                cmd.Parameters.AddWithValue("codcolorp", codcolorp);
                cmd.Parameters.AddWithValue("codcolors", codcolors);
                cmd.Parameters.AddWithValue("codtac", codtac);
                cmd.Parameters.AddWithValue("codfon", codfon);
                cmd.Parameters.AddWithValue("codtipotal", codtipotal);

                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto MuestraProductoParaCantidad(Int32 codPro)
        {
            clsProducto pro = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraProductoParaCantidad", con.conector);
                cmd.Parameters.AddWithValue("codpro", codPro);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.CodProducto = dr.GetInt32(0);
                        pro.Descripcion = dr.GetString(1);
                        pro.CodigoBarra = dr.GetString(2);
                    }
                }
                return pro;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public clsProducto MuestraDatosdeProductoMovimiento(Int32 codpro)
        {
            try
            {
                clsProducto pro = null;
                con.conectarBD();
                cmd = new MySqlCommand("MuestraDatosdeProductoMovimiento", con.conector);
                cmd.Parameters.AddWithValue("codpro", codpro);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.CodUnidadMedida = dr.GetInt32(0);
                        pro.UnidadDescrip = dr.GetString(1);
                        pro.CodigoBarra = dr.GetString(2);
                        pro.Codtalla = dr.GetInt32(3);
                        pro.valorTalla = dr.GetString(4);

                        if (dr["imagen"] != DBNull.Value && ((byte[])dr["imagen"]).Length > 1)
                        {
                            pro.FotoZapato = ByteArrayToImage((byte[])dr["imagen"]);
                        }

                        pro.PrecioCatalogo = dr.GetDecimal(6);
                    }
                }
                return pro;
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable CargaUnidadCompra(Int32 codPro, Int32 codAlm, Int32 compra_venta)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaUnidadCompra", con.conector);
                cmd.Parameters.AddWithValue("codPro", codPro);
                cmd.Parameters.AddWithValue("codAlm", codAlm);
                cmd.Parameters.AddWithValue("compra_venta", compra_venta);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CargaUnidaEquivalente(Int32 codPro, Int32 codAlm)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("CargaUnidadEquivalente", con.conector);
                cmd.Parameters.AddWithValue("codPro", codPro);
                cmd.Parameters.AddWithValue("codAlm", codAlm);                
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListaUnidadesEquivalentesCompra(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaUnidadesEquivalentesCompra", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodigoProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaUnidadesEquivalentesVenta1(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaUnidadesEquivalentesVenta1", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodigoProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaUnidadesEquivalentes(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaUnidadesEquivalentes", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodigoProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto CargaReferenciaProducto(Int32 codProducto)
        {
            try
            {
                clsProducto pro = null;
                con.conectarBD();
                cmd = new MySqlCommand("CargaReferenciaProducto", con.conector);
                cmd.Parameters.AddWithValue("codpro", codProducto);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        pro = new clsProducto();
                        pro.Referencia = dr.GetString(0);
                        pro.Descripcion = dr.GetString(1);
                    }
                }
                return pro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable ListaUnidadesEquivalentesVenta(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaUnidadesEquivalentesVenta", con.conector);
                cmd.Parameters.AddWithValue("codpro", CodigoProducto);
                cmd.Parameters.AddWithValue("codalma", codAlmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsUnidadEquivalente PrecioVenta(Int32 coduni, Int32 codalmacen)
        {
            clsUnidadEquivalente uni = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraPrecioVenta", con.conector);
                cmd.Parameters.AddWithValue("undequi", coduni);
                cmd.Parameters.AddWithValue("codalma", codalmacen);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        uni = new clsUnidadEquivalente();
                        uni.Stock = dr.GetDecimal(0);
                        uni.CodUnidad = dr.GetInt32(1);
                        uni.Precio = dr.GetDecimal(2);
                        uni.Tipo = dr.GetInt32(3);
                    }
                }
                return uni;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Decimal FactorProducto(Int32 codPro, Int32 undBase, Int32 undEqui, Int32 tipo)
        {
            Decimal uniEqui = 0;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("FactorProducto", con.conector);
                cmd.Parameters.AddWithValue("codProd", codPro);
                cmd.Parameters.AddWithValue("undBase", undBase);
                cmd.Parameters.AddWithValue("undEqui", undEqui);
                cmd.Parameters.AddWithValue("tipo", tipo);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        uniEqui = dr.GetDecimal(0);
                    }
                }
                return uniEqui;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsUnidadEquivalente Factor(Int32 codProducto, Int32 codUnidadMedida, Int32 codUnidaEqui)
        {
            clsUnidadEquivalente uni = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraFactor", con.conector);
                cmd.Parameters.AddWithValue("codpro", codProducto);
                cmd.Parameters.AddWithValue("coduni", codUnidadMedida);
                cmd.Parameters.AddWithValue("coduniEqui", codUnidaEqui);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        uni = new clsUnidadEquivalente();
                        uni.Factor = Convert.ToInt32(dr.GetDecimal(0));
                    }

                }
                return uni;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsProducto PrecioPromedio(Int32 codProducto, Int32 codalm)
        {
            clsProducto prod = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraPrecioPromedio", con.conector);
                cmd.Parameters.AddWithValue("codpro", codProducto);
                cmd.Parameters.AddWithValue("codalma", codalm);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        prod = new clsProducto();
                        prod.CodProducto = Convert.ToInt32(dr.GetDecimal(0));
                        prod.PrecioProm = Convert.ToDecimal(dr.GetDecimal(1));
                        prod.CodUnidadMedida = Convert.ToInt32(dr.GetDecimal(2));

                    }

                }
                return prod;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Int32 UnidadBase(Int32 codProd, Int32 codalma)
        {
            Int32 uni = 0;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("UnidadBase", con.conector);
                cmd.Parameters.AddWithValue("codProd", codProd);
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        uni = dr.GetInt32(0);
                    }
                }
                return uni;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable totalizadoAlmacenes()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("totalizadoAlmacenes", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listaCodigoBarras()
        {
            throw new NotImplementedException();
        }



        #endregion
    }

    #region Implementación de Interfaz IFotografia
    class mysqlFotografia : IFotografia
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;


        public Boolean AgregarFotografia(clsEntFotografia entfoto)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("GuardarFotografia", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codrelacion_ex", entfoto.Codrelacion);
                oParam = cmd.Parameters.AddWithValue("cod_almacen", entfoto.CodAlmacen);
                byte[] arreglobyte = clsImagen.ImagenAbyte(entfoto.Fotografia);
                if (arreglobyte.Length != 0)
                {
                    oParam = cmd.Parameters.AddWithValue("foto_ex", arreglobyte);
                }
                else
                {
                    oParam = cmd.Parameters.AddWithValue("foto_ex", 0);
                }
                oParam = cmd.Parameters.AddWithValue("codusuario_ex", entfoto.Codusuario);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();
                entfoto.Codfotografia = Convert.ToInt32(cmd.Parameters["newid"].Value);
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean ActualizarFotografia(clsEntFotografia entfoto)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("ActualizarFotografia", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("codfotografia_ex", entfoto.Codfotografia);
                cmd.Parameters.AddWithValue("codrelacion_ex", entfoto.Codrelacion);
                cmd.Parameters.AddWithValue("cod_almacen", entfoto.CodAlmacen);
                byte[] arreglobyte = clsImagen.ImagenAbyte(entfoto.Fotografia);
                if (arreglobyte.Length != 0)
                {
                    cmd.Parameters.AddWithValue("foto_ex", arreglobyte);
                }
                else
                {
                    cmd.Parameters.AddWithValue("foto_ex", 0);
                }
                cmd.Parameters.AddWithValue("codusuario_ex", entfoto.Codusuario);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else { return false; }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean EliminarFotografia(Int32 codfotografia, Int32 codAlmacen)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("EliminarFotografia", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codfotografia_ex", codfotografia);
                cmd.Parameters.AddWithValue("cod_almacen", codAlmacen);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else { return false; }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsEntFotografia CargaFotografia(Int32 codfotografia)
        {
            clsEntFotografia entfotografia = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("MuestraFotografia", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codfotografia_ex", codfotografia);
               
                dr = cmd.ExecuteReader();
                //dr = cmd.ExecuteScalar();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        entfotografia = new clsEntFotografia();
                        entfotografia.Codfotografia = dr.GetInt32(0);
                        entfotografia.Codrelacion = dr.GetInt32(1);
                        if (dr["foto"] != DBNull.Value && ((byte[])dr["foto"]).Length > 1)
                        {
                            entfotografia.Fotografia = clsImagen.ByteArrayToImage((byte[])dr["foto"]);
                        }
                        entfotografia.Codusuario = dr.GetInt32(3);
                        entfotografia.Estado = dr.GetBoolean(4);
                        entfotografia.Fechareg = dr.GetDateTime(5);
                    }
                }
                return entfotografia;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.conector.Dispose(); cmd.Dispose(); con.desconectarBD();
            }
        }
    }
    #endregion
}
