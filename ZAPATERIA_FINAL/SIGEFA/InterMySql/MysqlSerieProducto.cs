﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using SIGEFA.Entidades;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;

namespace SIGEFA.InterMySql
{
    class MysqlSerieProducto:ISerieProducto
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        #region Implementacion ISerieProducto

       
        public DataTable ListaSerieProducto()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaSerieProducto", con.conector);
                //cmd.Parameters.AddWithValue("codfam", CodFam);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsSerieProducto ListaSerieProductoCod(Int32 codSerie)
        {
            clsSerieProducto serieproducto = null;
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("ListaSerieProductoCod", con.conector);
                cmd.Parameters.AddWithValue("cod", codSerie);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        serieproducto = new clsSerieProducto();
                        serieproducto.CodSerieProducto = dr.GetInt32(0);
                        serieproducto.Referencia = dr.GetString(1);
                        serieproducto.Descripcion = dr.GetString(2);
                        serieproducto.Estado = dr.GetInt32(3);
                        serieproducto.Tallamin = dr.GetInt32(4);
                        serieproducto.Tallamax = dr.GetInt32(5);
                        serieproducto.Fecharegistro = dr.GetDateTime(6);
                        serieproducto.TallaminI = dr.GetInt32(7);
                        serieproducto.TallamaxI = dr.GetInt32(8);
                    }
                }
                return serieproducto;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }

        }


        #endregion


        #region Nuevos cambios

        public clsSerieProducto ListaTallaxSerie(Int32 codser)
        {
            try
            {
                clsSerieProducto serieproducto = null;

                con.conectarBD();
                cmd = new MySqlCommand("ListaTallaxSerie", con.conector);
                cmd.Parameters.AddWithValue("codser", codser);
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        serieproducto = new clsSerieProducto();
                        serieproducto.Tallamin = dr.GetInt32(0);
                        serieproducto.Tallamax = dr.GetInt32(1);
                        serieproducto.TallaminI = dr.GetInt32(2);
                        serieproducto.TallamaxI = dr.GetInt32(3);
                    }
                }
                return serieproducto;

            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Insert(clsSerieProducto ser)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("GuardaSerieProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                
                oParam = cmd.Parameters.AddWithValue("referencia", ser.Referencia);
                oParam = cmd.Parameters.AddWithValue("descripcion", ser.Descripcion);
                oParam = cmd.Parameters.AddWithValue("codusu", ser.CodUser);
                oParam = cmd.Parameters.AddWithValue("max", ser.Tallamax);
                oParam = cmd.Parameters.AddWithValue("min", ser.Tallamin);
                oParam = cmd.Parameters.AddWithValue("newid", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();

                ser.CodSerieProducto = Convert.ToInt32(cmd.Parameters["newid"].Value);

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean Update(clsSerieProducto ser)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaSerieProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codigo", ser.CodSerieProducto);
                cmd.Parameters.AddWithValue("refe", ser.Referencia);
                cmd.Parameters.AddWithValue("descri", ser.Descripcion);
                cmd.Parameters.AddWithValue("maxim", ser.Tallamax);
                cmd.Parameters.AddWithValue("minim", ser.Tallamin);
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean delete(clsSerieProducto ser)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("deleteSerieProducto", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("cod", ser.CodSerieProducto);
               
                int x = cmd.ExecuteNonQuery();
                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;

            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        #endregion



    }
}
