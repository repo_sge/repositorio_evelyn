﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.InterMySql
{
    class MysqlTesoreria : ITesoreria
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public bool insertPagoTesoreria(int fpago, int origencta,int origencaja, decimal mpago, int tpago, string descripcion, int codalma, int coduser, int idcompra)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("insertPagoTesoreria", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("fpago", fpago);
                oParam = cmd.Parameters.AddWithValue("idcta", origencta);
                oParam = cmd.Parameters.AddWithValue("idcaja", origencaja);
                oParam = cmd.Parameters.AddWithValue("mpago", mpago);
                oParam = cmd.Parameters.AddWithValue("tpago", tpago);
                oParam = cmd.Parameters.AddWithValue("descrip", descripcion);
                oParam = cmd.Parameters.AddWithValue("codalma", codalma);
                oParam = cmd.Parameters.AddWithValue("coduser", coduser);
                oParam = cmd.Parameters.AddWithValue("idcompra", idcompra);
                int x = cmd.ExecuteNonQuery();


                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listarTesoreria(DateTime finicio, DateTime ffin)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaPagosTesoreria", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("finicio", finicio);
                cmd.Parameters.AddWithValue("ffin", ffin);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public decimal saldoCajaAhorros()
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("saldoCajaAhorros", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                decimal x = Convert.ToDecimal(cmd.ExecuteScalar());

                return x;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public decimal saldoCajaGeneral()
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("saldoCajaGeneral", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                decimal x = Convert.ToDecimal(cmd.ExecuteScalar());

                return x;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public decimal saldoCtaCte(int codcta)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("saldoCtaCte", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codcta", codcta);
                decimal x = Convert.ToDecimal(cmd.ExecuteScalar());

                return x;


            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
