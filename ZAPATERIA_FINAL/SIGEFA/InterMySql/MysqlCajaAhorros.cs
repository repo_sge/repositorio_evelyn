﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.InterMySql
{
    class MysqlCajaAhorros : ICajaAhorros
    {
        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        public bool insertPagoCAhorros(int idalma, decimal monto, int idusuario, string nombre, string dni, int tipoegreso, string observa)
        {
            try
            {
                con.conectarBD();
                cmd = new MySqlCommand("insertPagoCAhorros", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("idalma", idalma);
                oParam = cmd.Parameters.AddWithValue("monto", monto);
                oParam = cmd.Parameters.AddWithValue("usuario", idusuario);
                oParam = cmd.Parameters.AddWithValue("nombre", nombre);
                oParam = cmd.Parameters.AddWithValue("dni", dni);
                oParam = cmd.Parameters.AddWithValue("tipoegreso", tipoegreso);
                oParam = cmd.Parameters.AddWithValue("observa", observa);
                int x = cmd.ExecuteNonQuery();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listaCajaAhorros(DateTime finicio, DateTime ffin)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaCajaAhorros", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("finicio", finicio);
                cmd.Parameters.AddWithValue("ffin", ffin);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
