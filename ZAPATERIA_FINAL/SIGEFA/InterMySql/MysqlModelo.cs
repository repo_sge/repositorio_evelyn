﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using SIGEFA.Entidades;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;

namespace SIGEFA.InterMySql
{
    class MysqlModelo:IModelo
    {

        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataTable tabla = null;

        #region Implementacion IModelo

        public DataTable ListaModelo()
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("ListaModelo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

       public DataTable ListraProveedorModelo( Int32 cod)
       {
           try
           {
               tabla = new DataTable();
               con.conectarBD();
               cmd = new MySqlCommand("MuestraProveedorModelo", con.conector);
               cmd.Parameters.AddWithValue("cod", cod);
               cmd.CommandType = CommandType.StoredProcedure;
               adap = new MySqlDataAdapter(cmd);
               adap.Fill(tabla);
               return tabla;

           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }



       public Boolean VerificaModelo(clsModelo model)
       {
           try
           {
               con.conectarBD();
               cmd = new MySqlCommand("VerificaModelo", con.conector);
               cmd.CommandType = CommandType.StoredProcedure;
               MySqlParameter oParam;
               oParam = cmd.Parameters.AddWithValue("des", model.Descripcion);
               oParam = cmd.Parameters.AddWithValue("codprovee", model.CodProveedor);
               oParam = cmd.Parameters.AddWithValue("newid2", 0);

               oParam.Direction = ParameterDirection.Output;
               int x = cmd.ExecuteNonQuery();

               model.CodModelo = Convert.ToInt32(cmd.Parameters["newid2"].Value);

               if (x != 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (MySqlException ex)
           {
               throw ex;
           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }

       public DataTable ListaModeloProveedorZ()
       {
           try
           {
               tabla = new DataTable();
               con.conectarBD();
               cmd = new MySqlCommand("ListaModeloProveedorZ", con.conector);
               cmd.CommandType = CommandType.StoredProcedure;
               adap = new MySqlDataAdapter(cmd);
               adap.Fill(tabla);
               return tabla;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }

       public DataTable MuestraModeloOC()
       {
           try
           {
               tabla = new DataTable();
               con.conectarBD();
               cmd = new MySqlCommand("CargaModeloOC", con.conector);
               cmd.CommandType = CommandType.StoredProcedure;
               adap = new MySqlDataAdapter(cmd);
               adap.Fill(tabla);
               return tabla;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }

       public Boolean Insert(clsModelo mod)
       {
           try
           {
               con.conectarBD();

               cmd = new MySqlCommand("GuardaModelo", con.conector);
               cmd.CommandType = CommandType.StoredProcedure;
               MySqlParameter oParam;
               oParam = cmd.Parameters.AddWithValue("descripcion", mod.Descripcion);
               //oParam = cmd.Parameters.AddWithValue("codusu", mod.CodUser);
               oParam = cmd.Parameters.AddWithValue("newid", 0);
               oParam.Direction = ParameterDirection.Output;
               int x = cmd.ExecuteNonQuery();

               //mod.CodMarcaNuevo = Convert.ToInt32(cmd.Parameters["newid"].Value);

               if (x != 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (MySqlException ex)
           {
               throw ex;
           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }

       public Boolean Update(clsModelo mod)
       {
           try
           {
               con.conectarBD();

               cmd = new MySqlCommand("ActualizaModelo", con.conector);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.AddWithValue("codmod", mod.CodModelo);
               cmd.Parameters.AddWithValue("descripcion", mod.Descripcion);
               int x = cmd.ExecuteNonQuery();
               if (x != 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (MySqlException ex)
           {
               throw ex;

           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }

       public Boolean Delete(Int32 CodModelo)
       {
           try
           {
               con.conectarBD();
               cmd = new MySqlCommand("EliminarModelo", con.conector);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.AddWithValue("codmod", CodModelo);
               int x = cmd.ExecuteNonQuery();
               if (x != 0)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (MySqlException ex)
           {
               throw ex;

           }
           finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
       }


        #endregion


    }
}
