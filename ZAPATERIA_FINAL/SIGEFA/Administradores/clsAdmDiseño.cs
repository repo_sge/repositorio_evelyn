﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.InterMySql;
using SIGEFA.Interfaces;
using SIGEFA.Entidades;
using System.Data;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmDiseño
    {
        IDiseño MCue = new MysqlDiseño();

        public Boolean insert(clsDiseño mar)
        {
            try
            {
                return MCue.Insert(mar);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean update(clsDiseño mar)
        {
            try
            {
                return MCue.Update(mar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean delete(Int32 Codmar)
        {
            try
            {
                return MCue.Delete(Codmar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable MuestraDiseño()
        {
            try
            {
                return MCue.ListaDiseño();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
