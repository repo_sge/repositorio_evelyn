﻿using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmDocumentoIdentidad
    {

        IDocumentoIdentidad MDocumentoIdentidad = new MysqlDocumentoIdentidad();

        /**
          * codigoTipoDocumento: codigo de Documento de Facturacion (BOLETA O FACTURA)
          */
        public DataTable ListaDocumentoIdentidad(Int32 codigoTipoDocumento)
        {
            try
            {
                return MDocumentoIdentidad.ListaDocumentoIdentidad(codigoTipoDocumento);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                                                          "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsDocumentoIdentidad MuestraDocumentoIdentidad(Int32 codigoDocumentoIdentidad)
        {
            try
            {
                return MDocumentoIdentidad.MuestraDocumentoIdentidad(codigoDocumentoIdentidad);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                                            "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsDocumentoIdentidad ObtenerDocumentoIdentidadDeVenta(Int32 codigoFacturaVenta)
        {
            try
            {
                return MDocumentoIdentidad.ObtenerDocumentoIdentidadDeVenta(codigoFacturaVenta);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                                            "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
