﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using SIGEFA.Administradores;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SIGEFA.Administradores
{
    class clsAdmProducto
    {
        IProducto Mpro = new MysqlProducto();

        public int insert(clsProducto pro)
        {
            try
            {
                return Mpro.Insert(pro);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               
                else
                    DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }

        public Boolean insertMasivo(DataTable pro)
        {
            try
            {
                return Mpro.InsertMasivo(pro);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else
                    DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean updateMasivo(DataTable pro)
        {
            try
            {
                return Mpro.UpdateMasivo(pro);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else
                    DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable listaCodigobarras()
        {
            try
            {
                return Mpro.listaCodigobarras();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else
                    DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean insertDetCodProducto(clsProducto pro,String []DetCodProducto)
        {
            try
            {
                return Mpro.InsertWhitDetalleCod(pro,DetCodProducto);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else
                    DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean insertproductoalmacen(clsProducto pro)
        {
            try
            {
                return Mpro.InsertProductoAlmacen(pro);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean insertcaracteristica(clsCaracteristicaProducto carpro)
        {
            try
            {
                return Mpro.InsertCaracteristica(carpro);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean insertnota(clsNotaProducto notapro)
        {
            try
            {
                return Mpro.InsertNota(notapro);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean insertunidadequivalente(clsUnidadEquivalente unidadequi)
        {
            try
            {
                return Mpro.InsertUnidad(unidadequi);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean updateDetCodProducto(clsProducto pro, String []DetcodProducto)
        {
            try 
            {
                return Mpro.UpdateDetCodProducto(pro,DetcodProducto);
            }
            catch(Exception ex)
            {

                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

    public Boolean DeleteDetCodProducto (clsProducto pro)
    {
        try
        {
            return Mpro.DeleteDetCodProducto(pro);
        }
        catch (Exception ex)
        {

            if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return false;
        }
    }

        public int update(clsProducto pro)
        {
            try
            {
                return Mpro.Update(pro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }

        public Boolean updateproductoalmacen(clsProducto pro)
        {
            try
            {
                return Mpro.UpdateProductoAlmacen(pro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean updateunidadequivalente(clsUnidadEquivalente unidadequi)
        {
            try
            {
                return Mpro.UpdateUnidad(unidadequi);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean delete(Int32 Codpro)
        {
            try
            {
                return Mpro.Delete(Codpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean deleteproductoalmacen(Int32 Codpro)
        {
            try
            {
                return Mpro.DeleteProductoAlmacen(Codpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean deletecaracteristica(Int32 Codcarpro)
        {
            try
            {
                return Mpro.DeleteCaracteristica(Codcarpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean deletenota(Int32 Codnota)
        {
            try
            {
                return Mpro.DeleteNota(Codnota);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean deleteunidadequivalente(Int32 Coduniequi)
        {
            try
            {
                return Mpro.DeleteUnidad(Coduniequi);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable MuestraProductos(Int32 Nivel, Int32 Codigo, Int32 CodAlmacen)
        {
            try
            {
                return Mpro.ListaProductos(Nivel, Codigo, CodAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable CatalogoProductos()
        {
            try
            {
                return Mpro.CatalogoProductos();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaProductosReporte(Int32 CodAlmacen, Int32 Tipo, Int32 Inicio)
        {
            try
            {
                return Mpro.ListaProductosReporte(CodAlmacen,Tipo, Inicio);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable StockProductoAlmacenes(Int32 CodEmpresa, Int32 CodProducto)
        {
            try
            {
                return Mpro.StockProductoAlmacenes(CodEmpresa, CodProducto);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable RelacionIngreso(Int32 Tipo, Int32 codalma)
        {
            try
            {
                return Mpro.RelacionProductosIngreso(Tipo, codalma);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }




        public DataTable RelacionIngresoPorProveedor(Int32 Tipo, Int32 codalma, Int32 codproveedor)
        {
            try
            {
                return Mpro.RelacionIngresoPorProveedor(Tipo, codalma, codproveedor);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable RelacionSalida(Int32 Tipo, Int32 CodAlmacen, Int32 CodLista)
        {
            try
            {
                return Mpro.RelacionProductosSalida(Tipo,CodAlmacen,CodLista);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraCaracteristicas(Int32 CodigoProducto)
        {
            try
            {
                return Mpro.ListaCaracteristicas(CodigoProducto);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraProductosProveedor(Int32 CodigoProducto, Int32 CodigoAlmacen)
        {
            try
            {
                return Mpro.MuestraProductosProveedor(CodigoProducto, CodigoAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraNotas(Int32 CodigoProducto)
        {
            try
            {
                return Mpro.ListaNotas(CodigoProducto);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

       

        public DataTable CargaUnidadesEquivalentes(Int32 CodigoProducto)
        {
            try
            {
                return Mpro.CargaUnidadesEquivalentes(CodigoProducto);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsUnidadEquivalente CargaUnidadEquivalente(Int32 CodigoUnidad, Int32 CodigoProducto)
        {
            try
            {
                return Mpro.CargaUnidadEquivalente(CodigoUnidad, CodigoProducto);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto CargaProducto(Int32 CodProducto, Int32 CodAlmacen)
        {
            try
            {
                return Mpro.CargaProducto(CodProducto,CodAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto CargaProductoDetalle(Int32 CodProducto, Int32 CodAlmacen, Int32 Caso, Int32 CodLista)
        {
            try
            {
                return Mpro.CargaProductoDetalle(CodProducto, CodAlmacen, Caso, CodLista);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
        public clsProducto CargaDatosProductoOrden(Int32 CodProducto, Int32 CodAlmacen,Int32 codusu, Decimal cant)
        {
            try
            {
                return Mpro.CargaDatosProductoOrden(CodProducto, CodAlmacen, codusu, cant);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsNotaIngreso BuscarProductoBar(Int32 codalma, String codebar, Int32 tipo)
        {
            try
            {
                return Mpro.BuscarProductoBar(codalma, codebar, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }


        public DataTable BuscarProductoDetalleBar(String Codebar, Int32 tipo, Int32 codalma)
        {
            try
            {
                return Mpro.BuscarProductoDetalleBar(Codebar, tipo, codalma);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable BuscarProductoDetalleSalBar(String codebar)
        {
            try
            {
                return Mpro.BuscarProductoDetalleSalBar(codebar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }

        }

        public clsProducto CargaProductoDetalleZ(Int32 codProb, Int32 CodAlm)
        {
            try
            {
                return Mpro.CargaProductoDetalleZ(codProb, CodAlm);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }       
        }

        public clsProducto CargaProductoDetalleR(String Referencia, Int32 CodAlmacen, Int32 Caso, Int32 CodLista)
        {
            try
            {
                return Mpro.CargaProductoDetalleR(Referencia, CodAlmacen, Caso, CodLista);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }        

        public DataTable ArbolProductos()
        {
            try
            {
                return Mpro.ArbolProductos();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto CargaProductoDetalle1(Int32 CodProducto, Int32 CodAlmacen, Int32 Caso, Int32 CodLista)
        {
            try
            {
                return Mpro.CargaProductoDetalle1(CodProducto, CodAlmacen, Caso, CodLista);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ArbolProductosZ()
        {
            try
            {
                return Mpro.ArbolProductosZ();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }


        public clsProducto MuestraProductosTransferencia(Int32 codProducto, Int32 codAlmacen)
        {
            try
            {
                return Mpro.MuestraProductosTransferencia(codProducto,codAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
        public clsProducto MuestraProductosTransferencia_nuevo(Int32 codProducto, Int32 codAlmacen)
        {
            try
            {
                return Mpro.MuestraProductosTransferencia_nuevo(codProducto, codAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable RelacionCotizacion(Int32 Tipo, Int32 CodAlmacen, Int32 CodLista)
        {
            try
            {
                return Mpro.RelacionProductosCotizacion(Tipo, CodAlmacen, CodLista);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Decimal CargaPrecioProducto(Int32 CodProducto, Int32 CodAlmacen, Int32 codmon)
        {
            try
            {
                return Mpro.CargaPrecioProducto(CodProducto, CodAlmacen, codmon);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }

        public DataTable MuestraStockAlmacenes()
        {
            try
            {
                return Mpro.MuestraStockAlmacenes();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable BuscarProducto(Int32 codProducto)
        {
            try
            {
                return Mpro.BuscarProducto(codProducto);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable RelacionProductos(Int32 codalma)
        {
            try
            {
                return Mpro.RelacionProductos(codalma);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraProductosZ(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel)
        {
            try
            {
                return Mpro.ListaProductosz(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraProductoRR(String referencia, Int32 cod)
        {
            try
            {
                return Mpro.ConsultaProductoRR(referencia, cod);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        
        }

        public clsProducto BuscaProductoAlmacenZ(Int32 codalma, String codebar)
        {
            try
            {
                return Mpro.BuscaProductoAlmacenZ(codalma, codebar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto BuscaPrecioCompraProductoAnterior(Int32 codpro)
        {
            try
            {
                return Mpro.BuscaPrecioCompraProductoAnterior(codpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable VerificaProductoIngresado(String refe)
        {
            try
            {
                return Mpro.VerificaProductoIngresado(refe);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }


        public DataTable ListaArticulosRequerimiento(Int32 cod)
        {
            try
            {
                return Mpro.ListaArticulosRequerimiento(cod);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable TallasxProducto(Int32 codpro)
        {
            try
            {
                return Mpro.TallasxProducto(codpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable TraeDatosProducto(Int32 codpro)
        {
            try
            {
                return Mpro.TraeDatosProducto(codpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable TallasxProducto2(Int32 codseriep, Int32 codtipota, Int32 codpro, Int32 codalma)
        {
            try
            {
                return Mpro.TallasxProducto2(codseriep, codtipota, codpro, codalma);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }


        public DataTable BuscaProductoZ2(Int32 codalma, String codigobar)
        {
            try
            {
                return Mpro.BuscaProductoZ2(codalma, codigobar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto BuscaProductoZ3(Int32 codalma, String codigobar)
        {
            try
            {
                return Mpro.BuscaProductoZ3(codalma, codigobar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        //Josué Joshua León Yalta
        public clsProducto BuscaProductoDetalleSerie(Int32 codAlmacen, String codigoBar) {
            try
            {
                return Mpro.BuscaProductoDetalleSerie(codAlmacen, codigoBar);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable CargarRangoTallas(Decimal talla1, Decimal talla2, Int32 codTipoTalla, Int32 codLinea)
        {
            try
            {
                return Mpro.CargarRangoTallas(talla1, talla2,codTipoTalla,codLinea);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        
        }
        //fin

        public Int32 VerificaCodigoProducto(Int32 codcarac, Int32 cotalla, Int32 coduser, Decimal precio)
        {
            try
            {
                return Mpro.VerificaCodigoProducto(codcarac, cotalla, coduser, precio);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }


        //Nuevos Cambios
        public DataTable ListaProductosz3(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp)
        {
            try
            {
                return Mpro.ListaProductosz3(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel, codcolorp);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaProductosz4(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors)
        {
            try
            {
                return Mpro.ListaProductosz4(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel, codcolorp, codcolors);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaProductosz5(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac)
        {
            try
            {
                return Mpro.ListaProductosz5(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel, codcolorp, codcolors, codtac);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaProductosz6(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon)
        {
            try
            {
                return Mpro.ListaProductosz6(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel, codcolorp, codcolors, codtac, codfon);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaProductosz7(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon, Int32 codtipot)
        {
            try
            {
                return Mpro.ListaProductosz7(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel, codcolorp, codcolors, codtac, codfon, codtipot);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean InsertDatosArticulo(clsProducto prod)
        {
            try
            {
                return Mpro.InsertDatosArticulo(prod);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else
                    DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable ListaProductosZ2(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel)
        {
            try
            {
                return Mpro.ListaProductosZ2(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraProductoDetalleRR2(Int32 codline, Int32 codmar, Int32 codfam, Int32 codser, Int32 codtip, Int32 codsubtip, Int32 codcor, Int32 codmodel, Int32 codcolorp, Int32 codcolors, Int32 codtac, Int32 codfon, Int32 codtipotal)
        {
            try
            {
                return Mpro.MuestraProductoDetalleRR2(codline, codmar, codfam, codser, codtip, codsubtip, codcor, codmodel, codcolorp, codcolors, codtac, codfon, codtipotal);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto MuestraProductoParaCantidad(Int32 codPro)
        {
            try
            {
                return Mpro.MuestraProductoParaCantidad(codPro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsProducto MuestraDatosdeProductoMovimiento(Int32 codpro)
        {
            try
            {
                return Mpro.MuestraDatosdeProductoMovimiento(codpro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }



        public DataTable CargaUnidadCompra(Int32 codPro, Int32 codAlm)
        {
            try
            {
                return Mpro.CargaUnidadCompra(codPro, codAlm, 0);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable cargaUnidadVenta(Int32 codPro, Int32 codAlm)
        {
            try
            {
                return Mpro.CargaUnidadCompra(codPro, codAlm, 1);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable cargaUnidadEquivalente(Int32 codPro, Int32 codAlm)
        {
            try
            {
                // ato return Mpro.CargaUnidadCompra(codPro, codAlm, 2);
                return Mpro.CargaUnidaEquivalente(codPro, codAlm);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraUnidadesEquivalentesCompra(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                return Mpro.ListaUnidadesEquivalentesCompra(CodigoProducto, codAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraUnidadesEquivalentesVenta(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                return Mpro.ListaUnidadesEquivalentesVenta(CodigoProducto, codAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraUnidadesEquivalentesVenta1(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                return Mpro.ListaUnidadesEquivalentesVenta1(CodigoProducto, codAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraUnidadesEquivalentes(Int32 CodigoProducto, Int32 codAlmacen)
        {
            try
            {
                return Mpro.ListaUnidadesEquivalentes(CodigoProducto, codAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }



        public clsProducto CargaReferenciaProducto(Int32 codProducto)
        {
            try
            {
                return Mpro.CargaReferenciaProducto(codProducto);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Int32 UnidadBase(Int32 codPro, Int32 codalma)
        {
            try
            {
                return Mpro.UnidadBase(codPro, codalma);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }

        public clsProducto PrecioPromedio(Int32 codProducto, Int32 codalm)
        {
            try
            {
                return Mpro.PrecioPromedio(codProducto, codalm);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsUnidadEquivalente Factor(Int32 codProducto, Int32 codUnidadMedida, Int32 codUnidaEqui)
        {
            return Mpro.Factor(codProducto, codUnidadMedida, codUnidaEqui);
        }

        public clsUnidadEquivalente PrecioVenta(Int32 coduni, Int32 codalmacen)
        {
            try
            {
                return Mpro.PrecioVenta(coduni, codalmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Decimal FactorProducto(Int32 codPro, Int32 undBase, Int32 undEqui, Int32 tipo)
        {
            try
            {
                return Mpro.FactorProducto(codPro, undBase, undEqui, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }

        public DataTable totalizadoAlmacenes()
        {
            try
            {
                return Mpro.totalizadoAlmacenes();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }


    }

    #region Reglas para Administrar las Fotografias
    public class clsAdmFotografia
    {
        IFotografia ifoto = new mysqlFotografia();
        public Boolean AgregarFotografia(clsEntFotografia entfoto)
        {
            try
            {
                return ifoto.AgregarFotografia(entfoto);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Agregar la Fotografía: " + ex.Message, "Gestión Relación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        public Boolean ActualizarFotografia(clsEntFotografia entfoto)
        {
            try
            {
                return ifoto.ActualizarFotografia(entfoto);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Actualizar la Fotografía: " + ex.Message, "Gestión Relación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        public Boolean EliminarFotografia(Int32 codfotografia, Int32 codalmacen)
        {
            try
            {
                return ifoto.EliminarFotografia(codfotografia, codalmacen);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Eliminar la Fotografía: " + ex.Message, "Gestión Relación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        public clsEntFotografia CargaFotografia(Int32 codfotografia)
        {
            try
            {
                return ifoto.CargaFotografia(codfotografia);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Cargar la Fotografía: " + ex.Message, "Gestión Relación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
    #endregion
}
