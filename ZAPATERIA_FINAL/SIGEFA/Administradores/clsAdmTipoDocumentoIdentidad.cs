﻿using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Administradores
{
    class clsAdmTipoDocumentoIdentidad
    {
        private ITipoDocumentoIdentidad itipodociden = new MysqlTipoDocumentoIdentidad();

        public List<clsTipoDocumentoIdentidad> Listar_tipo_documento_identidad()
        {
            return itipodociden.Listar_tipo_documento_identidad();
        }

    }
}
