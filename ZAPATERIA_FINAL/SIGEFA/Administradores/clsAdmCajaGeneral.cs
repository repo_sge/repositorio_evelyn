﻿using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmCajaGeneral
    {
        ICajaGeneral mcg = new MysqlCajaGeneral();

        public DataTable listaCajaGeneral(DateTime finicio, DateTime ffin)
        {
            try
            {
                return mcg.listaCajaGeneral(finicio, ffin);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean transferirCGeneral(int codcgeneral, int codcta, string noperacion, int coduser)
        {
            try
            {
                return mcg.transferirCGeneral(codcgeneral, codcta,noperacion,coduser);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

    }
}
