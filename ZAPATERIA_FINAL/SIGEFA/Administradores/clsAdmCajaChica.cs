﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using SIGEFA.Administradores;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SIGEFA.Administradores
{
    class clsAdmCajaChica
    {
        ICajaChica MCaja = new MysqlCajaChica();

        public Boolean insert(clsCajaChica caja)
        {
            try
            {
                return MCaja.Insert(caja);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean update(clsCajaChica caja)
        {
            try
            {
                return MCaja.Update(caja);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean delete(Int32 Codigo)
        {
            try
            {
                return MCaja.Delete(Codigo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable ListaCajaChica(Int32 codSucursal,Int32 tipo)
        {
            try
            {
                return MCaja.ListaCajaChica(codSucursal, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaCajaChicaFechas(Int32 codsucur, DateTime fecha1, DateTime fecha2, Int32 tipo)
        {
            try
            {
                return MCaja.ListaCajaChicaFechas(codsucur, fecha1, fecha2, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean RendicionCaja(Int32 Codigo)
        {
            try
            {
                return MCaja.RendicionCaja(Codigo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable ListaRendiciones(Int32 codSucursal, Int32 tipo)
        {
            try
            {
                return MCaja.ListaRendiciones(codSucursal, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaRendicionesXSucursal(Int32 codAlmacen, Int32 tipo)
        {
            try
            {
                return MCaja.ListaRendicionesXSucursal(codAlmacen, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaDetalleRendiciones(Int32 Codigo,Int32 tipo)
        {
            try
            {
                return MCaja.ListaDetalleRendiciones(Codigo, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean AnulaRendicionCajaChica(Int32 Codigo)
        {
            try
            {
                return MCaja.AnulaRendicionCajaChica(Codigo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
      
        public Boolean GestionaRendicionCajaChica(clsCajaChica caja)
        {
            try
            {
                return MCaja.GestionaRendicionCajaChica(caja);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean GestionaRendicion(Int32 Codigo, String Observacion, Int32 Tipo)
        {
            try
            {
                return MCaja.GestionaRendicion(Codigo, Observacion, Tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean ApruebaRendicion(Int32 Codigo, String Comm)
        {
            try
            {
                return MCaja.ApruebaRendicion(Codigo, Comm);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean DesapruebaRendicion(Int32 Codigo, String Comm)
        {
            try
            {
                return MCaja.DesapruebaRendicion(Codigo, Comm);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public clsCajaChica CargaSaldoCajaChica(Int32 CodAlmacen)
        {
            try
            {
                return MCaja.CargaSaldoCajaChica(CodAlmacen);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsCajaChica VerificaSaldoCajaChica(Int32 codSucursal,Int32 tipo)
        {
            try
            {
                return MCaja.VerificaSaldoCajaChica(codSucursal, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean InsertRendicion(clsCajaChica caja)
        {
            try
            {
                return MCaja.InsertRendicion(caja);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean InsertDetalleRendicion(clsCajaChica caja)
        {
            try
            {
                return MCaja.InsertDetalleRendicion(caja);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        
        public DataTable ListaRendicionesTesoreria()
        {
            try
            {
                return MCaja.ListaRendicionesTesoreria();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean GeneraLiquidacion(clsCajaChica caja)
        {
            try
            {
                return MCaja.GeneraLiquidacion(caja);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean InsertLiquidacion(clsCajaChica caja)
        {
            try
            {
                return MCaja.InsertLiquidacion(caja);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable ListaLiquidacionesVigentes(Int32 codSucursal,Int32 tipo)
        {
            try
            {
                return MCaja.ListaLiquidacionesVigentes(codSucursal, tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        //Implementado
        public clsCajaChica VerificaSaldoCajaChicaDiaria(Int32 codSucursal, DateTime fecha1)
        {
            try
            {
                return MCaja.VerificaSaldoCajaChicaDiaria(codSucursal, fecha1);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaCajaChicaDiaria(Int32 codSucursal, DateTime fecha1)
        {
            try
            {
                return MCaja.ListaCajaChicaDiaria(codSucursal, fecha1);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean insertMovimientoDiario(clsCajaChica caja)
        {
            try
            {
                return MCaja.InsertMovimientoDiario(caja);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: Documento Duplicado", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean updateMovimientoDiario(clsCajaChica caja)
        {
            try
            {
                return MCaja.UpdateMovimientoDiario(caja);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Boolean deleteMovimientoDiario(Int32 Codigo)
        {
            try
            {
                return MCaja.DeleteMovimientoDiario(Codigo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Decimal sumaVentaEfectivoDia(Int32 codSucursal, DateTime fecha1)
        {
            try
            {
                return MCaja.SumaVentaEfectivoDia(codSucursal, fecha1);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }

        public Boolean cerrarCaja(Int32 codSucursal, DateTime fecha1, Int32 tipConsulta)
        {
            try
            {
                return MCaja.CerrarCaja(codSucursal, fecha1, tipConsulta);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable ListaPagoCajaChica(Int32 tipo)
        {
            try
            {
                return MCaja.ListaPagoCajaChica(tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        //Fin Implementado

        public Boolean CerrarCajaChica(Int32 codSucursal, Int32 tipo, Decimal montocierre, Int32 codcajachica)
        {
            try
            {
                return MCaja.CerrarCajaChica(codSucursal, tipo, montocierre, codcajachica);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public Decimal traersaldo()
        {
            try
            {
                return MCaja.traersaldo();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }

            
        }

        public DataTable ListaDinero(Int32 tipo)
        {
            try
            {
                return MCaja.ListaDinero(tipo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Decimal TraeValor(Int32 codigo)
        {
            try
            {
                return MCaja.TraeValor(codigo);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0;
            }
        }
    }
}
