﻿using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmTesoreria
    {

        ITesoreria mtes = new MysqlTesoreria();

        public Boolean insertPagoTesoreria(int fpago, int origencta,int origencaja, decimal mpago, int tpago, string descripcion,int codalma,int coduser,int idcompra)
        {
            try
            {
                return mtes.insertPagoTesoreria(fpago,origencta,origencaja,mpago,tpago,descripcion,codalma,coduser, idcompra);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Duplicate entry")) DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: N°- de Documento Repetido", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                else DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        public DataTable listarTesoreria(DateTime finicio, DateTime ffin)
        {
            try
            {
                return mtes.listarTesoreria(finicio,ffin);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public decimal saldoCajaAhorros()
        {
            try
            {
                return mtes.saldoCajaAhorros();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0.0000m;
            }
        }
        public decimal saldoCajaGeneral()
        {
            try
            {
                return mtes.saldoCajaGeneral();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0.0000m;
            }
        }

        public decimal saldoCtaCte(int codcta)
        {
            try
            {
                return mtes.saldoCtaCte(codcta);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 0.0000m;
            }
        }


    }
}
