﻿using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Administradores
{
    class clsAdmRepositorio
    {
        IRepositorio repo = new MysqlRepositorio();

        public clsRepositorio listar_repositorio_xtscfm(clsRepositorio repositorio)
        {
            return repo.listar_archivo_xrepositorio(repositorio);
        }
        public int registrar_repositorio(clsRepositorio repositorio)
        {
            return repo.registrar_repositorio(repositorio);
        }
        public int actualizar_repositorio(clsRepositorio repositorio)
        {
            return repo.actualizar_repositorio(repositorio);
        }
        public DataTable listar_repositorio_xtsfe(clsSerie numeracion, DateTime inicio, DateTime fin, int idestado)
        {
            return repo.listar_repositorio_xtsfe(numeracion, inicio, fin, idestado);
        }
        public DataTable listar_repositorio_xtsfe_xcomprobante(clsSerie numeracion, clsFacturaVenta comprobante, String inicio, String fin)
        {
            return repo.listar_repositorio_xtsfe_xcomprobante(numeracion, comprobante, inicio,fin);
        }

        public clsRepositorio listar_archivo_xrepositorio(clsRepositorio repositorio)
        {
            return repo.listar_archivo_xrepositorio(repositorio);
        }

        public clsRepositorio listar_xidrepositorio(int idrepositorio)
        {
            return repo.listar_xidrepositorio(idrepositorio);
        }

        public int ValidarEnvio(int codfacturav)
        {
            return repo.validarEnvio(codfacturav);
        }
    }
}
