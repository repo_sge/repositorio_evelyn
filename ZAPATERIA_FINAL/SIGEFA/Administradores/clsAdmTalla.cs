﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using SIGEFA.Administradores;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SIGEFA.Administradores
{
    class clsAdmTalla
    {
        ITalla Mysqltalla = new MysqlTalla();

        public clsTalla CargaTallaDes(String val)
        {
            try
            {
                return Mysqltalla.CargaTallaDes(val);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsTalla CargaTallaCod(Int32 cod)
        {
            try
            {
                return Mysqltalla.CargaTallaCod(cod);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ConsultaTallaCompra(Decimal tallamin, Decimal tallamax, Int32 tipot, Int32 line)
        {
            try
            {
                return Mysqltalla.ConsultaTallaCompra(tallamin, tallamax, tipot, line);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable MuestraTallaOC()
        {
            try
            {
                return Mysqltalla.MuestraTallaOC();
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public DataTable ListaTallaporLinea(Int32 codline, Int32 min, Int32 max, Int32 tipot)
        {
            try
            {
                return Mysqltalla.ListaTallaporLinea(codline, min, max, tipot);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }


        public clsTalla DevolverCodTalla(Int32 nom)
        {
            try
            {
                return Mysqltalla.DevolverCodTalla(nom);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public clsTalla DevolverCodTalla(String nom, Int32 tipotalla)
        {
            try
            {
                return Mysqltalla.DevolverCodTalla(nom, tipotalla);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
    }
}
