﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;


namespace SIGEFA.Formularios
{
    class clsAdmTipoGasto
    {
        ITipoGasto Mystipo = new MysqlTipoGasto();

        public DataTable MostrarTiposGastos()
        {
            try
            {
                return Mystipo.MostrarTipoGasto();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
