﻿using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
    class clsAdmCajaAhorros
    {
        ICajaAhorros mca = new MysqlCajaAhorros();

        public DataTable listaCajaAhorros(DateTime finicio,DateTime ffin)
        {
            try
            {
                return mca.listaCajaAhorros(finicio, ffin);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }

        public Boolean insertPagoCAhorros(int idalma, decimal monto, int idusuario, string nombre, string dni, int tipoegreso, string observa)
        {
            try
            {
                return mca.insertPagoCAhorros(idalma,monto,idusuario,nombre,dni,tipoegreso,observa);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
    }
}
